console.log('\'Allo \'Allo!');
'use strict';
$(document).ready(function(){
	init();
});

function init(){
	initIsotope();
	initProfileTabs();

	$('.dropdown').on('show.bs.dropdown', function() {
		$(this).find('.dropdown-menu').first().stop(true, true).slideDown();
	});
	$('.dropdown').on('hide.bs.dropdown', function() {
		$(this).find('.dropdown-menu').first().stop(true, true).slideUp();
	});

};
/* isotope */
	var $container = $('.isotope-container');
function initIsotope(){
		$container.masonry({
			"gutter": 20,
			itemSelector: '.story-card'
		});
		$('.story-card').animate({opacity:'1'});
};

/* tabs  */
function initProfileTabs(){
	$('a[data-toggle="tab"]').on('shown.bs.tab', function() {
		initIsotope();
		profileTimeline();
	});
};
/*tabs */

/* timline :: starts*/
function profileTimeline(){
	var $timeline_block = $('.cd-timeline-block');

	//hide timeline blocks which are outside the viewport
	$timeline_block.each(function(){
		if($(this).offset().top > $(window).scrollTop()+$(window).height()*0.75) {
			$(this).find('.cd-timeline-img, .cd-timeline-content').addClass('is-hidden');
		}
	});

	//on scolling, show/animate timeline blocks when enter the viewport
	$(window).on('scroll', function(){
		$timeline_block.each(function(){
			if( $(this).offset().top <= $(window).scrollTop()+$(window).height()*0.75 && $(this).find('.cd-timeline-img').hasClass('is-hidden') ) {
				$(this).find('.cd-timeline-img, .cd-timeline-content').removeClass('is-hidden').addClass('bounce-in');
			}
		});
	});

}

/* timeline :: ends */
