function getNotifications() {
		$.ajax({
			type: "GET",
			url: "user_notifications",
			success: function (response) {
				$("#user_notification").html(response);

			},
			error: function(response){
				$('#user_notification').html('You have no notification at this time.');
			}
		});
	}
	function readNotification(){
		$("#notificationReadI").on('click',function(){
			$.ajax({
				type:"POST",
				url: "read_user_notifications",
				success: function(data){
					//alert('No New notification');
					getNotifications();
					console.log('Data Post Request initiated' + data);
				},
				error: function(data){
					console.log('didn\'t get response' + data)
				}
			});
		});
	}
	function anyNewNotification(){
		$.ajax({
			type: "GET",
			url: "new_notification_count",
			success: function(data){
				console.log('init new_notification_count');
				if(data > 0){
					$(".notificationCount").show();
					$(".notificationCount").html(data);
				}
				else{
					$(".notificationCount").hide();
				}
			},
			error: function(data){
				console.log("notification count failure")
			}
		});
	}
	function initNotifications(){
		'use strict';
		$(".link-noti").on('mouseover', function(){
			getNotifications();
		});
		readNotification();
		anyNewNotification();
		setInterval(anyNewNotification, 10000);
	}
	$(document).ready(function(){
		'use strict';
		initNotifications();
	});