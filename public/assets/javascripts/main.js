console.log('\'Allo \'Allo!');


function deleteClick(id){
	var proceed = confirm("Are you sure you want to delete this story?");
	if (proceed == true) {
		$('.tt-loader').removeClass('hide');
		event.preventDefault();
		$.ajax({
			url: "/deleteStory/"+id,
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: true,
			processData:false,
			success: function(data){
				$('.tt-loader').addClass('hide');
				if(data == 1){
					window.location.href = "/";
				}
				else{
					alert(data); 
				}
			},
			error: function(){
				$('.tt-loader').addClass('hide');
			}           
		});
	} 
	else {
	}
}
function initShareButtons(){
	$('.shareStory').sharrre({
		share: {
			twitter: true,
			facebook: true
		},
		template: '<button class="iso-btn share-tw"><i class="block fa fa-twitter"></i></button><button class="iso-btn share-fb"><i class="block fa fa-facebook"></i></button>',
		enableHover: false,
		enableTracking: false,
		buttons: { twitter: {via: 'triptroopme'}},
		render: function(api, options){
			$(api.element).on('click', '.share-tw', function() {
				api.openPopup('twitter');
			});
			$(api.element).on('click', '.share-fb', function() {
				api.openPopup('facebook');
			});
		}
	});
};
function initPostCarousel(){
	'use strict';
	var myCarousel = $('#postCarousel');
	myCarousel.append('<ol class="carousel-indicators"></ol>');
	var indicators = $('.carousel-indicators');
	myCarousel.find('.carousel-inner').children('.item').each(function(index) {
		(index === 0) ?
		indicators.append('<li data-target="#postCarousel" data-slide-to="'+index+'" class="active"></li>') :
		indicators.append('<li data-target="#postCarousel" data-slide-to="'+index+'"></li>');
	});
};

function initTitleFontSize(){
	'use strict';
	var $title = $('.title-wrapper h1');
	var $numWords = $title.text().length;
	if (($numWords >= 1) && ($numWords < 7)) {
		$title.css('font-size', '160px');
	}
	else if (($numWords >= 7) && ($numWords < 10)) {
		$title.css('font-size', '124px');
	}
	else if (($numWords >= 10) && ($numWords < 11)) {
		$title.css('font-size', '100px');
	}
	else if (($numWords >= 11) && ($numWords < 12)) {
		$title.css('font-size', '90px');
	}
	else if (($numWords >= 12) && ($numWords < 15)) {
		$title.css('font-size', '75px');
	}
	else if (($numWords >= 15) && ($numWords < 35)) {
		$title.css('font-size', '67px');
	}
	else if (($numWords >= 35) && ($numWords < 39)) {
		$title.css('font-size', '54px');
	}
	else if (($numWords >= 39) && ($numWords < 62)) {
		$title.css('font-size', '48px');
	}
	else {
		$title.css('font-size', '160px');
	}
};

function initPreventEnter() {
	$(window).keypress(function(event){
		if(event.keyCode == 13) {
			event.preventDefault();
			return false;
		}
	});
};


function initPostSearchQuery() {
///setup before functions
var typingTimer;                //timer identifier
var doneTypingInterval = 3000;  //time in ms, 5 second for example
//on keyup, start the countdown
$('#magicInput').keyup(function(){
	clearTimeout(typingTimer);
	if($('#magicInput').val) {
		typingTimer = setTimeout(function(){
//do stuff here e.g ajax call etc....
var v = $("#magicInput").val();
alert(v);
}, doneTypingInterval);
	}
});
};

function initInfiniteScroll() {
	var ias = $.ias({
		container:  '.scroller',
		negativeMargin: 350,
		item:       '.story-card',
		pagination: '.pagination',
		next:       '.pagination li:last-child a'
	});
	ias.extension(new IASSpinnerExtension());
	ias.extension(new IASTriggerExtension({text: 'Load more stories',offset: 3}));
//ias.extension(new IASNoneLeftExtension({text: "You have reached to the end"}));
}

function initInfiniteScrollOnTellers() {
	var ias = $.ias({
		container:  '.teller-scroller',
		negativeMargin: 350,
		item:       '.user-card',
		pagination: '.pagination',
		next:       '.pagination li:last-child a'
	});
	ias.extension(new IASSpinnerExtension());
	ias.extension(new IASTriggerExtension({text: 'Load more stories',offset: 3}));
	ias.extension(new IASNoneLeftExtension({text: "You have reached to the end"}));
}

function initDrawer() {
	$('.logo').on('click', function(){
		$('.drawer').addClass('pulled');
		$('.page-wrapper').addClass('pushed');
		$('.drawer-overlay').show()
	});
	$('.drawer-overlay').on('click', function(){
		$('.drawer').removeClass('pulled');
		$('.page-wrapper').removeClass('pushed');
		$(this).hide();
	});
} 

function init(){
	'use strict';
//initTitleFontSize();
initPostSearchQuery();

initShareButtons();	
//if($('.scroller').length) {
//	initInfiniteScroll();
//}
//if($('.teller-scroller').length) {
//	initInfiniteScrollOnTellers();
//};
$('.title-wrapper h1').on('keydown', function(){
//initTitleFontSize();
if (e.keyCode === 13) {
	document.execCommand('insertHTML', false, '<br><br>');
	return false;
}
});
$('#startName').on('keydown', function(){
	initPreventEnter();
});

$('.dropdown').on('show.bs.dropdown', function() {
	$(this).find('.dropdown-menu').first().stop(true, true).fadeIn('fast');
});
$('.dropdown').on('hide.bs.dropdown', function() {
	$(this).find('.dropdown-menu').first().stop(true, true).fadeOut('fast');
});

$('#take_tour_link').click(function(){
	introJs().start();
});

$(".open-menu").click(function() {
	$(".sidemenu").openMenu();
});
$('[data-toggle="tooltip"]').tooltip();

$('.languageSelect').on('click', function() {
	var la = $(this).attr('langVal');
	$.ajax({
		url: "/changeLocale/"+la,
		type: "GET",
		data:  new FormData(this),
		contentType: false,
		cache: true,
		processData:false,
		success: function(data){
			location.reload();    
		},
		error: function(){}           
	});

});	

$(function () {
	$('[data-toggle="tooltip"]').tooltip()
})



};
$(document).ready(function(){
	'use strict';
	init();

	if($('.story-header').length)
	{
		initDrawer();
	}

	$('.chosen-select').chosen({disable_search_threshold: 10});

	var offset = 220;
	var duration = 500;
	jQuery(window).scroll(function() {
		if (jQuery(this).scrollTop() > offset) {
			jQuery('.back-to-top').fadeIn(duration);
		} else {
			jQuery('.back-to-top').fadeOut(duration);
		}
	});

	jQuery('.back-to-top').click(function(event) {
		event.preventDefault();
		jQuery('html, body').animate({scrollTop: 0}, duration);
		return false;
	});

	$('.content').on( 'change keyup keydown paste cut', 'textarea', function (){
		$(this).height(0).height(this.scrollHeight);
	}).find( '.story-description' ).change();
	$('.story-description').keypress(function(event) {
		if (event.keyCode == 13) {
			event.preventDefault();
		}
	});
	var textfields = $('.contenteditable'); 
	textfields.each(function(){
		$(this).on('keypress', function(e){
			if(this.innerHTML.length >= this.getAttribute("max")){
				e.preventDefault();
				return false;
			}
		})
	});

	$(function() {
		var header = $(".trans-header");
		var windowHeight = $(window).height() - 50
		$(window).scroll(function() {
			var scroll = $(window).scrollTop();
			if (scroll >= windowHeight) {
				header.children(".tt-navbar").css('background', '#000');
			} else {
				header.children('.tt-navbar').css('background', 'none');
			}
		});
	});

	$(function () {
		$('#currency').change(function () {
			var selectedText = $(this).find("option:selected").text();
			var selectedValue = $(this).val();
			switch (selectedValue) { 
				case 'USD': 
				$('#currencyIcon').attr('class', 'fa fa-dollar');
				break;
				case 'EUR': 
				$('#currencyIcon').attr('class', 'fa fa-euro');
				break;
				case 'INR': 
				$('#currencyIcon').attr('class', 'fa fa-inr');
				break;
				case 'JPY': 
				$('#currencyIcon').attr('class', 'fa fa-jpy');
				break;
				default:
				$('#currencyIcon').attr('class', 'fa fa-dollar');
			}
		});
	});


});

jQuery(".items_wayscroll").waypoint(function() {
	var get_effect=$(this).attr("data-ease");
	$(this).addClass(get_effect);

}, {
	offset: '95%'
});

