function initInfoWindowOpener() {  
  $('#results-list').on('click', '.get-details', function(){
    $('.info-window-close').addClass('opened');
    $('.info-window').addClass('opened');
    $('.info-overlay').show('fast');
    $(".info-window").animate({scrollTop : 0},300);;
  });
}
function initInfoWindowClose() { 
    $('.info-window-close').removeClass('opened');
    $('.info-window').removeClass('opened');
    $('.info-overlay').hide('fast');
}
  $(document).ready(function(){  
    initInfoWindowOpener();
    $('.info-overlay, .info-window-close').on('click', function(){
      initInfoWindowClose();
    });
})