@extends('../layouts/show_layout')
@section('content')
<section class="page-wrapper settings-wrap clearfix text-normal">
  @include('/shared/header_new')
  <div class="content">
    <div class="col-xs-8 col-md-offset-2">
      <div class="white-bg">
      <h1><i class="fa fa-bell-o"></i> Notifications</h1>
        <ul id="user_notification" class="list-unstyled clearfix">
          @include('/shared/user_all_notifications')
        </ul>
      </div>
    </div>
  </div>
</section>
@stop