<div class="col-md-2">
	<div class="thumbnail" style="height: 350px">
		<!-- style="background: #{{bin2hex(openssl_random_pseudo_bytes(3));}}-->
		@if($user->id != Auth::user()['id'])
		@if(FollowModel::checkIfFollows(Auth::user()['id'], $user->id))
		<?php $followText = "following" ?>
		@else
		<?php $followText = "follow" ?>
		@endif

		@if (Auth::user() == false)
		<a href="" class="non-logged-link btn btn-xs absolute">
			<small id = "followLabel{{$user->id}}">
				{{$followText}}
			</small>
		</a>
		@else
		<a href="" id = "{{$user->id}}" onclick = "followClick(this.id);_gaq.push(['_trackEvent', 'Follow User action performed form StoryTellers', 'clicked'])" class="btn btn-xs btn-primary absolute capitalize {{$followText}}">
			<i class="fa fa-refresh fa-spin" style="display:none"></i>
			<small id = "followLabel{{$user->id}}">
				{{$followText}}
			</small>
		</a>
		@endif			
		@endif			
		<div class="clearfix">

			<div class="avatar">
				@if (Auth::user() == false)
				<a href="" class="non-logged-link">
					@else
					<a href="/profile/{{$user->id}}" onclick="_gaq.push(['_trackEvent', 'StoryTellers profile link actioned', 'clicked'])">
						@endif
						<img class="img-responsive" src="https://res.cloudinary.com/triptroop-prod/image/upload/w_500,h_500,c_fill/{{$user->clPath ? $user->clPath : $user->dpPath}}">
					</a>
				</div>
				<div class="user-meta">
					<h3 class="mar0 capitalize">
						@if (Auth::user() == false)
						<a href="" class="non-logged-link capitalize block">
							@else
							<a class="capitalize block" href="/profile/{{$user->id}}" onclick="_gaq.push(['_trackEvent', 'StoryTellers profile link actioned', 'clicked'])">
								@endif
								{{$user->firstname}} {{$user->lastname}}@if ($user->verified)<i data-toggle="tooltip" title="Verified Account" data-placement="top" class="align-middle ver-icon fa fa-check-circle"></i>@endif
							</a>
						</h3>
						
					<!--
					@if(DB::table('userinfo')->where('id', $user->id )->pluck('location'))
					<hr />
					<span class="location"><i class='icon-map-pin-streamline align-middle'></i> {{DB::table('userinfo')->where('id', $user->id )->pluck('location')}}</span>
					@endif
				-->
				<div class="info">
					<ul class="list-inline clearfix">
						<li class="stories-counter">
							@if (Auth::user() == false)
							<a href="" class="non-logged-link">
								@else
								<a href="/profile/{{$user->id}}" onclick="_gaq.push(['_trackEvent', 'StoryTellers profile link actioned', 'clicked'])">
									@endif
									<h2>{{DB::table('stories')->where('userid', $user->id )->where('published', '1' )->count()}}</h2>
									<h6 class="capitalize mar0"><?php echo Lang::get('common.stories')?></h6>
								</a>
							</li>
							<li class="followers-icon">
								<a href="" data-toggle="modal" data-target="#likersModal" onclick="getFollowers({{$user->id}},'{{$user->firstname}} {{$user->lastname}}');" data-modal-title="test">
									<h2 id="followersLabel{{$user->id}}">{{$user->followers}}</h2>
									<h6 class="mar0"><?php echo Lang::get('common.followers')?></h6>
								</a>
							</li>
							<li class="following-icon">
								<a href="" data-toggle="modal" data-target="#likersModal" onclick="getFollowings({{$user->id}},'{{$user->firstname}} {{$user->lastname}}');">
									<h2>{{$user->following}}</h2>
									<h6 class="mar0"><?php echo Lang::get('common.following')?></h6>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>