@extends("layouts.show_layout")
@section('content')
<section class="page-wrapper storytellers-wrap">
	@include('/shared/header_new')
	<header class="tt-header relative">
		<div class="header-meta">
			<h1><?php echo Lang::get('common.storytellers') ?></h1>
			<h2>{{DB::table('users')->where('confirmed', 1)->count()}} <?php echo Lang::get('common.storytellers') ?></h2>
		</diV>
		<ul class="list-unstyled absolute import-links mar0 text-center">
			<li>Invite friends from</li>
			<li><a href="#" class="fb"><i class="icon-facebook" onclick="_gaq.push(['_trackEvent', 'Facebook link of StoryTellers', 'clicked'])"></i></a></li>
			<li><a href="#" class="gplus"><i class="icon-google-plus" onclick="_gaq.push(['_trackEvent', 'Google Plus link of StoryTellers', 'clicked'])"></i></a></li>
			<li><a href="#" class="twitter"><i class="icon-twitter" onclick="_gaq.push(['_trackEvent', 'Twitter link of StoryTellers', 'clicked'])"></i></a></li>
			<li><a href="#" class="insta"><i class="icon-instagram-1" onclick="_gaq.push(['_trackEvent', 'Instagram link of StoryTellers', 'clicked'])"></i></a></li>
		</ul>
	</header>
	<div class="container-fluid">
		<div class="row">
			<div class="tt-content clearfix">
				<div class="fullsize-modal modal fade hide" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="icon-deletebutton"></i></button>
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-body clearfix">
								<h1 class="text-center">{USERNAME} follows/is followed by</h1>
								<div class="user-row relative clearfix">
									<a href="" class="non-logged-link btn btn-xs absolute">
										<small>
											Follow/Unfollow
										</small>
									</a>
									<div class="avatar">
										<a href="" class="non-logged-link">
											<img class="img-circle img-responsive" src="https://res.cloudinary.com/triptroop-prod/image/upload/w_72/v1431406595/{{DB::table('profile')->where('id', Auth::user()['id'])->pluck('clPath')}}">
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="teller-scroller">
					@foreach ($data as $user)
					@include("/storytellers/teller")
					@endforeach
					{{ $data->links() }}
				</div>

			</div>
		</div>
	</div>
</section>
@stop