@extends("layouts.show_layout")
@section('content')
<script data-cfasync="false" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script data-cfasync="false">
  $(document).ready(function(){
    $("#startCategoryForm").submit(function(event){
   // alert("start");
   event.preventDefault();
   $.ajax({
    url: "startSaveCategory",
    type: "POST",
    data:  new FormData(this),
    contentType: false,
    cache: true,
    processData:false,
    success: function(data){
      if(data == 1){
        console.log('Data saved.');
        $("#step2").addClass('hide');
        $("#step3").removeClass('hide');
      }
        //
        console.log(data);
      },
      error: function(data){
        alert((data));
      }           

    });
 });
    $("#startSaveNameForm").submit(function(event){
   // alert("start");
   event.preventDefault();
   $.ajax({
    url: "startSaveName",
    type: "POST",
    data:  new FormData(this),
    contentType: false,
    cache: true,
    processData:false,
    success: function(data){
      if(data == 1){
        console.log('Data saved.');
      }
      else{
        alert('Data could not saved.'); 
      }
        //
      },
      error: function(data){
        alert((data));
      }           
    });
 });
    
    $('#photoimg').on('change', function() {    
      $("#profileImageUploadForm").submit();
    });

    $("#profileImageUploadForm").submit(function(event){
      $('.tt-loader').show();
      event.preventDefault();
      $.ajax({
        url: "setup_profile_image",
        type: "POST",
        data:  new FormData(this),
        contentType: false,
        cache: true,
        processData:false,
        success: function(data){
          $('.tt-loader').hide();
          if(data != 'failed' && data != 'File with given name already exists'){
            //$("#profileImg").attr('src',data);
            $("#profileImgBig").attr('src','https://res.cloudinary.com/triptroop-prod/w_125'+data);
          }
          else{
            alert(data);
          }
        },
        error: function(){}           
      });
    });  
  });
function checkCategory(){
  //alert('here');
  var categories = "";
  var check = 0;
  for(var i = 1;  i <= 23;  i++){
    if($('#cat' + i).is(":checked")){
      check = 1;
      categories += ('->'+('cat' + i));
    }
  }
  categories += '->';
  if(check){
    return categories;  
  }
  else{
    return 0;
  }
}

function saveCategories(){
    //alert("hii");
    var checks = checkCategory();
  //  alert(checks);
  $("#cats").val(checks);

}

function saveName(){
 // alert($("#startNameHidden").val());
 $("#startNameHidden").val($("#startName").text());
   // alert($("#startNameHidden").val());
   $("#startSaveNameForm").submit();

 }
</script>
<div class="start-wrapper">
  @include('/shared/header_new')  
  <!-- Contain:: start -->
  <div class="container-fluid">
  <div class="row">
    <!-- invite content box -->
    <div id="fb-root"></div>
    <!-- invite content box -->
    <!-- ***************************************** step ::1 ****************************************** -->
    <div class="col-xs-8 col-xs-offset-2">
      <div class="steps-wrapper" name="welcome" id="step1">
        <h1 class="h1 title">
          <a href="#" class="btn btn-primary next-step-link pull-right" onclick="_gaq.push(['_trackEvent', 'Getting Started Welcome Page clicked for select Intrested Categories', 'clicked'])"><?php echo Lang::get('get-started.lets-go')?></a>  
          <div class="steps-count pull-right"><?php echo Lang::get('get-started.step-1-of-4')?></div>
          <?php echo Lang::get('get-started.we-are-glad-you-are-here')?>,  <span>@if (isset($data)) {{ $data->firstname }} @else {{Auth::user()['firstname']}} @endif</span>
        </h1>
        <div class="row step-content">
          <div class="col-md-12">
            <p><?php echo Lang::get('get-started.get-start-message1')?></p>

            <p><?php echo Lang::get('get-started.get-start-message2')?></p>

            <p><?php echo Lang::get('get-started.get-start-message3')?></p>

            <p>— Team TripTroop</p>
          </div>
              <!-- <div class="col-md-6">
                  <img src="/assets/images/story-card.png" alt="img preview" style="width:100%" />
                </div> -->
              </div>

              <div class="row step-content">
                <div class="col-md-12">
                  <!-- carousal starts :: starts -->
                  <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                      <li data-target="#myCarousel" data-slide-to="1"></li>
                      <li data-target="#myCarousel" data-slide-to="2"></li>
                      <li data-target="#myCarousel" data-slide-to="3"></li>
                      <li data-target="#myCarousel" data-slide-to="4"></li>
                      <li data-target="#myCarousel" data-slide-to="5"></li>
                      <li data-target="#myCarousel" data-slide-to="6"></li>
                      <li data-target="#myCarousel" data-slide-to="7"></li>
                      <li data-target="#myCarousel" data-slide-to="8"></li>
                      <li data-target="#myCarousel" data-slide-to="9"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                      <div class="item active">
                        <img src="http://res.cloudinary.com/triptroop/image/upload/v1438420014/start/1.jpg" alt="Story onboaring 1" />
                      </div>
                      <div class="item">
                        <img src="http://res.cloudinary.com/triptroop/image/upload/v1438420014/start/2.jpg" alt="Story onboaring 1" />
                      </div>
                      <div class="item">
                        <img src="http://res.cloudinary.com/triptroop/image/upload/v1438420014/start/3.jpg" alt="Story onboaring 1" />
                      </div>
                      <div class="item">
                        <img src="http://res.cloudinary.com/triptroop/image/upload/v1438420014/start/4.jpg" alt="Story onboaring 1" />
                      </div>
                      <div class="item">
                        <img src="http://res.cloudinary.com/triptroop/image/upload/v1438420014/start/5.jpg" alt="Story onboaring 1" />
                      </div>
                      <div class="item">
                        <img src="http://res.cloudinary.com/triptroop/image/upload/v1438420014/start/6.jpg" alt="Story onboaring 1" />
                      </div>
                      <div class="item">
                        <img src="http://res.cloudinary.com/triptroop/image/upload/v1438420014/start/7.jpg" alt="Story onboaring 1" />
                      </div>
                      <div class="item">
                        <img src="http://res.cloudinary.com/triptroop/image/upload/v1438420014/start/8.jpg" alt="Story onboaring 1" />
                      </div>
                      <div class="item">
                        <img src="http://res.cloudinary.com/triptroop/image/upload/v1438420014/start/9.jpg" alt="Story onboaring 1" />
                      </div>
                      <div class="item">
                        <img src="http://res.cloudinary.com/triptroop/image/upload/v1438420014/start/10.jpg" alt="Story onboaring 1" />
                      </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control hide" href="#myCarousel" role="button" data-slide="prev">
                      <span class="glyphicon glyphcat-icon-chevron-left" aria-hidden="true"></span>
                      <span class="sr-only"><?php echo Lang::get('get-started.previous')?></span>
                    </a>
                    <a class="right carousel-control hide" href="#myCarousel" role="button" data-slide="next">
                      <span class="glyphicon glyphcat-icon-chevron-right" aria-hidden="true"></span>
                      <span class="sr-only"><?php echo Lang::get('get-started.next')?></span>
                    </a>
                  </div>
                  <!-- carousal :: ends -->
                </div>
              </div>
            </div>
            <!-- ***************************************** step ::1 ****************************************** -->





            <!-- ***************************************** step ::2 ****************************************** -->
            <div class="steps-wrapper hide" name="categories" id="step2">
              <h1 class="h1 title"><?php echo Lang::get('get-started.what-interested')?> 
                <div class="pull-right">
                  <form id = "startCategoryForm">
                    <input type = "hidden" id = "cats" name = "category"/>
                    <button onclick="saveCategories();_gaq.push(['_trackEvent', 'Step 2 categories saved', 'clicked'])" class="btn btn-primary pull-right next-step-link">Continue</button>
                  </form>
                </div>
                <div class="pull-right steps-count"><?php echo Lang::get('get-started.step-2-of-4')?></div>
              </h1>
              <!-- <h1>What are you intrested in? <a href="{{url('/start/profile')}}" class="btn btn-primary pull-right">Continue</a></h1> -->
              <p><?php echo Lang::get('get-started.what-interested-message')?></p>
              <div class="row step-content">
                <div class="col-md-12"></div>
              </div>
              <ul class="categories list-unstyled clearfix list-inline text-left">
                <li>
                  @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat1->%')->count())
                  <input id="cat1" class="css-checkbox" type="checkbox" checked/>
                  @else
                  <input id="cat1" class="css-checkbox" type="checkbox" />
                  @endif 
                  <label for="cat1"  class="css-label">
                    <span class="img-circle a">
                      <strong class="fa fa-check-circle icon-checked"></strong>
                      <i class="cat-icon-adventure-travel"></i>
                    </span>
                    <p><?php echo Lang::get('common.advanture')?></p>
                  </label>
                </li>
                <li>
                  @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat2->%')->count())
                  <input id="cat2" class="css-checkbox" type="checkbox" checked/>
                  @else
                  <input id="cat2" class="css-checkbox" type="checkbox" />
                  @endif 
                  <label for="cat2" class="css-label">
                    <span class="img-circle b">
                      <strong class="fa fa-check-circle icon-checked"></strong>
                      <i class="cat-icon-arts-and-liesure"></i>
                    </span>
                    <p><?php echo Lang::get('common.arts-lieasure')?></p>
                  </label>
                </li>
                <li>
                  @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat3->%')->count())
                  <input id="cat3" class="css-checkbox" type="checkbox" checked/>
                  @else
                  <input id="cat3" class="css-checkbox" type="checkbox" />
                  @endif 
                  <label for="cat3" class="css-label">
                    <span class="img-circle c">
                      <strong class="fa fa-check-circle icon-checked"></strong>
                      <i class="cat-icon-beaches"></i>
                    </span>
                    <p><?php echo Lang::get('common.beach')?></p>
                  </label>
                </li>
                <li>
                 @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat4->%')->count())
                 <input id="cat4" class="css-checkbox" type="checkbox" checked/>
                 @else
                 <input id="cat4" class="css-checkbox" type="checkbox" />
                 @endif 
                 <label for="cat4" class="css-label">
                  <span class="img-circle d">
                    <strong class="fa fa-check-circle icon-checked"></strong>
                    <i class="cat-icon-budget-travel"></i>
                  </span>
                  <p><?php echo Lang::get('common.budget-traval')?></p>
                </label>
              </li>
              <li>
                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat5->%')->count())
                <input id="cat5" class="css-checkbox" type="checkbox" checked/>
                @else
                <input id="cat5" class="css-checkbox" type="checkbox" />
                @endif 
                <label for="cat5" class="css-label">
                  <span class="img-circle e">
                    <strong class="fa fa-check-circle icon-checked"></strong>
                    <i class="cat-icon-coasts-and-islands"></i>
                  </span>
                  <p><?php echo Lang::get('common.coasts-islands')?></p>
                </label>
              </li>
              <li>
                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat6->%')->count())
                <input id="cat6" class="css-checkbox" type="checkbox" checked/>
                @else
                <input id="cat6" class="css-checkbox" type="checkbox" />
                @endif 
                <label for="cat6" class="css-label">
                  <span class="img-circle f">
                    <strong class="fa fa-check-circle icon-checked"></strong>
                    <i class="cat-icon-diving-and-snorkelling"></i>
                  </span>
                  <p><?php echo Lang::get('common.diving-snorkelling')?></p>
                </label>
              </li>
              <li>
                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat7->%')->count())
                <input id="cat7" class="css-checkbox" type="checkbox" checked/>
                @else
                <input id="cat7" class="css-checkbox" type="checkbox" />
                @endif 
                <label for="cat7" class="css-label">
                  <span class="img-circle g">
                    <strong class="fa fa-check-circle icon-checked"></strong>
                    <i class="cat-icon-ecotourism"></i>
                  </span>
                  <p><?php echo Lang::get('common.ecotourism')?></p>
                </label>
              </li>
              <li>
                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat8->%')->count())
                <input id="cat8" class="css-checkbox" type="checkbox" checked/>
                @else
                <input id="cat8" class="css-checkbox" type="checkbox" />
                @endif 
                <label for="cat8" class="css-label">
                  <span class="img-circle h">
                    <strong class="fa fa-check-circle icon-checked"></strong>
                    <i class="cat-icon-family-travel"></i>
                  </span>
                  <p><?php echo Lang::get('common.family-travel')?></p>
                </label>
              </li>
              <li>
                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat9->%')->count())
                <input id="cat9" class="css-checkbox" type="checkbox" checked/>
                @else
                <input id="cat9" class="css-checkbox" type="checkbox" />
                @endif 
                <label for="cat9" class="css-label">
                  <span class="img-circle i">
                    <strong class="fa fa-check-circle icon-checked"></strong>
                    <i class="cat-icon-festivals-and-events"></i>
                  </span>
                  <p><?php echo Lang::get('common.festival-events')?></p>
                </label>
              </li>
              <li>
                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat10->%')->count())
                <input id="cat10" class="css-checkbox" type="checkbox" checked/>
                @else
                <input id="cat10" class="css-checkbox" type="checkbox" />
                @endif 
                <label for="cat10" class="css-label">
                  <span class="img-circle j">
                    <strong class="fa fa-check-circle icon-checked"></strong>
                    <i class="cat-icon-film-and-television"></i>
                  </span>
                  <p><?php echo Lang::get('common.film-television')?></p>
                </label>
              </li>
              <li>
                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat11->%')->count())
                <input id="cat11" class="css-checkbox" type="checkbox" checked/>
                @else
                <input id="cat11" class="css-checkbox" type="checkbox" />
                @endif 
                <label for="cat11" class="css-label">
                  <span class="img-circle k">
                    <strong class="fa fa-check-circle icon-checked"></strong>
                    <i class="cat-icon-food-and-drink"></i>
                  </span>
                  <p><?php echo Lang::get('common.food-drink')?></p>
                </label>
              </li>
              <li>
                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat12->%')->count())
                <input id="cat12" class="css-checkbox" type="checkbox" checked/>
                @else
                <input id="cat12" class="css-checkbox" type="checkbox" />
                @endif 
                <label for="cat12" class="css-label">
                  <span class="img-circle l">
                    <strong class="fa fa-check-circle icon-checked"></strong>
                    <i class="cat-icon-gear-and-tech"></i>
                  </span>
                  <p><?php echo Lang::get('common.gear-tech')?></p>
                </label>
              </li>
              <li>
                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat13->%')->count())
                <input id="cat13" class="css-checkbox" type="checkbox" checked/>
                @else
                <input id="cat13" class="css-checkbox" type="checkbox" />
                @endif 
                <label for="cat13" class="css-label">
                  <span class="img-circle m">
                    <strong class="fa fa-check-circle icon-checked"></strong>
                    <i class="cat-icon-honeymoons-and-romance"></i>
                  </span>
                  <p><?php echo Lang::get('common.honymoon-romance')?></p>
                </label>
              </li>
              <li>
                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat14->%')->count())
                <input id="cat14" class="css-checkbox" type="checkbox" checked/>
                @else
                <input id="cat14" class="css-checkbox" type="checkbox" />
                @endif 
                <label for="cat14" class="css-label">
                  <span class="img-circle n">
                    <strong class="fa fa-check-circle icon-checked"></strong>
                    <i class="cat-icon-luxury-travel"></i>
                  </span>
                  <p><?php echo Lang::get('common.luxury-travel')?></p>
                </label>
              </li>
              <li>
                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat15->%')->count())
                <input id="cat15" class="css-checkbox" type="checkbox" checked/>
                @else
                <input id="cat15" class="css-checkbox" type="checkbox" />
                @endif 
                <label for="cat15" class="css-label">
                  <span class="img-circle o">
                    <strong class="fa fa-check-circle icon-checked"></strong>
                    <i class="cat-icon-music"></i>
                  </span>
                  <p><?php echo Lang::get('common.music')?></p>
                </label>
              </li>
              <li>
                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat16->%')->count())
                <input id="cat16" class="css-checkbox" type="checkbox" checked/>
                @else
                <input id="cat16" class="css-checkbox" type="checkbox" />
                @endif 
                <label for="cat16" class="css-label">
                  <span class="img-circle p">
                    <strong class="fa fa-check-circle icon-checked"></strong>
                    <i class="cat-icon-off-the-beaten-track"></i>
                  </span>
                  <p><?php echo Lang::get('common.off-beaten-track')?></p>
                </label>
              </li>
              <li>
                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat17->%')->count())
                <input id="cat17" class="css-checkbox" type="checkbox" checked/>
                @else
                <input id="cat17" class="css-checkbox" type="checkbox" />
                @endif 
                <label for="cat17" class="css-label">
                  <span class="img-circle q">
                    <strong class="fa fa-check-circle icon-checked"></strong>
                    <i class="cat-icon-planes-and-trains"></i>
                  </span>
                  <p><?php echo Lang::get('common.planes-trains')?></p>
                </label>
              </li>
              <li>
                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat18->%')->count())
                <input id="cat18" class="css-checkbox" type="checkbox" checked/>
                @else
                <input id="cat18" class="css-checkbox" type="checkbox" />
                @endif 
                <label for="cat18" class="css-label">
                  <span class="img-circle r">
                    <strong class="fa fa-check-circle icon-checked"></strong>
                    <i class="cat-icon-road-trips"></i>
                  </span>
                  <p><?php echo Lang::get('common.road-trips')?></p>
                </label>
              </li>
              <li>
                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat19->%')->count())
                <input id="cat19" class="css-checkbox" type="checkbox" checked/>
                @else
                <input id="cat19" class="css-checkbox" type="checkbox" />
                @endif 
                <label for="cat19" class="css-label">
                  <span class="img-circle s">
                    <strong class="fa fa-check-circle icon-checked"></strong>
                    <i class="cat-icon-round-the-world-travel"></i>
                  </span>
                  <p><?php echo Lang::get('common.round-world-travel')?></p>
                </label>
              </li>
              <li>
                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat20->%')->count())
                <input id="cat20" class="css-checkbox" type="checkbox" checked/>
                @else
                <input id="cat20" class="css-checkbox" type="checkbox" />
                @endif 
                <label for="cat20" class="css-label">
                  <span class="img-circle t">
                    <strong class="fa fa-check-circle icon-checked"></strong>
                    <i class="cat-icon-travel-photography"></i>
                  </span>
                  <p><?php echo Lang::get('common.travel-photoghraphy')?></p>
                </label>
              </li>
              <li>
                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat21->%')->count())
                <input id="cat21" class="css-checkbox" type="checkbox" checked/>
                @else
                <input id="cat21" class="css-checkbox" type="checkbox" />
                @endif 
                <label for="cat21" class="css-label">
                  <span class="img-circle u">
                    <strong class="fa fa-check-circle icon-checked"></strong>
                    <i class="cat-icon-travel-shopping"></i>
                  </span>
                  <p><?php echo Lang::get('common.travel-shopping')?></p>
                </label>
              </li>
              <li>
                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat22->%')->count())
                <input id="cat22" class="css-checkbox" type="checkbox" checked/>
                @else
                <input id="cat22" class="css-checkbox" type="checkbox" />
                @endif 
                <label for="cat22" class="css-label">
                  <span class="img-circle w">
                    <strong class="fa fa-check-circle icon-checked"></strong>
                    <i class="cat-icon-walking-and-trekking"></i>
                  </span>
                  <p><?php echo Lang::get('common.walking-trekking')?></p>
                </label>
              </li>
              <li>
                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat23->%')->count())
                <input id="cat23" class="css-checkbox" type="checkbox" checked/>
                @else
                <input id="cat23" class="css-checkbox" type="checkbox" />
                @endif 
                <label for="cat23" class="css-label">
                  <span class="img-circle x">
                    <strong class="fa fa-check-circle icon-checked"></strong>
                    <i class="cat-icon-wildlife-and-nature"></i>
                  </span>
                  <p><?php echo Lang::get('common.wildlife-nature')?></p>
                </label>
              </li>
            </ul>
          </div>
          <!-- ***************************************** step ::2 ****************************************** -->





          <!-- ***************************************** step ::3 ****************************************** -->
          <div class="steps-wrapper hide" name="profile setup" id="step3">
            <h1 class="h1 title"><?php echo Lang::get('get-started.customize-your-profile')?>. 
              <a onclick="saveName();_gaq.push(['_trackEvent', 'Step 3 Done Customized profile Saved', 'clicked'])" href="{{'completedGettingStarted'}}" class="btn btn-primary pull-right next-step-link"><?php echo Lang::get('get-started.continue')?></a>
              <div class="pull-right steps-count"><?php echo Lang::get('get-started.step-3-of-4')?></div>
            </h1>
            <!-- <h1>Customize your profile. <a href="{{url('start/import')}}" class="btn btn-primary pull-right">Continue</a></h1> -->
            
            <p><?php echo Lang::get('get-started.customize-your-profile-message')?></p>
            <div class="row step-content">
              <div class="col-md-12">
                <div id="profile" class="clearfix">
                  <div class="portrate hidden-xs">
                    <form  id = "profileImageUploadForm">
                     <div class="userAvtar-container">
                      <table class="userAvtar" data-intro="Change User Avtar here" data-steps="1">
                        <tr>
                          <td>
                            @if(DB::table('profile')->where('id', Auth::user()['id'])->count() && DB::table('profile')->where('id', Auth::user()['id'])->pluck('clPath'))
                            <img name = "profilePicture" id = "profileImgBig" src="https://res.cloudinary.com/triptroop-prod/w_125,h_125,c_fill/{{DB::table('profile')->where('id', Auth::user()['id'])->pluck('clPath')}}" alt="change profile" />
                            @else
                            <img name = "profilePicture" id = "profileImgBig" src="https://res.cloudinary.com/triptroop/image/upload/assets/avatar.png" alt="change profile" />
                            @endif  
                            <!-- <img  alt="change profile" /> -->
                          </td>
                        </tr>
                      </table>
                      <span class="btn-file">
                        <span class="fa fa-pencil"></span>
                        <span class="fileinput-new"></span>
                        <!--span class="fileinput-exists">Change</span -->
                        <input type="file" id="photoimg" name="profilePic" multiple = "false">
                      </span>
                      <!-- <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput" data-no-turbolink="true">Remove</a> -->
                    </div>
                  </form>
                </div>
                <div class="title">
                  <form id = "startSaveNameForm">
                    <input type = "hidden" id = "startNameHidden" name = "name"/>
                    <h2>
                      <label style="color:#ccc" contenteditable="true" id = "startName">{{Auth::user()['firstname']}}</label>
                    </h2>
                  </form>
                  <!-- <h3> <i class="ion-android-pin v-mid "></i> <span>Pune, India</span></h3> -->
                </div>
              </div>
            </div>
          </div>
        </div>        
        <!-- ***************************************** step ::3 ****************************************** -->
      </div>

      <script data-cfasync="false" src="bower_components/jquery/dist/jquery.min.js"></script>
      <!-- next-previous code -->
      <script data-cfasync="false">
        $(document).ready(function(){
          $('.next-step-link').click(function(){
            var stepsWrapper = $(this).parents('.steps-wrapper');
            var nextStepsWrapper = stepsWrapper.next('.steps-wrapper');
       // alert(stepsWrapper);
       stepsWrapper.addClass('hide');
       nextStepsWrapper.removeClass('hide');
     });

        });
      </script>
      <!-- next-previous code -->
      <!-- invite frinds from facebook code -->
</div>

    </div>
    <!-- Contain:: end -->


  </div>
  @stop