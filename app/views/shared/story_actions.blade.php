<script data-cfasync="false" type="text/javascript">
	function likeClick(id){
		$('#like'+id).children('.fa-refresh').show();
		$('#like'+id).children('.fa + .fa').hide();
		$.post("/likeClick/"+id, function(data){
			var likedCount = parseInt($('#count-'+id).text());
			if(data == ""){
				window.location="{{URL::to('login')}}";
			}
			else{ 
				if(data == 1){  
					$('#like'+id).children('.fa-heart-o').addClass('fa-heart');
					$('#like'+id).children('.fa-heart-o').removeClass('fa-heart-o');
					$('#count-'+id).text(likedCount + 1)
					$('#like'+id).addClass('ed');
				}
				if(data == 2){  
					$.post("/likeRemove/"+id, function(data){
						if(data){
							$('#like'+id).children('.fa-heart').addClass('fa-heart-o');
							$('#like'+id).children('.fa-heart').removeClass('fa-heart');
							$('#count-'+id).text(likedCount - 1)
							$('#like'+id).removeClass('ed');
						} 
					});
				}
			}
		}).done(function() {
			$('#like'+id).children('.fa-refresh').hide();
			$('#like'+id).children('.fa + .fa').show();
		});
	}
	function wishClick(id){
		$('#wish'+id).children('.fa-refresh').show();
		$('#wish'+id).children('.fa + .fa').hide();
		$.post("/wishClick/"+id, function(data){
			if(data == ""){
				window.location="{{URL::to('login')}}";
			}
			else{ 
				if(data == 1){  
					$('#wish'+id).children('.fa-bookmark-o').addClass('fa-bookmark');
					$('#wish'+id).children('.fa-bookmark-o').removeClass('fa-bookmark-o');
					$('#wish'+id).addClass('ed');
				}
				if(data == 2){  
					$.post("/wishRemove/"+id, function(data){
						if(data){
							$('#wish'+id).children('.fa-bookmark').addClass('fa-bookmark-o');
							$('#wish'+id).children('.fa-bookmark').removeClass('fa-bookmark');
							$('#wish'+id).removeClass('ed');
						} 
					});
				}
			}
		}).done(function() {
			$('#wish'+id).children('.fa-refresh').hide();
			$('#wish'+id).children('.fa + .fa').show();
		});
	}
	$(".is-scroll-to-content").click(function() {
		$('html,body').animate({
			scrollTop: $(".tt-content").offset().top
		});
	});
	function getLikers(id){
		$('#likersModal').on('hidden.bs.modal', function () {
		  $('#likers').empty();
		});
		$('.modal-loader').removeClass('hide');
		$.ajax({
			url: "/getLikers/"+id,
			success: function(data){
				$('.modal-loader').addClass('hide');
				len = data.length;
				if(len > 0) {
					for(var i = 0; i < len; i++){
						$('#likers').append("<div class='col-md-4'>" +
							"<a class='block' href='/profile/"+data[i].id+"'><img style='background: #{{bin2hex(openssl_random_pseudo_bytes(3));}}' width='60' src='https://res.cloudinary.com/triptroop-prod/image/upload/h_90,w_90,c_fill/v1431406595/"+data[i].clPath+"' />"+data[i].firstname+"&nbsp;"+data[i].lastname+
							"</a></div>");
					}
				}
				else {
					$('#likers').append("<div class='col-md-4 col-md-offset-4 text-center'><h2>0<small class='block'>Likes</small></h2></div>");
				}
			},
			error: function(){}           
		})
	}

	function getFollowers(id, title){
		$('#likersModal').on('hidden.bs.modal', function () {
		  $('#likers').empty();
		});
		var title = title;
		$('#likersModal').on('show.bs.modal', function () {
			$('.modal-loader').removeClass('hide');
		  $('.modal-title').text(title + " is followed by");
		});
		$.ajax({
			url: "/getFollowers/"+id,
			success: function(data){
				$('.modal-loader').addClass('hide');
				len = data.length;
				if(len > 0) {
					for(var i = 0; i < len; i++){
						$('#likers').append("<div class='col-md-4'>" +
							"<a class='block' href='/profile/"+data[i].id+"'><img style='background: #{{bin2hex(openssl_random_pseudo_bytes(3));}}' width='60' src='https://res.cloudinary.com/triptroop-prod/image/upload/h_90,w_90,c_fill/v1431406595/"+data[i].clPath+"' />"+data[i].firstname+"&nbsp;"+data[i].lastname+
							"</a></div>");
					}
				}
				else {
					$('#likers').append("<div class='col-md-4 col-md-offset-4 text-center'><h2>0<small class='block'>Followers</small></h2></div>");
				}
			},
			error: function(){}           
		})
	}

	function getFollowings(id, title){
		$('#likersModal').on('hidden.bs.modal', function () {
		  $('#likers').empty();
		});
		var title = title;
		$('#likersModal').on('show.bs.modal', function () {
			$('.modal-loader').removeClass('hide');
		  $('.modal-title').text(title + " follows");
		});
		$.ajax({
			url: "/getFollowings/"+id,
			success: function(data){
				$('.modal-loader').addClass('hide');
				len = data.length;
				if(len > 0) {
					for(var i = 0; i < len; i++){
						$('#likers').append("<div class='col-md-4'>" +
							"<a class='block' href='/profile/"+data[i].id+"'><img style='background: #{{bin2hex(openssl_random_pseudo_bytes(3));}}' width='60' src='https://res.cloudinary.com/triptroop-prod/image/upload/h_90,w_90,c_fill/v1431406595/"+data[i].clPath+"' />"+data[i].firstname+"&nbsp;"+data[i].lastname+
							"</a></div>");
					}
				}
				else {
					$('#likers').append("<div class='col-md-4 col-md-offset-4 text-center'><h2>0<small class='block'>Following</small></h2></div>");
				}
			},
			error: function(){}           
		})
	}
</script>