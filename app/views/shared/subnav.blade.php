<div class="subnav">
	<ul class="nav nav-tabs">
		<li class="{{ Request::is( 'all') ? 'active' : '' }}"><a href="/all">All Stories</a></li>
		<li class="{{ Request::is( 'popular') ? 'active' : '' }}"><a href="/popular">Popular Stories</a></li>
		<li class="{{ Request::is( 'themes', 'themes/*') ? 'active' : '' }}"><a href="/themes">Story Themes</a></li>
	</ul>
</div>