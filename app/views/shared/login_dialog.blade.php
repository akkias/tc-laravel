<div id="loginModal" class="login-modal modal fade">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" aria-label="Close">&times;</button>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body pad0 clearfix">
                <div class="bg-container text-center">
                    <img class="logo" src="https://res.cloudinary.com/triptroop/image/upload/c_scale,w_150/assets/logo_white.png">
                    <div class="points text-left">
                        <h4>Why sign up?</h4>
                        <ul>
                            <li>Explore the world of stories</li>
                            <li>Follow authentic travellers</li>
                            <li>New stories notifications</li>
                        </ul>
                    </div>
                </div>
                <div id="tabs">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active"><a data-toggle="tab" href="#login-tab" onclick="_gaq.push(['_trackEvent', 'login-tab actioned from login dialog', 'clicked'])">{{Lang::get('common.sign-in')}}</a></li>
                        <li><a data-toggle="tab" href="#signup-tab" onclick="_gaq.push(['_trackEvent', 'signup-tab actioned from login dialog', 'clicked'])">{{Lang::get('common.sign-up')}}</a></li>
                    </ul>
                    <div class="tab-content clearfix">
                        <div id="signup-tab" class="tab-pane fade in clearfix">
                            {{ Form::open(array('id'=> 'signupForm', 'data-parsley-validate' => '')) }}
                            <ul class="errors">
                                @foreach($errors->all() as $message)
                                <li>{{ $message }}</li>
                                @endforeach
                            </ul>
                            <div class="form-group">
                                {{ Form::text('firstname', Session::has('fname') ? Session::get('fname') :Input::old('firstname'), array('class' => 'form-control input-lg', 'autocomplete' =>'off', 'placeholder' => 'First name', 'required' => '')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::text('lastname',  Session::has('lname') ? Session::get('lname') : Input::old('lastname'), array('class' => 'form-control input-lg', 'placeholder' => 'Last name', 'required' => '')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::email('emailid',Session::has('email') ? Session::get('email') :Input::old('email'), array('class' => 'form-control input-lg', 'placeholder' => 'Email', 'required' => '', 'data-parsley-trigger' =>'change', 'data-parsley-type' => 'email')) }}
                            </div>                  
                            <div class="form-group">
                                {{ Form::password('password',array('class' => 'form-control input-lg', 'placeholder' => 'Set Password', 'required' => '', 'title' => 'Password must contain at least 6 characters, including numbers', 'pattern' => '(?=.*\d)(?=.*[a-z]).{6,}', 'onchange' => 'this.setCustomValidity(this.validity.patternMismatch ? this.title : "");', 'name' => 'setpwd')) }}
                            </div>
                            <div class="btn-wrapper">
                                {{ Form::submit('Sign up', array('class' => 'btn btn-success btn-block', 'onclick' => '_gaq.push(["_trackEvent", "Sign up attempt from Login dialog", "clicked"])')) }}
                            </div>
                            <p class="help-block terms">
                                By <?php echo Lang::get('sign-up.create-an-account')?>, <?php echo Lang::get('sign-up.accept-terms')?> Triptroop's <a target="_blank" class="grey-link underline" href="/terms"><?php echo Lang::get('sign-up.terms-services')?></a>.
                            </p>
                            {{ Form::close() }} 
                            <div class="modal-footer">
                                <a class="btn btn-facebook" href="{{url('/login/fb/signup')}}" onclick="_gaq.push(['_trackEvent', 'SignUp attempt with facebook in signup dialog', 'clicked'])"><i class="fa fa-facebook align-middle"></i> Facebook</a>
                                <a class="btn mar0 btn-google-plus" id="signup_gmail_id" href="{{url('/login/gm/signup')}}"  onclick="_gaq.push(['_trackEvent', 'SignUp attempt with gmail in signup dialog', 'clicked'])"><i class="fa fa-google-plus align-middle"></i> Google</a>
                            </div>
                        </div>





                        <div id="login-tab" class="tab-pane fade in clearfix active">
                            {{ Form::open(array( 'id' => 'loginForm','data-parsley-validate' => '')) }}
                            <ul class="errors">
                                @foreach($errors->all() as $message)
                                <li>{{ $message }}</li>
                                @endforeach
                            </ul>
                            <div class="form-group">
                                {{ Form::email('emailid','', array('class' => 'email-input form-control input-lg', 'placeholder' => 'Email', 'required' => '')) }}
                            </div>                    
                            <div class="form-group">
                                {{ Form::password('password', array('class' => 'password-input form-control input-lg','placeholder' => 'Password', 'required' => '', 'title' => 'Password must contain at least 6 characters, including numbers', 'pattern' => '(?=.*\d)(?=.*[a-z]).{6,}', 'onchange' => 'this.setCustomValidity(this.validity.patternMismatch ? this.title : "");', 'name' => 'setpwd')) }}
                            </div>
                            <div class="btn-wrapper">
                                {{ Form::submit('Sign in', array('class' => 'btn btn-success btn-block', 'onclick' => '_gaq.push(["_trackEvent", "Login attempt  from Login dialog", "clicked"])')) }}
                            </div>
                            <p class="help-block text-right">{{ HTML::link('recover-password','Forgotten your password?',array('target' => '_self', 'class' => 'recover-pwd', 'onclick' => '_gaq.push(["_trackEvent", "Forgot Password from Login dialog", "clicked"])'))}}</p>
                            {{ Form::close() }}  
                            <div class="modal-footer">
                                <a class="btn btn-facebook" href="{{url('/login/fb')}}" onclick="_gaq.push(['_trackEvent', 'Login attempt with facebook from Login Dialog', 'clicked'])"><i class="fa fa-facebook align-middle"></i> Facebook</a>
                                <a class="btn mar0 btn-google-plus" id="login_gmail_id" href="{{url('/login/gm/login')}}" onclick="_gaq.push(['_trackEvent', 'Login attempt with gmail from Login Dialog', 'clicked'])"><i class="fa fa-google-plus align-middle"></i> Google</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>