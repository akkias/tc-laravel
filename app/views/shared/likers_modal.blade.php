<div id="likersModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" aria-label="Close">&times;</button>
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title text-center">People who liked this</h4>
			</div>
			<div class="modal-body">
			<div class="modal-loader text-center hide">
				<i style="font-size: 24px; padding: 35px;color: #CED4DA;" class="fa fa-refresh fa-spin"></i>
			</div>
				<div class="container-fluid">
					<div class="row" id="likers">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>