@if (NotificationModel::getNotification())
@foreach(NotificationModel::getNotification() as $notificationData)
<li class="clearfix">
	<article>
		<a href="/profile/{{$notificationData->notifier}}"><img class="img-circle" height="40" width="40" class="block" src="https://res.cloudinary.com/triptroop-prod/image/upload/w_90,h_90,c_fill/v1431406595/{{NotificationModel::getNotifierPic($notificationData->notifier)}}" /></a>
		<span class="data-cell"><a href="/profile/{{$notificationData->notifier}}">{{ NotificationModel::getNotifierName($notificationData->notifier)}}</a> {{$notificationData->notify}}</span>
	</article>
</li>
@endforeach
@else
	<li>
	<p class="text-center">No new notification present at this time click view all</p>
	</li>
@endif