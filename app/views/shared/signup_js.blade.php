<script data-cfasync="false" type="text/javascript">
	$("#signupForm").submit(function(event){
		event.preventDefault();
		$.ajax({
			url: "sign-up",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: true,
			processData:false,
			success: function(data){
				console.log(data)
				if(data == 1){
					$('.err').text('User with provided email already exists. Click "Sign In" ').slideDown();
				}
				else if(data == 2){
					$('.err').text('Account activation email has been sent to your provided email address.').slideDown();
				}
				else if(data == 3){
					$('.err').text("Operation failed. Please try again").slideDown();
				}
				else{
					alert('exception');
				}
			},
			error: function(){}           
		});
	});
</script>