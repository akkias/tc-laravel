<header class="clearfix">
	<div class="navbar text-center" style="top: 85px;">
		<ul class="nav">
			<li><a class="animsition-link home" title="Home" href="{{ URL::to( '/') }}"><i class="icon-home-house-streamline"></i><span><?php echo Lang::get('common.home')?></span></a></li>
			<li><a class="animsition-link editors-pick" title="Editor's Picks" href="{{ URL::to( 'editorspicks') }}"><i class="icon-award-stroke"></i><span><?php echo Lang::get('common.editors-picks')?></span></a></li>
			<li><a class="animsition-link storytellers" title="Storytellers" href="{{ URL::to( 'storytellers') }}"><i class="icon-speech-streamline-talk-user"></i><span><?php echo Lang::get('common.storytellers')?></span></a></li>
			<li class="dropdown"><a class="animsition-link themes" title="Themes" href="{{ URL::to( 'themes') }}"><i class="icon-painting-pallet-streamline"></i><span><?php echo Lang::get('common.themes')?></span></a>
				<ul class="text-center mega-menu">
					<li class="mega-menu-column pull-left">
						<ul class="clearfix list-unstyled">
							<li><a class="a" href="{{ URL::to( 'themes/adventure') }}"><?php echo Lang::get('common.advanture')?></a></li>
							<li><a class="b" href="{{ URL::to( 'themes/arts-and-liesure') }}"><?php echo Lang::get('common.arts-lieasure')?></a></li>
							<li><a class="c" href="{{ URL::to( 'themes/backpacking') }}"><?php echo Lang::get('common.backpacking')?></a></li>
							<li><a class="d" href="{{ URL::to( 'themes/beaches') }}"><?php echo Lang::get('common.beach')?></a></li>
							<li><a class="e" href="{{ URL::to( 'themes/budget-travel') }}"><?php echo Lang::get('common.budget-traval')?></a></li>
							<li><a class="f" href="{{ URL::to( 'themes/coasts-and-islands') }}"><?php echo Lang::get('common.coasts-islands')?></a></li>
							<li><a class="g" href="{{ URL::to( 'themes/diving-and-snorkelling') }}"><?php echo Lang::get('common.diving-snorkelling')?></a></li>
							<li><a class="h" href="{{ URL::to( 'themes/ecotourism') }}"><?php echo Lang::get('common.ecotourism')?></a></li>
						</ul>
					</li> 
					<li class="mega-menu-column pull-left">
						<ul class="clearfix list-unstyled">
							<li><a class="i" href="{{ URL::to( 'themes/family-travel') }}"><?php echo Lang::get('common.family-travel')?></a></li>
							<li><a class="j" href="{{ URL::to( 'themes/festival-and-events') }}"><?php echo Lang::get('common.festival-events')?></a></li>
							<li><a class="k" href="{{ URL::to( 'themes/film-and-television') }}"><?php echo Lang::get('common.film-television')?></a></li>
							<li><a class="l" href="{{ URL::to( 'themes/food-and-drink') }}"><?php echo Lang::get('common.food-drink')?></a></li>
							<li><a class="m" href="{{ URL::to( 'themes/gear-and-tech') }}"><?php echo Lang::get('common.gear-tech')?></a></li>
							<li><a class="n" href="{{ URL::to( 'themes/honeymoon-and-romance') }}"><?php echo Lang::get('common.honymoon-romance')?></a></li>
							<li><a class="o" href="{{ URL::to( 'themes/luxury-travel') }}"><?php echo Lang::get('common.luxury-travel')?></a></li>
							<li><a class="p" href="{{ URL::to( 'themes/music') }}"><?php echo Lang::get('common.music')?></a></li>
						</ul>
					</li>
					<li class="mega-menu-column pull-left">
						<ul class="clearfix list-unstyled">
							<li><a class="q" href="{{ URL::to( 'themes/off-the-beaten-track') }}"><?php echo Lang::get('common.off-beaten-track')?></a></li>
							<li><a class="r" href="{{ URL::to( 'themes/planes-and-trains') }}"><?php echo Lang::get('common.planes-trains')?></a></li>
							<li><a class="s" href="{{ URL::to( 'themes/road-trips') }}"><?php echo Lang::get('common.road-trips')?></a></li>
							<li><a class="t" href="{{ URL::to( 'themes/round-the-world-travel') }}"><?php echo Lang::get('common.round-world-travel')?></a></li>
							<li><a class="u" href="{{ URL::to( 'themes/travel-photograhpy') }}"><?php echo Lang::get('common.travel-photoghraphy')?></a></li>
							<li><a class="v" href="{{ URL::to( 'themes/travel-shopping') }}"><?php echo Lang::get('common.travel-shopping')?></a></li>
							<li><a class="w" href="{{ URL::to( 'themes/walking-and-trekking') }}"><?php echo Lang::get('common.walking-trekking')?></a></li>
							<li><a class="x" href="{{ URL::to( 'themes/wildlife-and-nature') }}"><?php echo Lang::get('common.wildlife-nature')?></a></li>
						</ul>
					</li>
				</ul><!-- dropdown-menu -->
			</li>
			<li><a class="animsition-link search" title="Search" href="{{ URL::to( 'search') }}"><i class="icon-search"></i><span><?php echo Lang::get('common.search')?></span></a></li>
			<li><a title="Search" href="{{ URL::to( 'profile') }}"><i class="icon-man-people-streamline-user"></i><span><?php echo Lang::get('common.my-profile')?></span></a></li>
		</ul>
		<ul class="tt-social list-unstyled">
			<li><a href="#"><i class="icon-facebook"></i></a></li>
			<li><a href="https://twitter.com/triptroopme" target="_blank"><i class="icon-twitter"></i></a></li>
			<li><a href="#"><i class="icon-google-plus"></i></a></li>
		</ul>


		<ul class="list-inline list-unstyled pull-right top-actions">
			@if (Request::path() == 'narrate')
			<li class="align-middle">
				<button onclick="saveStory()" class="btn">Save</button> 
				<button  onclick="publishStory()" class="btn">Publish</button>
			</li>
			@endif
			@if (Auth::user() == false)
			<li class="align-middle">
				<a class="btn" href="{{ URL::to( 'login') }}"><?php echo Lang::get('common.sign-in')?></a>
				<a class="btn" href="{{ URL::to( 'sign-up') }}"><?php echo Lang::get('common.sign-up')?></a>
			</li>
			@else
			<li>{{ HTML::linkAction('LogoutController@getLogout', 'Sign Out') }}</li>
			@endif
		</ul>
		<a class="pull-left logo" href="{{ URL::to( '/') }}">TRIPTROOP</a>
	</div>

	@if (Request::path() == '/')
	<div class="mast-head relative" style="margin-top:65px;">
		<article class="intro absolute text-center">
			<img class="tagline" src="/assets/images/tagline.png">
			{{Session::get('locale')}}
			<h2><?php echo Lang::get('messages.home-travel-stories'); ?></h2>
			<h3><?php echo Lang::get('home.tagline-share-stories')?></h3>
			<p>
				<a class="btn" href="{{ URL::to( 'sign-up') }}"><?php echo Lang::get('home.get-started')?></a>
				<br>
				<small><?php echo Lang::get('home.or')?><a href="#"><strong><?php echo Lang::get('home.exporer-stories')?></strong></a></small>
			</p>
		</article>
		<div class="absolute author-info">
			<a href="#"><strong>Akshay Sonawane</strong></a> <span>on jan 18, 2015</span>
		</div>
		<a class="absolute is-scroll-to-content" href="#"><i class="icon-downicon"></i></a>
	</div>
	@endif
</header>