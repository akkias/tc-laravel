<div class="btn-group pull-right" role="group">
  @if (Request::path() == 'narrate' || Request::is('edit/*'))
  <a href="#" class="btn save-story" onclick="saveStory(event);">
    <i class="fa fa-spin fa-refresh hide"></i>
    <span>{{Lang::get('common.save')}}</span>
  </a> 
  <a href="#" class="btn btn-primary publish-story" onclick="publishStory(event);">
    <i class="fa fa-spin fa-refresh hide"></i>
    <span>{{Lang::get('common.publish')}}</span>
  </a>
  @endif
  @if (Request::is('read/*'))
  @if ($readStory[0]->userid == Auth::user()['id'])
  <a id = "{{$readStory[0]->storyid}}"  class="btn del btn-danger" onclick="_gaq.push(['_trackEvent', 'Delete draft actioned view from Draft', 'clicked']); deleteClick(this.id); ">Delete</a>
  <a href="/edit/{{$readStory[0]->storyid}}" class="btn btn-primary" onclick="_gaq.push(['_trackEvent', 'Edit draft actioned view from Draft', 'clicked'])">Edit</a>
  @endif
  @endif
</div>
<header class="fixed-header trans-header fixed">
  <nav class="relative tt-navbar navbar navbar-default" style="background: none">
    <div class="navbar-header pull-left">
      <a onclick="_gaq.push(['_trackEvent', 'Header Logo Clicked Navigated to Landing Page', 'clicked'])" class="logo" href="{{URL::to('/')}}"><img src="/assets/images/tt_logo.png" height="30" alt="TripTroop"></a>
    </div>
    <ul class="list-unstyled nav-right pull-right clearfix nav">
      <li class="pull-right" ><button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button></li>
      @if (Auth::user() == false)
      <li class="align-middle non-logged-link pull-right">
        <a onclick="_gaq.push(['_trackEvent', 'Opened signin/signup dialog', 'clicked'])" class="pull-left"><?php echo Lang::get('common.sign-in')?> / <?php echo Lang::get('common.sign-up')?></a>
      </li>
      @else
      <li class="dropdown pull-right">
        <a class="pad0 nav-avatar img-circle" onclick="_gaq.push(['_trackEvent', 'Header User Dropdown Options actioned', 'clicked'])">
          @if(DB::table('profile')->where('id', Auth::user()['id'])->count() && DB::table('profile')->where('id', Auth::user()['id'])->pluck('clPath'))
          <img height="32" width="32" class="block img-circle" name = "profilePicture" id ="profileImg" src="https://res.cloudinary.com/triptroop-prod/image/upload/w_90,h_90,c_fill/v1431406595/{{DB::table('profile')->where('id', Auth::user()['id'])->pluck('clPath')}}" alt="change profile" />
          @else
          <img height="32" class="img-circle" name = "profilePicture" id ="profileImg" src="https://res.cloudinary.com/triptroop-prod/image/upload/w_90/v1431406595/assets/avatar.png" alt="change profile" />
          @endif  
          <i class="icon-downicon hidden"></i>
        </a>
        <div class="tt-menu profile-menu">
          <ul class="list-unstyled clearfix">
            <li><a href="/profile" onclick="_gaq.push(['_trackEvent', 'Profile Page link actioned', 'clicked'])"><i class="align-middle fa fa-user"></i><?php echo Lang::get('common.my-profile'); ?></a></li>
            <li><a href="/profile/drafts" onclick="_gaq.push(['_trackEvent', 'Draft Page link actioned', 'clicked'])"><i class="align-middle fa fa-file-o"></i><?php echo Lang::get('common.drafts'); ?></a></li>
            <li><a href="/profile/dreamlist" onclick="_gaq.push(['_trackEvent', 'Dreamlist Page link actioned', 'clicked'])"><i class="align-middle fa fa-bookmark-o"></i><?php echo Lang::get('common.dreamlist'); ?></a></li>
            <li><a href="/settings" onclick="_gaq.push(['_trackEvent', 'Settings Page link actioned', 'clicked'])"><i class="align-middle fa fa-cog"></i><?php echo Lang::get('common.settings'); ?></a></li>
            <li><a href="<?php echo URL::action('LogoutController@getLogout'); ?>"  onclick="_gaq.push(['_trackEvent', 'Logout link actioned', 'clicked'])"><i class="align-middle fa fa-sign-out"></i><?php echo Lang::get('common.sign-out'); ?></a></li>
          </ul>
        </div>
      </li>
      <li class="dropdown navbar-header pull-right">
        <a href="#" class="link-noti img-circle"><i class="glyphicon glyphicon-bell"></i> <span class="notificationCount"></span></a>
        <div class="tt-menu notifications-menu">
          <ul class="list-unstyled clearfix">
            <li class="title clearfix">
              <article>
                <h5>Notifications <a id="notificationReadI">Mark all as read</a></h5>
              </article>
            </li>
            <li>
              <ul id="user_notification" class="list-unstyled clearfix">
                @foreach(NotificationModel::getNotification() as $notificationData)
                <li class="clearfix">
                  <article>
                    <a href="/profile/{{$notificationData->notifier}}"><img class="img-circle" height="40" width="40" class="block" src="https://res.cloudinary.com/triptroop-prod/image/upload/w_90/v1431406595/{{NotificationModel::getNotifierPic($notificationData->notifier)}}" /></a>
                    <span class="data-cell"><a href="/profile/{{$notificationData->notifier}}">{{ NotificationModel::getNotifierName($notificationData->notifier)}}</a> {{$notificationData->notify}}</span>
                  </article>
                </li>
                @endforeach
              </ul>
            </li>
          </ul>
        </div>
      </li>
      @endif
      @if (Request::path() != 'narrate')
      <li class="pull-right">
        <a class="link-noti img-circle" href="{{URL::to('search')}}" onclick="_gaq.push(['_trackEvent', 'Search Page link actioned', 'clicked'])"><i title="Search" class="fa fa-search"></i></a>
      </li>
      <li class="pull-right">
        <a class="link-noti img-circle" href="{{URL::to('/narrate')}}" onclick="_gaq.push(['_trackEvent', 'Narrate Page link actioned', 'clicked'])"><i title="Narrate" class="fa fa-pencil"></i></a>
      </li>
      @endif
      @if (Request::path() == 'narrate' || Request::is('edit/*'))
      <li class="align-middle btns positive pull-right" onclick="_gaq.push(['_trackEvent', 'Narrate Story Save or Publish actioned', 'clicked'])">
        <button class="btn narrate pull-left" onclick="saveStory()"><i class="fa fa-floppy-o"></i> {{Lang::get('common.save')}}</button> 
        <button class="btn narrate pull-left" onclick="publishStory()"><i class="fa fa-arrow-circle-up"></i> {{Lang::get('common.publish')}}</button>
      </li>
      @endif
      @if (Request::is('read/*'))
      @if ($readStory[0]->userid == Auth::user()['id'])
      <li class="align-middle btns positive pull-right">
        <a id = "{{$readStory[0]->storyid}}"  class="pull-left del btn narrate" onclick="_gaq.push(['_trackEvent', 'Delete draft actioned view from Draft', 'clicked']); deleteClick(this.id); "><i title="Delete" class="fa fa-trash"></i> <span>Delete</span></a>
        <a href="/edit/{{$readStory[0]->storyid}}" class="pull-left btn narrate" onclick="_gaq.push(['_trackEvent', 'Edit draft actioned view from Draft', 'clicked'])"><i title="Edit" class="fa fa-pencil"></i> <span>Edit</span></a>
      </li>
      @endif
      @endif
    </ul>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="list-unstyled list-inline nav-left mar0">
        <li class="{{ Request::is( '/','/popular','themes', 'themes/*') ? 'active dropdown' : 'dropdown' }}">
          <a class="animsition-link home" title="Travel Stories" onclick="_gaq.push(['_trackEvent', 'Home Page link Actioned', 'clicked'])" href="{{ URL::to( '/all') }}"><span>Travel Stories</span><i class="fa fa-angle-down"></i></a>
          <div class="tt-menu">
            <ul class="list-unstyled clearfix text-left">
              <li class="fa fa-caret-up"></li>
              <li><a class="animsition-link all-stories" title="All Stories" onclick="_gaq.push(['_trackEvent', 'All Story link Actioned', 'clicked'])" href="{{ URL::to( 'all') }}"><span>All stories</span></a></li>
              <li><a class="animsition-link popular-stories" title="Popular Stories" onclick="_gaq.push(['_trackEvent', 'Popular Story link Actioned', 'clicked'])" href="{{ URL::to( 'popular') }}"><span><?php echo Lang::get('common.popular-stories')?></span></a></li>
              <li><a class="animsition-link themes" title="Themes" href="{{ URL::to( 'themes') }}" onclick="_gaq.push(['_trackEvent', 'Themes link Actioned', 'clicked'])"><span>Story <?php echo Lang::get('common.themes')?></span></a></li>
            </ul>
          </div>
        </li>
        <li class="{{ Request::is( '/explore') ? 'active' : '' }}">
          <a class="animsition-link" title="Explore places" onclick="_gaq.push(['_trackEvent', 'Explore places link Actioned', 'clicked'])" href="{{ URL::to( '/map') }}"><i class="fa fa-globe"></i> <span>Explore places</span></a>
        </li>
      </ul>
    </div>
  </nav>
</header>