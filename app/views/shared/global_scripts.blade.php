<!--Mandatory scripts for application-->
{{ HTML::script('/assets/javascripts/application.js', array('data-cfasync' => 'false'), false); }}

@include('/shared/story_actions')
@if (Request::path() == 'narrate' || Request::is('edit/*'))
<!--Mandatory for editor -->
{{ HTML::script('/bower_components/underscore/underscore-min.js', array('data-cfasync' => 'false'), false); }}
{{ HTML::script('/bower_components/sanitize.js/lib/sanitize.js', array('data-cfasync' => 'false'), false); }}
{{ HTML::script('/bower_components/dante/dist/js/dante-editor.js', array('data-cfasync' => 'false'), false); }}
{{ HTML::script('/assets/javascripts/moment.min.js', array('data-cfasync' => 'false'), false); }}
{{ HTML::script('/assets/javascripts/daterangepicker.js', array('data-cfasync' => 'false'), false); }}
@include('/shared/narrate_story_js')
@endif
@if (Request::is('read/*'))
<script data-cfasync="false">
	$(document).ready(function(){
		$('.defaultValue').parents('figcaption[contenteditable~="true"]').hide();
		
	});
</script>
@endif

@if (Request::is('profile/*'))
<script data-cfasync="false">
	$(document).ready(function(){
		$('#followButton').on('click', function(){
			$('#followButton').children('.fa-refresh').show();
			$.post("/followClick/"+$('#hiddenId').val()+'/'+{{Auth::user()['id']}}, function(data){
				if(data.ret == 2){
					$('#followLabel').text('<?php echo Lang::get('common.follow')?>');  
					$('#followersLabel').text(data.val);
				}
				else if(data.ret == 1){ 
					$('#followersLabel').text(data.val);
					$('#followLabel').text('<?php echo Lang::get('common.following')?>');
				}
			}).done(function() {
				$('#followButton').children('.fa-refresh').hide();
			});
		});
	});
</script>
@endif


@if (Auth::user() == true && Request::path() == 'storytellers')
@include('/shared/follow_storyteller')
@endif

@if (Request::path() == 'search')
@include('/shared/search_page_js')
@endif
@if (Auth::user() == false)
<script data-cfasync="false">
	$(document).ready(function(){
		$('#loginModal').on('show.bs.modal', function (event) {
			initSignUpForm();
			initloginForm();
		});
		$('#loginModal').on('hide.bs.modal', function (event) {
			$('.err').hide();
		});
		$('.non-logged-link').on('click', function(event){
			$("#loginModal").modal();
			event.preventDefault();
		});
	});
	/*
	$( document ).ajaxStart(function() {
  		$( ".tt-loader" ).show();
	}).ajaxComplete(function() {
  		$( ".tt-loader" ).hide();
	});
*/
</script>
@include('/shared/login_js')
@endif
<script src="">
	$( document ).ajaxStart(function() {
  		$( ".tt-loader" ).show();
	}).ajaxComplete(function() {
  		$( ".tt-loader" ).hide();
	});
</script>