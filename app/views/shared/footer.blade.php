<footer class="footer clearfix text-center">
	<ul class="list-unstyled list-inline">
		<li>
			<a class="animsition-link" title="Home" href="{{ URL::to( '/') }}"  onclick="_gaq.push(['_trackEvent', 'Home link  actioned from Footer', 'clicked'])"><span><?php echo Lang::get('common.home')?></span></a>
		</li>
		<li>
			<a class="animsition-link" title="Popular Stories" href="{{ URL::to( 'popular') }}" onclick="_gaq.push(['_trackEvent', 'Popular Story link actioned from Footer', 'clicked'])"><span><?php echo Lang::get('common.popular-stories')?></span></a>
		</li>
		<li>
			<a class="animsition-link" title="Themes" href="{{ URL::to( 'themes') }}" onclick="_gaq.push(['_trackEvent', 'Themes link actioned from Footer', 'clicked'])"><span><?php echo Lang::get('common.themes')?></span></a>
		</li>
		<li>
			<a class="animsition-link" title="Privacy Policy" href="{{ URL::to( 'privacy') }}" onclick="_gaq.push(['_trackEvent', 'Privacy link actioned from Footer', 'clicked'])"><span>Privacy</span></a>
		</li>
		<li>
			<a class="animsition-link" title="Terms and Condition" href="{{ URL::to( 'terms') }}" onclick="_gaq.push(['_trackEvent', 'Terms link actioned from Footer', 'clicked'])"><span>Terms</span></a>
		</li>
	</ul>
	<ul class="list-unstyled list-inline list-social">
	<li>
			<a class="img-circle fb" data-toggle="tooltip" title="Follow us on Facebook" onclick="_gaq.push(['_trackEvent', 'Facebook Page link Actioned', 'clicked'])" href="https://www.facebook.com/pages/TripTroop/1041831852498335?fref=ts" target="_blank">
				<i class="fa fa-facebook"></i>
			</a>
		</li>
		<li>
			<a class="img-circle tw"  data-toggle="tooltip" title="Follow us on Twitter" onclick="_gaq.push(['_trackEvent', 'Twitter Page link Actioned', 'clicked'])" href="https://twitter.com/triptroopme" target="_blank">
				<i class="fa fa-twitter"></i>
			</a>
		</li>
		<li>
			<a class="img-circle mail" data-toggle="tooltip" title="Send us an email" onclick="_gaq.push(['_trackEvent', 'Mail link Actioned', 'clicked'])" href="mailto:info@triptroop.me,master@triptroop.me">
				<i class="fa fa-paper-plane"></i>
			</a>
		</li>		
	</ul>
	<a class="back-to-top" href="#"><i class="fa fa-chevron-up"></i></a>
</footer> 