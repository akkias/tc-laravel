<script data-cfasync="false" type="text/javascript">
	function followClick(val){
		$('#'+val).children('.fa-refresh').show();
		$('#'+val).children('small').hide();
		$.post("/followClick/"+val+'/'+{{Auth::user()['id']}}, function(data){
			if(data.ret == 2){
				$('#'+val).addClass('follow');
				$('#'+val).removeClass('following');
				$('#followLabel'+val).text('Follow');
				$('#followersLabel'+val).text(data.val)
			}
			else if(data.ret == 1){ 
				$('#'+val).addClass('following');
				$('#'+val).removeClass('follow');
				$('#followLabel'+val).text('Following');
				$('#followersLabel'+val).text(data.val)
			}
		}).done(function() {
		    $('#'+val).children('.fa-refresh').hide();
		    $('#'+val).children('i + small').show();
		  });
		event.preventDefault();
	};
</script>