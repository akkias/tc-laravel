<script data-cfasync="false" type="text/javascript">

  // code to check if user leaving narrate page
  $(window).on('onbeforeunload', function(event) {
      event.returnValue = "Once you leave this page, U will lose your data..";
  });
/*  window.onbeforeunload = function(event) {
      event.returnValue = "Once you leave this page, U will lose your data..";
  };*/

//$('.tt-loader').addClass('show');
  editor = new Dante.Editor({
    el: "#editor",
    upload_url: "/upload",
   // store_url: "" ,
    spellcheck: true,
    default_loading_placeholder: 'http://avengers.eu1.frbit.net/assets/images/tt-loader.gif',
    disable_title: true
  });
  
  editor.start();

  $("#profileImageUploadForm").submit(function(event){
    $('.tt-loader').show();
    event.preventDefault();
    $.ajax({
      url: "setup_cover_image",
      type: "POST",
      data:  new FormData(this),
      contentType: false,
      cache: false,
      dataType: 'json',
      processData:false,
      success: function(data, textStatus, jqXHR){
        $('.tt-loader').hide();
        $('.alert-danger').alert('close');
        console.log(data)

        if(data != 'failed' && data != 'File with given name already exists'){
          //$('#narrateCover').css("background-image", "url(https://res.cloudinary.com/hnz6dacyv"+data+")");  
        }
        else{
          alert(data);
        }

      },
      error: function(){
        $('.tt-loader').hide();
      }           
    });
  });

  function readImage(input) {
    if ( input.files && input.files[0] ) {
        var FR= new FileReader();
        FR.onload = function(e) {
             $('#narrateCover').css("background-image", "url("+e.target.result+")");  
        };       
        FR.readAsDataURL( input.files[0] );
        event.stopPropagation();
    }
    return false;
}
  $('#photoimg').on('change', function() {    
    $("#profileImageUploadForm").submit();
    readImage( this );
  });
  $('input[name="daterange"]').daterangepicker();
  function checkCategory(){
    var categories = "";
    var check = 0;
    for(var i = 1;  i <= 24;  i++){
      if($('#cat' + i).is(":checked")){
        check = 1;
        categories += ('->'+('cat' + i));
      }
    }
    alert(categories)
    categories += '->';
    if(check){
      return categories;  
    }
    else{
      return 0;
    }
  }
  function storyTitleValidation(){
      if($("#storyTitle").text() === ""){
        $("#storyTitle").focus();
        return 0;
      }
      else{
        return 1;
      };
  }
  function saveStory(){
    var titleCheck = storyTitleValidation();
    var checks = checkCategory();
    if(assignStoryData() && titleCheck ? 1 : alert("Name your Story! Title can't be blank")){
      $("#saveId").val(1);
      $("#editorForm").submit();  
    }
    return false;
    $(window).off("onbeforeunload");
  }
  function assignStoryData(){
    $("#storyTitleId").val($('#storyTitle').text());  
    $("#storyDescriptionId").val($('#storyDescription').val());  
    $("#editorDataId").val(editor.getContent());
    $("#currencyId").val($('#currency').val());
    $("#dateId").val($('input[name="daterange"]').val());
    $("#expenseId").val($('#expenditure').val());
//    if($("#dateId").val() == ""){
//      alert('Select date range.');
//      return 0;
//    }
//if($("#expenseId").val() == ""){
//alert('Enter total expense.');
//return 0;
//}
// // console.log('storytitle - ' + $("#storyTitleId").val());
//  console.log('currency - ' + $("#currencyId").val());
//  console.log('date - ' + $("#dateId").val());
//  console.log('expense - ' + $("#expenseId").val());
// console.log($("#currencyId").val());
return 1;
}

function publishStory(){
  var titleCheck = storyTitleValidation();
  var checks = checkCategory();
  $("#checksId").val(checks);
  if(assignStoryData() && titleCheck ? 1 : alert("Name your Story! Title can't be blank") && checks ? 1 : alert('No category selected.')){
    $("#publishId").val(1);
    $("#editorForm").submit();
  }
  $(window).off("onbeforeunload");
}

$("#editorForm").submit(function(event){
//console.log( $('#daterange').val());
// console.log($('input[name="daterange"]').val());

event.preventDefault(); 
$.ajax({
  url: "saveStory",
  type: "POST",
  data:  new FormData(this),
  contentType: false,
  cache: false,
  processData:false,
  success: function(data){
    if($.type(data) == 'object'){
      $('body').append('<div class="err alert alert-success alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Story published.</div>');
      setTimeout(function(){
        window.location = "/read/"+data['published'];
      }, 5000);
    }
    else {
      if(data == 'Story saved.'){
        $('body').append('<div class="err alert alert-success alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Story saved.</div>');
      }
      else {
        $('body').append('<div class="err alert alert-danger alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data+'</div>');
      }
    }
    console.log(data)
  },
  error: function(){}           
});
});
$('#daterange').daterangepicker(null, function(start, end) {});
//window.onbeforeunload = function(e) {
  //  return "You haven't published your story yet. Do you want to leave without publishing?";
//};


/* Restric Title length:: starts */

$.fn.focusEnd = function() {
    $(this).focus();
    var tmp = $('<span />').appendTo($(this)),
        node = tmp.get(0),
        range = null,
        sel = null;

    if (document.selection) {
        range = document.body.createTextRange();
        range.moveToElementText(node);
        range.select();
    } else if (window.getSelection) {
        range = document.createRange();
        range.selectNode(node);
        sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
    }
    tmp.remove();
    return this;
}
/*
function StoryTitleLimit(){
    const limit = 60;
    $("#storyTitle").on('input', function (e) {
        var title = $('#storyTitle').text().length;
        if (title >= 60) {
            $('#storyTitle').text($('#storyTitle').text().substring(0,60));
            $('#storyTitle').focusEnd();       
                    e.preventDefault();
                }     
        });
}

StoryTitleLimit();
 Restrick Title length:: ends */
</script>

