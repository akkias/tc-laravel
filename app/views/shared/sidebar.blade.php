<nav class="menu-drawer">
	<div class="profile clearfix">
		<a href="#" class="close-drawer"><i class="ion-close-round"></i></a>
		<div class="avatar pull-left">
			<a href="#" class="">
				<img src="/assets/images/user.jpg" height="40" class="img-circle">
			</a>
		</div>
		<div class="user-meta">
			<a class="show name" href="#">Akshay Sonawane</a>
			<a class="show" href="#"><i class="ion-android-settings align-middle"></i> <small>Edit details</small></a>
		</div>
	</div>
	<div class="menu">
		<ul class="nav nav-pills nav-stacked">
			<li><a href="{{ URL::to( 'narrate') }}" class="btn">Narrate a story</a><hr></li>
			<li class="{{ Request::is( '/') ? 'active' : '' }}"><a href="{{ URL::to( '/') }}"><i class="ion-ios-home"></i>Home</a></li>
			<li class="{{ Request::is( 'editorspicks') ? 'active' : '' }}"><a href="{{ URL::to( 'editorspicks') }}"><i class="ion-ios-bookmarks"></i>Editor's Picks</a></li>
			<li class="{{ Request::is( 'storytellers') ? 'active' : '' }}"><a href="{{ URL::to( 'storytellers') }}"><i class="ion-ios-people"></i>StoryTellers</a></li>
			<li class="{{ Request::is( 'themes') ? 'active' : '' }}"><a href="{{ URL::to( 'themes') }}"><i class="ion-ios-color-filter"></i>Themes</a></li>
		</ul>
	</div>
	<div class="foot">
		<div class="social-icons">
			<ul>
				<li><a class="fb" href="#"><i class="ion-social-facebook"></i></a></li>
				<li><a class="twitter" href="#"><i class="ion-social-twitter"></i></a></li>
				<li><a class="linkedin" href="#"><i class="ion-social-linkedin"></i></a></li>
			</ul>
		</div>
		@if (Auth::user())
		<div>
			{{ HTML::linkAction('LogoutController@getLogout', 'Sign Out', array(), array('class' =>'sign-out')) }}
		</div>
		@endif
	</div>
</nav>