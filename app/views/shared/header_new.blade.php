<header class="fixed-header fixed">
<nav class="relative tt-navbar navbar navbar-default">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header pull-left">
<!-- <button type="button" onclick="_gaq.push(['_trackEvent', 'Main Menu-toggle lower resolution', 'clicked'])" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button> -->
<a onclick="_gaq.push(['_trackEvent', 'Header Logo Clicked Navigated to Landing Page', 'clicked'])" class="logo" href="{{URL::to('/')}}"><img src="/assets/images/tt_logo.png" height="30" alt="TripTroop"></a>
</div>
<ul class="list-unstyled nav-right pull-right clearfix nav">
<li class="pull-right" ><button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
</button></li>
<!--
<a onclick="_gaq.push(['_trackEvent', 'signup', 'opened sign up page'])"class="pull-left" href="{{ URL::to( 'sign-up') }}"><?php echo Lang::get('common.sign-up')?></a>
<a onclick="_gaq.push(['_trackEvent', 'button3', 'opened sign in page'])" class="pull-left" href="{{ URL::to( 'login') }}"><?php echo Lang::get('common.sign-in')?></a>
-->
@if (Auth::user() == false)
<li class="align-middle non-logged-link pull-right">
    <a onclick="_gaq.push(['_trackEvent', 'Opened signin/signup dialog', 'clicked'])" class="pull-left"><?php echo Lang::get('common.sign-in')?> / <?php echo Lang::get('common.sign-up')?></a>
</li>
@else
<li class="dropdown pull-right">
    <a class="pad0 nav-avatar img-circle" onclick="_gaq.push(['_trackEvent', 'Header User Dropdown Options actioned', 'clicked'])">
        @if(DB::table('profile')->where('id', Auth::user()['id'])->count() && DB::table('profile')->where('id', Auth::user()['id'])->pluck('clPath'))
        <img height="32" width="32" class="block img-circle" name = "profilePicture" id ="profileImg" src="https://res.cloudinary.com/triptroop-prod/image/upload/w_90,h_90,c_fill/v1431406595/{{DB::table('profile')->where('id', Auth::user()['id'])->pluck('clPath')}}" alt="change profile" />
        @else
        <img height="32" class="img-circle" name = "profilePicture" id ="profileImg" src="https://res.cloudinary.com/triptroop-prod/image/upload/w_90/v1431406595/assets/avatar.png" alt="change profile" />
        @endif  
        <i class="icon-downicon hidden"></i>
    </a>
    <div class="tt-menu profile-menu">
        <ul class="list-unstyled clearfix">
            <li><a href="/profile" onclick="_gaq.push(['_trackEvent', 'Profile Page link actioned', 'clicked'])"><i class="align-middle fa fa-user"></i><?php echo Lang::get('common.my-profile'); ?></a></li>
            <li><a href="/profile/drafts" onclick="_gaq.push(['_trackEvent', 'Draft Page link actioned', 'clicked'])"><i class="align-middle fa fa-file-o"></i><?php echo Lang::get('common.drafts'); ?></a></li>
            <li><a href="/profile/dreamlist" onclick="_gaq.push(['_trackEvent', 'Dreamlist Page link actioned', 'clicked'])"><i class="align-middle fa fa-bookmark-o"></i><?php echo Lang::get('common.dreamlist'); ?></a></li>
            <li><a href="/settings" onclick="_gaq.push(['_trackEvent', 'Settings Page link actioned', 'clicked'])"><i class="align-middle fa fa-cog"></i><?php echo Lang::get('common.settings'); ?></a></li>
            <li><a href="/logout"  onclick="_gaq.push(['_trackEvent', 'Logout link actioned', 'clicked'])"><i class="align-middle fa fa-sign-out"></i><?php echo Lang::get('common.sign-out'); ?></a></li>
        </ul>
    </div>
</li>
<li class="dropdown navbar-header pull-right">
    <a href="#" class="link-noti img-circle"><i class="glyphicon glyphicon-bell"></i> <span class="notificationCount"></span></a>
    <div class="tt-menu notifications-menu">
        <ul class="list-unstyled clearfix">
            <li class="title clearfix">
                <article>
                    <h5>Notifications <a id="notificationReadI">Mark all as read</a></h5>
                </article>
            </li>
            <li>
                <ul id="user_notification" class="list-unstyled clearfix">
                    @if (NotificationModel::getNotification())
                    @foreach(NotificationModel::getNotification() as $notificationData)
                    <li class="clearfix">
                        <article>
                            <a href="/profile/{{$notificationData->notifier}}"><img class="img-circle" height="40" width="40" class="block" src="https://res.cloudinary.com/triptroop/image/upload/w_90/v1431406595/{{NotificationModel::getNotifierPic($notificationData->notifier)}}" /></a>
                            <span class="data-cell"><a href="/profile/{{$notificationData->notifier}}">{{ NotificationModel::getNotifierName($notificationData->notifier)}}</a> {{$notificationData->notify}}</span>
                        </article>
                    </li>
                    @endforeach
                    @else 
                       <li>
                        <p class="text-center">No new notification present at this time click view all</p>
                       </li>
                    @endif
                </ul>
            </li>
            <li class="view-all-noti"><a id="view_all_notification" href="/notifications">View All</a></li>
        </ul>
    </div>

</li>
@endif
<li class="dropdown language-selector pull-right hidden-xs">
    <a class="active-language capitalize {{Session::get('locale')}}">{{Session::has('locale') ? Session::get('locale') : 'en'}}<span class="fa fa-angle-down"></span></a>
    <div class="tt-menu profile-menu">
        <ul class="list-unstyled clearfix">
            <li class="fa fa-caret-up"></li>
            <li><a href="" onchange="_gaq.push(['_trackEvent', 'Langauge English selected', 'clicked'])" class="languageSelect en" langVal="en">English</a></li>
            <li><a href="" onchange="_gaq.push(['_trackEvent', 'Langauge Spanish selected', 'clicked'])" class="languageSelect es" langVal="es">español</a></li>
            <li><a href="" onchange="_gaq.push(['_trackEvent', 'Langauge French selected', 'clicked'])" class="languageSelect fr" langVal="fr">français</a></li>
            <li><a href="" onchange="_gaq.push(['_trackEvent', 'Langauge German selected', 'clicked'])" class="languageSelect gr" langVal="gr">Deutsch</a></li>
            <li><a href="" onchange="_gaq.push(['_trackEvent', 'Langauge Japanese selected', 'clicked']) "class="languageSelect ja" langVal="ja">日本人</a></li>
            <li><a href="" onchange="_gaq.push(['_trackEvent', 'Langauge Portuguese selected', 'clicked'])" class="languageSelect pt" langVal="pt">português</a></li>
        </ul>
    </div>
</li>
<li class="pull-right">
    <a href="{{URL::to('search')}}" onclick="_gaq.push(['_trackEvent', 'Search Page link actioned', 'clicked'])"><i title="Search" class="fa fa-search"></i> Stories</a>
</li>
<li class="pull-right">
@if (Auth::user() == false)
    <a class="btn narrate non-logged-link" href="" onclick="_gaq.push(['_trackEvent', 'Narrate Page link actioned', 'clicked'])"><i title="Search" class="fa fa-pencil"></i> <span class="hidden-xs">{{Lang::get('common.narrate-story')}}</span></a>
    @else
    <a class="btn narrate" href="{{URL::to('/narrate')}}" onclick="_gaq.push(['_trackEvent', 'Narrate Page link actioned', 'clicked'])"><i title="Search" class="fa fa-pencil"></i> <span class="hidden-xs">{{Lang::get('common.narrate-story')}}</span></a>
    @endif
</li>
@if (Request::path() == 'narrate' || Request::is('edit/*'))
<li class="align-middle btns positive pull-right" onclick="_gaq.push(['_trackEvent', 'Narrate Story Save or Publish actioned', 'clicked'])">
    <a class="pull-left" href="" onclick="saveStory()">{{Lang::get('common.save')}}</a> 
    <a class="pull-left" href="" onclick="publishStory()">{{Lang::get('common.publish')}}</a>
</li>
@endif
@if (Request::is('read/*'))
@if ($readStory[0]->userid == Auth::user()['id'])
<li class="align-middle btns positive pull-right">
    <a id = "{{$readStory[0]->storyid}}"  class="pull-left del btn-danger" onclick="_gaq.push(['_trackEvent', 'Delete draft actioned view from Draft', 'clicked']); deleteClick(this.id); ">Delete</a>
    <a href="/edit/{{$readStory[0]->storyid}}" class="pull-left btn-info" onclick="_gaq.push(['_trackEvent', 'Edit draft actioned view from Draft', 'clicked'])">Edit</a>
</li>
@endif
@endif
</ul>
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="list-unstyled list-inline nav-left mar0">
        <li class="{{ Request::is( '/','/popular','themes', 'themes/*') ? 'active dropdown' : 'dropdown' }}">
            <a class="animsition-link home" title="Travel Stories" onclick="_gaq.push(['_trackEvent', 'Home Page link Actioned', 'clicked'])" href="{{ URL::to( 'all') }}"><span>Travel Stories</span><i class="fa fa-angle-down"></i></a>
            <div class="tt-menu">
                <ul class="list-unstyled clearfix text-left">
                    <li class="fa fa-caret-up"></li>
                    <li><a class="animsition-link all-stories" title="All Stories" onclick="_gaq.push(['_trackEvent', 'All Story link Actioned', 'clicked'])" href="{{ URL::to( 'all') }}"><span>All stories</span></a></li>
                    <li><a class="animsition-link popular-stories" title="Popular Stories" onclick="_gaq.push(['_trackEvent', 'Popular Story link Actioned', 'clicked'])" href="{{ URL::to( 'popular') }}"><span><?php echo Lang::get('common.popular-stories')?></span></a></li>
                    <li><a class="animsition-link themes" title="Themes" href="{{ URL::to( 'themes') }}" onclick="_gaq.push(['_trackEvent', 'Themes link Actioned', 'clicked'])"><span>Story <?php echo Lang::get('common.themes')?></span></a></li>
                </ul>
            </div>
        </li>
        <li class="{{ Request::is( '/explore') ? 'active' : '' }}">
            <a class="animsition-link" title="Explore places" onclick="_gaq.push(['_trackEvent', 'Explore places link Actioned', 'clicked'])" href="{{ URL::to( '/map') }}"><i class="fa fa-globe"></i> <span>Explore places</span></a>
        </li>
    </ul>
</div>
</nav>
@if (Request::is('all','popular','themes'))
    @include('/shared/subnav')
@endif
</header>
<!-- 
<nav class="rhs-slided-nav ">
<ul class="list-unstyled main-menu">
Include your navigation here
<li class="text-right"><a href="#" id="nav-close">X</a></li>

</ul>
</nav> -->