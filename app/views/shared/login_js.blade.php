<script data-cfasync="false" type="text/javascript">
/* Login event */
function initloginForm(){
    $("#loginForm").submit(function(event){
        event.preventDefault();
        $.ajax({
            url: "/login",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: true,
            processData:false,
            success: function(data){
                if(data == 1){
                    //window.location =  "{{URL::To('/')}}";
                    window.location.reload();
                }else if(data == 2){
                    $('body').append('<div class="err alert alert-success alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Please activate your account. Account activation email has been sent to your provided email address.</div>');
                }
                else if(data == 3){
                    $('body').append('<div class="err alert alert-danger alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Invalid credentials</div>');
                }
                else if(data == 5){
                    window.location =  "{{URL::To('start')}}";
                }
                else{
                    alert(data) ;
                }
            },
            error: function(){
                alert('Error occured');   
            }           
        });
    })
};

/* Sign up event */
function initSignUpForm(){
    $("#signupForm").submit(function(event){
        event.preventDefault();
        $.ajax({
            url: "/sign-up",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: true,
            processData:false,
            success: function(data){
                console.log(data)
                if(data == 1){
                    $('body').append('<div class="err alert alert-danger alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>User with provided email already exists. Click "Sign In"</div>');
                }
                else if(data == 2){
                    $('body').append('<div class="err alert alert-success alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Account activation email has been sent to your provided email address.</div>');
                }
                else if(data == 3){
                    $('body').append('<div class="err alert alert-danger alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Operation failed. Please try again</div>');
                }
                else{
                    alert('exception');
                }
            },
            error: function(){}           
        });
    })
};
</script>