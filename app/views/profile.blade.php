@extends('../layouts/profile_layout')
<div id="wrapper" class="profile-page">
  <a href="#sidebar" class="mobilemenu"><i class="icon-reorder"></i></a>
  <div id="sidebar">
    <div id="main-nav">
      @if(!isset($data))
      <a class="edit" href="/settings"><i class="ion-edit"></i></a>
      @endif      
      <div id="nav-container">
        <div id="profile" class="clearfix">
          <div class="portrate hidden-xs">
            <div class="userAvtar-container">
              <table class="userAvtar" data-intro="Change User Avarar here" data-steps="1">
                <tr>
                  <td>
                    <img src="/assets/images/profile/user-avtar-01.jpg" alt="change profile" />
                  </td>
                </tr>
              </table>
              <!-- <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput" data-no-turbolink="true">Remove</a> -->
            </div>
          </div>
          <div class="title">
            <h2>

              @if (isset($data))
              {{          $data->firstname }}
              @else
              {{Auth::user()['firstname']}}
              @endif

            </h2>
            <h3> <i class="ion-android-pin v-mid "></i> <span>Pune, India</span></h3>
          </div>

        </div>
        @if(!isset($data))
        <ul id="navigation">
          <li>
            <a href="#biography">
              <i class="align-middle ion-ios-person-outline"></i><?php echo Lang::get('common.about-me')?>
            </a>
          </li>  

          <li>
            <a href="#publications">
              <i class="align-middle ion-ios-paper-outline"></i><?php echo Lang::get('common.publish-trip')?>
            </a>
          </li> 

          <li>
            <a href="#drafts">
              <i class="align-middle ion-ios-paper-outline"></i><?php echo Lang::get('common.my-drafts')?>
            </a>
          </li>

          <li>
            <a href="#dreamlist">
              <i class="align-middle ion-ios-heart-outline"></i><?php echo Lang::get('common.dreamlist')?>
            </a>
          </li>

        </ul>
        @endif
      </div>        
    </div>

    <div class="social-icons">
      <ul>
        <li><a class="fb" href="#"><i class="ion-social-facebook"></i></a></li>
        <li><a class="twitter" href="#"><i class="ion-social-twitter"></i></a></li>
        <li><a class="linkedin" href="#"><i class="ion-social-linkedin"></i></a></li>
      </ul>
    </div>    
  </div>

  <div id="main">

    <div id="biography" class="page home" data-pos="home">
      <div class="pageheader">
        <div class="headercontent">
          <div class="section-container">

            <div class="row">
              <div class="col-sm-2 visible-sm"></div>
              <div class="col-sm-8 col-md-5">
                <div class="biothumb">
                  <img alt="image" src="/assets/images/profile/user-avtar-01.jpg"  class="img-responsive">
                  <div class="overlay">

                    <h1 class="">

                      @if (isset($data))
                      {{$data->firstname }}
                      @if($data->verified)
                      <i class="ion-checkmark-circled"></i>
                      @endif
                      <input type = "hidden" id = "hiddenId" value = "{{$data->id}}"/>
                      @else
                      {{Auth::user()['firstname']}}
                      @if(Auth::user()['verified'])
                      <i class="ion-checkmark-circled"></i>
                      @endif
                      <input type = "hidden" id = "hiddenId" value = "{{Auth::user()['id']}}"/>   
                      @endif
                    </h1>
                    <ul class="list-unstyled">
                      <li>Pune, India</li>
                    </ul>
                  </div> 
                </div>

              </div>
              <div class="clearfix visible-sm visible-xs"></div>
              <div class="col-sm-12 col-md-7">
                <h3 class="title clearfix"><span><?php echo Lang::get('common.bio')?></span></h3>
                <div class="row follow-details">
                  <div class="col-md-3"> <strong id = "followersLabel">
                    @if(isset($data))
                    {{ 
                    $data->followers;
                  }}
                  @else 
                  {{ Auth::user()['followers'];}}
                  @endif</strong> <p>Followers</p></div>
                  <div class="col-md-3"> <strong>
                    @if(isset($data))
                    {{ $data->following;}}
                    @else 
                    {{ Auth::user()['following'];}}
                    @endif</strong> <p>Followings</p></div>


                    @if(isset($data))
                    @if($data->id != Auth::user()['id'])
                    <button  id="followButton" class="btn btn-follow pull-right"  data-intro="Follow/Unfollow User" data-steps="2">
                      <i class="ion-android-person-add"></i>
                      @if($isFollow)  
                      <label id = "followLabel"><?php echo Lang::get('common.following')?></label>
                      @else
                      <label id = "followLabel"><?php echo Lang::get('common.followers')?></label>
                      @endif                
                    </button>
                    @endif
                    @endif       
                  </div>
                  <br>
                  <p class="bio-description"><?php echo Lang::get('profile.bio-info')?>A social psychologist and marketer, Jennifer Doe is the General Atlantic Professor of Marketing and Ormond Family Faculty at Stanford Universityâ€™s Graduate School of Business. Her research spans time, money and happiness. She focuses on questions such as: What actually makes people happy, as opposed to what they think makes them happy? tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor.sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                </div>  
              </div>
            </div>        
          </div>
        </div>

        <div class="pagecontents">
          <div class="section">
            <div class="section-container">
              <div class="row">
                <div class="col-md-12">
                  <div class="title text-center">
                    <h3><?php echo Lang::get('common.fav-travel-activities')?></h3>
                  </div>

                </div>
                <div class="col-md-12">
                  <ul class="categories list-unstyled clearfix list-inline text-left">
                    <li>
                      <input id="cat1" class="css-checkbox" type="checkbox" />
                      <label for="cat1"  class="css-label">
                        <span class="img-circle a">
                          <strong class="ion-checkmark-circled icon-checked"></strong>
                          <i class="icon-adventure-travel"></i>
                        </span>
                        <p><?php echo Lang::get('common.advanture')?></p>
                      </label>
                    </li>
                    <li>
                      <input id="cat2" class="css-checkbox" type="checkbox" />
                      <label for="cat2" class="css-label">
                        <span class="img-circle b">
                          <strong class="ion-checkmark-circled icon-checked"></strong>
                          <i class="icon-arts-and-liesure"></i>
                        </span>
                        <p><?php echo Lang::get('common.arts-lieasure')?></p>
                      </label>
                    </li>
                    <li>
                      <input id="cat3" class="css-checkbox" type="checkbox" />
                      <label for="cat3" class="css-label">
                        <span class="img-circle c">
                          <strong class="ion-checkmark-circled icon-checked"></strong>
                          <i class="icon-beaches"></i>
                        </span>
                        <p><?php echo Lang::get('common.beach')?></p>
                      </label>
                    </li>
                    <li>
                      <input id="cat4" class="css-checkbox" type="checkbox" />
                      <label for="cat4" class="css-label">
                        <span class="img-circle d">
                          <strong class="ion-checkmark-circled icon-checked"></strong>
                          <i class="icon-budget-travel"></i>
                        </span>
                        <p><?php echo Lang::get('common.budget-traval')?></p>
                      </label>
                    </li>
                    <li>
                      <input id="cat5" class="css-checkbox" type="checkbox" />
                      <label for="cat5" class="css-label">
                        <span class="img-circle e">
                          <strong class="ion-checkmark-circled icon-checked"></strong>
                          <i class="icon-coasts-and-islands"></i>
                        </span>
                        <p><?php echo Lang::get('common.coasts-islands')?></p>
                      </label>
                    </li>
                    <li>
                      <input id="cat6" class="css-checkbox" type="checkbox" />
                      <label for="cat6" class="css-label">
                        <span class="img-circle f">
                          <strong class="ion-checkmark-circled icon-checked"></strong>
                          <i class="icon-diving-and-snorkelling"></i>
                        </span>
                        <p><?php echo Lang::get('common.diving-snorkelling')?></p>
                      </label>
                    </li>
                    <li>
                      <input id="cat7" class="css-checkbox" type="checkbox" />
                      <label for="cat7" class="css-label">
                        <span class="img-circle g">
                          <strong class="ion-checkmark-circled icon-checked"></strong>
                          <i class="icon-ecotourism"></i>
                        </span>
                        <p><?php echo Lang::get('common.ecotourism')?></p>
                      </label>
                    </li>
                    <li>
                      <input id="cat8" class="css-checkbox" type="checkbox" />
                      <label for="cat8" class="css-label">
                        <span class="img-circle h">
                          <strong class="ion-checkmark-circled icon-checked"></strong>
                          <i class="icon-family-travel"></i>
                        </span>
                        <p><?php echo Lang::get('common.family-travel')?></p>
                      </label>
                    </li>
                    <li>
                      <input id="cat9" class="css-checkbox" type="checkbox" />
                      <label for="cat9" class="css-label">
                        <span class="img-circle i">
                          <strong class="ion-checkmark-circled icon-checked"></strong>
                          <i class="icon-festivals-and-events"></i>
                        </span>
                        <p><?php echo Lang::get('common.festival-events')?></p>
                      </label>
                    </li>
                    <li>
                      <input id="cat10" class="css-checkbox" type="checkbox" />
                      <label for="cat10" class="css-label">
                        <span class="img-circle j">
                          <strong class="ion-checkmark-circled icon-checked"></strong>
                          <i class="icon-film-and-television"></i>
                        </span>
                        <p><?php echo Lang::get('common.film-television')?></p>
                      </label>
                    </li>
                    <li>
                      <input id="cat11" class="css-checkbox" type="checkbox" />
                      <label for="cat11" class="css-label">
                        <span class="img-circle k">
                          <strong class="ion-checkmark-circled icon-checked"></strong>
                          <i class="icon-food-and-drink"></i>
                        </span>
                        <p><?php echo Lang::get('common.food-drink')?></p>
                      </label>
                    </li>
                    <li>
                      <input id="cat12" class="css-checkbox" type="checkbox" />
                      <label for="cat12" class="css-label">
                        <span class="img-circle l">
                          <strong class="ion-checkmark-circled icon-checked"></strong>
                          <i class="icon-gear-and-tech"></i>
                        </span>
                        <p><?php echo Lang::get('common.gear-tech')?></p>
                      </label>
                    </li>
                    <li>
                      <input id="cat13" class="css-checkbox" type="checkbox" />
                      <label for="cat13" class="css-label">
                        <span class="img-circle m">
                          <strong class="ion-checkmark-circled icon-checked"></strong>
                          <i class="icon-honeymoons-and-romance"></i>
                        </span>
                        <p><?php echo Lang::get('common.honymoon-romance')?></p>
                      </label>
                    </li>
                    <li>
                      <input id="cat14" class="css-checkbox" type="checkbox" />
                      <label for="cat14" class="css-label">
                        <span class="img-circle n">
                          <strong class="ion-checkmark-circled icon-checked"></strong>
                          <i class="icon-luxury-travel"></i>
                        </span>
                        <p><?php echo Lang::get('common.luxury-travel')?></p>
                      </label>
                    </li>
                    <li>
                      <input id="cat15" class="css-checkbox" type="checkbox" />
                      <label for="Music" class="css-label">
                        <span class="img-circle o">
                          <strong class="ion-checkmark-circled icon-checked"></strong>
                          <i class="icon-music"></i>
                        </span>
                        <p><?php echo Lang::get('common.music')?></p>
                      </label>
                    </li>
                    <li>
                      <input id="cat16" class="css-checkbox" type="checkbox" />
                      <label for="cat16" class="css-label">
                        <span class="img-circle p">
                          <strong class="ion-checkmark-circled icon-checked"></strong>
                          <i class="icon-off-the-beaten-track"></i>
                        </span>
                        <p><?php echo Lang::get('common.off-beaten-track')?></p>
                      </label>
                    </li>
                    <li>
                      <input id="cat17" class="css-checkbox" type="checkbox" />
                      <label for="cat17" class="css-label">
                        <span class="img-circle q">
                          <strong class="ion-checkmark-circled icon-checked"></strong>
                          <i class="icon-planes-and-trains"></i>
                        </span>
                        <p><?php echo Lang::get('common.planes-trains')?></p>
                      </label>
                    </li>
                    <li>
                      <input id="cat18" class="css-checkbox" type="checkbox" />
                      <label for="cat18" class="css-label">
                        <span class="img-circle r">
                          <strong class="ion-checkmark-circled icon-checked"></strong>
                          <i class="icon-road-trips"></i>
                        </span>
                        <p><?php echo Lang::get('common.road-trips')?></p>
                      </label>
                    </li>
                    <li>
                      <input id="cat19" class="css-checkbox" type="checkbox" />
                      <label for="cat19" class="css-label">
                        <span class="img-circle s">
                          <strong class="ion-checkmark-circled icon-checked"></strong>
                          <i class="icon-round-the-world-travel"></i>
                        </span>
                        <p><?php echo Lang::get('common.round-world-travel')?></p>
                      </label>
                    </li>
                    <li>
                      <input id="cat20" class="css-checkbox" type="checkbox" />
                      <label for="cat20" class="css-label">
                        <span class="img-circle t">
                          <strong class="ion-checkmark-circled icon-checked"></strong>
                          <i class="icon-travel-photography"></i>
                        </span>
                        <p><?php echo Lang::get('common.travel-photoghraphy')?></p>
                      </label>
                    </li>
                    <li>
                      <input id="cat21" class="css-checkbox" type="checkbox" />
                      <label for="cat21" class="css-label">
                        <span class="img-circle u">
                          <strong class="ion-checkmark-circled icon-checked"></strong>
                          <i class="icon-travel-shopping"></i>
                        </span>
                        <p><?php echo Lang::get('common.travel-shopping')?></p>
                      </label>
                    </li>
                    <li>
                      <input id="cat22" class="css-checkbox" type="checkbox" />
                      <label for="cat22" class="css-label">
                        <span class="img-circle w">
                          <strong class="ion-checkmark-circled icon-checked"></strong>
                          <i class="icon-walking-and-trekking"></i>
                        </span>
                        <p><?php echo Lang::get('common.walking-trekking')?></p>
                      </label>
                    </li>
                    <li>
                      <input id="cat23" class="css-checkbox" type="checkbox" />
                      <label for="cat23" class="css-label">
                        <span class="img-circle x">
                          <strong class="ion-checkmark-circled icon-checked"></strong>
                          <i class="icon-wildlife-and-nature"></i>
                        </span>
                        <p><?php echo Lang::get('common.wildlife-nature')?></p>
                      </label>
                    </li>
                  </ul>

                </div>
              </div>    
            </div>

          </div>
        </div>
      </div>

      <div id="publications" class="page">
        <div class="page-container">
          <div class="pageheader">
            <div class="headercontent">
              <div class="section-container">
                <h2 class="title"><?php echo Lang::get('common.publish-trip')?></h2>
              </div>
            </div>
          </div>

          <div class="pagecontents">
            <div class="section">
              <div class="section-container">
                <div class="isotope-container">
                  @if(isset($storyData))
                  @foreach ($storyData as $story)
                  <div class="story-card">
                    <div class="actions">
                      <a href="#" class="iso-btn fav"><i class="ion-thumbsup"></i></a>
                      <a href="#" class="iso-btn dream"><i class="icon ion-android-favorite"></i></a>
                    </div>
                    <figure style="background-image: url(/assets/images/gallery/1.jpg);">&nbsp;</figure>
                    <div class="details">
                      <i class="glyphicon glyphicon-chevron-up"></i>
                      <h2><a href="#">{{ $story->title }} </a></h2>
                      <p>{{ File::get($story->filepath) }}</p>
                      <div class="credits">
                        <ul class="credits-itm list-unstyled list-inline">

                          <li></li>
                          <li></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  @endforeach
                  @else
                  No story published
                  {{HTML::link('narrate',  'Publish Now')}}
                  @endif  
                </div>


              </div>                     
            </div> 
          </div>  
        </div>
      </div>

      <div id="drafts" class="page">
        <div class="pageheader">
          <div class="headercontent">
            <div class="section-container">
              <h2 class="title"><?php echo Lang::get('common.drafts')?></h2>
            </div>
          </div>
        </div>

        <div class="pagecontents">
          <div class="section">
            <div class="section-container">
              <div class="isotope-container">
                @if(isset($storyData))
                @foreach ($storyData as $story)
                <div class="story-card">
                  <div class="actions">
                    <a href="#" class="iso-btn fav"><i class="icon-thumbsup"></i></a>
                    <a href="#" class="iso-btn dream"><i class="icon-bookmark2"></i></a>
                  </div>
                  <figure style="background-image: url(/assets/images/gallery/1.jpg);">&nbsp;</figure>
                  <div class="details">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                    <h2><a href="#">{{ $story->title }} </a></h2>
                    <p>{{ File::get($story->filepath) }}</p>
                    <div class="credits">
                      <ul class="credits-itm list-unstyled list-inline">

                        <li></li>
                        <li></li>
                      </ul>
                    </div>
                  </div>
                </div>
                @endforeach
                @else
                No story Drafted
                {{HTML::link('narrate',  'Publish Now')}}
                @endif  
              </div>


            </div>                     
          </div> 
        </div>  
      </div>

      <div id="dreamlist" class="page">
        <div class="pageheader">
          <div class="headercontent">
            <div class="section-container">
              <h2 class="title"><?php echo Lang::get('common.dreamlist')?></h2>
            </div>
          </div>
        </div>

        <div class="pagecontents">
          <div class="section">
            <div class="section-container">
              <div class="isotope-container">
                @if(isset($storyData))
                @foreach ($storyData as $story)
                <div class="story-card">
                  <div class="actions">
                    <a href="#" class="iso-btn fav"><i class="ion-thumbsup"></i></a>
                    <a href="#" class="iso-btn dream"><i class="icon ion-android-favorite"></i></a>
                  </div>
                  <figure style="background-image: url(/assets/images/gallery/1.jpg);">&nbsp;</figure>
                  <div class="details">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                    <h2><a href="#">{{ $story->title }} </a></h2>
                    <p>{{ File::get($story->filepath) }}</p>
                    <div class="credits">
                      <ul class="credits-itm list-unstyled list-inline">

                        <li></li>
                        <li></li>
                      </ul>
                    </div>
                  </div>
                </div>
                @endforeach
                @else
                No story Drafted
                {{HTML::link('narrate',  'Publish Now')}}
                @endif  
              </div>


            </div>                     
          </div> 
        </div>
      </div>

      <div id="overlay"></div>

    </div>
  </div>