<html>
<head>
<title>TripTroop</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700" rel="stylesheet" type="text/css">
<style type="text/css">
		body{ font-family: 'Roboto Condensed', sans-serif; font-size:16px; color:rgb(87, 87, 87); background:#DAD8D8;}
		.full-width{ margin:0px; padding:0px; width:100%; background:#fff;}

		.logos a{ font-size:25px; color:#999; text-decoration:none; }
		.Table_01{
			margin:5px auto;
			max-width:650px;

		
		}
		.Table_02{
			margin:0px auto;
			max-width:650px;


			

		}
		
		img{
			max-width:100%;

			border:0px;
		}
		.text-left{ text-align:left;}
		.text-center{ text-align:center;}
		.socialicon-wrap{ padding-right:15px; text-align:right; padding-top:5px;}
		.share{ font-size:20px; padding:10px 0px;}
		.welcome-msg{  padding:10px 0px 30px; font-size:25px; }
		.spacelr15{ padding-left:15px; padding-right:15px;}
		.space15{ padding:15px}
		.spaceb15{ padding-bottom:15px}
		.pinkTxt{ color:rgb(87, 87, 87);}
		.whiteTxt{ color:#fff;}

		.space30{ padding:30px;}
		.spaceLR30{ padding:0px 30px; }
		.spaceTB30{ padding-top:30px; padding-bottom:30px;  }
		.heading2{ font-size:18px; margin:0px; padding:0px; color:rgb(87, 87, 87); margin-bottom:5px;}
		.heading2 span{ display:inline-block; }
		.no-underline{ text-decoration:none;}
		.socialicon-wrap a{ margin:0px 2px;}
		.wish-table{ max-width:650px; margin:auto;}
		.white_bg{ background:#fff}
		.halfwhite_bg{ background:#efefef; border-top:1px solid #E7E7E7; border-bottom:1px solid #E7E7E7;}	
		.spaceB30{ padding-bottom:30px;}
		.welcome-box{ background:url(images/test.jpg) no-repeat 50% 100%;  margin:auto; padding:20px 0px 20px 0px; color:#fff; background-size:cover;  }	
		.welcome-box table{ color:#fff;}

.logo-table-container{ display:inline-block; margin:0px 0px 40px 0px}
.size30{ font-size:30px;}

.reset{ padding:30px 15px;}
		.reset a{ background:#0A9E01; display:inline-block; text-decoration:none; border-radius:3px; padding:8px 40px; color:#fff; font-size:18px;  }
		
		
.courve-box{ border-radius:10px; -moz-border-radius:10px; -webkit-border-radius:10px; margin:10px;}		

</style>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="font-family: 'Roboto Condensed', sans-serif;font-size: 16px;color: rgb(87, 87, 87);background: #DAD8D8;">
<table style="max-width:600px; margin:auto; border:1px solid #E7E7E7" border="0" cellpadding="0" cellspacing="0">
	<tr>
    	<td>


<!-- header start from here --> 
<table class="full-width header" border="0" cellpadding="0" cellspacing="0" style="margin: 0px;padding: 0px;width: 100%;background: #fff;">
	<tr>
    	<td>

<table class=" full-width" border="0" cellpadding="0" cellspacing="0" style="margin: 0px;padding: 0px;width: 100%;background: #fff;">
	<tr>
    	<td class="logos-social">

<table class="  full-width" border="0" cellpadding="0" cellspacing="0" style="margin: 0px;padding: 0px;width: 100%;background: #fff;">
	<tr>
    	<td>
			<a href="https://www.triptroop.me/" target="_blank" style="background:#0A9E01; width:50px; height:40px; display:inline-block; text-align:center; padding-top:10px; float:left;"><img src="https://www.triptroop.me/assets/images/logo_init.png" width="20" style="max-width: 100%;border: 0px;"></a>							
		</td>
        <td style="text-align:right; padding-right:10px;">
<a href="https://www.facebook.com/pages/TripTroop/1041831852498335?fref=ts" target="_blank"><img src="http://res.cloudinary.com/triptroop/image/upload/v1436594788/facebook_h3yccu.png" width="40" style="max-width: 100%;border: 0px;"></a>
<a href="https://twitter.com/triptroopme" target="_blank"><img src="http://res.cloudinary.com/triptroop/image/upload/v1436594788/twitter_uly2u4.png" width="40" style="max-width: 100%;border: 0px;"></a>

        </td>
    </tr>
</table>        

			        	
        </td>
		
    </tr>	
</table>
        
        </td>
    </tr>
</table> 
<!-- header start from here --> 



<table class="full-width" border="0" cellpadding="0" cellspacing="0" align="center" style="margin: 0px;padding: 0px;width: 100%;background: #fff;">
	<tr>
    	<td style="">



<div class="halfwhite_bg courve-box" style="background: #efefef;border-top: 1px solid #E7E7E7;border-bottom: 1px solid #E7E7E7;border-radius: 10px;-moz-border-radius: 10px;-webkit-border-radius: 10px;margin: 10px;">
<table id="Table_02" border="0" cellpadding="0" cellspacing="0">
	<tr>
    	<td class="welcome-msg  spacelr15" style="padding: 10px 0px 30px;font-size: 25px;padding-left: 15px;padding-right: 15px;">
        		<div>
                	Hello {{$name}},
                </div>
                
        </td>
    </tr>	
    
    
    <tr>
    	<td class="spacelr15" style="padding-left: 15px;padding-right: 15px;">
<p>You recently requested to reset your password. you can reset your password by following Reset button below. if you no longer need to reset your password, you can ignore this message and your password will not be reset </p>


</td>
    </tr>
    
    <tr>
        <td class="text-center reset" style="text-align: center;padding: 30px 15px;">
            <a href="{{Request::root()}}/activateRecover/{{$str1}}/{{$eid}}/{{$str2}}" style="background: #0A9E01;display: inline-block;text-decoration: none;border-radius: 3px;padding: 8px 40px;color: #fff;font-size: 18px;">Reset Password</a>
            
        </td>
    </tr>
    
    <tr>
    	<td class="text-center " style="text-align: center;">&nbsp;
				
        </td>
    </tr>
    
</table>

</div>



<div class="halfwhite_bg courve-box" style="background: #efefef;border-top: 1px solid #E7E7E7;border-bottom: 1px solid #E7E7E7;border-radius: 10px;-moz-border-radius: 10px;-webkit-border-radius: 10px;margin: 10px;">
<table class="Table_02" border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto;max-width: 650px;">
    <tr>
    	<td class="space15 spaceTB30" style="padding: 15px;padding-top: 30px;padding-bottom: 30px;">
        <h2 class="heading2" style="font-size: 18px;margin: 0px;padding: 0px;color: rgb(87, 87, 87);margin-bottom: 5px;"><span style="display: inline-block;">Help</span></h2>
        	 Reach out to us for even a minutest query at  <a class="pinkTxt" href="mailto:contact@triptroop.me" target="_blank" style="color: rgb(87, 87, 87);">contact@triptroop.me </a> and we will revert soon.
        <img src="http://res.cloudinary.com/triptroop/image/upload/v1436595432/transparent_xtd1lv.png" style="max-width: 100%;border: 0px;">
        </td>
    </tr>
    
</table>
</div>



       
        </td>
    </tr>
</table>
<div class="white_bg" style="background: #fff;">
<table class="Table_02" border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto;max-width: 650px;">
    <tr>
    	<td class="space15" style="padding: 15px;">
		Cheers - Team <a href="https://www.triptroop.me/" class="pinkTxt" style="color: rgb(87, 87, 87);">triptroop.me </a>   
         <img src="http://res.cloudinary.com/triptroop/image/upload/v1436595432/transparent_xtd1lv.png" style="max-width: 100%;border: 0px;">
        </td>
        
    </tr>
    
</table> 
</div>

        </td>
    </tr>
</table>




</body>
</html>