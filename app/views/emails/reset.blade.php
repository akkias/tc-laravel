<html>
  <head>
    <title>TripTroop</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  </head>
  <body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="background: #F0F0F0;font-family: &quot;Roboto Condensed&quot;, sans-serif;font-size: 16px">

<!-- 
<table class="full-width" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
        
        </td>
    </tr>
</table>
-->

<!-- header start from here --> 
<table class="full-width header" border="0" cellpadding="0" cellspacing="0" style="margin: 0;padding: 0;width: 100%;background: #fff;border-bottom: 5px solid #ed1768"><tr><td>

<table id="Table_01" border="0" cellpadding="0" cellspacing="0" style="margin: 5px auto;max-width: 650px"><tr><td class="logos">

                        <a href="https://www.triptroop.me/" target="_blank" style="font-size: 25px;color: #999;text-decoration: none"><img src="https://res.cloudinary.com/triptroop/image/upload/c_scale,w_100/v1430485007/assets/logo_pink.png" width="100" style="border: 0"/></a>
        </td>
        
    </tr></table></td>
    </tr></table><!-- header start from here --><table border="0" cellpadding="0" cellspacing="0" align="center"><tr><td style="">

<table id="Table_02" border="0" cellpadding="0" cellspacing="0" style="margin: 5px auto;max-width: 650px;border: 1px solid #ed1768;border-bottom-width: 5px;background: #fff">


    <tr>
        <td class="welcome-msg  spacelr15" style="background: #fff;padding: 30px 0px;font-size: 25px;color: #001136;padding-left: 15px;padding-right: 15px;">
                <div>
                    Hello {{$name}},
                </div>
                
        </td>
    </tr>   
    
    
    <tr>
        <td class="spacelr15" style="padding-left: 15px;padding-right: 15px;">

You recently requested to reset your password. you can reset your password by following Reset button below. if you no longer need to reset your password, you can ignore this message and your password will not be reset        
        </td>
    </tr>
    
    <tr>
        <td class="text-center reset" style="text-align: center;padding: 30px 15px;">
            <a href="#" style="background: #ed1768;display: inline-block;text-decoration: none;border-radius: 3px;padding: 8px 40px;color: #fff;font-size: 18px;">Reset Password</a>
        </td>
    </tr>

    
    <tr><td>
<table border="0" cellpadding="0" cellspacing="0" class="socil_icons" align="center"><tr><td align="center">
            <h2 class="heading2" style="font-size: 18px;margin: 0;padding: 0;color: #ed1768;margin-bottom: 5px">Follow Us On </h2>          
        </td>
    </tr><tr><td class="socialicon-wrap" style="padding-top: 5px">
            <a href="https://www.facebook.com/pages/TripTroop/1041831852498335?fref=ts" target="_blank"><img src="http://res.cloudinary.com/triptroop/image/upload/v1436594788/facebook_h3yccu.png" width="40" style="max-width: 100%;border: 0px;"></a>
<a href="https://twitter.com/triptroopme" target="_blank"><img src="http://res.cloudinary.com/triptroop/image/upload/v1436594788/twitter_uly2u4.png" width="40" style="max-width: 100%;border: 0px;"></a>
        </td>
    </tr></table></td>
    </tr><tr><td class="space15" style="padding: 15px">
        Cheers - Team <a href="https://www.triptroop.me/" class="pinkTxt" style="color: #ed1768">triptroop.me </a>   
        </td>
    </tr></table></td>
    </tr></table><table border="0" cellpadding="0" cellspacing="0" class="wish-table" style="max-width: 650px;margin: auto"><tr><td class="text-center share spacelr15" style="text-align: center;font-size: 20px;padding: 10px 0;padding-left: 15px;padding-right: 15px">
            Happy travelling and all the best!!!
        </td>
    </tr></table></body>
</html>
