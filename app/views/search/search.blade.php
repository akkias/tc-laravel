@extends("layouts.show_layout")
@section('content')
<section class="page-wrapper search-wrap">
    @include('/shared/header_new')
    <div class="searchbar-wrap"> 
        <form class="mar0 relative" id = "magicInputForm" autocomplete="off">
            <input id="magicInput1" type="text" class="form-control"  placeholder="Search TripTroop" name = 'searchText' onfocus="_gaq.push(['_trackEvent', 'User Entered string to Search', 'focused'])" />
            <input type="hidden" id="hiddenSort" name="sortValue"/>
            <input type="hidden" id="hiddenTheme" name="themeValue"/>
            <input type="hidden" id="hiddenCountry" name="countryValue"/>
            <button class="search-btn" type="submit"><i class="icon-search" onclick="_gaq.push(['_trackEvent', 'User Submit Entered Search String', 'clicked'])"></i></button>
        </form>
    </div>
    <div class="tt-content">
<div class="row">
    <div class="col-sm-9">
        <h2 class="capitalize"><?php echo Lang::get('common.stories')?> (<label id = "storyCount">0</label>)</h2>
        <div class="box-wrap clearfix">
            <div class="box-list1"></div>
          <!--   @if(isset($stories))
                <div class="box-list">
                <a href="#box link" class="box-link">
                    <div class="box-cover" style="background:url(assets/images/cover-1424529588.jpg) no-repeat center">
                        <div class="content">
                            <h3><a href="#h3">MISSION OF BERNAL</a></h3>
                            <p><a href="#p">2015 Ice Climbing season in the Alps</a></p>
                            <div class="author">
                                <b>By</b>
                                <a class="author-name" href="#">Sam Fu</a>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @endif -->
            <!-- <div class="box-list">
                <a href="#box link" class="box-link">
                    <div class="box-cover" style="background:url(assets/images/cover-1424529588.jpg) no-repeat center">
                        <div class="content">
                            <h3><a href="#h3">MISSION OF BERNAL</a></h3>
                            <p><a href="#p">2015 Ice Climbing season in the Alps</a></p>
                            <div class="author">
                                <b>By</b>
                                <a class="author-name" href="#">Sam Fu</a>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="box-list">
                <a href="#box link" class="box-link">
                    <div class="box-cover" style="background:url(assets/images/cover-1424529589.jpg) no-repeat center">
                        <div class="content">
                            <h3><a href="#h3">MISSION OF BERNAL</a></h3>
                            <p><a href="#p">2015 Ice Climbing season in the Alps</a></p>
                            <div class="author">
                                <b>By</b>
                                <a class="author-name" href="#">Sam Fu</a>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="box-list">
                <a href="#box link" class="box-link">
                    <div class="box-cover" style="background:url(assets/images/cover-1424529588.jpg) no-repeat center">
                        <div class="content">
                            <h3><a href="#h3">MISSION OF BERNAL</a></h3>
                            <p><a href="#p">2015 Ice Climbing season in the Alps</a></p>
                            <div class="author">
                                <b>By</b>
                                <a class="author-name" href="#">Sam Fu</a>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="box-list">
                <a href="#box link" class="box-link">
                    <div class="box-cover" style="background:url(assets/images/cover-1424529589.jpg) no-repeat center">
                        <div class="content">
                            <h3><a href="#h3">MISSION OF BERNAL</a></h3>
                            <p><a href="#p">2015 Ice Climbing season in the Alps</a></p>
                            <div class="author">
                                <b>By</b>
                                <a class="author-name" href="#">Sam Fu</a>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="box-list">
                <a href="#box link" class="box-link">
                    <div class="box-cover" style="background:url(assets/images/cover-1424529588.jpg) no-repeat center">
                        <div class="content">
                            <h3><a href="#h3">MISSION OF BERNAL</a></h3>
                            <p><a href="#p">2015 Ice Climbing season in the Alps</a></p>
                            <div class="author">
                                <b>By</b>
                                <a class="author-name" href="#">Sam Fu</a>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="box-list">
                <a href="#box link" class="box-link">
                    <div class="box-cover" style="background:url(assets/images/cover-1424529589.jpg) no-repeat center">
                        <div class="content">
                            <h3><a href="#h3">MISSION OF BERNAL</a></h3>
                            <p><a href="#p">2015 Ice Climbing season in the Alps</a></p>
                            <div class="author">
                                <b>By</b>
                                <a class="author-name" href="#">Sam Fu</a>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="box-list">
                <a href="#box link" class="box-link">
                    <div class="box-cover" style="background:url(assets/images/cover-1424529588.jpg) no-repeat center">
                        <div class="content">
                            <h3><a href="#h3">MISSION OF BERNAL</a></h3>
                            <p><a href="#p">2015 Ice Climbing season in the Alps</a></p>
                            <div class="author">
                                <b>By</b>
                                <a class="author-name" href="#">Sam Fu</a>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="box-list">
                <a href="#box link" class="box-link">
                    <div class="box-cover" style="background:url(assets/images/cover-1424529589.jpg) no-repeat center">
                        <div class="content">
                            <h3><a href="#h3">MISSION OF BERNAL</a></h3>
                            <p><a href="#p">2015 Ice Climbing season in the Alps</a></p>
                            <div class="author">
                                <b>By</b>
                                <a class="author-name" href="#">Sam Fu</a>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="box-list">
                <a href="#box link" class="box-link">
                    <div class="box-cover" style="background:url(assets/images/cover-1424529588.jpg) no-repeat center">
                        <div class="content">
                            <h3><a href="#h3">MISSION OF BERNAL</a></h3>
                            <p><a href="#p">2015 Ice Climbing season in the Alps</a></p>
                            <div class="author">
                                <b>By</b>
                                <a class="author-name" href="#">Sam Fu</a>
                            </div>
                        </div>
                    </div>
                </a>
            </div> -->

            <!-- <div class="load-more clearfix"><a href="#" class="btn btn-danger">See More Stories</a></div> -->
        </div>


    </div>
    <div class="col-sm-3">
        <h2><?php echo Lang::get('search.people')?> (<label id = "peopleCount">0</label>)</h2>
        <div class="media-list">
            <!-- <div class="media">
                <div class="avater-box">
                    <img src="assets/images/avater-img.jpg" class="img-circle" />
                </div>
                <div class="media-body">
                    <h4>Media Heading</h4>
                    <p>A seven-month quest hossme A seven-month quest home ...</p>
                    <a href="#" class="btn btn-follow">+ Follow</a>
                </div>
            </div>
            <div class="media">
                <div class="avater-box">
                    <img src="assets/images/avater-img.jpg" class="img-circle" />
                </div>
                <div class="media-body">
                    <h4>Media Heading</h4>
                    <p>A seven-month quest home A seven-month quest home ...</p>
                    <a href="#" class="btn btn-follow">+ Follow</a>
                </div>
            </div>
            <div class="media">
                <div class="avater-box">
                    <img src="assets/images/avater-img.jpg" class="img-circle" />
                </div>
                <div class="media-body">
                    <h4>Media Heading</h4>
                    <p>A seven-month quest home A seven-month quest home ...</p>
                    <a href="#" class="btn btn-follow">+ Follow</a>
                </div>
            </div>
            <div class="media">
                <div class="avater-box">
                    <img src="assets/images/avater-img.jpg" class="img-circle" />
                </div>
                <div class="media-body">
                    <h4>Media Heading</h4>
                    <p>A seven-month quest home A seven-month quest home ...</p>
                    <a href="#" class="btn btn-follow">+ Follow</a>
                </div>
            </div>
            <div class="media">
                <div class="avater-box">
                    <img src="assets/images/avater-img.jpg" class="img-circle" />
                </div>
                <div class="media-body">
                    <h4>Media Heading</h4>
                    <p>A seven-month quest home A seven-month quest home ...</p>
                    <a href="#" class="btn btn-follow">+ Follow</a>
                </div>
            </div><div class="media">
            <div class="avater-box">
                <img src="assets/images/avater-img.jpg" class="img-circle" />
            </div>
            <div class="media-body">
                <h4>Media Heading</h4>
                <p>A seven-month quest home A seven-month quest home ...</p>
                <a href="#" class="btn btn-follow">+ Follow</a>
            </div>
        </div>
        <div class="media">
        <div class="avater-box">
            <img src="assets/images/avater-img.jpg" class="img-circle" />
        </div>
        <div class="media-body">
            <h4>Media Heading</h4>
            <p>A seven-month quest home A seven-month quest home ...</p>
            <a href="#" class="btn btn-follow">+ Follow</a>
        </div>
    </div> -->

    <!-- <div class="load-more clearfix"><a href="#" class="btn btn-danger btn-block">See More People</a></div> -->
</div>

</div>

</div>
</div>
</div>
@stop