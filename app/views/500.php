<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>500 unexpected error</title>
    <link href="/assets/stylesheets/bootstrap.css" rel="stylesheet">
  </head>
  <body class="map-bg" onload="_gaq.push(['_trackEvent', 'Strikes 500', 'loaded'])">
  <div class="unexpected-error">
  		<h2>Ouch!</h2>
        <h2><?php echo Lang::get('error.look-like-something-wrong')?></h2>
        <img src="/assets/images/500_img.png" alt="500 image" />
   </div>
  </body>
</html>