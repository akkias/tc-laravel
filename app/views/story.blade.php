<?php $counter=1 ?>
@foreach ($data as $story)
<div class="story-card col-md-4 col-sm-6 col-xs-12">
  <div id="story{{ $story->storyid }}">
    <div class="author-data">
      <img height="30" style="background-color: #{{bin2hex(openssl_random_pseudo_bytes(3));}};" src="https://res.cloudinary.com/triptroop-prod/image/upload/w_60,h_60,c_fill/{{$story->clPath ? $story->clPath : $story->dpPath}}">
      {{HTML::link('/profile/'.$story->userid , $story->firstname,"",array('onclick' => '_gaq.push(["_trackEvent", "StoryTeller profile link clicked from storyCard", "clicked"])'))}}
      {{HTML::link('/profile/'.$story->userid , $story->lastname,"",array('onclick' => '_gaq.push(["_trackEvent", "StoryTeller profile link clicked from storyCard", "clicked"])'))}}
      @if($story->verified)
      <i data-toggle="tooltip" title="Verified Account" data-placement="top" class="fa fa-check-circle ver-icon"></i>
      @endif
      <div class="actions">
        @if (Auth::user() == false)
        <button class="non-logged-link btn fav"><i class="fa fa-heart-o block"></i></button>          
        <button class="non-logged-link btn dream"><i class="fa fa-bookmark-o block"></i></button>
        @else
        @if(DB::table('liked')->where('id', Auth::user()['id'])->where('likeTo', 'like', '%'.'->'.$story->storyid.'->'.'%')->count())
        <button id = "{{ 'like'.$story->storyid }}" onclick="likeClick({{ $story->storyid }});_gaq.push(['_trackEvent', 'Story added to Liked action done', 'clicked'])"  class="btn fav ed">
          <i class="fa fa-refresh fa-spin" style="display:none"></i>
          <i class="fa fa-heart block"></i>
        </button>          
        @else
        <button id = "{{ 'like'.$story->storyid }}" onclick="likeClick({{ $story->storyid }});_gaq.push(['_trackEvent', 'Story Like action performed', 'clicked'])"  class="btn fav">
          <i class="fa fa-refresh fa-spin" style="display:none"></i>
          <i class="fa fa-heart-o block"></i>
        </button>
        @endif
        @if(DB::table('wishlist')->where('id', Auth::user()['id'])->where('wishTo', 'like', '%'.'->'.$story->storyid.'->'.'%')->count())
        <button id = "{{ 'wish'.$story->storyid }}" onclick="wishClick({{ $story->storyid }});_gaq.push(['_trackEvent', 'Story added to wishlist action done', 'clicked'])" class="btn dream ed">
          <i class="fa fa-refresh fa-spin" style="display:none"></i>
          <i class="fa fa-bookmark block"></i>
        </button>
        @else
        <button id = "{{ 'wish'.$story->storyid }}" onclick="wishClick({{ $story->storyid }});_gaq.push(['_trackEvent', 'Story add to wishlist action performed', 'clicked'])" class="btn dream">
          <i class="fa fa-refresh fa-spin" style="display:none"></i>
          <i class="fa fa-bookmark-o block"></i>
        </button>       
        @endif
        @endif
      </div>
    </div>
    <div class="story-cover" style="background: url(https://res.cloudinary.com/triptroop-prod/image/upload/w_500,c_limit/{{$story->coverClPath}})"> 
      {{HTML::link('read/'.$story->storyid, "",array('class' => 'story-link'))}}
    </div>
    <div class="story-meta">
      <?php $likerslist = LikeModel::getLikingUsers($story->storyid)?>
      <h2 class="capitalize">{{HTML::link('read/'.$story->storyid, $story->title, "",array('onclick' => '_gaq.push(["_trackEvent", "Story link clicked from storyCard", "clicked"])'))}}</h2>
      <p>{{Str::limit($story->storyDescription, 100)}}</p>
    </div>
    <div class="footer clearfix">
      @if (Auth::user() == false)
      <span class="capitalize pull-right text-center likes like-count btn non-logged-link"><small><i class="fa fa-heart"></i></small> <span id="count-{{$story->storyid}}">{{$story->count}}</span> <?php echo Lang::get('common.likes') ?></span>
      @else
      <span data-toggle="modal" data-target="#likersModal" id = "{{ 'getLikers'.$story->storyid }}" onclick="getLikers({{ $story->storyid }});" class="btn pull-right capitalize text-center likes like-count"><small><i class="fa fa-heart"></i></small> <span id="count-{{$story->storyid}}">{{$story->count}}</span> <?php echo Lang::get('common.likes') ?></span>
      @endif
      <span class="hits pull-right"><small><i class="fa fa-eye"></i></small> {{$story->hits}} Views</span>
      <span class="date"><small><i class="fa fa-calendar"></i></small> {{ date('d-F-Y', strtotime($story->createdAt)) }}</span>
    </div>
  </div>
</div>
@endforeach