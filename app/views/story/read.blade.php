@extends("layouts.read_layout")
@section('content')
{{HTML::style('/bower_components/dante/dist/css/dante-editor.css', array(), false);}}
{{HTML::style('/assets/stylesheets/dante-overridden.css', array(), false);}}
@include('/shared/opaque_header')
<section class="post-wrapper page-wrapper">
	<section class="post-cover relative" style="background-image: url(https://res.cloudinary.com/triptroop/image/upload/w_1200/{{$readStory[0]->coverClPath}});">
		<div class="title-wrapper text-center">
			<div class="content">
				<div class="block">
					<h1 id="storyTitle">{{$readStory[0]->title}}</h1>
					@if ($readStory[0]->storyDescription)<p class="story-description">{{$readStory[0]->storyDescription}}</p>@endif
					<div class="author-info">
						<em>by</em> <a href="#">{{$readStory[0]->firstname}} {{$readStory[0]->lastname}}</a> <span>on {{ date('F d, Y', strtotime($readStory[0]->createdAt)) }}</span>
					</div>
				</div>
			</div>
		</div>
		<div class="actions text-center pull-right">
			@if (Auth::user() == false)
			<button class="non-logged-link iso-btn fav"><i class="fa fa-heart-o block"></i></button>          
			<button class="non-logged-link iso-btn dream"><i class="fa fa-bookmark-o block"></i></button>
			@else
			@if(DB::table('liked')->where('id', Auth::user()['id'])->where('likeTo', 'like', '%'.'->'.$readStory[0]->storyid.'->'.'%')->count())
			<button id = "{{ 'like'.$readStory[0]->storyid }}" onclick="likeClick({{ $readStory[0]->storyid }})"  class="iso-btn fav ed"><i class="fa fa-heart-o"></i></button>          
			@else
			<button id = "{{ 'like'.$readStory[0]->storyid }}" onclick="likeClick({{ $readStory[0]->storyid }})"  class="iso-btn fav"><i class="fa fa-heart-o"></i></button>
			@endif
			@if(DB::table('wishlist')->where('id', Auth::user()['id'])->where('wishTo', 'like', '%'.'->'.$readStory[0]->storyid.'->'.'%')->count())
			<button id = "{{ 'wish'.$readStory[0]->storyid }}" onclick="wishClick({{ $readStory[0]->storyid }})" class="iso-btn dream ed"><i class="fa fa-bookmark-o"></i></button>
			@else
			<button id = "{{ 'wish'.$readStory[0]->storyid }}" onclick="wishClick({{ $readStory[0]->storyid }})" class="iso-btn dream"><i class="fa fa-bookmark-o"></i></button>       
			@endif
			@endif
			<div class="shareStory" data-url="{{Request::url();}}" data-text="{{$readStory[0]->title}}"></div>
		</div>
		<a class="absolute is-scroll-to-content" href="#"><i class="fa fa-chevron-down"></i></a>
	</section>
	<div class="story-meta-bar text-center">
		<div class="author-info">
			<a href="/profile/{{$readStory[0]->id}}" class="hidden">
				<img class="hide img-circle" src="https://res.cloudinary.com/triptroop/image/upload/w_50,h_50,c_fill/{{$readStory[0]->clPath ? $readStory[0]->clPath : $readStory[0]->dpPath}}">
				<strong>{{$readStory[0]->firstname}} {{$readStory[0]->lastname}}</strong>
				@if($readStory[0]->verified)
				<i data-toggle="tooltip" data-placement="top" title="Tooltip on top" class="ion-checkmark-circled"></i>
				@endif
			</a>
			@if($readStory[0]->tripDate || $readStory[0]->expense)
			<ul class="list-unstyled list-inline info read-info ohidden">
				@if($readStory[0]->tripDate)
				<li class="calendar-li"><i class="icon-calendar align-middle"></i>Duration: {{$readStory[0]->tripDate}}</li>
				@endif
				@if($readStory[0]->expense)
				<li>Trip Cost: 
					@if ($readStory[0]->currency == 'INR')
					<i class="fa fa-rupee align-middle"></i>
					@elseif ($readStory[0]->currency == 'EUR')
					<i class="fa fa-euro align-middle"></i>
					@elseif ($readStory[0]->currency == 'JPN')
					<i class="fa fa-yen align-middle"></i>
					@else 
					<i class="fa fa-dollar align-middle"></i>
					@endif{{$readStory[0]->expense}}
				</li>
				@endif
			</ul>
			@endif
			<div class="hidden">
				
				<span>{{ date('F d, Y', strtotime($readStory[0]->createdAt)) }}</span>
				<i class="fa fa-circle"></i>
				@if (Auth::user() == false)
				<span class="capitalize text-center likes like-count non-logged-link">
					<small><i class="fa fa-heart"></i></small> 
					<span id="count-{{$readStory[0]->storyid}}">{{$readStory[0]->count}}</span> <?php echo Lang::get('common.likes') ?>
				</span>
				@else 
				<span data-toggle="modal" data-target="#likersModal" id = "{{ 'getLikers'.$readStory[0]->storyid}}" onclick="getLikers({{$readStory[0]->storyid}});" class="capitalize text-center likes like-count">
					<small><i class="fa fa-heart"></i></small> 
					<span id="count-{{$readStory[0]->storyid}}">{{$readStory[0]->count}}</span> <?php echo Lang::get('common.likes') ?>
				</span>
				@endif
				<i class="fa fa-circle"></i>
				<span class="hits"><small><i class="fa fa-eye"></i></small> {{$readStory[0]->hits}} Views</span>
			</div>
		</div>
	</div>
	<div class="post">
		<div class="postContent content readContent tt-content">
			{{$readStory[0]->filepath}}
		</div>
	</div>
	<footer class="clearfix">
		<div>
			<ul class="text-center relative cat list-unstyled list-inline">
				<li class="title block"><?php echo Lang::get('narrate-story.category-hashtags')?></li>
				@foreach ($categories as $category)
				<li class="ohidden">
					<a href="/themes/{{str_replace(' ', '-',DB::table('category_map')->where('ckey',$category)->pluck('cvalue'))}}">
						{{DB::table('category_map')->where('ckey',$category)->pluck('cvalue')}}</a>
					</li>
					@endforeach
				</ul>
			</div>
			<div class="narrator-info clearfix text-center">
				<div class="author relative">
					<a class="avatar block" href="/profile/{{$readStory[0]->id}}">
						<img class="img-circle img-responsive" src="https://res.cloudinary.com/triptroop/image/upload/w_100,h_100,c_fill/{{$readStory[0]->clPath ? $readStory[0]->clPath : $readStory[0]->dpPath}}">
					</a>
					<div class="meta">
						<a href="/profile/{{$readStory[0]->id}}" class="name">
							{{$readStory[0]->firstname}} {{$readStory[0]->lastname}}
							@if($readStory[0]->verified)
							<i data-toggle="tooltip" data-placement="top" title="Tooltip on top" class="ion-checkmark-circled"></i>
							@endif
						</a>
						<small class="block">NARRATED on {{$readStory[0]->createdAt}}</small>
						<p>{{$readStory[0]->description}}</p>
					</div>
				</div>
			</div>
		</footer>
	</section>
	@stop
