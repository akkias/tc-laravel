@extends("layouts.show_layout")
@section('content')
{{HTML::style('/bower_components/dante/dist/css/dante-editor.css', array(), false);}}
{{HTML::style('/assets/stylesheets/dante-overridden.css', array(), false);}}
@include('/shared/opaque_header')
<section class="crt-wrapper page-wrapper">
  <div id="thumbnail-fine-uploader"></div>
  <section class="post-cover" id="narrateCover" style="background-image: url(https://res.cloudinary.com/triptroop-prod/image/upload/w_1400/{{$readStory[0]->coverClPath}}); background-size:cover">
    <section class="title-wrapper text-center">
      <div class="content clearfix">
        <div class="block">
          <h1 max="65" id="storyTitle" class="contenteditable" contenteditable="true" data-placeholder="<?php echo Lang::get('narrate-story.name-your-story')?>">{{$readStory[0]->title}}</h1>
          <textarea maxlength="320" id="storyDescription" class="story-description" placeholder="Write a short introduction">{{$readStory[0]->storyDescription}}</textarea>
          <div class="author-info">
            <em>by</em> <a href="#">{{Auth::user()['firstname'] }} {{Auth::user()['lastname']}}</a> <span>on {{date('Y/m/d ')}}</span>
          </div>
        </div>
      </div>
    </section>
    <a class="absolute is-scroll-to-content" href="#"><i class="ion-ios-arrow-down"></i></a>
    <div class="upld-info-tip ohidden absolute">
      <form  id = "profileImageUploadForm">
        <div class="select-img relative">
          <button type="button" class="btn btn-primary">
            <i class="fa fa-picture-o"></i>Add cover image
          </button>
          <input type="file" id="photoimg" name="profilePic" multiple = "false" class="absolute">
        </div>
      </form>
    </div>
  </section>
  <section class="post relative">
    <section class="story clearfix">
      <div class="content clearfix">
        <form action = "saveStory" method = "post" id = "editorForm">
          <input type = "hidden" name = "editorData" id = "editorDataId"/>
          <input type = "hidden" name = "storyTitle" id = "storyTitleId"/>
          <input type = "hidden" name = "storyDescription" id = "storyDescriptionId"/>
          <input type = "hidden" name = "checks" id = "checksId"/>
          <input type = "hidden" name = "dateData" id = "dateId"/>
          <input type = "hidden" name = "expenseData" id = "expenseId"/>
          <input type = "hidden" name = "currency" id = "currencyId"/>
          <input type = "hidden" name = "publish" id = "publishId"/>
          <input type = "hidden" name = "save" id = "saveId"/>
          <div id="editor" class="clearfix">{{$readStory[0]->filepath}} </div>
        </form>
      </div>
    </section>
  </section>

  <section class="meta clearfix">
    <div class="post-meta">
      <div class="content">
        <h1 class="text-center">Trip details</h1>
        <div class="row">
          <div class="col-md-4">
            <label>When did you travel?</label>
            <div class="left-inner-addon">
              <i class="fa fa-calendar-plus-o"></i>
              <input class="form-control" class="daterangeinput" name="daterange" value = "{{$readStory[0]->tripDate}}"placeholder="Select duration of the trip" id="daterange"/>
            </div>
          </div>
          <div class="col-md-4">
            <label><?php echo Lang::get('narrate-story.total-expenditure')?></label>
            <select id = "currency" class="form-control" value = "{{$readStory[0]->currency}}">
              <option value = "USD" @if($readStory[0]->currency == 'USD')selected @endif>USD</option>
              <option value = "EUR" @if($readStory[0]->currency == 'EUR')selected @endif>EUR</option>
              <option value = "INR" @if($readStory[0]->currency == 'INR')selected @endif>INR</option>  
              <option value = "JPY" @if($readStory[0]->currency == 'JPY')selected @endif>JPY</option>  
            </select>
          </div>
          <div class="col-md-4">
            <label>&nbsp;</label>
            <div class="left-inner-addon">
              <i id="currencyIcon" class="fa fa-{{strtolower($readStory[0]->currency)}}"></i>
              <input type="number" value = "{{$readStory[0]->expense}}" data-parsley-trigger="change focusin focusout" data-parsley-type="digits" class="form-control" id = "expenditure" placeholder="Insert amount" />
            </div>
          </div>
          <div class="col-md-12 add-places">
            <label>Places Travelled</label>
            <div class="left-inner-addon">
              <i class="fa fa-map-o"></i>
              <input class="form-control" type="text" name="" value="">
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="content text-center">
      <h1>Classify your trip</h1>
      <ul class="categories list-unstyled clearfix list-inline text-left">
        <li>
          @if(DB::table('stories')->where('storyid', $readStory[0]->storyid)->where('category', 'like', '%->cat1->%')->count())
          <input id="cat1" class="css-checkbox" type="checkbox" checked />
          @else
          <input id="cat1" class="css-checkbox" type="checkbox" />
          @endif
          <label for="cat1"  class="css-label">
            <span class="img-circle a">
              <strong class="fa fa-check-circle icon-checked"></strong>
              <i class="cat-icon-adventure-travel"></i>
            </span>
            <p><?php echo Lang::get('common.advanture')?></p>
          </label>
        </li>
        <li>
          @if(DB::table('stories')->where('storyid', $readStory[0]->storyid)->where('category', 'like', '%->cat2->%')->count())
          <input id="cat2" class="css-checkbox" type="checkbox" checked />
          @else
          <input id="cat2" class="css-checkbox" type="checkbox" />
          @endif
          <label for="cat2" class="css-label">
            <span class="img-circle b">
              <strong class="fa fa-check-circle icon-checked"></strong>
              <i class="cat-icon-arts-and-liesure"></i>
            </span>
            <p><?php echo Lang::get('common.arts-lieasure')?></p>
          </label>
        </li>
        <li>
          @if(DB::table('stories')->where('storyid', $readStory[0]->storyid)->where('category', 'like', '%->cat3->%')->count())
          <input id="cat3" class="css-checkbox" type="checkbox" checked />
          @else
          <input id="cat3" class="css-checkbox" type="checkbox" />
          @endif
          <label for="cat3" class="css-label">
            <span class="img-circle c">
              <strong class="fa fa-check-circle icon-checked"></strong>
              <i class="cat-icon-backpacking"></i>
            </span>
            <p>Backpacking</p>
          </label>
        </li>
        <li>
          @if(DB::table('stories')->where('storyid', $readStory[0]->storyid)->where('category', 'like', '%->cat4->%')->count())
          <input id="cat4" class="css-checkbox" type="checkbox" checked />
          @else
          <input id="cat4" class="css-checkbox" type="checkbox" />
          @endif
          <label for="cat4" class="css-label">
            <span class="img-circle d">
              <strong class="fa fa-check-circle icon-checked"></strong>
              <i class="cat-icon-beaches"></i>
            </span>
            <p><?php echo Lang::get('common.beach')?></p>
          </label>
        </li>
        <li>
          @if(DB::table('stories')->where('storyid', $readStory[0]->storyid)->where('category', 'like', '%->cat5->%')->count())
          <input id="cat5" class="css-checkbox" type="checkbox" checked />
          @else
          <input id="cat5" class="css-checkbox" type="checkbox" />
          @endif
          <label for="cat5" class="css-label">
            <span class="img-circle e">
              <strong class="fa fa-check-circle icon-checked"></strong>
              <i class="cat-icon-budget-travel"></i>
            </span>
            <p><?php echo Lang::get('common.budget-traval')?></p>
          </label>
        </li>
        <li>
          @if(DB::table('stories')->where('storyid', $readStory[0]->storyid)->where('category', 'like', '%->cat6->%')->count())
          <input id="cat6" class="css-checkbox" type="checkbox" checked />
          @else
          <input id="cat6" class="css-checkbox" type="checkbox" />
          @endif
          <label for="cat6" class="css-label">
            <span class="img-circle f">
              <strong class="fa fa-check-circle icon-checked"></strong>
              <i class="cat-icon-coasts-and-islands"></i>
            </span>
            <p><?php echo Lang::get('common.coasts-islands')?></p>
          </label>
        </li>
        <li>
          @if(DB::table('stories')->where('storyid', $readStory[0]->storyid)->where('category', 'like', '%->cat7->%')->count())
          <input id="cat7" class="css-checkbox" type="checkbox" checked />
          @else
          <input id="cat7" class="css-checkbox" type="checkbox" />
          @endif
          <label for="cat7" class="css-label">
            <span class="img-circle g">
              <strong class="fa fa-check-circle icon-checked"></strong>
              <i class="cat-icon-diving-and-snorkelling"></i>
            </span>
            <p><?php echo Lang::get('common.diving-snorkelling')?></p>
          </label>
        </li>
        <li>
          @if(DB::table('stories')->where('storyid', $readStory[0]->storyid)->where('category', 'like', '%->cat8->%')->count())
          <input id="cat8" class="css-checkbox" type="checkbox" checked />
          @else
          <input id="cat8" class="css-checkbox" type="checkbox" />
          @endif
          <label for="cat8" class="css-label">
            <span class="img-circle h">
              <strong class="fa fa-check-circle icon-checked"></strong>
              <i class="cat-icon-ecotourism"></i>
            </span>
            <p><?php echo Lang::get('common.ecotourism')?></p>
          </label>
        </li>
        <li>
          @if(DB::table('stories')->where('storyid', $readStory[0]->storyid)->where('category', 'like', '%->cat9->%')->count())
          <input id="cat9" class="css-checkbox" type="checkbox" checked />
          @else
          <input id="cat9" class="css-checkbox" type="checkbox" />
          @endif
          <label for="cat9" class="css-label">
            <span class="img-circle i">
              <strong class="fa fa-check-circle icon-checked"></strong>
              <i class="cat-icon-family-travel"></i>
            </span>
            <p><?php echo Lang::get('common.family-travel')?></p>
          </label>
        </li>
        <li>
          @if(DB::table('stories')->where('storyid', $readStory[0]->storyid)->where('category', 'like', '%->cat10->%')->count())
          <input id="cat10" class="css-checkbox" type="checkbox" checked />
          @else
          <input id="cat10" class="css-checkbox" type="checkbox" />
          @endif
          <label for="cat10" class="css-label">
            <span class="img-circle j">
              <strong class="fa fa-check-circle icon-checked"></strong>
              <i class="cat-icon-festivals-and-events"></i>
            </span>
            <p><?php echo Lang::get('common.festival-events')?></p>
          </label>
        </li>
        <li>
          @if(DB::table('stories')->where('storyid', $readStory[0]->storyid)->where('category', 'like', '%->cat11->%')->count())
          <input id="cat11" class="css-checkbox" type="checkbox" checked />
          @else
          <input id="cat11" class="css-checkbox" type="checkbox" />
          @endif
          <label for="cat11" class="css-label">
            <span class="img-circle k">
              <strong class="fa fa-check-circle icon-checked"></strong>
              <i class="cat-icon-film-and-television"></i>
            </span>
            <p><?php echo Lang::get('common.film-television')?></p>
          </label>
        </li>
        <li>
          @if(DB::table('stories')->where('storyid', $readStory[0]->storyid)->where('category', 'like', '%->cat12->%')->count())
          <input id="cat12" class="css-checkbox" type="checkbox" checked />
          @else
          <input id="cat12" class="css-checkbox" type="checkbox" />
          @endif
          <label for="cat12" class="css-label">
            <span class="img-circle l">
              <strong class="fa fa-check-circle icon-checked"></strong>
              <i class="cat-icon-food-and-drink"></i>
            </span>
            <p><?php echo Lang::get('common.food-drink')?></p>
          </label>
        </li>
        <li>
          @if(DB::table('stories')->where('storyid', $readStory[0]->storyid)->where('category', 'like', '%->cat13->%')->count())
          <input id="cat13" class="css-checkbox" type="checkbox" checked />
          @else
          <input id="cat13" class="css-checkbox" type="checkbox" />
          @endif
          <label for="cat13" class="css-label">
            <span class="img-circle m">
              <strong class="fa fa-check-circle icon-checked"></strong>
              <i class="cat-icon-gear-and-tech"></i>
            </span>
            <p><?php echo Lang::get('common.gear-tech')?></p>
          </label>
        </li>
        <li>
          @if(DB::table('stories')->where('storyid', $readStory[0]->storyid)->where('category', 'like', '%->cat14->%')->count())
          <input id="cat14" class="css-checkbox" type="checkbox" checked />
          @else
          <input id="cat14" class="css-checkbox" type="checkbox" />
          @endif
          <label for="cat14" class="css-label">
            <span class="img-circle n">
              <strong class="fa fa-check-circle icon-checked"></strong>
              <i class="cat-icon-honeymoons-and-romance"></i>
            </span>
            <p><?php echo Lang::get('common.honymoon-romance')?></p>
          </label>
        </li>
        <li>
          @if(DB::table('stories')->where('storyid', $readStory[0]->storyid)->where('category', 'like', '%->cat15->%')->count())
          <input id="cat15" class="css-checkbox" type="checkbox" checked />
          @else
          <input id="cat15" class="css-checkbox" type="checkbox" />
          @endif
          <label for="cat15" class="css-label">
            <span class="img-circle o">
              <strong class="fa fa-check-circle icon-checked"></strong>
              <i class="cat-icon-luxury-travel"></i>
            </span>
            <p><?php echo Lang::get('common.luxury-travel')?></p>
          </label>
        </li>
        <li>
          @if(DB::table('stories')->where('storyid', $readStory[0]->storyid)->where('category', 'like', '%->cat16->%')->count())
          <input id="cat16" class="css-checkbox" type="checkbox" checked />
          @else
          <input id="cat16" class="css-checkbox" type="checkbox" />
          @endif
          <label for="cat16" class="css-label">
            <span class="img-circle p">
              <strong class="fa fa-check-circle icon-checked"></strong>
              <i class="cat-icon-music"></i>
            </span>
            <p><?php echo Lang::get('common.music')?></p>
          </label>
        </li>
        <li>
          @if(DB::table('stories')->where('storyid', $readStory[0]->storyid)->where('category', 'like', '%->cat17->%')->count())
          <input id="cat17" class="css-checkbox" type="checkbox" checked />
          @else
          <input id="cat17" class="css-checkbox" type="checkbox" />
          @endif
          <label for="cat17" class="css-label">
            <span class="img-circle q">
              <strong class="fa fa-check-circle icon-checked"></strong>
              <i class="cat-icon-off-the-beaten-track"></i>
            </span>
            <p><?php echo Lang::get('common.off-beaten-track')?></p>
          </label>
        </li>
        <li>
          @if(DB::table('stories')->where('storyid', $readStory[0]->storyid)->where('category', 'like', '%->cat18->%')->count())
          <input id="cat18" class="css-checkbox" type="checkbox" checked />
          @else
          <input id="cat18" class="css-checkbox" type="checkbox" />
          @endif
          <label for="cat18" class="css-label">
            <span class="img-circle r">
              <strong class="fa fa-check-circle icon-checked"></strong>
              <i class="cat-icon-planes-and-trains"></i>
            </span>
            <p><?php echo Lang::get('common.planes-trains')?></p>
          </label>
        </li>
        <li>
          @if(DB::table('stories')->where('storyid', $readStory[0]->storyid)->where('category', 'like', '%->cat19->%')->count())
          <input id="cat19" class="css-checkbox" type="checkbox" checked />
          @else
          <input id="cat19" class="css-checkbox" type="checkbox" />
          @endif
          <label for="cat19" class="css-label">
            <span class="img-circle s">
              <strong class="fa fa-check-circle icon-checked"></strong>
              <i class="cat-icon-road-trips"></i>
            </span>
            <p><?php echo Lang::get('common.road-trips')?></p>
          </label>
        </li>
        <li>
          @if(DB::table('stories')->where('storyid', $readStory[0]->storyid)->where('category', 'like', '%->cat20->%')->count())
          <input id="cat20" class="css-checkbox" type="checkbox" checked />
          @else
          <input id="cat20" class="css-checkbox" type="checkbox" />
          @endif
          <label for="cat20" class="css-label">
            <span class="img-circle t">
              <strong class="fa fa-check-circle icon-checked"></strong>
              <i class="cat-icon-round-the-world-travel"></i>
            </span>
            <p><?php echo Lang::get('common.round-world-travel')?></p>
          </label>
        </li>
        <li>
          @if(DB::table('stories')->where('storyid', $readStory[0]->storyid)->where('category', 'like', '%->cat21->%')->count())
          <input id="cat21" class="css-checkbox" type="checkbox" checked />
          @else
          <input id="cat21" class="css-checkbox" type="checkbox" />
          @endif
          <label for="cat21" class="css-label">
            <span class="img-circle u">
              <strong class="fa fa-check-circle icon-checked"></strong>
              <i class="cat-icon-travel-photography"></i>
            </span>
            <p><?php echo Lang::get('common.travel-photoghraphy')?></p>
          </label>
        </li>
        <li>
          @if(DB::table('stories')->where('storyid', $readStory[0]->storyid)->where('category', 'like', '%->cat22->%')->count())
          <input id="cat22" class="css-checkbox" type="checkbox" checked />
          @else
          <input id="cat22" class="css-checkbox" type="checkbox" />
          @endif
          <label for="cat22" class="css-label">
            <span class="img-circle v">
              <strong class="fa fa-check-circle icon-checked"></strong>
              <i class="cat-icon-travel-shopping"></i>
            </span>
            <p><?php echo Lang::get('common.travel-shopping')?></p>
          </label>
        </li>
        <li>
          @if(DB::table('stories')->where('storyid', $readStory[0]->storyid)->where('category', 'like', '%->cat23->%')->count())
          <input id="cat23" class="css-checkbox" type="checkbox" checked />
          @else
          <input id="cat23" class="css-checkbox" type="checkbox" />
          @endif
          <label for="cat23" class="css-label">
            <span class="img-circle w">
              <strong class="fa fa-check-circle icon-checked"></strong>
              <i class="cat-icon-walking-and-trekking"></i>
            </span>
            <p><?php echo Lang::get('common.walking-trekking')?></p>
          </label>
        </li>
        <li>
          @if(DB::table('stories')->where('storyid', $readStory[0]->storyid)->where('category', 'like', '%->cat24->%')->count())
          <input id="cat24" class="css-checkbox" type="checkbox" checked />
          @else
          <input id="cat24" class="css-checkbox" type="checkbox" />
          @endif
          <label for="cat24" class="css-label">
            <span class="img-circle x">
              <strong class="fa fa-check-circle icon-checked"></strong>
              <i class="cat-icon-wildlife-and-nature"></i>
            </span>
            <p><?php echo Lang::get('common.wildlife-nature')?></p>
          </label>
        </li>
        <li>
          @if(DB::table('stories')->where('storyid', $readStory[0]->storyid)->where('category', 'like', '%->cat25->%')->count())
          <input id="cat25" class="css-checkbox" type="checkbox" checked />
          @else
          <input id="cat25" class="css-checkbox" type="checkbox" />
          @endif
          <label for="cat25" class="css-label">
            <span class="img-circle y">
              <strong class="fa fa-check-circle icon-checked"></strong>
              <i class="cat-icon-solo"></i>
            </span>
            <p>Solo</p>
          </label>
        </li>
        <li>
          @if(DB::table('stories')->where('storyid', $readStory[0]->storyid)->where('category', 'like', '%->cat26->%')->count())
          <input id="cat26" class="css-checkbox" type="checkbox" checked />
          @else
          <input id="cat26" class="css-checkbox" type="checkbox" />
          @endif
          <label for="cat26" class="css-label">
            <span class="img-circle z">
              <strong class="fa fa-check-circle icon-checked"></strong>
              <i class="cat-icon-conference"></i>
            </span>
            <p>Conference</p>
          </label>
        </li>
      </ul>
<!--
      <h1>Add hashtags</h1>
      <input placeholder="Add a tag" name="tags" id="input-tags" value="foo,bar,baz" />
    -->
  </div>
</section>
</section>
@stop