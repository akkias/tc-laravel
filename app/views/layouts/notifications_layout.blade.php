<!doctype html>
<html lang="en">
<head>
	<meta name=viewport content="width=device-width, initial-scale=1">
	<meta name="Description" content="Triptroop uniquely provides Travel Story writting, Online Story Collaboration Platform, Follow StoryTellers and stay updated with new Travel inspiration. Share awesomeness with social media and Discover Travelling Inspirations" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="Keywords" content="Travelogues, Storytellers, Travel Blogs, Travel Stories" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<title>TripTroop - {{Lang::get('messages.home-travel-stories')}}</title>
	<link rel="shortcut icon" href="https://res.cloudinary.com/triptroop/image/upload/assets/favicon.png" />

	<meta property="og:title" content="TripTroop - {{Lang::get('messages.home-travel-stories')}}" />
	<meta property="og:description" content="Triptroop uniquely provides Travel Story writting, Online Story Collaboration Platform, Follow StoryTellers and stay updated with new Travel inspiration. Share awesomeness with social media and Discover Travelling Inspirations" />
	<meta property="og:image" content="{{Request::root()}}/assets/images/logo_final.jpg" />
	<meta property="og:url" content="{{Request::root()}}/" />
	<meta property="og:site_name" content="TripTroop" />
	<meta property="fb:app_id" content="1396027074044187" />
	<meta property="article:publisher" content="{{Request::root()}}/" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="https://www.triptroop.me/" />
	<meta property="og:image" content="{{Request::root()}}/assets/images/logo_final.jpg" />
	<meta property="og:image:type" content="image/jpeg" />
	@include('../shared/global_styles')
	@include('../shared/analyticstracking')
</head>
<body class="fadeIn animated">
	@if (Session::has('msg'))
	<div class="animated tt-toast alert alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{Session::get('msg')}}</div>
	@endif
	<div class="tt-loader">
		<img src="/assets/images/tt-loader.gif">
	</div>
	<div id="wrapper">
		@yield('notification_content')
		
		@if (Request::path() != 'narrate' && Request::path() != 'edit')
		<div class="compose hidden">
			<div class="tooltip left" role="tooltip">
				<div class="tooltip-arrow"></div>
				<div class="tooltip-inner">
					{{ Lang::get('common.narrate-story') }}
				</div>
			</div>
			@if (Auth::user() == false)
			<a href="#" onclick="_gaq.push(['_trackEvent', 'Compose link Actioned Redirect to Login', 'clicked'])" class="img-circle non-logged-link"><i class="fa fa-pencil"></i></a>
			@else
			<a href="{{url('/narrate')}}" onclick="_gaq.push(['_trackEvent', 'Compose link Actioned Redirect to Narrate', 'clicked'])" class="img-circle"><i class="fa fa-pencil"></i></a>
			@endif
		</div>
		@endif
		@if (Auth::user() == false)
		@include('/shared/login_dialog')
		@endif
		@if (Request::path() != 'narrate' && Request::path() != 'edit' && Request::path() != 'map')
		@include('/shared/footer')
		@endif
		@include('/blades/confirm-account')	
		@include('/shared/likers_modal')
		@include('shared/global_scripts')
	</div>
</body>
</html>
