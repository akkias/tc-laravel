<!doctype html>
<html lang="en">
<head>
    <meta name=viewport content="width=device-width, initial-scale=1">
    <meta name="Description" content="Triptroop uniquely provides Travel Story writting, Online Story Collaboration Platform, Follow StoryTellers and stay updated with new Travel inspiration. Share awesomeness with social media and Discover Travelling Inspirations" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="Keywords" content="Travelogues, Storytellers, Travel Blogs, Travel Stories" />
    <title>TripTroop - {{Lang::get('messages.home-travel-stories')}}</title>
    <link rel="shortcut icon" href="https://res.cloudinary.com/triptroop/image/upload/assets/favicon.png" />
    @include('../shared/global_styles')
    @include('../shared/analyticstracking')
</head>
<body class="guest-body fadeIn animated">
    <div class="bg"></div>
    <div class="tt-loader">
        <img src="/assets/images/tt-loader.gif">
    </div>
    @yield('content')
    @include('shared/global_scripts') 
    @if (Request::path() == 'login')
    <script data-cfasync="false" type="text/javascript">
        $("#loginForm").submit(function(event){
            event.preventDefault();
            $.ajax({
                url: "login",
                type: "POST",
                data:  new FormData(this),
                contentType: false,
                cache: true,
                processData:false,
                success: function(data){
                    if(data == 1){
                        window.location =  "{{URL::To('/')}}";
                    }else if(data == 2){
                        $('body').append('<div class="err alert alert-danger alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Please activate your account. Account activation email has been sent to your provided email address.</div>');
                    }
                    else if(data == 3){
                        $('body').append('<div class="err alert alert-danger alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Invalid credentials</div>');
                    }
                    else if(data == 5){
                        window.location =  "{{URL::To('start')}}";
                    }
                    else{
                        alert(data) ;
                    }
                },
                error: function(){
                    alert('Error occured');   
                }           
            });
});
$( document ).ajaxStart(function() {
    $( ".tt-loader" ).show();
}).ajaxComplete(function() {
    $( ".tt-loader" ).hide();
});
</script>
@elseif (Request::path() == 'sign-up')
<script data-cfasync="false" type="text/javascript">
    $("#signupForm").submit(function(event){
        event.preventDefault();
        $.ajax({
            url: "sign-up",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: true,
            processData:false,
            success: function(data){
                console.log(data)
                if(data == 1){
                    $('body').append('<div class="err alert alert-danger alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>User with provided email already exists. Click "Sign In"</div>');
                }
                else if(data == 2){
                    $('body').append('<div class="err alert alert-info alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Account activation email has been sent to your provided email address.</div>');
                }
                else if(data == 3){
                    $('body').append('<div class="err alert alert-danger alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Operation failed. Please try again</div>');
                }
                else{
                    alert('exception');
                }
            },
            error: function(){}           
        });
});
</script>
@endif

@if (Request::path() == 'recover-password')
<script data-cfasync="false" type="text/javascript">
    $(document).ready(function(event){
        $("#recoverForm").submit(function(event){
            $('.tt-loader').show();
            event.preventDefault();
            $.ajax({
                url: "recoverPassword",
                type: "POST",
                data:  new FormData(this),
                contentType: false,
                cache: true,
                processData:false,
                success: function(data){
                    $('.tt-loader').hide();
                    if(data == '1'){
                        $('body').append('<div class="err alert alert-info alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Please check your email for instruction</div>');
                        setTimeout(function(){
                            window.location = "{{Request::root()}}/login";     
                        }, 5000);
                    }
                    else{
                     $('body').append('<div class="err alert alert-danger alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data+'</div>');
                 }
             },
             error: function(){
                $('.tt-loader').hide();
                alert('Error occured. please try again');   
            }           
        });
})
});
</script>
@endif

@if (Request::is('activateRecover','activateRecover/*'))
<script data-cfasync="false" type="text/javascript">
    $(document).ready(function(event){
        $("#activateRecoverForm").submit(function(event){
            $('.tt-loader').show();
            event.preventDefault();
            $.ajax({
                url: "/confirmRecoverPassword",
                type: "POST",
                data:  new FormData(this),
                contentType: false,
                cache: true,
                processData:false,
                success: function(data){
                    $('.tt-loader').hide();
                    if(data == 1){
                       $('body').append('<div class="err alert alert-success alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Recover password mail has been sent to your email id. Click the link and proceed</div>');
                       setTimeout(function(){
                        window.location = "{{Request::root()}}/login";     
                    }, 5000);
                   }
                   else if(data == 2){
                       $('body').append('<div class="err alert alert-danger alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>New password and confirm new password are not same. Please try again!</div>');
                  // window.location = "http://localhost:8000/login"; 

              }
              else if(data == 3)
              {
               $('body').append('<div class="err alert alert-danger alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>This email address is not registered with us. Please register.</div>');
           }

       },
       error: function(error){
        $('.tt-loader').hide();
        console.log(error); 
        alert('Error occured. please try again');   
    }           
});
})
});
</script>
@endif




</body>
</html>
