<!doctype html>
<html lang="en">
<head>
	<meta name=viewport content="width=device-width, initial-scale=1">
	<title>{{$readStory[0]->title}}</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="Keywords" content="Travelogues, Storytellers, Travel Blogs, Travel Stories" />
	<meta name="Description" content="Triptroop uniquely provides Travel Story writting, Online Story Collaboration Platform, Follow StoryTellers and stay updated with new Travel inspiration. Share awesomeness with social media and Discover Travelling Inspirations" />
	
	<meta property="og:title" content="{{$readStory[0]->title}}" />
	<meta property="og:description" content="Triptroop uniquely provides Travel Story writting, Online Story Collaboration Platform, Follow StoryTellers and stay updated with new Travel inspiration. Share awesomeness with social media and Discover Travelling Inspirations" />
	<meta property="og:image" content="https://res.cloudinary.com/triptroop-prod/image/upload/w_1200/{{$readStory[0]->coverClPath}}.jpg" />
	<meta property="og:url" content="{{Request::root()}}/{{Request::path()}}" />
	<meta property="og:site_name" content="TripTroop" />
	<meta property="fb:app_id" content="1396027074044187" />
	<meta property="article:publisher" content="{{Request::root()}}" />
	<meta property="article:author" content="{{Request::root()}}/profile/{{$readStory[0]->id}}">



	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@triptroopme">
	<meta name="twitter:title" content="{{$readStory[0]->title}}">
	<meta name="twitter:description" content="Triptroop uniquely provides Travel Story writting, Online Story Collaboration Platform, Follow StoryTellers and stay updated with new Travel inspiration. Share awesomeness with social media and Discover Travelling Inspirations" />
	<meta name="twitter:image" content="https://res.cloudinary.com/triptroop-prod/image/upload/w_1200/{{$readStory[0]->coverClPath}}.jpg">




	<link rel="shortcut icon" href="https://res.cloudinary.com/triptroop/image/upload/assets/favicon.png" />
	@include('../shared/global_styles')
	@include('../shared/analyticstracking')
</head>
<body>	
	<div id="wrapper">
		@yield('content')
		<div class="compose">
			
			@if (Request::path() != 'narrate' && Request::path() != 'edit')
			<div class="tooltip left hidden" role="tooltip">
				<div class="tooltip-arrow"></div>
				<div class="tooltip-inner">
					{{ Lang::get('common.narrate-story') }}
				</div>
			</div>
			@if (Auth::user() == false)
			<a href="#" onclick="_gaq.push(['_trackEvent', 'Compose link Actioned Redirect to Login', 'clicked'])" class="hidden img-circle non-logged-link"><i class="fa fa-pencil"></i></a>
			@else
			<a href="{{url('/narrate')}}" onclick="_gaq.push(['_trackEvent', 'Compose link Actioned Redirect to Narrate', 'clicked'])" class="hidden img-circle"><i class="fa fa-pencil"></i></a>
			@endif
			@endif
		</div>
		<input type = "hidden" id = "readTitle" value = "{{Session::get('readTitle')}}"  />

		@if (Auth::user() == false)
		@include('/shared/login_dialog')
		@endif
		@include('/shared/footer')
		@include('shared/global_scripts')
		@include('/shared/likers_modal')
	</div>
	@if(Request::exists('published'))
		<div class="modal animated lightSpeedIn text-center" id="shareStoryModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" aria-label="Close">&times;</button>
					<div class="modal-body">
						<h3>Thank You!</h3>
						<p>Your story has been published on TripTroop</p>
						<div>
							<ul class="list-unstyled broadcast-story clearfix">
								<li class="capitalize pull-left">Share your story</li>
								<li class="pull-left shareStory" data-url="{{Request::url();}}" data-text="{{$readStory[0]->title}}"></li>
							</ul>	
						</div>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<script>
			$(document).ready(function(){
				$('#shareStoryModal').modal({backdrop: 'static', keyboard: false});
			});
		</script>
	@endif
	<script type="text/javascript">
		window.location.hash = document.getElementById('readTitle').value;
	</script>
</body>
</html>
