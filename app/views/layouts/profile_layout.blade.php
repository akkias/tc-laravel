<!doctype html>
<html lang="en">
<head>
	<meta name="Description" content="Triptroop uniquely provides Travel Story writting, Online Story Collaboration Platform, Follow StoryTellers and stay updated with new Travel inspiration. Share awesomeness with social media and Discover Travelling Inspirations" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>TripTroop - {{Lang::get('messages.home-travel-stories')}}</title>
	<link rel="shortcut icon" href="/assets/favicon.ico">
    @include('../shared/global_styles')
</head>
<body onload="_gaq.push(['_trackEvent', 'On Profile View', 'loaded'])">
	@include('shared/analyticstracking')
	@yield('content')
	@include('shared/global_scripts') 
	</body>
</html>
