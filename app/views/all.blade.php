@extends("layouts.show_layout")
@section('content')
<div class="page-wrapper isotope-wrapper">
	@include('/shared/header_new')
	<section class="container-fluid pad0">
		<div class="tt-content pad0 clearfix">
			<section class="col-md-12 isotope-container landing-wrapper loggedIn">
				<div class="scroller">
					<div class="story-row">
						@include('./story')
					</div>
					{{ $data->links() }}
				</div>
			</section>
		</div>
		@include('/shared/likers_modal')
	</section>
</div>
@stop