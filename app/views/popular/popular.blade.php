@extends("layouts.show_layout")
@section('content')
<section class="page-wrapper popular-wrap">
	@include('/shared/header_new')
	<header class="tt-header">
		<div class="header-meta">
			<h1><?php echo Lang::get('common.popular-stories')?></h1>
			<h2>{{DB::table('stories')->join('users', 'stories.userid', '=', 'users.id')->where('stories.count', '>', '10')->count()}} <?php echo Lang::get('common.stories')?></h2>
		</diV>
	</header>
	<div class="container-fluid pad0">
		<div class="tt-content clearfix">
			<section class="col-md-12 isotope-container">
				@if (DB::table('stories')->join('users', 'stories.userid', '=', 'users.id')->where('stories.count', '>', '10')->count() > 0)
				<ul class="list-unstyled">
					@include('./story')
				</ul>
				@else
				<div class="not-available">
					<img height="200" src="http://res.cloudinary.com/triptroop/image/upload/assets/no-story.jpg">
					<br><br><br>
					<h2><?php echo Lang::get('popular.no-popular-story-message')?></h2>
					<h3><?php echo Lang::get('popular.first-one-narrate')?> <span><?php echo Lang::get('common.popular-story')?></span></h3>
					<p><a class="btn btn-primary" href="{{URL::to('/narrate')}}" onclick="_gaq.push(['_trackEvent', 'Narrate Stories from Popular stories', 'clicked'])"><i class="fa fa-pencil align-middle"></i> <?php echo Lang::get('common.narrate-story')?></a></p>
				</div>
				@endif
			</section>
		</div>
	</div>
</section>
@stop