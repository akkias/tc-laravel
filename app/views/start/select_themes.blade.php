<script data-cfasync="false">
function checkCategory(){
  //alert('here');
     var categories = "";
     var check = 0;
      for(var i = 1;  i <= 23;  i++){
        if($('#cat' + i).is(":checked")){
          check = 1;
          categories += ('->'+('cat' + i));
        }
      }
      categories += '->';
     // alert(categories);
      if(check){
        return categories;  
      }
      else{
        return 0;
      }
  }
 $(document).ready(function() {
    $("#categoryForm").submit(function(event){
      event.preventDefault();
       $.ajax({
            url: "saveCategories",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: true,
            processData:false,
            success: function(data){
            if(data == 1){
                    window.location =  "{{URL::To('start')}}";
                }
                else{
                    alert(data) ;
                }
            },
            error: function(){}           
            });
    });


});
    function saveCategory(){
        var cats = checkCategory();
       // alert(cats);        
        $("#categories").val(cats);
        alert($("#categories").val());
        $("#categoryForm").submit();
    }
    

</script>


@extends('../layouts/non-login_layout')
<div class="start-wrapper">
<!-- Header section :: starts -->
    <div class="container-fluid">
      <header>
        <nav class="navbar navbar-inverse navbar-fixed-top">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="text-center navbar-brand" href="#">TRIPTROOP</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">

                <ul class="nav navbar-nav navbar-left">
                  <li class="active"><a>Steps 2 of 3</a></li>
              </div>
            </div>
          </nav>
        </header>
    </div>
<!-- Header section :: ends -->


<!-- Contain:: start -->
    <div class="container-fluid">
        <!-- step ::1  -->
          <div class="steps-wrapper">
            <form id = "categoryForm">
              <input type = "hidden" name = "cats" id = "categories" value = ""/>
            <h1>What are you intrested in? <button  onclick = "saveCategory();_gaq.push(['_trackEvent', 'Getting Started selected Intrested Categories continue for Customize profile', 'clicked'])" class="btn btn-primary pull-right">Continue</button></h1>
            </form>
            <p>Click to choose options below and we'll sugeest some good stuff for you.</p>
             <div class="row">
              <div class="col-md-12"></div>
            </div>
              <ul class="categories list-unstyled clearfix list-inline text-left">
        <li>
          <input id="cat1" class="css-checkbox" type="checkbox" />
          <label for="cat1"  class="css-label">
            <span class="img-circle a">
              <strong class="ion-checkmark-circled icon-checked"></strong>
              <i class="icon-adventure-travel"></i>
            </span>
            <p>Adventure</p>
          </label>
        </li>
        <li>
          <input id="cat2" class="css-checkbox" type="checkbox" />
          <label for="cat2" class="css-label">
            <span class="img-circle b">
              <strong class="ion-checkmark-circled icon-checked"></strong>
              <i class="icon-arts-and-liesure"></i>
            </span>
            <p>Arts and Liesure</p>
          </label>
        </li>
        <li>
          <input id="cat3" class="css-checkbox" type="checkbox" />
          <label for="cat3" class="css-label">
            <span class="img-circle c">
              <strong class="ion-checkmark-circled icon-checked"></strong>
              <i class="icon-beaches"></i>
            </span>
            <p>Beaches</p>
          </label>
        </li>
        <li>
          <input id="cat4" class="css-checkbox" type="checkbox" />
          <label for="cat4" class="css-label">
            <span class="img-circle d">
              <strong class="ion-checkmark-circled icon-checked"></strong>
              <i class="icon-budget-travel"></i>
            </span>
            <p>Budget Travel</p>
          </label>
        </li>
        <li>
          <input id="cat5" class="css-checkbox" type="checkbox" />
          <label for="cat5" class="css-label">
            <span class="img-circle e">
              <strong class="ion-checkmark-circled icon-checked"></strong>
              <i class="icon-coasts-and-islands"></i>
            </span>
            <p>Coasts and Islands</p>
          </label>
        </li>
        <li>
          <input id="cat6" class="css-checkbox" type="checkbox" />
          <label for="cat6" class="css-label">
            <span class="img-circle f">
              <strong class="ion-checkmark-circled icon-checked"></strong>
              <i class="icon-diving-and-snorkelling"></i>
            </span>
            <p>Diving and Snorkelling</p>
          </label>
        </li>
        <li>
          <input id="cat7" class="css-checkbox" type="checkbox" />
          <label for="cat7" class="css-label">
            <span class="img-circle g">
              <strong class="ion-checkmark-circled icon-checked"></strong>
              <i class="icon-ecotourism"></i>
            </span>
            <p>Ecotourism</p>
          </label>
        </li>
        <li>
          <input id="cat8" class="css-checkbox" type="checkbox" />
          <label for="cat8" class="css-label">
            <span class="img-circle h">
              <strong class="ion-checkmark-circled icon-checked"></strong>
              <i class="icon-family-travel"></i>
            </span>
            <p>Family Travel</p>
          </label>
        </li>
        <li>
          <input id="cat9" class="css-checkbox" type="checkbox" />
          <label for="cat9" class="css-label">
            <span class="img-circle i">
              <strong class="ion-checkmark-circled icon-checked"></strong>
              <i class="icon-festivals-and-events"></i>
            </span>
            <p>Festival and Events</p>
          </label>
        </li>
        <li>
          <input id="cat10" class="css-checkbox" type="checkbox" />
          <label for="cat10" class="css-label">
            <span class="img-circle j">
              <strong class="ion-checkmark-circled icon-checked"></strong>
              <i class="icon-film-and-television"></i>
            </span>
            <p>Film and Television</p>
          </label>
        </li>
        <li>
          <input id="cat11" class="css-checkbox" type="checkbox" />
          <label for="cat11" class="css-label">
            <span class="img-circle k">
              <strong class="ion-checkmark-circled icon-checked"></strong>
              <i class="icon-food-and-drink"></i>
            </span>
            <p>Food and Drink</p>
          </label>
        </li>
        <li>
          <input id="cat12" class="css-checkbox" type="checkbox" />
          <label for="cat12" class="css-label">
            <span class="img-circle l">
              <strong class="ion-checkmark-circled icon-checked"></strong>
              <i class="icon-gear-and-tech"></i>
            </span>
            <p>Gear and Tech</p>
          </label>
        </li>
        <li>
          <input id="cat13" class="css-checkbox" type="checkbox" />
          <label for="cat13" class="css-label">
            <span class="img-circle m">
              <strong class="ion-checkmark-circled icon-checked"></strong>
              <i class="icon-honeymoons-and-romance"></i>
            </span>
            <p>Honeymoon and Romance</p>
          </label>
        </li>
        <li>
          <input id="cat14" class="css-checkbox" type="checkbox" />
          <label for="cat14" class="css-label">
            <span class="img-circle n">
              <strong class="ion-checkmark-circled icon-checked"></strong>
              <i class="icon-luxury-travel"></i>
            </span>
            <p>Luxury Travel</p>
          </label>
        </li>
        <li>
          <input id="cat15" class="css-checkbox" type="checkbox" />
          <label for="Music" class="css-label">
            <span class="img-circle o">
              <strong class="ion-checkmark-circled icon-checked"></strong>
              <i class="icon-music"></i>
            </span>
            <p>Music</p>
          </label>
        </li>
        <li>
          <input id="cat16" class="css-checkbox" type="checkbox" />
          <label for="cat16" class="css-label">
            <span class="img-circle p">
              <strong class="ion-checkmark-circled icon-checked"></strong>
              <i class="icon-off-the-beaten-track"></i>
            </span>
            <p>Off the Beaten Track</p>
          </label>
        </li>
        <li>
          <input id="cat17" class="css-checkbox" type="checkbox" />
          <label for="cat17" class="css-label">
            <span class="img-circle q">
              <strong class="ion-checkmark-circled icon-checked"></strong>
              <i class="icon-planes-and-trains"></i>
            </span>
            <p>planes and Trains</p>
          </label>
        </li>
        <li>
          <input id="cat18" class="css-checkbox" type="checkbox" />
          <label for="cat18" class="css-label">
            <span class="img-circle r">
              <strong class="ion-checkmark-circled icon-checked"></strong>
              <i class="icon-road-trips"></i>
            </span>
            <p>Road Trips</p>
          </label>
        </li>
        <li>
          <input id="cat19" class="css-checkbox" type="checkbox" />
          <label for="cat19" class="css-label">
            <span class="img-circle s">
              <strong class="ion-checkmark-circled icon-checked"></strong>
              <i class="icon-round-the-world-travel"></i>
            </span>
            <p>Round the world Travel</p>
          </label>
        </li>
        <li>
          <input id="cat20" class="css-checkbox" type="checkbox" />
          <label for="cat20" class="css-label">
            <span class="img-circle t">
              <strong class="ion-checkmark-circled icon-checked"></strong>
              <i class="icon-travel-photography"></i>
            </span>
            <p>Travel Photograhpy</p>
          </label>
        </li>
        <li>
          <input id="cat21" class="css-checkbox" type="checkbox" />
          <label for="cat21" class="css-label">
            <span class="img-circle u">
              <strong class="ion-checkmark-circled icon-checked"></strong>
              <i class="icon-travel-shopping"></i>
            </span>
            <p>Travel Shopping</p>
          </label>
        </li>
        <li>
          <input id="cat22" class="css-checkbox" type="checkbox" />
          <label for="cat22" class="css-label">
            <span class="img-circle w">
              <strong class="ion-checkmark-circled icon-checked"></strong>
              <i class="icon-walking-and-trekking"></i>
            </span>
            <p>Walking and Trekking</p>
          </label>
        </li>
        <li>
          <input id="cat23" class="css-checkbox" type="checkbox" />
          <label for="cat23" class="css-label">
            <span class="img-circle x">
              <strong class="ion-checkmark-circled icon-checked"></strong>
              <i class="icon-wildlife-and-nature"></i>
            </span>
            <p>Wlid life and Nature</p>
          </label>
        </li>
      </ul>

          </div>
        <!-- step ::1  -->
        <div>

        </div>
    </div>
<!-- Contain:: end -->


</div>