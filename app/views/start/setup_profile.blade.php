@extends('../layouts/non-login_layout')
<div class="start-wrapper">
<!-- Header section :: starts -->
    <div class="container-fluid">
      <header>
        <nav class="navbar navbar-inverse navbar-fixed-top">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="text-center navbar-brand" href="#">TRIPTROOP</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">

                <ul class="nav navbar-nav navbar-left">
                  <li class="active"><a>Steps 3 of 3</a></li>
              </div>
            </div>
          </nav>
        </header>
    </div>
<!-- Header section :: ends -->


<!-- Contain:: start -->
    <div class="container-fluid">
        <!-- step ::1  -->
          <div class="steps-wrapper">
            <h1>Customize your profile. <a href="{{url('start/import')}}" onclick="_gaq.push(['_trackEvent', 'Getting Started clicked Customized Profile and continue for landing page', 'clicked'])" class="btn btn-primary pull-right">Continue</a></h1>
            
            <p>Add a photo to show your unique personality and style.</p>
             <div class="row">
              <div class="col-md-12">
                  <div id="profile" class="clearfix">
                    <div class="portrate hidden-xs">
                       <div class="userAvtar-container">
                          <table class="userAvtar" data-intro="Change User Avarar here" data-steps="1">
                            <tr>
                              <td>
                                <img src="/assets/images/profile/user-avtar-01.jpg" alt="change profile" />
                              </td>
                            </tr>
                          </table>
                          <span class="btn-file">
                            <span class="ion-edit"></span>
                            <span class="fileinput-new"></span>
                            <!--span class="fileinput-exists">Change</span -->
                            <input type="file" name="...">
                          </span>
                          <!-- <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput" data-no-turbolink="true">Remove</a> -->
                       </div>
                    </div>
                    <div class="title">
                      <h2>
                        <span style="color:#ccc" contenteditable="true">Harish </span>
                        @if (isset($data))
                        {{          $data->firstname }}
                        @else
                        {{Auth::user()['firstname']}}
                        @endif

                      </h2>
                      <h3> <i class="ion-android-pin v-mid "></i> <span>Pune, India</span></h3>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        <!-- step ::1  -->
        <div>

        </div>
    </div>
<!-- Contain:: end -->


</div>
