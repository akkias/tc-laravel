@extends('../layouts/non-login_layout')
<div class="start-wrapper">
<!-- Header section :: starts -->
    <div class="container-fluid">
      <header>
        <nav class="navbar navbar-inverse navbar-fixed-top">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="text-center navbar-brand" href="#">TRIPTROOP</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">

                <ul class="nav navbar-nav navbar-left">
                  <li class="active"><a>Steps 3 of 3</a></li>
              </div>
            </div>
          </nav>
        </header>
    </div>
<!-- Header section :: ends -->


<!-- Contain:: start -->
    <div class="container-fluid">
        <!-- step ::1  -->
          <div class="steps-wrapper">
            <h1><?php echo Lang::get('get-started.find-people-know')?>. <a href="{{url('completedGettingStarted')}}" class="btn btn-primary pull-right"><?php echo Lang::get('get-started.continue')?></a></h1>
            
            <p><?php echo Lang::get('get-started.find-people-know-message')?></p>
             <div class="row">
              <div class="col-md-12"></div>
            </div>
            <div class="import-social-apps">
                <button class="btn btn-default"><i class="ion-social-google"></i> <p>Gmail</p> </button>            
                <button class="btn btn-default"><i class="ion-android-mail"></i> <p>Outlook</p>  </button>            
                <button class="btn btn-default"><i class="ion-social-yahoo"></i> <p>Yahoo</p> </button>            
                <button class="btn btn-default"><i class="ion-social-windows"></i> <p>Windows Live <br>Hotmail</p> </button>            
             </div>
          </div>
        <!-- step ::1  -->
        <div>

        </div>
    </div>
<!-- Contain:: end -->


</div>