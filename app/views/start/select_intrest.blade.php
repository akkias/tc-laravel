@extends('../layouts/non-login_layout')
<div class="start-wrapper">
<!-- Header section :: starts -->
    <div class="container-fluid">
      <header>
        <nav class="navbar navbar-inverse navbar-fixed-top">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="text-center navbar-brand" href="#">TRIPTROOP</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">

                <ul class="nav navbar-nav navbar-left">
                  <li class="active"><a>Steps 1 of 3</a></li>
              </div>
            </div>
          </nav>
        </header>
    </div>
<!-- Header section :: ends -->


<!-- Contain:: start -->
    <div class="container-fluid">
        <!-- step ::1  -->
          <div class="steps-wrapper">
            <h1>What are you intrested in?</h1>
            
            <p>Choose one or more of the options below and we'll sugeest some good stuff for you.</p>
 
              <ul class="categories-profile">
                <li><span class="a"><i class="icon-adventure-travel"></i></span><p>Adventure</p></li>
                <li><span class="b"><i class="icon-arts-and-liesure"></i></span><p>Arts and Liesure</p></li>
                <li><span class="c"><i class="icon-beaches"></i></span><p>Beaches</p></li>
                <li><span class="d"><i class="icon-budget-travel"></i></span><p>Budget Travel</p></li>
                <li><span class="e"><i class="icon-coasts-and-islands"></i></span><p>Coasts and Islands</p></li>
                <li><span class="f"><i class="icon-diving-and-snorkelling"></i></span><p>Diving and Snorkelling</p></li>
                <li><span class="g"><i class="icon-ecotourism"></i></span><p>Ecotourism</p></li>
                <li><span class="h"><i class="icon-family-travel"></i></span><p>Family Travel</p></li>
                <li><span class="i"><i class="icon-festivals-and-events"></i></span><p>Festival and Events</p></li>
                <li><span class="k"><i class="icon-film-and-television"></i></span><p>Film and Television</p></li>
                <li><span class="l"><i class="icon-food-and-drink"></i></span><p>Food and Drink</p></li>
                <li><span class="m"><i class="icon-gear-and-tech"></i></span><p>Gear and Tech</p></li>
                <li><span class="n"><i class="icon-honeymoons-and-romance"></i></span><p>Honeymoons and Romance</p></li>
                <li><span class="o"><i class="icon-luxury-travel"></i></span><p>Luxury Travel</p></li>
                <li><span class="p"><i class="icon-music"></i></span><p>Music</p></li>
                <li><span class="q"><i class="icon-off-the-beaten-track"></i></span><p>Off the Beaten Track</p></li>
                <li><span class="r"><i class="icon-planes-and-trains"></i></span><p>planes and Trains</p></li>
                <li><span class="s"><i class="icon-road-trips"></i></span><p>Road Trips</p></li>
                <li><span class="t"><i class="icon-round-the-world-travel"></i></span><p>Round the world Travel</p></li>
                <li><span class="u"><i class="icon-travel-photography"></i></span><p>Travel Photograhpy</p></li>
                <li><span class="v"><i class="icon-travel-shopping"></i></span><p>Travel Shopping</p></li>
                <li><span class="w"><i class="icon-walking-and-trekking"></i></span><p>Walking and Trekking</p></li>
                <li><span class="x"><i class="icon-wildlife-and-nature"></i></span><p>Wlid life and Nature</p></li>
              </ul>
            <div class="row">
              <div class="col-md-12"><a href="{{url()}}" onclick="_gaq.push(['_trackEvent', 'Getting Started Welcome Page clicked for select Intrested Categories', 'clicked'])" class="btn btn-primary pull-right">Let's go!</a></div>
            </div>
          </div>
        <!-- step ::1  -->
        <div>

        </div>
    </div>
<!-- Contain:: end -->


</div>
