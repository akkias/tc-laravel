<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>500 unexpected error</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
  </head>
  <body class="map-bg"  onload="_gaq.push(['_trackEvent', 'Strikes 404', 'loaded'])">
     <div class="page-not-found">
        <img src="images/404_img.png" alt="" />
        <h2><?php echo Lang::get('error.you-got-lost')?></h2>
   </div>
  </body>
</html> 