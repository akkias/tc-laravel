@extends("layouts.show_layout")
@section('content')
<section class="page-wrapper profile-wrap">
  @include('/shared/header_new')
  @include('/profile/profile_header')
  <!-- Published stories -->
  <div class="clearfix isotope-container">
    <div class="story-row">
      @if($storyData)
      @foreach ($storyData as $story)
      <div id="story{{ $story->storyid }}">
        <div class="story-card col-md-4 col-sm-6 col-xs-12">
          <div class="author-data">
            <img height="32" style="background-color: #{{bin2hex(openssl_random_pseudo_bytes(3));}};" src="https://res.cloudinary.com/triptroop-prod/image/upload/w_60,h_60,c_fill/{{$story->clPath ? $story->clPath : $story->dpPath}}">
            {{HTML::link('/profile/'.$story->userid , $story->firstname,"",array('onclick' => '_gaq.push(["_trackEvent", "StoryTeller profile link clicked from storyCard", "clicked"])'))}}
            {{HTML::link('/profile/'.$story->userid , $story->lastname,"",array('onclick' => '_gaq.push(["_trackEvent", "StoryTeller profile link clicked from storyCard", "clicked"])'))}}
            @if($story->verified)
            <i data-toggle="tooltip" title="Verified Account" data-placement="top" class="fa fa-check-circle ver-icon"></i>
            @endif
            <div class="actions">
              @if(!empty($data))
              @if(DB::table('liked')->where('id', Auth::user()['id'])->where('likeTo', 'like', '%'.'->'.$story->storyid.'->'.'%')->count())
              <button id = "{{ 'like'.$story->storyid }}" onclick="likeClick({{ $story->storyid }});_gaq.push(['_trackEvent', 'Liked Done form dreamlist', 'clicked'])"   class="btn fav ed">
                <i class="fa fa-refresh fa-spin" style="display:none"></i>
                <i class="fa fa-heart-o block"></i>
              </button>          
              @else
              <button id = "{{ 'like'.$story->storyid }}" onclick="likeClick({{ $story->storyid }});_gaq.push(['_trackEvent', 'click to Like form dreamlist', 'clicked'])"  class="btn fav">
                <i class="fa fa-refresh fa-spin" style="display:none"></i>
                <i class="fa fa-heart-o block"></i>
              </button>
              @endif

              @if(DB::table('wishlist')->where('id', Auth::user()['id'])->where('wishTo', 'like', '%'.'->'.$story->storyid.'->'.'%')->count())
              <button id = "{{ 'wish'.$story->storyid }}" onclick="wishClick({{ $story->storyid }});_gaq.push(['_trackEvent', 'Added to Wish Done form dreamlist', 'clicked'])" class="btn dream ed">
                <i class="fa fa-refresh fa-spin" style="display:none"></i>
                <i class="fa fa-bookmark-o block"></i>
              </button>
              @else
              <button id = "{{ 'wish'.$story->storyid }}" onclick="wishClick({{ $story->storyid }});_gaq.push(['_trackEvent', 'click to wish Done form dreamlist', 'clicked'])" class="btn dream">
                <i class="fa fa-refresh fa-spin" style="display:none"></i>
                <i class="fa fa-bookmark-o block"></i>
              </button>       
              @endif
              @else
              <a href="/edit/{{$story->storyid}}" class="btn" onclick="_gaq.push(['_trackEvent', 'Edit draft actioned view from Draft', 'clicked'])"><i class="fa fa-pencil"></i></a>
              <button id = "{{$story->storyid}}"  class="btn" onclick="_gaq.push(['_trackEvent', 'Delete draft actioned view from Draft', 'clicked']); deleteClick(this.id); "><i class="fa fa-trash-o"></i></button>
              @endif
            </div>
          </div>
          <div class="story-cover" style="background-image:  url(https://res.cloudinary.com/triptroop-prod/image/upload/w_800/{{$story->coverClPath}});">
            {{HTML::link('read/'.$story->storyid, "",array('class' => 'story-link'))}}
          </div>
          <div class="story-meta">
            <h2 class="capitalize">{{HTML::link('read/'.$story->storyid, $story->title)}}</h2>
            <p>{{Str::limit($story->storyDescription, 100)}}</p>
          </div>

          <div class="footer clearfix">
            @if (Auth::user() == false)
            <span class="capitalize pull-right text-center likes like-count btn non-logged-link"><small><i class="fa fa-heart"></i></small> <span id="count-{{$story->storyid}}">{{$story->count}}</span> <?php echo Lang::get('common.likes') ?></span>
            @else
            <span data-toggle="modal" data-target="#likersModal" id = "{{ 'getLikers'.$story->storyid }}" onclick="getLikers({{ $story->storyid }});" class="btn pull-right capitalize text-center likes like-count"><small><i class="fa fa-heart"></i></small> <span id="count-{{$story->storyid}}">{{$story->count}}</span> <?php echo Lang::get('common.likes') ?></span>
            @endif
            <span class="hits pull-right"><small><i class="fa fa-eye"></i></small> {{$story->hits}} Views</span>
            <span class="date"><small><i class="fa fa-calendar"></i></small> {{($story->createdAt)}}</span>
          </div>
        </div>
      </div>
      @endforeach
      @else 
      <div class="not-available text-center">
        <img height="200" src="http://res.cloudinary.com/triptroop/image/upload/assets/no-story.jpg">
        <h1>You haven’t published any stories yet.</h1>
        <br>
        <p><a class="btn btn-primary" href="{{URL::to('/narrate')}}"><i class="fa fa-pencil align-middle"></i> <?php echo Lang::get('common.narrate-story')?></a></p>
      </div>
      @endif
    </div>
  </div>
</section>
@stop
