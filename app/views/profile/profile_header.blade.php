<style type="text/css">
  ol li {
    float: left;
    background-color: black;
    margin: 1px 20px 0;
    width: 150px;
    padding: 10px;
  }
</style>
<div class="profile-header relative text-center clearfix">
  <div class="avatar">
    @if (!empty($data))
    <div class="non-profile-img profile-img-box">
      <img class="img-circle" name="profilePicture" id ="profileImg" src="https://res.cloudinary.com/triptroop-prod/image/upload/h_150,w_150,c_fill/{{$data[0]->clPath ? $data[0]->clPath : $data[0]->dpPath}}" alt="User Avtar" />
      <div class="overly" data-toggle="modal" data-target="#avtar_prof_model">
        <div class="zoom-img"><i class="fa fa-search-plus"></i></div>
      </div>
      <div class="modal fade" id="avtar_prof_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-body">
              <img style="background: #{{bin2hex(openssl_random_pseudo_bytes(3));}}" name="profilePicture" id ="profileImg" src="https://res.cloudinary.com/triptroop-prod/image/upload/h_550,w_550,c_fill/{{$data[0]->clPath ? $data[0]->clPath : $data[0]->dpPath}}" alt="User Avtar" />
            </div>
          </div>
        </div>
      </div>
    </div>
    @elseif(DB::table('profile')->where('id', Auth::user()['id'])->count() && DB::table('profile')->where('id', Auth::user()['id'])->pluck('clPath'))
    <div class="non-profile-img profile-img-box">
      <img class="img-circle" name="profilePicture" id ="profileImg" src="https://res.cloudinary.com/triptroop-prod/image/upload/h_150,w_150,c_fill/v1431406595/{{DB::table('profile')->where('id', Auth::user()['id'])->pluck('clPath')}}" alt="User Avtar" />
      <div class="overly" data-toggle="modal" data-target="#avtar_prof_model">
        <div class="zoom-img"><i class="fa fa-search-plus"></i></div>
      </div>
      <div class="modal fade" id="avtar_prof_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-body">
              <img name="profilePicture" id ="profileImg" src="https://res.cloudinary.com/triptroop-prod/image/upload/h_550,w_550,c_fill/v1431406595/{{DB::table('profile')->where('id', Auth::user()['id'])->pluck('clPath')}}" alt="User Avtar" />
            </div>
          </div>
        </div>
      </div>
    </div>
    @else
    <div class="non-profile-img profile-img-box">
      <img class="img-circle" name="profilePicture" id ="profileImg" src="https://res.cloudinary.com/triptroop/image/upload/h_150,w_150,c_fill/v1431406595/assets/avatar.png" alt="user Avtar" />
      <div class="overly" data-toggle="modal" data-target="#profile_model">
        <div class="zoom-img"><i class="fa fa-search-plus"></i></div>
      </div>
      <div class="modal fade" id="profile_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            
            <div class="modal-body">
              <img class="img-circle" name="profilePicture" id ="profileImg" src="https://res.cloudinary.com/triptroop/image/upload/c_fill/v1431406595/assets/avatar.png" height="150" alt="user Avtar" />
            </div>
            
          </div>
        </div>
      </div>
    </div>
    @endif  
  </div>
  <h1 class="name">
    @if (!empty($data))
    {{$data[0]->firstname}} {{$data[0]->lastname}}
    @if ($data[0]->verified)
    <small class="align-middle"><i data-toggle="tooltip" title="Verified Account" data-placement="top" class="align-middle ver-icon fa fa-check-circle"></i></small>
    @endif

    @else
    <strong>{{Auth::user()['firstname']}} {{Auth::user()['lastname']}}</strong>

    @if (Auth::user()['verified'])
    <small class="align-middle"><i data-toggle="tooltip" title="Verified Account" data-placement="top" class="align-middle ver-icon fa fa-check-circle"></i></small>
    @endif

    @endif  
  </h1>
  <p class="desc">
    @if (!empty($data))
    {{$userData[0]->description}}
    @endif
    <!--  -->
  </p>
  <ul class="list-unstyled list-inline">
    @if (!empty($data))
    <li>
      <a href="" data-toggle="modal" data-target="#likersModal" onclick="getFollowings({{$data[0]->id}},'{{$data[0]->firstname}} {{$data[0]->lastname}}');">
        <small class="block"><?php echo Lang::get('common.following')?></small>
        <span class="count">
          {{$data[0]->following}}
        </span>
      </a>
    </li>
    <li>
      <a href="" data-toggle="modal" data-target="#likersModal" onclick="getFollowers({{$data[0]->id}},'{{$data[0]->firstname}} {{$data[0]->lastname}}');">
        <small class="block"><?php echo Lang::get('common.followers')?></small>
        <span class="count">
          {{$data[0]->followers}}
        </span>
      </a>
    </li>
    @else
    <li>
      <a href="" data-toggle="modal" data-target="#likersModal" onclick="getFollowings({{Auth::user()['id']}}, '{{Auth::user()['firstname']}} {{Auth::user()['lastname']}}');">
        <small class="block"><?php echo Lang::get('common.following')?></small>
        <span class="count">
          {{Auth::user()['following'];}}
        </span>
      </a>
    </li>
    <li>
      <a href="" data-toggle="modal" data-target="#likersModal" onclick="getFollowers({{Auth::user()['id']}}, '{{Auth::user()['firstname']}} {{Auth::user()['lastname']}}');">
        <small class="block"><?php echo Lang::get('common.followers')?></small>
        <span class="count">
          {{Auth::user()['followers'];}}
        </span>
      </a>
    </li> 
    @endif



    <li class="hide">
      <small class="block"><?php echo Lang::get('common.social')?></small>
      <a class="sicon" href="#"><i class="icon-facebook"></i></a>
      <a class="sicon" href="#"><i class="icon-twitter"></i></a>
      <a class="sicon" href="#"><i class="icon-google-plus"></i></a>
    </li>
    @if(!empty($data))
    <li>
      <a id = "followButton" class="btn">
        <i class="fa fa-refresh fa-spin" style="display:none"></i>
        @if($isFollow)  
        <span id = "followLabel"><?php echo Lang::get('common.following')?></span>
        @else
        <span id = "followLabel"><?php echo Lang::get('common.follow')?></span>
        @endif   
      </a>
      <input type = "hidden" id = "hiddenId" value = "{{$data[0]->id}}"/>
    </li>
    @endif 
  </ul>

  @if(empty($data))
  <div class="subnav">
    <ul class="nav nav-tabs text-center list-inline">
      <li class="{{ Request::is('profile','profile/Auth::user()["id"]') ? 'active' : '' }}"><a href="{{ URL::to( 'profile/') }}"><i class="fa fa-file-text-o"></i> <span><?php echo Lang::get('common.publish-trip')?></span></a></li>
      <li class="{{ Request::is('profile/drafts') ? 'active' : '' }}"><a href="{{ URL::to( 'profile/drafts') }}"><i class="fa fa-file-o"></i> <span><?php echo Lang::get('common.my-drafts')?></span></a></li>
      <li class="{{ Request::is('profile/dreamlist') ? 'active' : '' }}"><a href="{{ URL::to( 'profile/dreamlist') }}"><i class="fa fa-bookmark-o"></i> <span><?php echo Lang::get('common.dreamlist')?></span></a></li>
      </ul>
    </div>
    @endif 
  </div>