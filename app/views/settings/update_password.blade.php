@extends("layouts.profile_layout")
@section('content')
<script data-cfasync="false" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script data-cfasync="false">
 
  $(document).ready(function() {
    $("#editPasswordForm").submit(function(event){
        //	alert('edit pswd');
        //alert("ep form");
//        var checks = checkCategory();
        //$("#editProfileForm").text(checkCategory());
       // alert();
       event.preventDefault();
       $.ajax({
        url: "updatePassword",
        type: "POST",
        data:  new FormData(this),
        contentType: false,
        cache: true,
        processData:false,
        success: function(data){
                //alert(data);
                console.log(data);
                if(data == 1){
                 alert("Data Updated");
 //                   window.location =  "{{URL::To('/')}}";
}
else if(data == 2){
 alert("Old password doesnt match");
 //                   window.location =  "{{URL::To('/')}}";
}
else if(data == 3){
 alert("New password and Confirm password doesnt match");
 //                   window.location =  "{{URL::To('/')}}";
}
else{
 alert("Could not update the password, Please try again");
}
},
error: function(){
  alert('Error occured');   
}           
});
     });

});
</script>
<section class="page-wrapper settings-wrap clearfix">
  @include('/shared/header_new')
  <div class="content">
    <div class="col-md-10 col-md-offset-1">
      <div id="sidebar" class="col-md-3">
       <div class="clearfix white-bg">
        <div class="profile clearfix">
          <div class="portrate pull-left">
            <form  id = "profileImageUploadForm">
              <div class="userAvtar-container">
                <table class="userAvtar" data-intro="Change User Avtar here" data-steps="1">
                  <tr>
                    <td>
                      @if(DB::table('profile')->where('id', Auth::user()['id'])->count() && DB::table('profile')->where('id', Auth::user()['id'])->pluck('clPath'))
                      <img name = "profilePicture" id = "profileImgBig" src="https://res.cloudinary.com/triptroop-prod/w_80/{{DB::table('profile')->where('id', Auth::user()['id'])->pluck('clPath')}}" alt="change profile" />
                      @else
                      <img name = "profilePicture" id = "profileImgBig" src="https://res.cloudinary.com/triptroop/image/upload/assets/avatar.png" alt="change profile" height="80" />
                      @endif  
                      <!-- <img  alt="change profile" /> -->
                    </td>
                  </tr>
                </table>
                <span class="btn-file">
                  <span class="fa fa-pencil"></span>
                  <span class="fileinput-new"></span>
                  <!--span class="fileinput-exists">Change</span -->
                  <input type="file" id="photoimg" name="profilePic" multiple = "false">
                </span>
                <!-- <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput" data-no-turbolink="true">Remove</a> -->
              </div>
            </form>
          </div>
          <div class="profile-meta">
            <h2>
              {{Auth::user()['firstname']}}
            </h2>
          </div>
        </div>
      </div>
      <div class="clearfix white-bg ohidden setting-actions">
        <ul class="nav nav-pills nav-stacked">
         <li>
          <a href="{{URL::to('settings')}}"  onclick="_gaq.push(['_trackEvent', 'Basic Info link actioned from Update password - edit Profile', 'clicked'])">
           <i class="align-middle ion-ios-person-outline"></i><?php echo Lang::get('settings.basic-info')?>
         </a>
       </li> 
       <li class="active">
        <a href="{{URL::to('update-password')}}" onclick="_gaq.push(['_trackEvent', 'Update password link actioned from Update passsword - edit Profile', 'clicked'])">
         <i class="align-middle ion-key"></i><?php echo Lang::get('update-password.update-password')?>
       </a>
     </li> 
   </ul>
 </div>
</div>

<div class="col-md-9">
 <div class="white-bg clearfix">
  <h2 class="title">
   <span class="capitalize"><?php echo Lang::get('common.password')?></span>
   <small class="block"><?php echo Lang::get('update-password.update-password')?></small>
 </h2>
 <div class="content">
   <div class="row">
    <div class="col-md-12">
      {{ Form::open(array('url' => '', 'data-parsley-validate' => '', 'id' => 'editPasswordForm')) }}
      <div class="form-group clearfix">
        <label class="col-md-3 text-right"><?php echo Lang::get('update-password.current-password')?></label>
        {{ Form::password('old-pw', array('class' => 'col-md-5', 'placeholder' => 'Current Password', 'required' => '')) }}
      </div>
      <div class="form-group clearfix">
        <label class="col-md-3 text-right"><?php echo Lang::get('update-password.new-password')?></label>
        {{ Form::password('new-pw', array('class' => 'col-md-5', 'placeholder' => 'New Password', 'required' => '')) }}
      </div>
      <div class="form-group clearfix">
        <label class="col-md-3 text-right"><?php echo Lang::get('update-password.confirm-password')?></label>
        {{ Form::password('confirm-new-reset-pw', array('class' => 'col-md-5', 'placeholder' => 'Confirm New Password', 'required' => '')) }}
      </div>

      <div class="btn-wrapper">
        <label class="col-md-3"></label>
        <input class="btn btn-primary" type="submit" value="<?php echo Lang::get('update-password.update-password')?>"  onclick="_gaq.push(['_trackEvent', 'Password Updated from edit Profile', 'clicked'])">
      </div>
    </form> 
  </div>
</div>    
</div>
</div>
</div>
</div>
</section>
@stop