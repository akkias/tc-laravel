
@extends("layouts.show_layout")
@section('content')
<script data-cfasync="false" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script data-cfasync="false">
  function checkCategory(){
    var categories = "";
    var check = 0;
    for(var i = 1;  i <= 23;  i++){
      if($('#cat' + i).is(":checked")){
        check = 1;
        categories += ('->'+('cat' + i));
      }
    }
    categories += '->';
    if(check){
      return categories;  
    }
    else{
      return 0;
    }
  }

  function saveCategories(){
    //alert("hii");
    var checks = checkCategory();
  //  alert(checks);
  $("#checksId").val(checks);

}

$(document).ready(function() {
  $("#editProfileForm").submit(function(event){
    saveCategories();
    event.preventDefault();
    $.ajax({
      url: "updateBase",
      type: "POST",
      data:  new FormData(this),
      contentType: false,
      cache: true,
      processData:false,
      success: function(data){
        console.log(data);
        if(data == 1){
          $('body').append('<div class="err alert alert-info alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Your profile has been updated.</div>');
        }
        else{
          alert(data) ;
        }
      },
      error: function(){
        alert('Error occured');   
      }           
    });
  });


  $('#photoimg').on('change', function() {    
    $("#profileImageUploadForm").submit();
  });

  $("#profileImageUploadForm").submit(function(event){
    $('.tt-loader').show();
    event.preventDefault();
    $.ajax({
      url: "/setup_profile_image",
      type: "POST",
      data:  new FormData(this),
      contentType: false,
      cache: true,
      processData:false,
      success: function(data){
        $('.tt-loader').hide();
        if(data != 'failed' && data != 'File with given name already exists'){
            //$("#profileImg").attr('src',data);
            $("#profileImgBig").attr('src','https://res.cloudinary.com/triptroop-prod/w_80'+data);
          }
          else{
            alert(data);
          }
        },
        error: function(){}           
      });
  }); 





});
</script>
<section class="page-wrapper settings-wrap clearfix">
  @include('/shared/header_new')
  <div class="content">
  <div class="col-md-10 col-md-offset-1">
      <div id="sidebar" class="col-md-3">
        <div class="clearfix white-bg">
          <div class="profile clearfix">
            <div class="portrate pull-left">
              <form  id = "profileImageUploadForm">
                <div class="userAvtar-container">
                  <table class="userAvtar" data-intro="Change User Avtar here" data-steps="1">
                    <tr>
                      <td>
                        @if(DB::table('profile')->where('id', Auth::user()['id'])->count() && DB::table('profile')->where('id', Auth::user()['id'])->pluck('clPath'))
                        <img name = "profilePicture" id = "profileImgBig" src="https://res.cloudinary.com/triptroop/w_80/{{DB::table('profile')->where('id', Auth::user()['id'])->pluck('clPath')}}" alt="change profile" />
                        @else
                        <img name = "profilePicture" id = "profileImgBig" src="https://res.cloudinary.com/triptroop/image/upload/assets/avatar.png" alt="change profile" height="80" />
                        @endif  
                        <!-- <img  alt="change profile" /> -->
                      </td>
                    </tr>
                  </table>
                  <span class="btn-file">
                    <span class="fa fa-pencil"></span>
                    <span class="fileinput-new"></span>
                    <!--span class="fileinput-exists">Change</span -->
                    <input type="file" id="photoimg" name="profilePic" multiple = "false">
                  </span>
                  <!-- <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput" data-no-turbolink="true">Remove</a> -->
                </div>
              </form>
            </div>
            <div class="profile-meta">
              <h2>
                {{Auth::user()['firstname']}}
              </h2>
            </div>
          </div>


        </div>
        <div class="clearfix white-bg ohidden setting-actions">
          <ul class="nav nav-pills nav-stacked">
            <li class="active">
              <a href="{{URL::to('settings')}}" onclick="_gaq.push(['_trackEvent', 'Basic Info link actioned from edit Profile', 'clicked'])">
                <i class="align-middle icon-user"></i><?php echo Lang::get('settings.basic-info')?>
              </a>
            </li> 
            <li>
              <a href="{{URL::to('update-password')}}" onclick="_gaq.push(['_trackEvent', 'Update Password link actioned from edit Profile', 'clicked'])">
                <i class="align-middle icon-key"></i><?php echo Lang::get('update-password.update-password')?>
              </a>
            </li> 
          </ul>
        </div>
      </div>

      <div id="main" class="col-md-9">
        <div class="white-bg clearfix">
          <h2 class="title">
            <span class="capitalize"><?php echo Lang::get('common.account')?></span>
            <small class="block"><?php echo Lang::get('settings.basic-info-message')?></small>
          </h2>

          <div class="content">
            <div class="pagecontents">
              <div class="section">
                <div class="section-container">
                  <div class="row">
                    <div class="col-md-12">
                      <form accept-charset="UTF-8" data-parsley-validate="" id = "editProfileForm">
                        <input name="_token" type="hidden" value="wt75hWadUlZI9Q7ogip4ln0NmKsbfJp6ZLQjzRMG">
                        <div class="form-group clearfix">
                          <label class="col-md-3 text-right"><?php echo Lang::get('common.first-name')?></label>
                          <input class="col-md-5" placeholder="First Name" required="" name="old-firstname" type="text" value="{{$data[0]->firstname == '' ? '' : $data[0]->firstname}}" onfocus="_gaq.push(['_trackEvent', 'update first name in edit Profile', 'onfocused'])">
                        </div>
                        <div class="form-group clearfix">
                          <label class="col-md-3 text-right"><?php echo Lang::get('common.last-name')?></label>
                          <input class="col-md-5" placeholder="Last Name" required="" name="old-lastname" type="text" value="{{$data[0]->lastname == '' ? '' : $data[0]->lastname}}" onfocus="_gaq.push(['_trackEvent', 'update Last name in edit Profile', 'onfocused'])">
                        </div>
                        <div class="form-group clearfix">
                          <label class="col-md-3 text-right"><?php echo Lang::get('common.gender')?></label>
                          <!-- <input class="col-md-6" placeholder="Last Name" required="" name="old-lastnme" type="text" value=""> -->
                          <select class="col-md-5" name = "gender" onchange="_gaq.push(['_trackEvent', 'Gender Changed in edit Profile', 'onchange'])">
                            <option @if ($data[0]->gender == 'male')selected="selected"@endif value = "male"><?php echo Lang::get('common.male')?></option>
                            <option @if($data[0]->gender == 'female')selected="selected"@endif value = "female"><?php echo Lang::get('common.female')?></option>
                          </select>
                        </div>

                        <hr />

                        <div class="form-group clearfix">
                          <label class="col-md-3 text-right"><?php echo Lang::get('common.email')?></label>
                          <input class="col-md-5" placeholder="eg@gmail.com" required="" name="old-email" type="email" value="{{$data[0]->emailid == '' ? '' : $data[0]->emailid}}" onfocus="_gaq.push(['_trackEvent', 'update email-id in edit Profile', 'onfocused'])">
                          <small class="col-md-offset-3 block info"><?php echo Lang::get('settings.email-info')?></small>
                        </div>


                        <hr />

                        <div class="form-group clearfix">
                          <label class="col-md-3 text-right"><?php echo Lang::get('common.fav-travel-activities')?></label>
                          <div class="col-md-9">

                            <ul class="categories list-unstyled clearfix list-inline text-left">
                              <li>
                                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat1->%')->count())
                                <input id="cat1" class="css-checkbox" type="checkbox" checked />
                                @else
                                <input id="cat1" class="css-checkbox" type="checkbox" />
                                @endif
                                <label for="cat1"  class="css-label">
                                  <span class="img-circle a">
                                    <strong class="fa fa-check-circle icon-checked"></strong>
                                    <i class="cat-icon-adventure-travel"></i>
                                  </span>
                                  <p><?php echo Lang::get('common.advanture')?></p>
                                </label>
                              </li>
                              <li>
                                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat2->%')->count())
                                <input id="cat2" class="css-checkbox" type="checkbox" checked />
                                @else
                                <input id="cat2" class="css-checkbox" type="checkbox" />
                                @endif
                                <label for="cat2" class="css-label">
                                  <span class="img-circle b">
                                    <strong class="fa fa-check-circle icon-checked"></strong>
                                    <i class="cat-icon-arts-and-liesure"></i>
                                  </span>
                                  <p><?php echo Lang::get('common.arts-lieasure')?></p>
                                </label>
                              </li>
                              <li>
                                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat3->%')->count())
                                <input id="cat3" class="css-checkbox" type="checkbox" checked />
                                @else
                                <input id="cat3" class="css-checkbox" type="checkbox" />
                                @endif
                                <label for="cat3" class="css-label">
                                  <span class="img-circle c">
                                    <strong class="fa fa-check-circle icon-checked"></strong>
                                    <i class="cat-icon-backpacking"></i>
                                  </span>
                                  <p>Backpacking</p>
                                </label>
                              </li>
                              <li>
                                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat4->%')->count())
                                <input id="cat4" class="css-checkbox" type="checkbox" checked />
                                @else
                                <input id="cat4" class="css-checkbox" type="checkbox" />
                                @endif
                                <label for="cat4" class="css-label">
                                  <span class="img-circle d">
                                    <strong class="fa fa-check-circle icon-checked"></strong>
                                    <i class="cat-icon-beaches"></i>
                                  </span>
                                  <p><?php echo Lang::get('common.beach')?></p>
                                </label>
                              </li>
                              <li>
                                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat5->%')->count())
                                <input id="cat5" class="css-checkbox" type="checkbox" checked />
                                @else
                                <input id="cat5" class="css-checkbox" type="checkbox" />
                                @endif
                                <label for="cat5" class="css-label">
                                  <span class="img-circle e">
                                    <strong class="fa fa-check-circle icon-checked"></strong>
                                    <i class="cat-icon-budget-travel"></i>
                                  </span>
                                  <p><?php echo Lang::get('common.budget-traval')?></p>
                                </label>
                              </li>
                              <li>
                                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat6->%')->count())
                                <input id="cat6" class="css-checkbox" type="checkbox" checked />
                                @else
                                <input id="cat6" class="css-checkbox" type="checkbox" />
                                @endif
                                <label for="cat6" class="css-label">
                                  <span class="img-circle f">
                                    <strong class="fa fa-check-circle icon-checked"></strong>
                                    <i class="cat-icon-coasts-and-islands"></i>
                                  </span>
                                  <p><?php echo Lang::get('common.coasts-islands')?></p>
                                </label>
                              </li>
                              <li>
                                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat7->%')->count())
                                <input id="cat7" class="css-checkbox" type="checkbox" checked />
                                @else
                                <input id="cat7" class="css-checkbox" type="checkbox" />
                                @endif
                                <label for="cat7" class="css-label">
                                  <span class="img-circle g">
                                    <strong class="fa fa-check-circle icon-checked"></strong>
                                    <i class="cat-icon-diving-and-snorkelling"></i>
                                  </span>
                                  <p><?php echo Lang::get('common.diving-snorkelling')?></p>
                                </label>
                              </li>
                              <li>
                                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat8->%')->count())
                                <input id="cat8" class="css-checkbox" type="checkbox" checked />
                                @else
                                <input id="cat8" class="css-checkbox" type="checkbox" />
                                @endif
                                <label for="cat8" class="css-label">
                                  <span class="img-circle h">
                                    <strong class="fa fa-check-circle icon-checked"></strong>
                                    <i class="cat-icon-ecotourism"></i>
                                  </span>
                                  <p><?php echo Lang::get('common.ecotourism')?></p>
                                </label>
                              </li>
                              <li>
                                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat9->%')->count())
                                <input id="cat9" class="css-checkbox" type="checkbox" checked />
                                @else
                                <input id="cat9" class="css-checkbox" type="checkbox" />
                                @endif
                                <label for="cat9" class="css-label">
                                  <span class="img-circle i">
                                    <strong class="fa fa-check-circle icon-checked"></strong>
                                    <i class="cat-icon-family-travel"></i>
                                  </span>
                                  <p><?php echo Lang::get('common.family-travel')?></p>
                                </label>
                              </li>
                              <li>
                                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat10->%')->count())
                                <input id="cat10" class="css-checkbox" type="checkbox" checked />
                                @else
                                <input id="cat10" class="css-checkbox" type="checkbox" />
                                @endif
                                <label for="cat10" class="css-label">
                                  <span class="img-circle j">
                                    <strong class="fa fa-check-circle icon-checked"></strong>
                                    <i class="cat-icon-festivals-and-events"></i>
                                  </span>
                                  <p><?php echo Lang::get('common.festival-events')?></p>
                                </label>
                              </li>
                              <li>
                                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat11->%')->count())
                                <input id="cat11" class="css-checkbox" type="checkbox" checked />
                                @else
                                <input id="cat11" class="css-checkbox" type="checkbox" />
                                @endif
                                <label for="cat11" class="css-label">
                                  <span class="img-circle k">
                                    <strong class="fa fa-check-circle icon-checked"></strong>
                                    <i class="cat-icon-film-and-television"></i>
                                  </span>
                                  <p><?php echo Lang::get('common.film-television')?></p>
                                </label>
                              </li>
                              <li>
                                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat12->%')->count())
                                <input id="cat12" class="css-checkbox" type="checkbox" checked />
                                @else
                                <input id="cat12" class="css-checkbox" type="checkbox" />
                                @endif
                                <label for="cat12" class="css-label">
                                  <span class="img-circle l">
                                    <strong class="fa fa-check-circle icon-checked"></strong>
                                    <i class="cat-icon-food-and-drink"></i>
                                  </span>
                                  <p><?php echo Lang::get('common.food-drink')?></p>
                                </label>
                              </li>
                              <li>
                                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat13->%')->count())
                                <input id="cat13" class="css-checkbox" type="checkbox" checked />
                                @else
                                <input id="cat13" class="css-checkbox" type="checkbox" />
                                @endif
                                <label for="cat13" class="css-label">
                                  <span class="img-circle m">
                                    <strong class="fa fa-check-circle icon-checked"></strong>
                                    <i class="cat-icon-gear-and-tech"></i>
                                  </span>
                                  <p><?php echo Lang::get('common.gear-tech')?></p>
                                </label>
                              </li>
                              <li>
                                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat14->%')->count())
                                <input id="cat14" class="css-checkbox" type="checkbox" checked />
                                @else
                                <input id="cat14" class="css-checkbox" type="checkbox" />
                                @endif
                                <label for="cat14" class="css-label">
                                  <span class="img-circle n">
                                    <strong class="fa fa-check-circle icon-checked"></strong>
                                    <i class="cat-icon-honeymoons-and-romance"></i>
                                  </span>
                                  <p><?php echo Lang::get('common.honymoon-romance')?></p>
                                </label>
                              </li>
                              <li>
                                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat15->%')->count())
                                <input id="cat15" class="css-checkbox" type="checkbox" checked />
                                @else
                                <input id="cat15" class="css-checkbox" type="checkbox" />
                                @endif
                                <label for="cat15" class="css-label">
                                  <span class="img-circle o">
                                    <strong class="fa fa-check-circle icon-checked"></strong>
                                    <i class="cat-icon-luxury-travel"></i>
                                  </span>
                                  <p><?php echo Lang::get('common.luxury-travel')?></p>
                                </label>
                              </li>
                              <li>
                                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat16->%')->count())
                                <input id="cat16" class="css-checkbox" type="checkbox" checked />
                                @else
                                <input id="cat16" class="css-checkbox" type="checkbox" />
                                @endif
                                <label for="cat16" class="css-label">
                                  <span class="img-circle p">
                                    <strong class="fa fa-check-circle icon-checked"></strong>
                                    <i class="cat-icon-music"></i>
                                  </span>
                                  <p><?php echo Lang::get('common.music')?></p>
                                </label>
                              </li>
                              <li>
                                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat17->%')->count())
                                <input id="cat17" class="css-checkbox" type="checkbox" checked />
                                @else
                                <input id="cat17" class="css-checkbox" type="checkbox" />
                                @endif
                                <label for="cat17" class="css-label">
                                  <span class="img-circle q">
                                    <strong class="fa fa-check-circle icon-checked"></strong>
                                    <i class="cat-icon-off-the-beaten-track"></i>
                                  </span>
                                  <p><?php echo Lang::get('common.off-beaten-track')?></p>
                                </label>
                              </li>
                              <li>
                                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat18->%')->count())
                                <input id="cat18" class="css-checkbox" type="checkbox" checked />
                                @else
                                <input id="cat18" class="css-checkbox" type="checkbox" />
                                @endif
                                <label for="cat18" class="css-label">
                                  <span class="img-circle r">
                                    <strong class="fa fa-check-circle icon-checked"></strong>
                                    <i class="cat-icon-planes-and-trains"></i>
                                  </span>
                                  <p><?php echo Lang::get('common.planes-trains')?></p>
                                </label>
                              </li>
                              <li>
                                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat19->%')->count())
                                <input id="cat19" class="css-checkbox" type="checkbox" checked />
                                @else
                                <input id="cat19" class="css-checkbox" type="checkbox" />
                                @endif
                                <label for="cat19" class="css-label">
                                  <span class="img-circle s">
                                    <strong class="fa fa-check-circle icon-checked"></strong>
                                    <i class="cat-icon-road-trips"></i>
                                  </span>
                                  <p><?php echo Lang::get('common.road-trips')?></p>
                                </label>
                              </li>
                              <li>
                                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat20->%')->count())
                                <input id="cat20" class="css-checkbox" type="checkbox" checked />
                                @else
                                <input id="cat20" class="css-checkbox" type="checkbox" />
                                @endif
                                <label for="cat20" class="css-label">
                                  <span class="img-circle t">
                                    <strong class="fa fa-check-circle icon-checked"></strong>
                                    <i class="cat-icon-round-the-world-travel"></i>
                                  </span>
                                  <p><?php echo Lang::get('common.round-world-travel')?></p>
                                </label>
                              </li>
                              <li>
                                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat21->%')->count())
                                <input id="cat21" class="css-checkbox" type="checkbox" checked />
                                @else
                                <input id="cat21" class="css-checkbox" type="checkbox" />
                                @endif
                                <label for="cat21" class="css-label">
                                  <span class="img-circle u">
                                    <strong class="fa fa-check-circle icon-checked"></strong>
                                    <i class="cat-icon-travel-photography"></i>
                                  </span>
                                  <p><?php echo Lang::get('common.travel-photoghraphy')?></p>
                                </label>
                              </li>
                              <li>
                                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat22->%')->count())
                                <input id="cat22" class="css-checkbox" type="checkbox" checked />
                                @else
                                <input id="cat22" class="css-checkbox" type="checkbox" />
                                @endif
                                <label for="cat22" class="css-label">
                                  <span class="img-circle v">
                                    <strong class="fa fa-check-circle icon-checked"></strong>
                                    <i class="cat-icon-travel-shopping"></i>
                                  </span>
                                  <p><?php echo Lang::get('common.travel-shopping')?></p>
                                </label>
                              </li>
                              <li>
                                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat23->%')->count())
                                <input id="cat23" class="css-checkbox" type="checkbox" checked />
                                @else
                                <input id="cat23" class="css-checkbox" type="checkbox" />
                                @endif
                                <label for="cat23" class="css-label">
                                  <span class="img-circle w">
                                    <strong class="fa fa-check-circle icon-checked"></strong>
                                    <i class="cat-icon-walking-and-trekking"></i>
                                  </span>
                                  <p><?php echo Lang::get('common.walking-trekking')?></p>
                                </label>
                              </li>
                              <li>
                                @if(DB::table('userInfo')->where('id', Auth::user()['id'])->where('categories', 'like', '%->cat24->%')->count())
                                <input id="cat24" class="css-checkbox" type="checkbox" checked />
                                @else
                                <input id="cat24" class="css-checkbox" type="checkbox" />
                                @endif
                                <label for="cat24" class="css-label">
                                  <span class="img-circle x">
                                    <strong class="fa fa-check-circle icon-checked"></strong>
                                    <i class="cat-icon-wildlife-and-nature"></i>
                                  </span>
                                  <p><?php echo Lang::get('common.wildlife-nature')?></p>
                                </label>
                              </li>
                            </ul>
                          </div>
                        </div>

                        <hr />

                        <div class="form-group clearfix tel-number">
                          <label class="col-md-3 text-right"><?php echo Lang::get('common.phone-number')?></label>
                          <input class="col-md-1 text-center" placeholder="+1" required="" name="old-tel-pin" value="{{$data[0]->pin == '' ? '' : $data[0]->pin }}" type="text">
                          <input class="col-md-4" placeholder="" required="" name="old-telno" type="text" value="{{$data[0]->mbl == '' ? '' : $data[0]->mbl }}" onfocus="_gaq.push(['_trackEvent', 'update Mobile in edit Profile', 'onfocused'])">
                        </div>

                        <div class="form-group clearfix">
                          <label class="col-md-3 text-right"><?php echo Lang::get('common.location')?></label>
                          <input class="col-md-5" placeholder="Location" required="" name="old-location" type="text" value="{{$data[0]->location == '' ? '' : $data[0]->location}}" onfocus="_gaq.push(['_trackEvent', 'Update Location in edit Profile', 'onfocused'])">
                        </div>

                        <div class="form-group clearfix">
                          <label class="col-md-3 text-right"><?php echo Lang::get('common.description')?></label>
                          <textarea class="col-md-5" name = "desc" placeholder="Discription" onfocus="_gaq.push(['_trackEvent', 'Updated Descri in edit Profile', 'onfocused'])">{{$data[0]->description == '' ? '' : $data[0]->description}}</textarea>
                        </div>

                        <div class="btn-wrapper">
                          <label class="col-md-3 text-right"></label>
                          <button class="btn btn-primary" type="submit" onclick="_gaq.push(['_trackEvent', 'Submitted updated details in edit Profile', 'clicked'])"><?php  echo Lang::get('common.save')?>  </button>
                        </div>
                        <input type = "hidden" name = "checks" id = "checksId"/>
                      </form>
                    </div>

                  </div>    
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  @stop