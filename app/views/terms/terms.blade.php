@extends("layouts.show_layout")
@section('content')
<section class="page-wrapper settings-wrap clearfix text-normal">
 @include('/shared/header_new')
 <div class="content">
  <div class="col-xs-8 col-md-offset-2">
    <div class="white-bg">
      <h1>Terms & Conditions</h1>
      <p>
        <strong><a href="https://www.triptroop.me">https://www.triptroop.me</a></strong> <?php echo Lang::get('terms.terms-message-para')?>        
      </p> <br />
      <ul class="fa-ul">
        <li><i class="fa-li fa fa-star-o"></i><?php echo Lang::get('terms.terms-condition-mgs1')?></li> 
        <li><i class="fa-li fa fa-star-o"></i><?php echo Lang::get('terms.terms-condition-mgs2')?></li>
        <li><i class="fa-li fa fa-star-o"></i><?php echo Lang::get('terms.terms-condition-mgs3')?></li>
        <li><i class="fa-li fa fa-star-o"></i><?php echo Lang::get('terms.terms-condition-mgs4')?> <a href="/privacy">Privacy Policy.</a></li>
        <li><i class="fa-li fa fa-star-o"></i><?php echo Lang::get('terms.terms-condition-mgs5')?></li>
        <li><i class="fa-li fa fa-star-o"></i><?php echo Lang::get('terms.terms-condition-mgs6')?></li>
        <li><i class="fa-li fa fa-star-o"></i><?php echo Lang::get('terms.terms-condition-mgs7')?></li>
        <li><i class="fa-li fa fa-star-o"></i><?php echo Lang::get('terms.terms-condition-mgs8')?></li>
        <li><i class="fa-li fa fa-star-o"></i><?php echo Lang::get('terms.terms-condition-mgs9')?></li>
        <li><i class="fa-li fa fa-star-o"></i><?php echo Lang::get('terms.terms-condition-mgs10')?></li>
        <li><i class="fa-li fa fa-star-o"></i><?php echo Lang::get('terms.terms-condition-mgs11')?></li>
        <li><i class="fa-li fa fa-star-o"></i><?php echo Lang::get('terms.terms-condition-mgs12')?> </li>
        <li><i class="fa-li fa fa-star-o"></i><?php echo Lang::get('terms.terms-condition-mgs13')?></li>
        <li><i class="fa-li fa fa-star-o"></i><?php echo Lang::get('terms.terms-condition-mgs14')?> <a href="mailto:info@triptroop.me">info@triptroop.me.</a></li>
        <li><i class="fa-li fa fa-star-o"></i><?php echo Lang::get('terms.terms-condition-mgs15')?></li>
      </ul> <hr >
      <h3 class="text-center"><?php echo Lang::get('terms.terms-thanks-mgs')?></h3>
    </div>
  </div>
</div>
</section>  

@stop
