@extends("layouts.show_layout")
@section('content')
<div class="page-wrapper">
	@include('/shared/header_new')
	<section class="container-fluid pad0">
		<div class="tt-content pad0">
			<section class="col-md-12 isotope-container">
				<div class="scroller">
					<ul>
						@include('./story')
					</ul>
				</div>
			</section>
		</div>
	</section>
@stop