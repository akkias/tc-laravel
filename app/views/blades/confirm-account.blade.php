<script>
$(document).ready(function(){
    $("#oauthBox").submit(function(event){
        $('.tt-loader').show();
        event.preventDefault();
        $.ajax({
            url: "oauthConfirm",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: true,
            processData:false,
            success: function(data){
                //alert((data));
                  $('.tt-loader').hide();
                if(data == 1){
                        alert("Account Confirmed successfully.");
                        window.location = "{{URL::To('/')}}";
                }
                else if(data == 2){
                    alert("Account could not confirmed. Try again");
                }
                else{
                    alert(data);
                }
            },
            error: function(error){
                  $('.tt-loader').hide();
                alert(error);

            }           
        });
    })
});
</script>
<div  id = "oauthConfirmBox" class="confirm-modal modal ">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body clearfix">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="bg-container text-center">
                <!-- <img class="logo" src="https://res.cloudinary.com/triptroop/image/upload/c_scale,w_150/assets/logo_white.png"> -->
                    <div class="modal-header">
                        <h1 class="mar0">Welcome to TripTroop!</h1>
                        <h2>Lets TripTroop growing your network! Mind Confirming a few details first?</h2>
                    </div>

<form id = "oauthBox">
<div class="row confirm-user-content">
                            <div class="col-md-12">
                                <div class="col-md-4 text-right">Name*</div>
                                <div class="col-md-8"><input  type="text" name = "name" readonly value="{{DB::table('users')->where('id', Auth::user()['id'])->pluck('firstname')}} {{DB::table('users')->where('id', Auth::user()['id'])->pluck('lastname')}}" class="col-md-12 form-control" /></div>
                            </div>       
                            <div class="col-md-12">
                                <div class="col-md-4 text-right">Email*</div>
                                <div class="col-md-8"><input type="text"  name = "email"readonly value="{{DB::table('users')->where('id', Auth::user()['id'])->pluck('emailid')}}" class="col-md-12 form-control" /></div>
                            </div>                        
                            <div class="col-md-12">
                                <div class="col-md-4 text-right">Set Password</div>
                                <div class="col-md-8"><input type="password" class="col-md-12 form-control" title="Password must contain at least 6 characters, including numbers" required pattern="(?=.*\d)(?=.*[a-z]).{6,}" name="pwd1" 
                                    onchange="this.setCustomValidity(this.validity.patternMismatch ? this.title : ''); if(this.checkValidity()) form.pwd2.pattern = this.value;" /></div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-4 text-right">Confirm Password</div>
                                <div class="col-md-8"><input  type="password" class="col-md-12 form-control"  title="Please enter the same Password as above." required pattern="(?=.*\d)(?=.*[a-z]).{6,}" name="pwd2" onchange="
  this.setCustomValidity(this.validity.patternMismatch ? this.title : '');" /></div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-4 text-right"></div>
                                <div class="col-md-8 text-left"><input type="checkbox" required /> I agree to the <a href="/terms">terms and conditions</a></div>
                            </div>                        
                           
                            <div class="col-md-12">
                                <div class="col-md-4 text-right"></div>
                                <div class="col-md-8"> <input type="submit" class="btn btn-success pull-right" value = "Confirm Account"/></div>
                            </div> 
                        </div>

</form>

                    
                    <!--  -->
                    
                </div>
            </div>
        </div>
    </div>
</div>