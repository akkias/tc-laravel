@extends('../layouts/profile_layout')
<div class="settings-wrapper col-md-4 col-md-offset-4"  onload="_gaq.push(['_trackEvent', 'On Settings View', 'loaded'])">
    <section class="page-container">
        <h1 class="text-center"><?php echo Lang::get('update-password.update-password')?>?</h1>
        {{ Form::open(array('url' => '/updatePassword','method' => 'post', 'data-parsley-validate' => '')) }}
        <div class="form-group">
            {{ Form::password('old-pw', array('class' => 'form-control', 'placeholder' => 'Current Password', 'required' => '')) }}
        </div> 
        <div class="form-group">
            {{ Form::password('new-pw', array('class' => 'form-control', 'placeholder' => 'New Password', 'required' => '')) }}
            {{ Form::password('confirm-new-reset-pw`', array('class' => 'form-control', 'placeholder' => 'Confirm New Password', 'required' => '')) }}
        </div>
        <div class="btn-wrapper">
            {{ Form::submit('Update Password', array('class' => 'btn btn-primary btn-block')) }}
        </div>
        {{ Form::close()}}
    </section>  
</div>