@extends('../layouts/non-login_layout')

<div class="form-wrapper text-center login-wrapper"  onload="_gaq.push(['_trackEvent', 'On Reset Password View', 'loaded'])">
    <section class="page-container">
        <h1>Set your Password <?#php echo Lang::get('update-password.reset-your-password')?></h1>

        <p class="eta centered intro"> Enter your password below to login your TripTroop account.<?#php echo Lang::get('update-password.reset-password-info')?></p>

        <!-- {{ Form::open(array('url' => 'registration','class' => 'form centered')) }} -->
        {{ Form::open(array('url' => '', 'data-parsley-validate' => '')) }}
        <div class="form clearfix">
            <div class="form-group relative">
                <i class="ion-key"></i>
                {{ Form::password('reset-pw', array('class' => 'forgot-password-email-input', 'placeholder' => 'New Password', 'required' => '')) }}
            </div>
            <div class="form-group relative">
                <i class="ion-key"></i>
                {{ Form::password('confirm-reset-pw', array('class' => 'forgot-password-email-input', 'placeholder' => 'Confirm Password', 'required' => '')) }}
            </div>            
                
            <div class="btn-wrapper relative">
                {{ Form::submit('Recover Password', array('class' => 'btn btn-success btn-block')) }}
            </div>
        </div>
        <!-- sign in using email:: end -->

        {{ Form::close() }}     

    </section>
</div>