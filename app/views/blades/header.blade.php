<header class="fixed-header absolute">
  <nav class="tt-navbar navbar navbar-default" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" onclick="_gaq.push(['_trackEvent', 'Header Logo Clicked Navigated to Landing Page', 'clicked'])" href="{{URL::to('/')}}"><img src="/assets/images/tt_logo.png" height="40" alt="TripTroop"></a>
      <ul class="pull-left top-ul list-unstyled">
        <li class="dropdown">
          <a href="#" class="btn dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li class="{{ Request::is( '/') ? 'active' : '' }}">
              <a class="animsition-link home" title="Home" onclick="_gaq.push(['_trackEvent', 'Home Page link Actioned', 'clicked'])" href="{{ URL::to( '/') }}">
                <span><?php echo Lang::get('common.home')?></span>
              </a>
            </li>
            <li class="{{ Request::is( '/') ? 'active' : '' }}">
              <a class="animsition-link home" title="Home" onclick="_gaq.push(['_trackEvent', 'Home Page link Actioned', 'clicked'])" href="{{ URL::to( '/') }}">
                <span><?php echo Lang::get('common.home')?></span>
              </a>
            </li>
            <li class="{{ Request::is( 'popular') ? 'active' : '' }}">
              <a class="animsition-link popular-stories" title="Popular Stories" onclick="_gaq.push(['_trackEvent', 'Popular Story link Actioned', 'clicked'])" href="{{ URL::to( 'popular') }}">
                <span><?php echo Lang::get('common.popular-stories')?></span>
              </a>
            </li>
            <li class="{{ Request::is( 'storytellers') ? 'active' : '' }}">
              <a class="animsition-link storytellers" title="Storytellers"  onclick="_gaq.push(['_trackEvent', 'Storytellers link Actioned', 'clicked'])"  href="{{ URL::to( 'storytellers') }}">
                <span><?php echo Lang::get('common.storytellers')?></span>
              </a>
            </li>
            <li class="{{ Request::is( 'themes', 'themes/*') ? 'active' : '' }} dropdown">
              <a class="animsition-link themes" title="Themes" href="{{ URL::to( 'themes') }}" onclick="_gaq.push(['_trackEvent', 'Themes link Actioned', 'clicked'])">
                <span><?php echo Lang::get('common.themes')?></span>
              </a>
            </li>
          </ul>
        </li>
      </ul>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-ex1-collapse pull-right">
      <ul class="nav navbar-nav navbar-right top-ul">
        @if (Auth::user() == false)
        <li class="align-middle non-logged-link">
          <a onclick="_gaq.push(['_trackEvent', 'Opened signin/signup dialog', 'clicked'])" class="pull-left"><?php echo Lang::get('common.sign-in')?> / <?php echo Lang::get('common.sign-up')?></a>
        </li>
        @else
        <li class="dropdown">
          <a data-toggle="dropdown" class="dropdown-toggle" onclick="_gaq.push(['_trackEvent', 'Header User Dropdown Options actioned', 'clicked'])">
            @if(DB::table('profile')->where('id', Auth::user()['id'])->count() && DB::table('profile')->where('id', Auth::user()['id'])->pluck('clPath'))
            <img height="34" width="34" class="img-circle" name = "profilePicture" id ="profileImg" src="https://res.cloudinary.com/triptroop/image/upload/w_90,h_90,c_fill/v1431406595/{{DB::table('profile')->where('id', Auth::user()['id'])->pluck('clPath')}}" alt="change profile" />
            @else
            <img height="34" name = "profilePicture" id ="profileImg" src="https://res.cloudinary.com/triptroop/image/upload/w_90/v1431406595/assets/avatar.png" height="37" alt="change profile" />
            @endif  
            <span class="fa fa-angle-down"></span></a>
          </a>
          <ul class="dropdown-menu">
            <li><a href="/profile" onclick="_gaq.push(['_trackEvent', 'Profile Page link actioned', 'clicked'])"><i class="align-middle fa fa-user"></i><?php echo Lang::get('common.my-profile'); ?></a></li>
            <li><a href="/profile/drafts" onclick="_gaq.push(['_trackEvent', 'Draft Page link actioned', 'clicked'])"><i class="align-middle fa fa-file-o"></i><?php echo Lang::get('common.drafts'); ?></a></li>
            <li><a href="/profile/dreamlist" onclick="_gaq.push(['_trackEvent', 'Dreamlist Page link actioned', 'clicked'])"><i class="align-middle fa fa-bookmark-o"></i><?php echo Lang::get('common.dreamlist'); ?></a></li>
            <li><a href="/settings" onclick="_gaq.push(['_trackEvent', 'Settings Page link actioned', 'clicked'])"><i class="align-middle fa fa-cog"></i><?php echo Lang::get('common.settings'); ?></a></li>
            <li><a href="<?php echo URL::action('LogoutController@getLogout'); ?>"  onclick="_gaq.push(['_trackEvent', 'Logout link actioned', 'clicked'])"><i class="align-middle fa fa-sign-out"></i><?php echo Lang::get('common.sign-out'); ?></a></li>
          </ul>
        </li>
        @endif
        <li class="dropdown language-selector hidden-xs">
          <a data-toggle="dropdown" href="" class="btn dropdown-toggle active-language capitalize {{Session::get('locale')}}">{{Session::has('locale') ? Session::get('locale') : 'en'}} <span class="fa fa-globe"></span> <span class="fa fa-angle-down"></span></a><div class="tt-menu profile-menu">
          <ul class="dropdown-menu">
            <li><a href="" onchange="_gaq.push(['_trackEvent', 'Langauge English selected', 'clicked'])" class="languageSelect en" langVal="en">EN</a></li>
            <li><a href="" onchange="_gaq.push(['_trackEvent', 'Langauge Spanish selected', 'clicked'])" class="languageSelect es" langVal="es">ES</a></li>
            <li><a href="" onchange="_gaq.push(['_trackEvent', 'Langauge French selected', 'clicked'])" class="languageSelect fr" langVal="fr">FR</a></li>
            <li><a href="" onchange="_gaq.push(['_trackEvent', 'Langauge German selected', 'clicked'])" class="languageSelect gr" langVal="gr">GR</a></li>
            <li><a href="" onchange="_gaq.push(['_trackEvent', 'Langauge Japanese selected', 'clicked']) "class="languageSelect ja" langVal="ja">JA</a></li>
            <li><a href="" onchange="_gaq.push(['_trackEvent', 'Langauge Portuguese selected', 'clicked'])" class="languageSelect pt" langVal="pt">PT</a></li>
          </ul>
        </li>
        <li>
          <a href="#" class="img-circle btn">
            <span class="fa fa-bell"></span></a>
          </a>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
    <form class="relative searchbar" id = "magicInputForm" autocomplete="off">
    <a href="#" class="pull-right btn">Narrate a story</a>
      <input id="magicInput1" type="search" class="form-control"  placeholder="Search TripTroop" name = 'searchText' onfocus="_gaq.push(['_trackEvent', 'User Entered string to Search', 'focused'])" />
      <input type = "hidden"  id = "hiddenSort" name = "sortValue"/>
      <input type = "hidden"  id = "hiddenTheme" name = "themeValue"/>
      <input type = "hidden"  id = "hiddenCountry" name = "countryValue"/>
    </form>
  </nav>
  <nav class="tt-navbar navbar navbar-default hidden">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header pull-left">
      <!-- <button type="button" onclick="_gaq.push(['_trackEvent', 'Main Menu-toggle lower resolution', 'clicked'])" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button> -->
      <a onclick="_gaq.push(['_trackEvent', 'Header Logo Clicked Navigated to Landing Page', 'clicked'])" class="logo" href="{{URL::to('/')}}"><img src="/assets/images/tt_logo.png" height="40" alt="TripTroop"></a>
    </div>

    <ul class="list-unstyled nav-right pull-right clearfix nav">
    <!--
        <a onclick="_gaq.push(['_trackEvent', 'signup', 'opened sign up page'])"class="pull-left" href="{{ URL::to( 'sign-up') }}"><?php echo Lang::get('common.sign-up')?></a>
        <a onclick="_gaq.push(['_trackEvent', 'button3', 'opened sign in page'])" class="pull-left" href="{{ URL::to( 'login') }}"><?php echo Lang::get('common.sign-in')?></a>
      -->
      @if (Auth::user() == false)
      <li class="align-middle non-logged-link pull-right">
        <a onclick="_gaq.push(['_trackEvent', 'Opened signin/signup dialog', 'clicked'])" class="pull-left"><?php echo Lang::get('common.sign-in')?> / <?php echo Lang::get('common.sign-up')?></a>
      </li>
      @else
      <li class="dropdown pull-right">
        <a class="pad0" onclick="_gaq.push(['_trackEvent', 'Header User Dropdown Options actioned', 'clicked'])">
          @if(DB::table('profile')->where('id', Auth::user()['id'])->count() && DB::table('profile')->where('id', Auth::user()['id'])->pluck('clPath'))
          <img height="60" width="60" class="block" name = "profilePicture" id ="profileImg" src="https://res.cloudinary.com/triptroop/image/upload/w_90,h_90,c_fill/v1431406595/{{DB::table('profile')->where('id', Auth::user()['id'])->pluck('clPath')}}" alt="change profile" />
          @else
          <img height="60" name = "profilePicture" id ="profileImg" src="https://res.cloudinary.com/triptroop/image/upload/w_90/v1431406595/assets/avatar.png" height="37" alt="change profile" />
          @endif  
          <i class="fa fa-bars"></i>
        </a>
        <div class="tt-menu profile-menu">
          <ul class="list-unstyled clearfix">
            <li><a href="/profile" onclick="_gaq.push(['_trackEvent', 'Profile Page link actioned', 'clicked'])"><i class="align-middle fa fa-user"></i><?php echo Lang::get('common.my-profile'); ?></a></li>
            <li><a href="/profile/drafts" onclick="_gaq.push(['_trackEvent', 'Draft Page link actioned', 'clicked'])"><i class="align-middle fa fa-file-o"></i><?php echo Lang::get('common.drafts'); ?></a></li>
            <li><a href="/profile/dreamlist" onclick="_gaq.push(['_trackEvent', 'Dreamlist Page link actioned', 'clicked'])"><i class="align-middle fa fa-bookmark-o"></i><?php echo Lang::get('common.dreamlist'); ?></a></li>
            <li><a href="/settings" onclick="_gaq.push(['_trackEvent', 'Settings Page link actioned', 'clicked'])"><i class="align-middle fa fa-cog"></i><?php echo Lang::get('common.settings'); ?></a></li>
            <li><a href="<?php echo URL::action('LogoutController@getLogout'); ?>"  onclick="_gaq.push(['_trackEvent', 'Logout link actioned', 'clicked'])"><i class="align-middle fa fa-sign-out"></i><?php echo Lang::get('common.sign-out'); ?></a></li>
          </ul>
        </div>
      </li>
      @endif
      <li class="dropdown language-selector pull-right hidden-xs">
        <a href="" class="active-language capitalize {{Session::get('locale')}}">{{Session::has('locale') ? Session::get('locale') : 'en'}}<span class="fa fa-angle-down"></span></a>
        <div class="tt-menu profile-menu">
          <ul class="list-unstyled clearfix">
            <li><a href="" onchange="_gaq.push(['_trackEvent', 'Langauge English selected', 'clicked'])" class="languageSelect en" langVal="en">EN</a></li>
            <li><a href="" onchange="_gaq.push(['_trackEvent', 'Langauge Spanish selected', 'clicked'])" class="languageSelect es" langVal="es">ES</a></li>
            <li><a href="" onchange="_gaq.push(['_trackEvent', 'Langauge French selected', 'clicked'])" class="languageSelect fr" langVal="fr">FR</a></li>
            <li><a href="" onchange="_gaq.push(['_trackEvent', 'Langauge German selected', 'clicked'])" class="languageSelect gr" langVal="gr">GR</a></li>
            <li><a href="" onchange="_gaq.push(['_trackEvent', 'Langauge Japanese selected', 'clicked']) "class="languageSelect ja" langVal="ja">JA</a></li>
            <li><a href="" onchange="_gaq.push(['_trackEvent', 'Langauge Portuguese selected', 'clicked'])" class="languageSelect pt" langVal="pt">PT</a></li>
          </ul>
        </div>
      </li>
      <li class="pull-right">
        <a href="{{URL::to('search')}}" class="btn" onclick="_gaq.push(['_trackEvent', 'Search Page link actioned', 'clicked'])"><i title="Search" class="fa fa-search"></i></a>
      </li>
      <li class="pull-right narrate">
        <a href="{{URL::to('narrate')}}" class="btn btn-narrate" onclick="_gaq.push(['_trackEvent', 'Search Page link actioned', 'clicked'])"><i class="fa fa-plus"></i> Narrate a story</a>
      </li>
      <li class="pull-right" ><button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button></li>
      @if (Request::path() == 'narrate' || Request::is('edit/*'))
      <li class="align-middle btns positive pull-right" onclick="_gaq.push(['_trackEvent', 'Narrate Story Save or Publish actioned', 'clicked'])">
        <a class="pull-left" href="" onclick="saveStory()">{{Lang::get('common.save')}}</a> 
        <a class="pull-left" href="" onclick="publishStory()">{{Lang::get('common.publish')}}</a>
      </li>
      @endif
      @if (Request::is('read/*'))
      @if ($readStory[0]->userid == Auth::user()['id'])
      <li class="align-middle btns positive pull-right">
        <a id = "{{$readStory[0]->storyid}}"  class="pull-left del btn-danger" onclick="_gaq.push(['_trackEvent', 'Delete draft actioned view from Draft', 'clicked']); deleteClick(this.id); ">Delete</a>
        <a href="/edit/{{$readStory[0]->storyid}}" class="pull-left btn-info" onclick="_gaq.push(['_trackEvent', 'Edit draft actioned view from Draft', 'clicked'])">Edit</a>
      </li>
      @endif
      @endif
    </ul>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="list-unstyled list-inline nav-left mar0">
        <li class="{{ Request::is( '/') ? 'active' : '' }}">
          <a class="animsition-link home" title="Home" onclick="_gaq.push(['_trackEvent', 'Home Page link Actioned', 'clicked'])" href="{{ URL::to( '/') }}"><span><?php echo Lang::get('common.home')?></span></a>
        </li><li class="{{ Request::is( 'popular') ? 'active' : '' }}">
        <a class="animsition-link popular-stories" title="Popular Stories" onclick="_gaq.push(['_trackEvent', 'Popular Story link Actioned', 'clicked'])" href="{{ URL::to( 'popular') }}"><span><?php echo Lang::get('common.popular-stories')?></span></a>
      </li><li class="{{ Request::is( 'storytellers') ? 'active' : '' }}">
      <a class="animsition-link storytellers" title="Storytellers"  onclick="_gaq.push(['_trackEvent', 'Storytellers link Actioned', 'clicked'])"  href="{{ URL::to( 'storytellers') }}"><span><?php echo Lang::get('common.storytellers')?></span></a>
    </li><li class="{{ Request::is( 'themes', 'themes/*') ? 'active' : '' }} dropdown">
    <a class="animsition-link themes" title="Themes" href="{{ URL::to( 'themes') }}" onclick="_gaq.push(['_trackEvent', 'Themes link Actioned', 'clicked'])"><span><?php echo Lang::get('common.themes')?></span><i class="fa fa-angle-down"></i></a>
    <div class="tt-menu mega-menu dropup text-left">
      <ul class="list-unstyled clearfix">
        <li class="mega-menu-column pull-left">
          <ul class="clearfix list-unstyled">
            <li><a class="a" href="{{ URL::to( 'themes/adventure') }}" onclick="_gaq.push(['_trackEvent', 'Adventure Theme Actioned', 'clicked'])"><?php echo Lang::get('common.advanture')?></a></li>
            <li><a class="b" href="{{ URL::to( 'themes/arts-and-liesure') }}" onclick="_gaq.push(['_trackEvent', 'Art and Liesure Theme Actioned', 'clicked'])"><?php echo Lang::get('common.arts-lieasure')?></a></li>
            <li><a class="c" href="{{ URL::to( 'themes/backpacking') }}" onclick="_gaq.push(['_trackEvent', 'Backpacking Theme Actioned', 'clicked'])"><?php echo Lang::get('common.backpacking')?></a></li>
            <li><a class="d" href="{{ URL::to( 'themes/beaches') }}" onclick="_gaq.push(['_trackEvent', 'Beaches Theme Actioned', 'clicked'])"><?php echo Lang::get('common.beach')?></a></li>
            <li><a class="e" href="{{ URL::to( 'themes/budget-travel') }}" onclick="_gaq.push(['_trackEvent', 'Adventure Theme Actioned', 'clicked'])"><?php echo Lang::get('common.budget-traval')?></a></li>
            <li><a class="f" href="{{ URL::to( 'themes/coasts-and-islands') }}" onclick="_gaq.push(['_trackEvent', 'Coast &amp; Islands Theme Actioned', 'clicked'])"><?php echo Lang::get('common.coasts-islands')?></a></li>
            <li><a class="g" href="{{ URL::to( 'themes/diving-and-snorkelling') }}" onclick="_gaq.push(['_trackEvent', 'Diving &ampp; Snorkelling Theme Actioned', 'clicked'])"><?php echo Lang::get('common.diving-snorkelling')?></a></li>
            <li><a class="h" href="{{ URL::to( 'themes/ecotourism') }}" onclick="_gaq.push(['_trackEvent', 'Ecotourisam Theme Actioned', 'clicked'])"><?php echo Lang::get('common.ecotourism')?></a></li>
          </ul>
        </li> 
        <li class="mega-menu-column pull-left">
          <ul class="clearfix list-unstyled">
            <li><a class="i" href="{{ URL::to( 'themes/family-travel') }}" onclick="_gaq.push(['_trackEvent', 'Family &amp; Travel Theme Actioned', 'clicked'])"><?php echo Lang::get('common.family-travel')?></a></li>
            <li><a class="j" href="{{ URL::to( 'themes/festival-and-events') }}" onclick="_gaq.push(['_trackEvent', 'Festival &amp; Events Theme Actioned', 'clicked'])"><?php echo Lang::get('common.festival-events')?></a></li>
            <li><a class="k" href="{{ URL::to( 'themes/film-and-television') }}" onclick="_gaq.push(['_trackEvent', 'Film &amp; Telivision Theme Actioned', 'clicked'])"><?php echo Lang::get('common.film-television')?></a></li>
            <li><a class="l" href="{{ URL::to( 'themes/food-and-drink') }}" onclick="_gaq.push(['_trackEvent', 'Food &amp; Drink Theme Actioned', 'clicked'])"><?php echo Lang::get('common.food-drink')?></a></li>
            <li><a class="m" href="{{ URL::to( 'themes/gear-and-tech') }}" onclick="_gaq.push(['_trackEvent', 'Gear &amp; Tech Theme Actioned', 'clicked'])"><?php echo Lang::get('common.gear-tech')?></a></li>
            <li><a class="n" href="{{ URL::to( 'themes/honeymoon-and-romance') }}" onclick="_gaq.push(['_trackEvent', 'Honymoon &amp; Romance Theme Actioned', 'clicked'])"><?php echo Lang::get('common.honymoon-romance')?></a></li>
            <li><a class="o" href="{{ URL::to( 'themes/luxury-travel') }}" onclick="_gaq.push(['_trackEvent', 'Luxury &amp; Travel Theme Actioned', 'clicked'])"><?php echo Lang::get('common.luxury-travel')?></a></li>
            <li><a class="p" href="{{ URL::to( 'themes/music') }}" onclick="_gaq.push(['_trackEvent', 'Music Theme Actioned', 'clicked'])"><?php echo Lang::get('common.music')?></a></li>
          </ul>
        </li>
        <li class="mega-menu-column pull-left">
          <ul class="clearfix list-unstyled">
            <li><a class="q" href="{{ URL::to( 'themes/off-the-beaten-track') }}"  onclick="_gaq.push(['_trackEvent', 'Off Bearen Track Theme Actioned', 'clicked'])"><?php echo Lang::get('common.off-beaten-track')?></a></li>
            <li><a class="r" href="{{ URL::to( 'themes/planes-and-trains') }}"  onclick="_gaq.push(['_trackEvent', 'Planes &amp; Trains Theme Actioned', 'clicked'])"><?php echo Lang::get('common.planes-trains')?></a></li>
            <li><a class="s" href="{{ URL::to( 'themes/road-trips') }}" onclick="_gaq.push(['_trackEvent', 'Road Trips Theme Actioned', 'clicked'])"><?php echo Lang::get('common.road-trips')?></a></li>
            <li><a class="t" href="{{ URL::to( 'themes/round-the-world-travel') }}" onclick="_gaq.push(['_trackEvent', 'Round World Travel Theme Actioned', 'clicked'])"><?php echo Lang::get('common.round-world-travel')?></a></li>
            <li><a class="u" href="{{ URL::to( 'themes/travel-photograhpy') }}" onclick="_gaq.push(['_trackEvent', 'Photoghraphy Theme Actioned', 'clicked'])"><?php echo Lang::get('common.travel-photoghraphy')?></a></li>
            <li><a class="v" href="{{ URL::to( 'themes/travel-shopping') }}" onclick="_gaq.push(['_trackEvent', 'Travel &amp; Shopping Theme Actioned', 'clicked'])"><?php echo Lang::get('common.travel-shopping')?></a></li>
            <li><a class="w" href="{{ URL::to( 'themes/walking-and-trekking') }}" onclick="_gaq.push(['_trackEvent', 'Walking &amp; Trekking Theme Actioned', 'clicked'])"><?php echo Lang::get('common.walking-trekking')?></a></li>
            <li><a class="x" href="{{ URL::to( 'themes/wildlife-and-nature') }}" onclick="_gaq.push(['_trackEvent', 'WildLife &amp; Nature Theme Actioned', 'clicked'])"><?php echo Lang::get('common.wildlife-nature')?></a></li>
          </ul>
        </li>
      </ul><!-- dropdown-menu -->
    </div>
  </li>
</ul>
</div>
</nav>
</header>
@if (Request::path() == '/' && !Auth::user())
<span class="hidden"><?php $random = rand(1,18); echo $random; ?></span>
<div class="mast-head relative" style="background-image: url(http://res.cloudinary.com/triptroop/image/upload/w_1200/v1432885289/storyCover/1/14/054f90797ba5b8d6d8f464889542fdfc.jpg)">
  <article class="intro text-center">
    <!--<div class="mast-head relative" data-bg="https://s3-us-west-2.amazonaws.com/triptroop/assets/images/landing/9.jpeg">-->
    <h1 style="font-size: 120px;font-family: great vibes;"><small style="color: white;display:block;font-size: 40%;">It's not about</small>the destination<small style="color: white;display:block;font-size: 40%;">it's about</small>the journey</h1>
    <h2>{{Lang::get('home.tagline-home-stories')}}</h2>
    <h3>{{Lang::get('home.tagline-share-stories')}}</h3>
    <p>                         
      <a class="btn btn-success non-logged-link" href="{{ URL::to( 'sign-up') }}"  onclick="_gaq.push(['_trackEvent', 'SignUp Actioned from Header', 'clicked'])">{{Lang::get('home.get-started')}}</a>
      <br>
      <small class="hide">{{Lang::get('home.or')}} <a href="#"  onclick="_gaq.push(['_trackEvent', 'Exloare stories Actioned from User Flyout', 'clicked'])"><strong>{{Lang::get('home.exporer-stories')}}</strong></a></small>
    </p>
  </article>
  <div class="absolute author-info hide">
    <a href="#"  onclick="_gaq.push(['_trackEvent', 'User Profile Actioned from User flyout', 'clicked'])"><strong>Akshay Sonawane</strong></a> <span>on jan 18, 2015</span>
  </div>
  <span class="absolute is-scroll-to-content"  onclick="_gaq.push(['_trackEvent', 'ScrollTop Action perfomed', 'clicked'])"><i class="fa fa-chevron-down"></i></span>
</div>
@endif