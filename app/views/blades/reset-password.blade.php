@extends('../layouts/non-login_layout')

<div class="form-wrapper text-center login-wrapper"  onload="_gaq.push(['_trackEvent', 'On Reset Password View', 'loaded'])">
    <section class="page-container">
        <h1><?php echo Lang::get('update-password.reset-your-password')?></h1>

        <p class="eta centered intro"><?php echo Lang::get('update-password.reset-password-info')?></p>

        <!-- {{ Form::open(array('url' => 'registration','class' => 'form centered')) }} -->
        {{ Form::open(array('url' => '', 'data-parsley-validate' => '')) }}
        <div class="form clearfix">
            <div class="form-group relative">
                <i class="ion-key"></i>
                {{ Form::password('reset-pw', array('class' => 'forgot-password-email-input', 'placeholder' => 'New Password', 'required' => '', 'title' => 'Password must contain at least 6 characters, including numbers', 'pattern' => '(?=.*\d)(?=.*[a-z]).{6,}', 'onchange' => 'this.setCustomValidity(this.validity.patternMismatch ? this.title : ""); if(this.checkValidity()) form.pwd2.pattern = this.value;', 'name' => 'pw1')) }}
            </div>
            <div class="form-group relative">
                <i class="ion-key"></i>
                {{ Form::password('confirm-reset-pw', array('class' => 'forgot-password-email-input', 'placeholder' => 'Confirm Password', 'required' => '', 'title' => 'Please enter the same Password as above.', 'pattern' => '(?=.*\d)(?=.*[a-z]).{6,}', 'onchange' => 'this.setCustomValidity(this.validity.patternMismatch ? this.title : "");', 'name' => 'pw2')) }}
            </div>            
                
            <div class="btn-wrapper relative">
                {{ Form::submit('Recover Password', array('class' => 'btn btn-primary btn-block')) }}
            </div>
        </div>
        <!-- sign in using email:: end -->

        {{ Form::close() }}     

    </section>
</div>