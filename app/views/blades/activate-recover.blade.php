@extends("layouts.non-login_layout")
@section('content')
<div class="blurbg"></div>
@include('/shared/opaque_header')
<div class="form-wrapper text-center login-wrapper" onload="_gaq.push(['_trackEvent', 'On Recover Password View', 'loaded'])">
    <section class="page-container">
        <h1><img src="/assets/images/logo_final.png" alt="TripTroop" height="174"></h1>
        <p class="hide eta centered intro"><?php echo Lang::get('update-password.forgot-password-info')?></p>
        <!-- {{ Form::open(array('url' => 'registration','class' => 'form centered')) }} -->
        {{ Form::open(array('id' => 'activateRecoverForm', 'data-parsley-validate' => '')) }}
        <div class="form clearfix">
            
            <div class="controls">
                {{ Form::email('email',$eid, array('class' => 'email-input form-control', 'placeholder' => 'Email', 'required' => '', 'readonly')) }}
                {{ Form::password('pswd', array('class' => 'password-input form-control', 'placeholder' => 'New Password', 'required' => '', 'title' => 'Password must contain at least 6 characters, including numbers', 'pattern' => '(?=.*\d)(?=.*[a-z]).{6,}', 'onchange' => 'this.setCustomValidity(this.validity.patternMismatch ? this.title : ""); if(this.checkValidity()) form.pwd2.pattern = this.value;')) }}
                {{ Form::password('cpswd', array('class' => 'password-input form-control', 'placeholder' => 'Confirm New Password', 'required' => '', 'title' => 'Please enter the same Password as above.', 'pattern' => '(?=.*\d)(?=.*[a-z]).{6,}', 'onchange' => 'this.setCustomValidity(this.validity.patternMismatch ? this.title : "");')) }}
                {{ Form::submit('Confirm Password', array('class' => 'btn btn-primary btn-block')) }}
            </div>
        </div>
        <!-- sign in using email:: end -->
        {{ Form::close() }}     
    </section>
</div>
<footer class="text-right footer">
    <ul class="list-unstyled list-inline">
        <li><a href="{{ URL::to( '/terms') }}"><?php echo Lang::get('sign-up.terms-services')?></a></li>
        <li><a href="{{ URL::to( '/privacy') }}">Privacy</a></li>
    </ul>
</footer>
@stop