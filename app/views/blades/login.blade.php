@extends("layouts.non-login_layout")
@section('content')
{{Session::put('login_gmail_id', 'in')}}
<!-- Flash notice :: start -->
@if(Session::has('login_error'))
<div class="alert alert-warning flash-notice">
    <a href="#" class="close" data-dismiss="alert">&times;</a>
    <div id="flash_notice">{{ Session::get('login_error') }}</div>
</div>
@endif
<div class="col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
    <div class="row">
        <div class="login-box">
            <div class="with-email col-md-12">
                <h3 class="h3 text-center">Login with email</h3>
                {{ Form::open(array( 'id' => 'loginForm', 'class' => 'parsleyForm', 'parsley-validate' => 'true')) }}
                @foreach($errors->all() as $message)
                <ul class="errors">
                    <li>{{ $message }}</li>
                </ul>
                @endforeach
                <div class="form-group">
                    {{ Form::email('emailid','', array('class' => 'email-input form-control', 'placeholder' => 'Email', 'required' => '')) }}
                </div>
                <div class="form-group">
                    {{ Form::password('password', array('class' => 'password-input form-control','placeholder' => 'Password', 'title' => 'Password must contain at least 6 characters, including numbers', 'pattern' => '(?=.*\d)(?=.*[a-z]).{6,}', 'onchange' => 'this.setCustomValidity(this.validity.patternMismatch ? this.title : ""); if(this.checkValidity()) form.pwd2.pattern = this.value;', 'name' => 'setpwd')) }}
                </div>
                {{ Form::submit('Sign in', array('class' => 'btn btn-primary btn-action btn-block')) }}
                <p class="small">
                    {{ HTML::link('recover-password','Forgotten your password?',array('target' => '_self', 'class' => 'recover-pwd'))}}<br>
                    <a class="hide" href="/users/confirmation/new">Didn't receive confirmation instructions?</a>
                </p>
                {{ Form::close() }}
            </div>
            <div class="with-social col-md-6 login hidden">
                <h3 class="h3 text-center">Login with social media</h3>
                <div class="social-btns">
                    <a class="btn-block btn-facebook btn"  href="{{url('/login/fb')}}"><i class="fa fa-facebook-f"></i>Facebook</a>
                    <a class="btn-block btn-twitter btn" href=""><i class="fa fa-twitter"></i>Twitter</a>
                    <a class="btn-block btn-google-plus btn" href="{{url('/login/gmail')}}"><i class="fa fa-google-plus"></i>Google Plus</a>
                </div>
            </div>
        </div>
        <div class="col-md-12 text-center">
            Don't have an account yet? <a href="/sign-up">Sign up</a>
        </div>
    </div>
</div>



<footer class="text-right footer">
    <ul class="list-unstyled list-inline">
        <li><a href="{{ URL::to( '/terms') }}"><?php echo Lang::get('sign-up.terms-services')?></a></li>
        <li><i class="fa fa-circle"></i></li>
        <li><a href="{{ URL::to( '/privacy') }}">Privacy</a></li>
    </ul>
</footer>
@stop