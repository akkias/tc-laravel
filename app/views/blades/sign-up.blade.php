@extends("layouts.non-login_layout")
@section('content')
<!-- Flash notice :: start -->
@if(Session::has('signup_error'))
<div class="alert alert-warning flash-notice">
    <a href="#" class="close" data-dismiss="alert">&times;</a>
    <div id="flash_notice">{{ Session::get('signup_error') }}</div>
</div>
@endif
<!-- Flash notice :: ends -->
<div class="col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4"  onload="_gaq.push(['_trackEvent', 'On Sign UP View', 'loaded'])">
    <div class="row">
        <div class="login-box">
            <div class="with-email col-md-12">
                <h3 class="h3 text-center">Join with email</h3>
                <p class="hide eta centered intro"><?php echo Lang::get('sign-up.login-message')?></p>
                <!-- {{ Form::open(array('url' => 'registration','class' => 'form centered')) }} -->
                {{ Form::open(array('id'=> 'signupForm', 'data-parsley-validate' => '')) }}
                @foreach($errors->all() as $message)
                <ul class="errors">
                    <li>{{ $message }}</li>
                </ul>
                @endforeach
                <div class="err"></div>
               
                <!-- sign up using oauth:: end -->
                <div class="form-group">   
                    {{ Form::text('firstname', Session::has('fname') ? Session::get('fname') :Input::old('firstname'), array('class' => 'first-name-input form-control', 'autocomplete' =>'off', 'placeholder' => 'First name', 'required' => '')) }}
                </div>
                <div class="form-group">   
                    {{ Form::text('lastname',  Session::has('lname') ? Session::get('lname') : Input::old('lastname'), array('class' => 'last-name-input form-control', 'placeholder' => 'Last name', 'required' => '')) }}
                </div>
                <div class="form-group">   
                    {{ Form::email('emailid',Session::has('email') ? Session::get('email') :Input::old('email'), array('class' => 'email-input form-control', 'placeholder' => 'Email', 'required' => '', 'data-parsley-trigger' =>'change', 'data-parsley-type' => 'email')) }}
                </div>
                <div class="form-group">   
                    {{ Form::password('password',array('class' => 'password-input form-control', 'placeholder' => 'Set Password', 'required' => '', 'title' => 'Password must contain at least 6 characters, including numbers', 'pattern' => '(?=.*\d)(?=.*[a-z]).{6,}', 'onchange' => 'this.setCustomValidity(this.validity.patternMismatch ? this.title : "");', 'name' => 'setpwd')) }}
                </div>
                <p>
                    <small class="text-muted">
                        By <?php echo Lang::get('sign-up.create-an-account')?>, <?php echo Lang::get('sign-up.accept-terms')?> Triptroop's <a target="_blank" class="grey-link underline" href="/terms"><?php echo Lang::get('sign-up.terms-services')?></a>.
                    </small>
                </p>
                {{ Form::submit( Session::get('social_action') == true ? 'Confirm' : 'Sign Up',  array('class' => 'btn btn-primary btn-block')) }}
                <small class="terms hide">
                    By <?php echo Lang::get('sign-up.create-an-account')?>, <br><?php echo Lang::get('sign-up.accept-terms')?> Triptroop's <a target="_blank" class="grey-link underline" href="/terms-of-service"><?php echo Lang::get('sign-up.terms-services')?></a>.
                </small>
                <!-- sign up using email:: end -->
                {{ Form::close() }}  
            </div>
            <div class="with-social col-md-6 signup hidden">
                <h3 class="h3 text-center">Join with social media</h3>
                <div class="social-btns">
                    {{Session::put('login_gmail_id', 'up')}}
                    <a class="btn-block btn-facebook btn"  href="{{url('/login/fb/signup')}}"><i class="fa fa-facebook-f"></i>Facebook</a>
                    <a class="btn-block btn-twitter btn" href=""><i class="fa fa-twitter"></i>Twitter</a>
                    <a class="btn-block btn-google-plus btn" href="{{url('/login/gmail')}}"><i class="fa fa-google-plus"></i>Google Plus</a>
                </div>
            </div>
        </div>
        <div class="col-md-12 text-center">
            Already have an account? <a href="/login">Sign in</a>
        </div>
    </div>
</div>


<!-- signup using email:: content ::  ends -->
<footer class="text-right footer">
    <ul class="list-unstyled list-inline">
        <li><a href="{{ URL::to( '/terms') }}"><?php echo Lang::get('sign-up.terms-services')?></a></li>
        <li><i class="fa fa-circle"></i></li>
        <li><a href="{{ URL::to( '/privacy') }}">Privacy</a></li>
    </ul>
</footer>
@stop