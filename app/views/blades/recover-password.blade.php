@extends("layouts.non-login_layout")
@section('content')
<div class="col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4" onload="_gaq.push(['_trackEvent', 'On Recover Password View', 'loaded'])">
    <div class="row">
        <div class="login-box">
            <div class="with-email col-md-12">
                <h3 class="h3 text-center">Forgot your password?</h3>
                <p class="text-muted"><?php echo Lang::get('update-password.forgot-password-info')?></p>
                <br>  
                {{ Form::open(array('id' => 'recoverForm', 'data-parsley-validate' => '')) }}
                <div class="form clearfix">
                    <div class="form-group">
                        {{ Form::email('emailid','', array('class' => 'form-control email-input', 'placeholder' => 'Email', 'required' => '')) }}
                    </div>
                    <a class="back-to pull-right" href="/login"><i class="fa fa-angle-left"></i> Back to login</a>
                    {{ Form::submit('Reset Password', array('class' => 'btn btn-primary btn-action')) }}
                </div>
                <!-- sign in using email:: end -->
                {{ Form::close() }}     
            </div>
        </div>
    </div>
</div>

<footer class="text-right footer">
    <ul class="list-unstyled list-inline">
        <li><a href="{{ URL::to( '/terms') }}"><?php echo Lang::get('sign-up.terms-services')?></a></li>
        <li><i class="fa fa-circle"></i></li>
        <li><a href="{{ URL::to( '/privacy') }}">Privacy</a></li>
    </ul>
</footer>
@stop