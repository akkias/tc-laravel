<div class="row">
  <section class="col-md-3">
    <h2>A</h2>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Afghanistan</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Algeria</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Angola</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Argentina</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Australia</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Austria</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Azerbaijan</a>
    </article>
  </section>
  <section class="col-md-3">
    <h2>B</h2>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Bangladesh</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Belarus</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Belgium</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Bolivia</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Botswana</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)">
      </div><a class="get-cities">Brazil</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)">
      </div><a class="get-cities">Bulgaria</a>
    </article>
  </section>
  <section class="col-md-3">
    <h2>C</h2>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)">
      </div><a class="get-cities">Cambodia</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)">
      </div><a class="get-cities">Canada</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)">
      </div><a class="get-cities">Chile</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)">
      </div><a class="get-cities">China</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)">
      </div><a class="get-cities">Colombia</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)">
      </div><a class="get-cities">Costa Rica</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)">
      </div><a class="get-cities">Curaçao</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)">
      </div><a class="get-cities">Czech Republic</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)">
      </div><a class="get-cities">Côte d'Ivoire</a>
    </article>
  </section>
  <section class="col-md-3">
    <h2>D</h2>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)">
      </div><a class="get-cities">Dominican Republic</a>
    </article>
  </section>
</div>
<div class="row">
  <section class="col-md-3">
    <h2>E</h2>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)">
      </div><a class="get-cities">Ecuador</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)">
      </div><a class="get-cities">Egypt</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)">
      </div><a class="get-cities">El Salvador</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Ethiopia</a>
    </article>
  </section>
  <section class="col-md-3">
    <h2>F</h2>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Finland</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">France</a>
    </article>
  </section>
  <section class="col-md-3">
    <h2>G</h2>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Georgia (country)</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Germany</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Ghana</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Greece</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Guatemala</a>
    </article>
  </section>
  <section class="col-md-3">
    <h2>H</h2>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Hungary</a>
    </article>
  </section>
</div>
<div class="row">
  <section class="col-md-3">
    <h2>I</h2>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        India
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Indonesia</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Iran</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Italy</a>
    </article>
  </section>
  <section class="col-md-3">
    <h2>J</h2>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Jamaica</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Japan</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Jersey</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Jordan</a>
    </article>
  </section>
  <section class="col-md-3">
    <h2>K</h2>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Kenya</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Kuwait</a>
    </article>
  </section>
  <section class="col-md-3">
    <h2>L</h2>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Lebanon</a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">Libya</a>
    </article>
  </section>
</div>
<div class="row">
  <section class="col-md-3">
    <h2>M</h2>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Malaysia
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Martinique
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Mexico
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Mongolia
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Morocco
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Myanmar
      </a>
    </article>
  </section>
  <section class="col-md-3">
    <h2>N</h2>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Nepal
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Netherlands
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        New Zealand
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Nicaragua
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Nigeria
      </a>
    </article>
  </section>
  <section class="col-md-3">
    <h2>P</h2>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Pakistan
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Palestine
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Panama
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Paraguay
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Peru
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Philippines
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Poland
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Portugal
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Puerto Rico
      </a>
    </article>
  </section>
  <section class="col-md-3">
    <h2>Q</h2>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Qatar
      </a>
    </article>
  </section>
</div>
<div class="row">
  <section class="col-md-3">    
    <h2>R</h2>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Romania
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Russia
      </a>
    </article>
  </section>
  <section class="col-md-3">
    <h2>S</h2>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Saudi Arabia
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Senegal
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Slovenia
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        South Africa
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        South Korea
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Spain
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Sudan
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Sweden
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Switzerland
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Syria
      </a>
    </article>
  </section>
  <section class="col-md-3">
    <h2>T</h2>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Taiwan
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Tanzania
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Thailand
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Tunisia
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Turkey
      </a>
    </article>
  </section>
  <section class="col-md-3">
    <h2>U</h2>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Uganda
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Ukraine
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        United Arab Emirat
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        United Kingdom
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        United States
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Uruguay
      </a>
    </article>
  </section>
</div>
<div class="row">
  <section class="col-md-3">
    <h2>V</h2>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Venezuela
      </a>
    </article>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Vietnam
      </a>
    </article>
  </section>
  <section class="col-md-3">
    <h2>Y</h2>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Yemen
      </a>
    </article>
  </section>
  <section class="col-md-3">
    <h2>Z</h2>
    <article class="wbox place-card">
      <div class="figure" style="background-image: url(https://res.cloudinary.com/hnz6dacyv/image/upload/w_500,c_limit/storyCover/22/3/Dandeli_National_Park_2)"></div>
      <a class="get-cities">
        Zimbabwe
      </a>
    </article>
  </section>  
</div>