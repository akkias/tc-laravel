@extends("layouts.show_layout")
@section('content')
<div class="page-wrapper countries-wrapper">
  @include('/shared/header_new')
  <script src="http://connect.facebook.net/en_US/all.js"></script>
  {{ HTML::script('/assets/javascripts/getPlacesAround.js', array('data-cfasync' => 'false'), true); }}
  <h1 class="h1 page-title capitalize">
    India
    <div class="filter">
      <input id="filterPlace" class="filter" type="search" name="" value="" placeholder="Search city">
      <i class="fa fa-search"></i>
    </div>
  </h1>
  <div class="clearfix" style="padding: 0; max-width: 1170px; margin: 8px auto 20px auto">
    <div class="row">
      <div class="data"></div>
    </div>

    <script id="intro" type="text/x-handlebars-template">
      @{{#each objects}}
      <article class="place-card pull-left relative">
      <div class="tt-name">
        <div style="background: url(@{{cover.source}}) center / cover" class="figure" title="@{{location.city}}"></div>
        <a id="@{{id}}" title="@{{location.city}}">@{{location.city}}</a>
        </div>
      </article>
      @{{/each}}
    </script>
    <button class="btn btn-success" onclick = "getCountryCities('India')">Get country cities</button>
  </div>
</div>
@stop