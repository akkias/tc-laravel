@extends("layouts.show_layout")
@section('content')
<div class="page-wrapper isotope-wrapper">
  @include('/shared/header_new')
  
  PLACES AROUND YOU <BR>
  <button class="btn btn-success" onclick = "getHotelsAround()">Hotels</button>
  <button class="btn btn-success" onclick = "getRestaurants()">Restaurant/Cafe</button>
  <button class="btn btn-success" onclick = "getForts()">Historical Places</button>
  <button class="btn btn-success" onclick = "getMonuments()">Monument</button>
  <button class="btn btn-success" onclick = "getPub()">Pub</button>
  <button class="btn btn-success" onclick = "searchPlace('Pune Hotel')">Search Result</button>
  <br><br><br><br>

  <button class="btn btn-success" onclick = "getCountries()">Get countries</button>
  <button class="btn btn-success" onclick = "getCountryDetails(103742892997739)">Get country details</button>
  <button class="btn btn-success" onclick = "getCountryCities('India')">Get country cities</button>
  <button class="btn btn-success" onclick = "getCityDetails(106078429431815)">Get city</button>
<!--
  you can get the city name from the getCityDetails(id) return value - location.city property
-->
<button class="btn btn-success" onclick = "getCityPlaces('Mumbai')">Get city places</button>
<button class="btn btn-success" onclick = "getPlaceDetails(194477633937567)">Get place details</button>

<div class="data"></div>
<script id="intro" type="text/x-handlebars-template">
  @{{#each objects}}
    <article class="wbox">
      <div class="figure" style="background-image: url(@{{picture.data.url}})" title="@{{name}}"></div>
      <a title=" alt="@{{name}}"">@{{name}}  ----  @{{id}}</a>
    </article>
  @{{/each}}
</script>



@stop