@extends("layouts.show_layout")
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
{{ HTML::script('/bower_components/handlebars/handlebars.js', array('data-cfasync' => 'false'), false); }}
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places" type="text/javascript"></script>
<script src="/assets/javascripts/getPlacesAround.js"></script>


<script>





  var geocoder;
  var map;
  var markers = Array();
  var infos = Array();

  function initialize() {
// prepare Geocoder
geocoder = new google.maps.Geocoder();

// set initial position (New York)
var myLatlng = new google.maps.LatLng(40.7143528,-74.0059731);

var myOptions = { // default map options
  zoom: 14,
  center: myLatlng,
  mapTypeId: google.maps.MapTypeId.ROADMAP
};
map = new google.maps.Map(document.getElementById('googleMap'), myOptions);
//
initAutocomplete();
}

// clear overlays function
function clearOverlays() {
  if (markers) {
    for (i in markers) {
      markers[i].setMap(null);
    }
    markers = [];
    infos = [];
  }
}

// clear infos function
function clearInfos() {
  if (infos) {
    for (i in infos) {
      if (infos[i].getMap()) {
        infos[i].close();
      }
    }
  }
}
var address = "";
// find address function
function findAddress() {
  var address = document.getElementById("gmap_where").value;
// script uses our 'geocoder' in order to find location by address name
if(address != 0) {
  geocoder.geocode( { 'address': address}, function(results, status) {
if (status == google.maps.GeocoderStatus.OK) { // and, if everything is ok

// we will center map
var addrLocation = results[0].geometry.location;
map.setCenter(addrLocation);

// store current coordinates into hidden variables
document.getElementById('lat').value = results[0].geometry.location.lat();
document.getElementById('lng').value = results[0].geometry.location.lng();

// and then - add new custom marker
var addrMarker = new google.maps.Marker({
  position: addrLocation,
  map: map,
  title: results[0].formatted_address
});
} else {
  alert('Geocode was not successful for the following reason: ' + status);
}
})
}


}

// find custom places function
function findPlaces() {

// prepare variables (filter)
var type = document.getElementById('gmap_type').value;
var radius = document.getElementById('gmap_radius').value;
var keyword = document.getElementById('gmap_keyword').value;

var lat = document.getElementById('lat').value;
var lng = document.getElementById('lng').value;
var cur_location = new google.maps.LatLng(lat, lng);

// prepare request to Places
var request = {
  location: cur_location,
  radius: radius,
  types: [type]
};
if (keyword) {
  request.keyword = [keyword];
}

// send request
service = new google.maps.places.PlacesService(map);
service.nearbySearch(request, createMarkers);
}

// create markers (from 'findPlaces' function)
function createMarkers(results, status, pagination) {
//console.log("pagination : " + pagination.hasNextPage);
//console.log(pagination.nextPage());
if (status == google.maps.places.PlacesServiceStatus.OK) {

// if we have found something - clear map (overlays)
clearOverlays();


console.log(results);
var template = Handlebars.compile($("#places-results-list").html());
$(".results-list-container").animate({scrollTop : 0},300);;
$("#results-list").html(template({objects:results}));


//placeDetails("ChIJDTph0yBawokRlYyt-eYKDLQ");
// and create new markers by search result
for (var i = 0; i < results.length; i++) {
  createMarker(results[i]);
}
} else if (status == google.maps.places.PlacesServiceStatus.ZERO_RESULTS) {
  alert('Sorry, nothing is found');
}
}

// creare single marker function
function createMarker(obj) {
//   console.log(obj);
//  if(obj.photos){
//   console.log(obj.photos[0].getUrl({'maxWidth': 300, 'maxHeight': 200}));
//  }
// prepare new Marker object
var mark = new google.maps.Marker({
  position: obj.geometry.location,
  map: map,
  title: obj.name
});
markers.push(mark);

// prepare info window
var infowindow = new google.maps.InfoWindow({
  content: '<img src="' + obj.icon + '" /><font style="color:#000;">' + obj.name + 
  '<br />Rating: ' + obj.rating + '<br />Vicinity: ' + obj.vicinity + '</font>'
});

// add event handler to current marker
google.maps.event.addListener(mark, 'click', function() {
  clearInfos();
  infowindow.open(map,mark);
});
infos.push(infowindow);
}

function placeDetails(placeId){
  var request = {
    placeId: placeId
  };

  var service = new google.maps.places.PlacesService(map);
  service.getDetails(request, function(place, status) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
      console.log(place)
      
      var template = Handlebars.compile($("#embed-places-details").html());
      $(".info-window").html(template(place));

      var marker = new google.maps.Marker({
        map: map,
        position: place.geometry.location
      });
    }
  });
}

function initAutocomplete() {

  var input = document.getElementById('gmap_where');
  var searchBox = new google.maps.places.SearchBox(input);
}

google.maps.event.addDomListener(window, 'load', initialize);

</script>
<div class="page-wrapper countries-wrapper">
  @include('/shared/header_new')
  <style>
    .map-search-form .form-group {
      width: 30%;
      padding-right: 10px;
      float: left;
    }
    .map-search-form .form-group:last-child {
      width: 10%;
      padding: 0;
    }
  </style>
  <div class="result-container">
    <div class="info-overlay">
    </div>
    <div class="info-window-close fixed"><span>&times;</span></div>
    <div class="info-window">
      ad
    </div>
    <div class="results-list pull-right relative">
      <div class="place-searchbar-container">
        <div class="map-search-form">
          <div class="form-group">
            <label class="hidden" for="gmap_type">Type:</label>
            <select id="gmap_type" class="chosen-select" data-placeholder="What are you lookin for?">
              <option value=""></option>
              <option value="amusement_park">amusement park</option>
              <option value="atm">atm</option>
              <option value="bank">bank</option>
              <option value="bar">bar</option>
              <option value="cafe">cafe</option>
              <option value="food">food</option>
              <option value="hospital">hospital</option>
              <option value="library">library</option>
              <option value="police">police</option>
              <option value="restaurants">restaurants</option>
              <option value="store">store</option>
            </select>
          </div>
          <div class="form-group">
            <label class="hidden" for="gmap_where">Where:</label>
            <input placeholder="Search for place" class="form-control place-input" id="gmap_where" type="text" name="gmap_where" onblur = "findAddress()"/>
          </div>
          <div class="button hide">
            <div id="button2" class="button" onclick="findAddress(); return false;">Search for address</div>
            <label for="gmap_keyword">Keyword (optional):</label>
            <input id="gmap_keyword" type="text" name="gmap_keyword" />
          </div>
          <div class="form-group">
            <label class="hidden" for="gmap_radius">Radius:</label>
            <select id="gmap_radius" class="chosen-select">
              <option value="500">Inside 500m</option>
              <option value="1000">Inside 1000m</option>
              <option value="1500">Inside 2500m</option>
              <option value="5000">Inside 5000m</option>
              <option value="5000">Inside 7500m</option>
              <option value="5000">Inside 10000m</option>            
            </select>
          </div>
          <div class="form-group">
            <input type="hidden" id="lat" name="lat" value="40.7143528" />
            <input type="hidden" id="lng" name="lng" value="-74.0059731" />
            <button id="button1" class="btn btn-primary btn-block" onclick="findPlaces(); return false;"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </div>
      <div class="results-list-container">
        <ul class="list-unstyled" id="results-list">
          <li class="get-details">
            <div class="cell relative">
              <img src="/assets/images/1.jpg" alt="" class="img-responsive">
            </div>
            <div class="cell">
              <span class="fav pull-right"><i class="fa fa-bookmark-o"></i> <small>Bookmark</small></span>
              <h3>Food Court</h3>
              <strong class="rating text-center">4.3</strong>
              <small class="block text-muted"><i class="fa fa-map-marker"></i> bandra kurla complex (nr income tax office behind madhava bldg)</small>
            </div>
          </li>
          <li class="get-details">
            <div class="cell relative">
              <img src="/assets/images/1.jpg" alt="" class="img-responsive">
            </div>
            <div class="cell">
              <span class="fav pull-right"><i class="fa fa-bookmark-o"></i> <small>Bookmark</small></span>
              <h3>Food Court</h3>
              <strong class="rating text-center">4.3</strong>
              <small class="block text-muted"><i class="fa fa-map-marker"></i> bandra kurla complex (nr income tax office behind madhava bldg)</small>
            </div>
          </li>
        </ul>
      </div>
      <div id = "loadMapData">
      </div>

    </div>
    <div class="map pull-left">
      <div id="googleMap" style="width:100%;height:100%;"></div>
    </div>
  </div>

  <script id="places-results-list" type="text/x-handlebars-template">
    @{{#each objects}}
    <li class="get-details" id="@{{place_id}}" onclick="placeDetails('@{{place_id}}')">
      <div class="cell relative">
        <img src="@{{icon}}" alt="" class="img-responsive">
      </div>
      <div class="cell">
        <span class="hidden fav pull-right"><i class="fa fa-bookmark-o"></i> <small>Bookmark</small></span>
        <h3>@{{name}}</h3>
        @{{#if rating}}
        <strong class="rating img-circle text-center">@{{rating}}</strong>
        @{{/if}}
        <small class="block text-muted"><i class="fa fa-map-marker"></i> @{{vicinity}}</small>
      </div>
    </li>
    @{{/each}}
  </script>
  <style>
    .gallery {
      height: 330px;
      width: 450px;
    }
    .info-window a {
      color: #000;
    }
    .info-window h1 {
      font-size: 22px;
      margin: 0;
    }
    .info-window h2 {
      font-size: 14px;
      font-weight: normal;
      margin: 15px 0 0;
    }
    .info-window .place-title {
      padding: 25px;
      background: #ff5a5f;
      color: #FFF;
    }
    .place-rating {
      color: #ff5a5f;
      background: #FFF;
      line-height: 32px;
      padding: 0 5px;
      width: 32px;
      height: 32px;
      margin-bottom: 5px;
      margin-right: 5px;
      display: inline-block;
      font-size: 12px;
      text-align: center;
    }
    .info-window .place-meta {
      padding: 25px;
      font-size: 14px;
    }
    .info-window .fa {
      font-size: 18px;
      min-width: 18px;
      text-align: center;
      color: #FF5A5F;
      float: left;
      margin-right: 15px;
    }
    .info-window .fa + span {
      line-height: 1.4;
      margin-left: 15px;
      overflow: hidden;
      display: block;
    }
    .info-window .sep {
      border-top: 1px solid #DDD;
      padding: 25px;
      margin: 25px -25px 0;
    }
    .info-window .subtitle {
      font-size: 20px;
      color: #555;
      margin: 0 0 25px;
    }
    .info-window .review-meta {
      margin-left: 47px;
    }
  </style>
  <script id="embed-places-details" type="text/x-handlebars-template">
    <div class="gallery">
      @{{#each photos}}
          @{{photo_reference}}
      @{{/each}}
    </div>
    <div class="place-title">
      <h1>@{{name}}</h1>
      @{{#if rating}}
      <h2><span class="place-rating img-circle">@{{rating}}</span> @{{user_ratings_total}} reviews</h2>
      @{{/if}}
    </div>
    <div class="place-meta">


      @{{#if formatted_address}}
      <p><i class="fa fa-map-marker"></i> <span>@{{formatted_address}}</span></p>
      @{{/if}}
      @{{#if website}}
      <p><i class="fa fa-globe"></i> <span class="text-truncate"><a href="@{{website}}">@{{website}}</a></span></p>
      @{{/if}}
      @{{#if international_phone_number}}
      <p><i class="fa fa-phone"></i> <span>@{{international_phone_number}}</span></p>
      @{{/if}}

      @{{#if reviews}}
      <div class="sep">
        <h2 class="subtitle">Review summary</h2>
        @{{#each reviews}}
        <div class="review">
          @{{#if profile_photo_url}}
          <img class="img-circle pull-left" src="@{{profile_photo_url}}" height="40" width="40"  />
          @{{else}}
          <img class="img-circle pull-left" src="https://lh3.googleusercontent.com/-YhofeTY_GF0/AAAAAAAAAAI/AAAAAAAAAAA/tOyap83pzpc/s120-c/photo.jpg" height="40" width="40"  />
          @{{/if}}
          <div class="review-meta">
            <p><a href="@{{author_url}}" target="_blank">@{{author_name}}</a></p>
            <p>@{{time}}</p>
            <p>@{{text}}</p>
            <p>@{{rating}}</p>
          </div>
        </div>
        <hr>
        @{{/each}}
      </div>
      @{{/if}}
    </div>
  </script>
</div>
@stop