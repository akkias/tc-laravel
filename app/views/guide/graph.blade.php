<!DOCTYPE html>
<html>
<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script>
  </script>
</head>
<body>
  <script src="http://connect.facebook.net/en_US/all.js"></script>
  <script>

    var currentLat = 13.12;
    var currentLong = 77.61;
    var distance = 5000;

    function getLocation() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
      } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
      }
    }

    function showPosition(position) {
      currentLat = position.coords.latitude;
      currentLong = position.coords.longitude;
    //x.innerHTML = "Latitude: " + position.coords.latitude + 
    //"<br>Longitude: " + position.coords.longitude;  
  }

  getLocation();
  
  window.fbAsyncInit = function() {
    FB.init({
        appId      : '930261146992571', // App ID
        channelUrl : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
        status     : true, // check login status
        cookie     : true, // enable cookies to allow the server to access the session
        xfbml      : true  // parse XFBML
      });
    // this shouldn't be called directly, but instead should be initiated with a user click event
    FB.login(function(response) {
      if (response.authResponse) {

        console.log('Access Token: ' + response.authResponse.accessToken);
        
        function getPlaceDetails(id){
          FB.api('/'+ id,  {fields: 'picture, id, name, location, category_list, category, about, description,ratings'}, function(response) {
            console.log( "place info" );
            console.log(response);
          });
        }
        console.log(response);
      } else {
        console.log('User cancelled login or did not fully authorize.');
      }
    });

  };

  (function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s); js.id = id;
   js.src = "//connect.facebook.net/en_US/sdk.js";
   fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));





  function getHotelsAround(){
    FB.api('/search?q=hotels&type=place&center=' + currentLat + ','+currentLong+ '&distance='+ distance, function(response) {
      var arr = jQuery.grep(response.data, function( n, i ) {
        var catCheck =  false;
        if(n.category_list){
          catCheck = categoryCheck(n.category_list, "Hotel");
        }
        return ( n.category == "Hotel" || catCheck);
      });
      console.log( "hotels around you" );
      console.log( arr);
    });
  }


  function getRest(){
    FB.api('/search?q=Restaurant&type=place&center=' + currentLat + ','+currentLong+ '&distance='+ distance, function(response) {
      var arr = jQuery.grep(response.data, function( n, i ) {
        var catCheck =  false;
        if(n.category_list){
          catCheck = categoryCheck(n.category_list, "Restaurant/cafe");
        }
        return ( n.category == "Restaurant/cafe" || catCheck);
      });
      console.log( "rests around you" );
      console.log( arr);
    });
  }


  function getForts(){
    FB.api('/search?q=Fort&type=place&center=' + currentLat + ','+currentLong+ '&distance='+ distance, function(response) {
      var arr = jQuery.grep(response.data, function( n, i ) {
        var catCheck =  false;
        if(n.category_list){
          catCheck = categoryCheck(n.category_list, "Historical Place");
        }
        return ( n.category == "Historical Place" || catCheck );
      });
      console.log( "forts around you" );
      console.log( arr);
    });
  }

  function getMonuments(){
    FB.api('/search?q=Monument&type=place&center=' + currentLat + ','+currentLong+ '&distance='+ distance, function(response) {
      var arr = jQuery.grep(response.data, function( n, i ) {
        var catCheck =  false;
        if(n.category_list){
          catCheck = categoryCheck(n.category_list, "Monument");
        }
        return ( n.category == "Monument" || catCheck );
      });
      console.log( "monuments around you" );
      console.log( arr);
    });
  }

  function searchPlace(str){
    FB.api('/search?q='+ str + '&type=place', function(response) {
      var arr = jQuery.grep(response.data, function( n, i ) {
//    return ( n.category == "Restaurant/cafe" || n.category == "Hotel" || n.category == "Local Business" );
return ( n);
});
      console.log( "search result" );
      console.log( arr);
    });
  }



//browse 

function getCountries(){
  FB.api('/search?q=Country&type=place&center=37.76,-122.427&distance=', {fields: 'picture, id, name, location, category_list, category'},function(response) {
    var arr = jQuery.grep(response.data, function( n, i ) {
      return ( n.category == "Country" );
    });
    console.log( "country list" );
    console.log( arr);
  });

}

function getCountryDetails(id){
  FB.api('/'+ id,  {fields: 'picture, id, name, location, category_list, category, about, description'}, function(response) {
    console.log( "Country info" );
    console.log(response);
  });
}

function checkCountryCity(location, country){
  if(location.country == country ){
    return true;
  }
  return false;
}

function categoryCheck1(obj, check){
  var objLength = obj.length;
  if(objLength){
    for(i = 0 ; i <  objLength ; i++){
      if(obj[i].name == check ){
        return true;
      }
    }
  }
}

function getCountryCities(country){
  //alert(country);
  FB.api('/search?q='+country+'&type=place', function(response) {
    var arr = jQuery.grep(response.data, function( n, i ) {
      var countryCheck =  false;
      if(n.location){
        countryCheck = checkCountryCity(n.location, country);
      }
      var cityCheck =  false;
      if(n.category_list){
        cityCheck = categoryCheck(n.category_list, "City");
      }
      return ( n.category == "City" && cityCheck && countryCheck);
    });
    console.log( "get country cities" );
    console.log( arr);
    console.log(response);
  });

}

function getCityDetails(id){
  FB.api('/'+ id,  {fields: 'picture, id, name, location, category_list, category, about, description'}, function(response) {
    console.log( "City info" );
    console.log(response);
  });
}


function getPlace(city, placeType){
  FB.api('/search?q='+ city + ' ' + placeType +'&type=place', function(response) {
    var arr = jQuery.grep(response.data, function( n, i ) {
      var catCheck =  false;
      if(n.category_list){
        catCheck = categoryCheck(n.category_list, placeType);
      }
      return ( n.category == placeType || catCheck);
    });
    console.log( placeType + " in " + city );
    console.log( arr);
  });

}

function getCityPlaces(city){
  console.log(city + " places");
  getPlace(city, "Hotel");
  getPlace(city, "Restaurant/Cafe");
  getPlace(city, "Family Style Restaurant");
  getPlace(city, "Bar");
  getPlace(city, "Pub");
  getPlace(city, "Hospital");
  getPlace(city, "Hospital/Clinic");
  getPlace(city, "Concert Venue");
  getPlace(city, "Movie Theater");
  getPlace(city, "Shopping Mall");
  getPlace(city, "Historical Place");
  getPlace(city, "Hindu Temple");
  getPlace(city, "Monument");
  getPlace(city, "Church");
  getPlace(city, "Religious center");
  getPlace(city, "Religious Organization");
  getPlace(city, "Club");
  getPlace(city, "Sports/recreation");
  getPlace(city, "Sports club");
  getPlace(city, "Library");
  getPlace(city, "Boating");
  getPlace(city, "Non-Governmental Organization (NGO)");
  getPlace(city, "History Museum");
  getPlace(city, "Modern Art Museum");
  getPlace(city, "Museum");
  getPlace(city, "Park");

}




</script>
<div id="div1" ><h2>Let jQuery AJAX Change This Text</h2></div>

<button onclick = "cl()">Get External Content</button>
PLACES AROUND YOU <BR>
<button onclick = "getHotels()">Hotels</button>
<button onclick = "getForts()">Historical Places</button>
<button onclick = "getMonuments()">Monument</button>
<button onclick = "getRest()">Restaurant/Cafe</button>
<button onclick = "getPub()">Pub</button>
<button onclick = "searchPlace('Pune Hotel')">Search Result</button>
<br><br><br><br>

<button onclick = "getCountries()">Get countries</button>
<button onclick = "getCountryDetails(103742892997739)">Get country details</button>
<button onclick = "getCountryCities('United Kingdom')">Get country cities</button>
<button onclick = "getCityDetails(106078429431815)">Get city</button>
<!--
  you can get the city name from the getCityDetails(id) return value - location.city property
-->
<button onclick = "getCityPlaces('Mumbai')">Get city places</button>
<button onclick = "getPlaceDetails(194477633937567)">Get place details</button>



</body>
</html>
