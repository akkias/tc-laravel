@extends("layouts.show_layout")
@section('content')
<div class="page-wrapper countries-wrapper">
  @include('/shared/header_new')
  <h1 class="h1 page-title">
    <a class="load-all-countries hide" onclick="loadAllCountries()">//</a>
    <span class="dummy-title">ALL DESTINATIONS ON TRIPTROOP.ME</span>
    <div class="filter">
    <input id="filterPlace" class="filter" type="search" name="" value="" placeholder="Filter">
      <i class="fa fa-search"></i>
    </div>
  </h1>
  <div class="clearfix" style="margin-top: 20px;padding: 0; max-width: 1170px; margin: 20px auto">
    <div class="row">
      <div class="data"></div>
    </div>
    <script id="intro" type="text/x-handlebars-template">
      @{{#each objects}}
      <div class="col-md-3 place-card">
        <article class="wbox">
          <div style="background: url(@{{picture.data.url}}) center / cover" class="figure" title="@{{location.city}}"></div>
          <a id="@{{id}}" title="@{{location.city}}">@{{location.city}}</a>
        </article>
      </div>
      @{{/each}}
    </script>

    <div id="loadCountries"></div>
    <div id="loadCities"></div>
    <div id="loadPlace"></div>
  </div>
</div>
@stop