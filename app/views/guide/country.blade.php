@extends("layouts.show_layout")
@section('content')
<div class="page-wrapper countries-wrapper">
  @include('/shared/header_new')
  <h1 class="h1 page-title capitalize">
    India
  </h1>
  <div class="clearfix" style="padding: 0; max-width: 1170px; margin: 8px auto 20px auto">
    <div class="row">
      <div class="data"></div>
    </div>
    <script id="intro" type="text/x-handlebars-template">
      @{{#each objects}}
      <article class="place-card pull-left relative">
      <div class="tt-name">
        <div style="background: url(@{{cover.source}}) center / cover" class="figure" title="@{{location.city}}"></div>
        <a id="@{{id}}" title="@{{location.city}}">@{{location.city}}</a>
        </div>
      </article>
      @{{/each}}
    </script>
    <button id="adsadas" class="btn btn-success" onclick = "getCountryCities('India')">Get country cities</button>
  </div>
</div>
@stop