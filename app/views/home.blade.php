@extends("layouts.show_layout")
@section('content')
<div class="page-wrapper isotope-wrapper">
	<div class="mast-head text-center" style="margin-top: -56px;height: 650px;background-image: url(/assets/images/hero/b.jpg);background-size: cover;display:table;width: 100%;">
		<div>
			<div>
				<h2>Smart + Simple City Guides</h2>
				<h3>Find the best places to eat, drink, shop, or visit in any city in the world. </h3>
				<a href="/map" class="btn btn-primary">Explore places</a>
			</div>    
		</div>
	</div>
	@include('/shared/header_new')
	<section class="how">
		<div class="container">
			<div class="row text-center">
				<div class="section-title text-center">
					<h2>
						How it works
						<small class="block text-muted">Plan your next trip in one place with the ultimate mobile travel guide</small>
					</h2>
				</div>
				<div class="col-md-3">
					<div class="thumbnail search">
						<i></i>
						<div class="caption">
							<h3>Search</h3>
							<p>Explore cities and find inspiration from like-minded travellers and local experts</p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="thumbnail plan">
						<i></i>
						<div class="caption">
							<h3>Plan</h3>
							<p>Create wish lists with friends of things to help on your next adventure</p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="thumbnail experience">
						<i></i>
						<div class="caption">
							<h3>Experience</h3>
							<p>Immerse yourself with tailored recommendations for hotels, restaurants, activities, attractions, nightlife & more</p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="thumbnail share">
						<i></i>
						<div class="caption">
							<h3>Share</h3>
							<p>Log and share your beautiful travel moments to help others when planning their next trip</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="container-fluid pad0">
		<div class="tt-content pad0 clearfix">
			<section class="col-md-12 isotope-container landing-wrapper loggedIn">
				<div class="scroller clearfix">
					<div class="story-row">
						@include('./story')
					</div>
				</div>
				<div class="text-center landing-story-btns">
					<a href="/all" class="btn">All Stories</a>
					<a href="/popular" class="btn">Popular Stories</a>
				</div>
			</section>
		</div>
		@include('/shared/likers_modal')
	</section>
</div>
@stop