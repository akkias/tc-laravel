@extends("layouts.show_layout")
@section('content')
<section class="page-wrapper catergories-wrap">
	@include('/shared/header_new')
	<header class="tt-header">
		<div class="header-meta">
			<h1>{{Lang::get('common.themes')}}</h1>
			<h2>{{DB::table('stories')->where('published', '1')->count()}} <?php echo Lang::get('common.stories')?></h2>
		</diV>
	</header>
	<div class="container-fluid">
		<div class="row">
			<div class="tt-content clearfix">
			<div class="theme-card relative adventure card-half">
					<a href="{{ URL::to( 'themes/adventure') }}">
						<i class="category-icon"></i>
						<h4><?php echo Lang::get('common.advanture')?></h4>
						<br>
						<small>{{DB::table('stories')->where('category', 'like', '%->cat1->%')->count()}} <?php echo Lang::get('common.stories')?></small>
					</a>
				</div>
				<div class="theme-card relative arts-and-liesure items_wayscroll " data-ease="fadeIn animated">
					<a href="{{ URL::to( 'themes/arts-and-liesure') }}">
						<i class="category-icon"></i>
						<h4><?php echo Lang::get('common.arts-lieasure')?></h4>
						<br>
						<small>{{DB::table('stories')->where('category', 'like', '%->cat2->%')->count()}} <?php echo Lang::get('common.stories')?></small>
					</a>
				</div>
				<div class="theme-card relative backpacking items_wayscroll " data-ease="fadeIn animated">
					<a href="{{ URL::to( 'themes/backpacking') }}">
						<i class="category-icon"></i>
						<h4>{{Lang::get('common.backpacking')}}</h4>
						<br>
						<small>{{DB::table('stories')->where('category', 'like', '%->cat3->%')->count()}} <?php echo Lang::get('common.stories')?></small>
					</a>
				</div>
				<div class="theme-card relative beaches items_wayscroll " data-ease="fadeIn animated">
					<a href="{{ URL::to( 'themes/beaches') }}">
						<i class="category-icon"></i>
						<h4><?php echo Lang::get('common.beach')?></h4>
						<br>
						<small>{{DB::table('stories')->where('category', 'like', '%->cat4->%')->count()}} <?php echo Lang::get('common.stories')?></small>
					</a>
				</div>
				<div class="theme-card relative budget-travel card-half items_wayscroll " data-ease="fadeIn animated">
					<a href="{{ URL::to( 'themes/budget-travel') }}">
						<i class="category-icon"></i>
						<h4><?php echo Lang::get('common.budget-traval')?></h4>
						<br>
						<small>{{DB::table('stories')->where('category', 'like', '%->cat5->%')->count()}} <?php echo Lang::get('common.stories')?></small>
					</a>
				</div>
				<div class="theme-card relative coasts-and-islands items_wayscroll " data-ease="fadeIn animated">
					<a href="{{ URL::to( 'themes/coasts-and-islands') }}">
						<i class="category-icon"></i>
						<h4><?php echo Lang::get('common.coasts-islands')?></h4>
						<br>
						<small>{{DB::table('stories')->where('category', 'like', '%->cat6->%')->count()}} <?php echo Lang::get('common.stories')?></small>
					</a>
				</div>
				<div class="theme-card relative diving-and-snorkelling items_wayscroll " data-ease="fadeIn animated">
					<a href="{{ URL::to( 'themes/diving-and-snorkelling') }}">
						<i class="category-icon"></i>
						<h4><?php echo Lang::get('common.diving-snorkelling')?></h4>
						<br>
						<small>{{DB::table('stories')->where('category', 'like', '%->cat7->%')->count()}} <?php echo Lang::get('common.stories')?></small>
					</a>
				</div>
				<div class="theme-card relative ecotourism items_wayscroll " data-ease="fadeIn animated">
					<a href="{{ URL::to( 'themes/ecotourism') }}">
						<i class="category-icon"></i>
						<h4><?php echo Lang::get('common.ecotourism')?></h4>
						<br>
						<small>{{DB::table('stories')->where('category', 'like', '%->cat8->%')->count()}} <?php echo Lang::get('common.stories')?></small>
					</a>
				</div>
				<div class="theme-card relative family-travel card-half items_wayscroll " data-ease="fadeIn animated">
					<a href="{{ URL::to( 'themes/family-travel') }}">
						<i class="category-icon"></i>
						<h4><?php echo Lang::get('common.family-travel')?></h4>
						<br>
						<small>{{DB::table('stories')->where('category', 'like', '%->cat9->%')->count()}} <?php echo Lang::get('common.stories')?></small>
					</a>
				</div>
				<div class="theme-card relative festival-and-events card-half items_wayscroll " data-ease="fadeIn animated">
					<a href="{{ URL::to( 'themes/festival-and-events') }}">
						<i class="category-icon"></i>
						<h4><?php echo Lang::get('common.festival-events')?></h4>
						<br>
						<small>{{DB::table('stories')->where('category', 'like', '%->cat10->%')->count()}} <?php echo Lang::get('common.stories')?></small>
					</a>
				</div>
				<div class="theme-card relative film-and-television items_wayscroll " data-ease="fadeIn animated">
					<a href="{{ URL::to( 'themes/film-and-television') }}">
						<i class="category-icon"></i>
						<h4><?php echo Lang::get('common.film-television')?></h4>
						<br>
						<small>{{DB::table('stories')->where('category', 'like', '%->cat11->%')->count()}} <?php echo Lang::get('common.stories')?></small>
					</a>
				</div>
				<div class="theme-card relative food-and-drink items_wayscroll " data-ease="fadeIn animated">
					<a href="{{ URL::to( 'themes/food-and-drink') }}">
						<i class="category-icon"></i>
						<h4><?php echo Lang::get('common.food-drink')?></h4>
						<br>
						<small>{{DB::table('stories')->where('category', 'like', '%->cat12->%')->count()}} <?php echo Lang::get('common.stories')?></small>
					</a>
				</div>
				<div class="theme-card relative gear-and-tech items_wayscroll " data-ease="fadeIn animated">
					<a href="{{ URL::to( 'themes/gear-and-tech') }}">
						<i class="category-icon"></i>
						<h4><?php echo Lang::get('common.gear-tech')?></h4>
						<br>
						<small>{{DB::table('stories')->where('category', 'like', '%->cat13->%')->count()}} <?php echo Lang::get('common.stories')?></small>
					</a>
				</div>
				<div class="theme-card relative honeymoon-and-romance items_wayscroll " data-ease="fadeIn animated">
					<a href="{{ URL::to( 'themes/honeymoon-and-romance') }}">
						<i class="category-icon"></i>
						<h4><?php echo Lang::get('common.honymoon-romance')?></h4>
						<br>
						<small>{{DB::table('stories')->where('category', 'like', '%->cat14->%')->count()}} <?php echo Lang::get('common.stories')?></small>
					</a>
				</div>
				<div class="theme-card relative luxury-travel items_wayscroll " data-ease="fadeIn animated">
					<a href="{{ URL::to( 'themes/luxury-travel') }}">
						<i class="category-icon"></i>
						<h4><?php echo Lang::get('common.luxury-travel')?></h4>
						<br>
						<small>{{DB::table('stories')->where('category', 'like', '%->cat15->%')->count()}} <?php echo Lang::get('common.stories')?></small>
					</a>
				</div>
				<div class="theme-card relative music items_wayscroll " data-ease="fadeIn animated">
					<a href="{{ URL::to( 'themes/music') }}">
						<i class="category-icon"></i>
						<h4><?php echo Lang::get('common.music')?></h4>
						<br>
						<small>{{DB::table('stories')->where('category', 'like', '%->cat16->%')->count()}} <?php echo Lang::get('common.stories')?></small>
					</a>
				</div>
				<div class="theme-card relative off-the-beaten-track items_wayscroll " data-ease="fadeIn animated">
					<a href="{{ URL::to( 'themes/off-the-beaten-track') }}">
						<i class="category-icon"></i>
						<h4><?php echo Lang::get('common.off-beaten-track')?></h4>
						<br>
						<small>{{DB::table('stories')->where('category', 'like', '%->cat17->%')->count()}} <?php echo Lang::get('common.stories')?></small>
					</a>
				</div>
				<div class="theme-card relative planes-and-trains items_wayscroll " data-ease="fadeIn animated">
					<a href="{{ URL::to( 'themes/planes-and-trains') }}">
						<i class="category-icon"></i>
						<h4><?php echo Lang::get('common.planes-trains')?></h4>
						<br>
						<small>{{DB::table('stories')->where('category', 'like', '%->cat18->%')->count()}} <?php echo Lang::get('common.stories')?></small>
					</a>
				</div>
				<div class="theme-card relative road-trips card-half items_wayscroll " data-ease="fadeIn animated" >
					<a href="{{ URL::to( 'themes/road-trips') }}">
						<i class="category-icon road"></i>
						<h4><?php echo Lang::get('common.road-trips')?></h4>
						<br>
						<small>{{DB::table('stories')->where('category', 'like', '%->cat19->%')->count()}} <?php echo Lang::get('common.stories')?></small>
					</a>
				</div>
				<div class="theme-card relative round-the-world-travel items_wayscroll " data-ease="fadeIn animated">
					<a href="{{ URL::to( 'themes/round-the-world-travel') }}">
						<i class="category-icon"></i>
						<h4><?php echo Lang::get('common.round-world-travel')?></h4>
						<br>
						<small>{{DB::table('stories')->where('category', 'like', '%->cat20->%')->count()}} <?php echo Lang::get('common.stories')?></small>
					</a>
				</div>
				<div class="theme-card relative travel-photograhpy card-half items_wayscroll " data-ease="fadeIn animated">
					<a href="{{ URL::to( 'themes/travel-photograhpy') }}">
						<i class="category-icon"></i>
						<h4><?php echo Lang::get('common.travel-photoghraphy')?></h4>
						<br>
						<small>{{DB::table('stories')->where('category', 'like', '%->cat21->%')->count()}} <?php echo Lang::get('common.stories')?></small>
					</a>
				</div>
				<div class="theme-card relative travel-shopping items_wayscroll " data-ease="fadeIn animated">
					<a href="{{ URL::to( 'themes/travel-shopping') }}">
						<i class="category-icon"></i>
						<h4><?php echo Lang::get('common.travel-shopping')?></h4>
						<br>
						<small>{{DB::table('stories')->where('category', 'like', '%->cat22->%')->count()}} <?php echo Lang::get('common.stories')?></small>
					</a>
				</div>
				<div class="theme-card relative walking-and-trekking items_wayscroll " data-ease="fadeIn animated">
					<a href="{{ URL::to( 'themes/walking-and-trekking') }}">
						<i class="category-icon"></i>
						<h4><?php echo Lang::get('common.walking-trekking')?></h4>
						<br>
						<small>{{DB::table('stories')->where('category', 'like', '%->cat23->%')->count()}} <?php echo Lang::get('common.stories')?></small>
					</a>
				</div>
				<div class="theme-card relative wildlife-and-nature items_wayscroll " data-ease="fadeIn animated">
					<a href="{{ URL::to( 'themes/wildlife-and-nature') }}">
						<i class="category-icon"></i>
						<h4><?php echo Lang::get('common.wildlife-nature')?></h4>
						<br>
						<small>{{DB::table('stories')->where('category', 'like', '%->cat24->%')->count()}} <?php echo Lang::get('common.stories')?></small>
					</a>
				</div>
				
			</div>
		</div>
	</div>
</section>
@stop