@extends("layouts.show_layout")
@section('content')
<section class="page-wrapper catergories-wrap">
	@include('/shared/header_new')
	@include('/shared/subnav')
	<?php 
	$categoryName = str_replace('-', ' ', $category);
	?>
	<?php
	$catId = DB::table('cat_name')->where('default_name',$category)->pluck('actual_name');
	?>
	<header class="tt-header {{$category}}">
		<div class="header-meta">
			<h1>{{$categoryName}}</h1>
			<h2>{{DB::table('stories')->join('users', 'stories.userid', '=', 'users.id')->where('stories.published', 1)->where('stories.category', 'like', '%'.'->'.$catId.'->'.'%')->count()}} STORIES</h2>
		</diV>
	</header>
	<div class="container-fluid pad0">
		<div class="tt-content clearfix pad0">
		@if (DB::table('stories')->join('users', 'stories.userid', '=', 'users.id')->where('stories.published', 1)->where('stories.category', 'like', '%'.'->'.$catId.'->'.'%')->count() > 1)
			<div class="section-title pull-left text-center hide">
				<h2><a href="#" class="active">Recent</a><a href="#">Popular</a></h2>
			</div>
			@endif
			<section class="col-md-12 isotope-container">
				@if (DB::table('stories')->join('users', 'stories.userid', '=', 'users.id')->where('stories.published', 1)->where('stories.category', 'like', '%'.'->'.$catId.'->'.'%')->count() > 0)
				<ul class="list-unstyled">
					@include('./story')
				</ul>
				@else 
				<div class="not-available">
				<img height="200" src="http://res.cloudinary.com/triptroop/image/upload/assets/no-story.jpg">
				<br><br><br>
					<h2>There are no {{$categoryName}} stories available at this time</h2>
					<h3>Be the first one to narrate a <span>{{$categoryName}} story</span></h3>
					@if (Auth::user() == false)
					<p><a class="non-logged-link btn btn-primary" href="#"><i class="fa fa-pencil align-middle"></i> <?php echo Lang::get('common.narrate-story')?></a></p>
					@else
					<p><a class="btn btn-primary" href="{{url('/narrate')}}"><i class="fa fa-pencil align-middle"></i> <?php echo Lang::get('common.narrate-story')?></a></p>
					@endif
				</div>
				@endif
			</section>
		</div>
	</div>
</section>
@stop