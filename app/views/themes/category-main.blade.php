@extends('../layouts/show_layout')
<div class="catergories-wrap">
	<div class="categories-header">
		<div class="header-meta">
			<h1>Categories</h1>
			<a id="open-quicknav-hook" class="active"><?php echo Lang::get('narrate-story.what-stories-discover?')?>
				<span></span>
			</a>
		</diV>
		<a class="absolute is-scroll-to-content" href="#"><i class="ion-ios-arrow-down"></i></a>
	</div>
	
	<div id="wrapper-quick-nav">
		
		<div id="root-category-quick-nav" style="" class="open">
			<h2 class="explore-more content" style="z-index: 1;">
				<span><a href="/categories" onclick="_gaq.push(['_trackEvent', 'Categories link actioned', 'clicked'])" title="Categories"><?php echo Lang::get('narrate-story.explore-categories')?></a></span>
			</h2>
			<div class="categories-quick-nav clear">
				<a id="cat1" class="cat-title" href="/categories/adventure"><?php echo Lang::get('common.advanture')?></a>
				<a id="cat2" class="cat-title" href="/categories/arts-and-liesure"><?php echo Lang::get('common.arts-lieasure')?></a>
				<a id="cat3" class="cat-title" href="/categories/beaches"><?php echo Lang::get('common.beach')?></a>
				<a id="cat4" class="cat-title" href="/categories/budget-travel"><?php echo Lang::get('common.budget-traval')?></a>
				<a id="cat5" class="cat-title" href="/categories/coasts-islands"><?php echo Lang::get('common.coasts-islands')?></a>
				<a id="cat6" class="cat-title" href="/categories/diving-snorkelling"><?php echo Lang::get('common.diving-snorkelling')?></a>
				<a id="cat7" class="cat-title" href="/categories/ecotourism"><?php echo Lang::get('common.ecotourism')?></a>
				<a id="cat8" class="cat-title" href="/categories/family-travel"><?php echo Lang::get('common.family-travel')?></a>
				<a id="cat9" class="cat-title" href="/categories/festival-events"><?php echo Lang::get('common.festival-events')?></a>
				<a id="cat10" class="cat-title" href="/categories/film-televisions"><?php echo Lang::get('common.film-television')?></a>
				<a id="cat11" class="cat-title" href="/categories/food-drink"><?php echo Lang::get('common.food-drink')?></a>
				<a id="cat12" class="cat-title" href="/categories/gear-tech"><?php echo Lang::get('common.gear-tech')?></a>
				<a id="cat13" class="cat-title" href="/categories/honymoon-romance"><?php echo Lang::get('common.honymoon-romance')?></a>
				<a id="cat14" class="cat-title" href="/categories/luxury-travel"><?php echo Lang::get('common.luxury-travel')?></a>
				<a id="cat15" class="cat-title" href="/categories/music"><?php echo Lang::get('common.music')?></a>
				<a id="cat16" class="cat-title" href="/categories/off-the-beaten-track"><?php echo Lang::get('common.off-beaten-track')?></a>
				<a id="cat17" class="cat-title" href="/categories/planes-trains"><?php echo Lang::get('common.planes-trains')?></a>
				<a id="cat18" class="cat-title" href="/categories/road-trips"><?php echo Lang::get('common.road-trips')?></a>
				<a id="cat19" class="cat-title" href="/categories/round-the-world-travel"><?php echo Lang::get('common.round-world-travel')?></a>
				<a id="cat20" class="cat-title" href="/categories/travel-photography"><?php echo Lang::get('common.travel-photoghraphy')?></a>
				<a id="cat21" class="cat-title" href="/categories/travel-shopping"><?php echo Lang::get('common.travel-shopping')?></a>
				<a id="cat22" class="cat-title" href="/categories/walking-trekking"><?php echo Lang::get('common.walking-trekking')?></a>
				<a id="cat23" class="cat-title" href="/categories/wildlife-nature"><?php echo Lang::get('common.wildlife-nature')?></a>
			</div>
		</div>
	</div>
</div>
