<?php
Route::get('/', function()
{
try{
		app()->setLocale(Session::has('locale') ? Session::get('locale') : 'en');
		$storie = new Storie;
		$data = Storie::getStories();
		return View::make('home', array('data' => $data)) ;
	}
	catch(Exception $ex){
		return $ex;		
	}
})->before('visited');
Route::get('all', function()
{

try{
		app()->setLocale(Session::has('locale') ? Session::get('locale') : 'en');
		$storie = new Storie;
		$data = Storie::getAllStories();
		return View::make('all', array('data' => $data)) ;
	}
	catch(Exception $ex){
		return $ex;		
	}
})->before('visited');

Route::get('/logout', function()
{
    Auth::logout();
    return Redirect::to('all')->with('msg', 'You have successfully logged out.');
});

Route::get('followers_stories', function()
{
	try {
		app()->setLocale(Session::has('locale') ? Session::get('locale') : 'en');
		$storie = new Storie;
		$data = DB::table('stories')->join('users', 'stories.userid', '=', 'users.id')->join('profile', 'stories.userid', '=', 'profile.id')->whereIn('userid',explode('->', DB::table('follow')->where('id',Auth::user()['id'])->pluck('followers')))->orderBy('updatedAt', 'DESC')->paginate(20);
		return View::make('followers_stories', array('data' => $data));
	}
	catch(Exception $ex){
		return $ex;		
	}		
})->before('visited');

Route::get('/changeLocale/{lang}', function ($lang = null){
	try{
		Session::put('locale', $lang);
	}
	catch(Exception $ex){
		return $ex;		
	}	
});

Route::get('/category/{category?}', function($category = null)
{
	try{
		$data = DB::table('stories')->join('users', 'stories.userid', '=', 'users.id')->where('stories.published', 1)->where('stories.category', 'like', '%'.'->'.$category.'->'.'%')->take(20)->get();
		return View::make('home', array('data' => $data)) ;
	}
	catch(Exception $ex){
		return $ex;		
	}	
})->before('auth')->before('visited');
/* sign-up routes :: starts */
Route::get('sign-up/{social?}', function($social = null)
{
	try{
		if($social == null){
			Session::put('social_action',false);	
		}
		
    	return View::make('blades.sign-up');
    }
	catch(Exception $ex){
		return $ex;		
	}
});

//Route::controller('Authentication', 'LoginController');
Route::post('sign-up/{social?}', function($social = null)
{
	if($social == null || $social == "social"){
		$res = SignupModel::signUpNewUser(Input::all(),  Session::get('social_action') );	
		//$res = SignupModel::signUpNewUser();	
		return $res;
	}
	else{
		return "page not found";
	}
	

});
/* sign-up routes :: ends */
Route::get('completedGettingStarted', function (){
	try{
		app()->setLocale(Session::has('locale') ? Session::get('locale') : 'en');
		DB::table('users')->where('id', Auth::user()['id'])->update(array('visited' => 1));
		return Redirect::to('/');
	}
	catch(Exception $ex){
		return $ex;		
	}	
})->before('auth');

Route::post('startSaveCategory', function(){
//	return Input::all();
	$res = ProfileModel::saveGettingCategories(Input::get('category'));
	return $res;
});

Route::post('startSaveName', function(){
	//return str_replace(Input::get('name'), "\r\n","");
	$res = ProfileModel::saveGettingName(Input::get('name'));
	return $res;
});

Route::get('start', function(){
	app()->setLocale(Session::has('locale') ? Session::get('locale') : 'en');
	return View::make('gettingStarted');
	})->before('auth');
	Route::get('start/themes', function(){
		return View::make('start.select_themes');
	})->before('auth');
	Route::get('start/profile', function(){
		return View::make('start.setup_profile');
	})->before('auth');
	Route::get('start/import', function(){
		return View::make('start.import_contacts');
	})->before('auth');

Route::post('start/saveCategories', function(){
	$res = ProfileModel::saveGettingCategories(Input::get('cats'));
	return $res;
});

Route::get('activate/{hashOne?}/{id?}/{hashTwo?}', function($hashOne = null, $id = null, $hashTwo = null)
{
	try{
		Session::put('activate_email', DB::table('users')->where('id', $id)->pluck('emailid'));
			if($id){
					$user = new User;
					if(User::where('id', $id)->count()){
						$data = User::where('id', $id)->where('confirmed', 0)->count();
						if($data){
							$fname = DB::table('users')->where('id', $id)->pluck('firstname');	
		        		    Mail::send('emails.welcome', array('name' => $fname), function($message)
							{
								$message->to(Session::get('activate_email'))->subject('Welcome to the hub of travel stories');
						     //$message->to('akshaysonawaane@gmail.com', '')->subject('Welcome!');
						});
		        		    DB::table('users')
	    			        ->where('id', $id)
		        		    ->update(array('confirmed' => 1));
		        		    Session::forget('activate_email');
							return Redirect::To('login')->with('login_error', 'Account verified successfully.');
						}
						else{
							return Redirect::To('/')->with('activate_error', 'Account has already been verified.');
						}	
					}
					else{
						return Redirect::To('/')->with('activate_error', 'This email address is not registered with us. Please register.');
					}
				}	
			else{
				return View::make('/');		
			}
    }
	catch(Exception $ex){
		return $ex;		
	}
});


/* Login/sign-in routes :: starts */
Route::get('login', function()
{
	try{
		app()->setLocale(Session::has('locale') ? Session::get('locale') : 'en');
	    return View::make('blades.login');
	 }
	catch(Exception $ex){
		return $ex;		
	}   
})->before('guest');

Route::post('login', function()
{
	$res = LoginModel::loginUser(Input::get('emailid'), Input::get('setpwd'));	
	return $res;
	
});

Route::get("profile/{name?}", function($name = null){
	try{
		app()->setLocale(Session::has('locale') ? Session::get('locale') : 'en');
			if($name == Auth::user()['id']){
				$name = null;
			}
			if($name){
				if(strcmp($name,"drafts") == 0){
					$draftData = ProfileModel::draftStories(Auth::user()['id']);
				//return $storyData;

					$userData = DB::table('users')->join('userInfo', 'userInfo.id', '=', 'users.id')->where('users.id', Auth::user()['id'])->join('profile', 'users.id', '=', 'profile.id')->get();
	//			return $draftData;
					return View::make('profile.drafts', array('userData' => $userData ,'draftData' => $draftData));	
				}
				if(strcmp($name,"dreamlist") == 0){
					$wishData = ProfileModel::wishlistStories(Auth::user()['id']);
				//return $storyData;

					$userData = DB::table('users')->join('userInfo', 'userInfo.id', '=', 'users.id')->where('users.id', Auth::user()['id'])->join('profile', 'users.id', '=', 'profile.id')->get();
	//			return $wishData;
					return View::make('profile.dreamlist', array('userData' => $userData ,'wishData' => $wishData));	
				}
				//return $userData;
					$userData = DB::table('users')->join('userInfo', 'userInfo.id', '=', 'users.id')->where('users.id', $name)->join('profile', 'users.id', '=', 'profile.id')->get();
					$user = new User;
					$storyData = ProfileModel::publishedStories($name);
					$data = DB::table('users')->join('profile', 'users.id', '=', 'profile.id')->where('users.id', $name)->get();
					$followCheck = FollowModel::checkIfFollows(Auth::user()->id, $name);
					//return $data[0]->dpPath;
					return View::make('profile.profile', array('data' => $data, 'userData' => $userData ,'isFollow' => $followCheck, 'storyData' => $storyData));
					//return $followCheck;
			}
			else{
				$storyData = ProfileModel::publishedStories(Auth::user()['id']);
				$userData = DB::table('users')->join('userInfo', 'userInfo.id', '=', 'users.id')->where('users.id', Auth::user()['id'])->join('profile', 'users.id', '=', 'profile.id')->get();
				return View::make('profile.profile', array('userData' => $userData , 'storyData' => $storyData));		
			}
	}
	catch(Exception $ex){
		return $ex;		
	}		
})->before('auth');

Route::post('/followClick/{id1?}/{id2?}', function($id1 = null, $id2 = null){
	return FollowModel::follow($id2, $id1);
})->before('auth');


Route::get('/wishClick', function(){
	
	//$liked = LikeModel::likeTo($id);
return Redirect::To('login');	
	
})->before('auth')->before('oauthlogin');


Route::get('/likeClick', function(){
	
	$liked = LikeModel::likeTo($id);
//return Redirect::To('login');	
	
})->before('auth')->before('oauthlogin');


Route::get('/getLikers/{id?}', function($id = null){
	$data = LikeModel::getLikingUsers($id);
	return $data;	
});

Route::get('/getFollowers/{id?}', function($id = null){
	$data = FollowModel::getFollowers($id);
	return $data;	
});

Route::get('/getFollowings/{id?}', function($id = null){
	$data = FollowModel::getFollowing($id);
	return $data;	
});

Route::post('/likeClick/{id?}', function($to = null){
	
	//return 'sach';
	if(Auth::guest()){
		return "";
	}
	$id = Auth::user()['id'];
		$check = DB::table('oauthcheck')->where('id', $id)->count(); 
		if($check){
			return 4;
		}
		else{
			$liked = LikeModel::likeTo($to); 
			return $liked;		
		}
	
});

Route::post('/likeRemove/{id?}', function($to = null){
	//return 'sach';
	if(Auth::guest()){
		return "";
	}
	$id = Auth::user()['id'];
		$check = DB::table('oauthcheck')->where('id', $id)->count(); 
		if($check){
			return 4;
		}
		else{
				$liked = LikeModel::likeToRemove($to); 
	return $liked;
		}

});


Route::post('/wishClick/{id?}', function($to = null){
	if(Auth::guest()){
		return "";
	}
	$id = Auth::user()['id'];
		$check = DB::table('oauthcheck')->where('id', $id)->count(); 
		if($check){
			return 4;
		}
		else{
			$wished = WishModel::wishTo($to); 
			return $wished;		
		}
	
});

Route::post('/wishRemove/{id?}', function($to = null){
	
	if(Auth::guest()){
		return "";
	}
	$id = Auth::user()['id'];
		$check = DB::table('oauthcheck')->where('id', $id)->count(); 
		if($check){
			return 4;
		}
		else{
	$wished = WishModel::wishToRemove($to); 
	return $wished;
}
});

Route::get('settings',function(){
	try{
		app()->setLocale(Session::has('locale') ? Session::get('locale') : 'en');
		$data = DB::table('users')->join('userInfo', 'userInfo.id', '=', 'users.id')->where('users.id', Auth::user()['id'])->get();
		//return $data;
		return View::make('settings.edit_profile', array('data' => $data));
	}
	catch(Exception $ex){
		return $ex;		
	}
})->before('auth')->before('visited	');

Route::post("editProfile", function(){
	//return Inout::all();
	$res =  ProfileModel::updateProfile(Input::get('old-firstname'), Input::get('old-lastname'), Input::get('gender'), Input::get('old-email'),Input::get('old-tel-pin'),Input::get('old-telno'),Input::get('old-location'),Input::get('desc'),Input::get('checks'));
	return $res;
});

Route::get('update-password',function(){
	try{
		app()->setLocale(Session::has('locale') ? Session::get('locale') : 'en');
		return View::make('settings.update_password');
	}
	catch(Exception $ex){
		return $ex;		
	}
})->before('auth')->before('visited	');
/* settings routes :: ends */

Route::controller('logoutC', 'LogoutController');

/* logout/signout routes :: ends */

Route::get('reset-password',function(){
	app()->setLocale(Session::has('locale') ? Session::get('locale') : 'en');
	return View::make('blades.reset-password');
})->before('oauthlogin');

Route::post('updatePassword',function(){
	$res =  ProfileModel::updatePassword(Input::get('old-pw'), Input::get('new-pw'), Input::get('confirm-new-reset-pw'));
	return $res;
})->before('auth');

Route::post('updateBase',function(){
	$res =  ProfileModel::updateBase(Input::get('old-firstname'), Input::get('old-lastname'), Input::get('old-email'), Input::get('old-tel-pin'), Input::get('old-telno'), Input::get('old-location'), Input::get('desc'), Input::get('checks'), Input::get('gender'));
	return $res;
})->before('auth');


Route::post('recoverPassword', function(){
	//return Input::get('emailid');
	return ProfileModel::recoverPassword(Input::get('emailid'));
});

Route::post('confirmRecoverPassword', function(){
	//return Input::all();
	//return 66;
	return ProfileModel::confirmRecoverPassword(Input::get('email'), Input::get('pswd'), Input::get('cpswd'));
});

Route::get('recover-password', function()
{
	app()->setLocale(Session::has('locale') ? Session::get('locale') : 'en');
    return View::make('blades.recover-password');
});

Route::get('activateRecover/{hashOne?}/{id?}/{hashTwo?}', function($hashOne = null, $id = null , $hashTwo = null)
{
	//return $id;
	app()->setLocale(Session::has('locale') ? Session::get('locale') : 'en');
	$eid = DB::table('users')->where('id', $id)->pluck('emailid');
    return View::make('blades.activate-recover',  array('eid' => $eid));
});
/* Recover Password / Forget Password routes :: ends */


//Route to narrate a story
Route::get('/read/{id?}', function($id = null)
{
	app()->setLocale(Session::has('locale') ? Session::get('locale') : 'en');
	Storie::hitCounter($id);
	$data = DB::table('stories')->join('users', 'stories.userid', '=', 'users.id')->join('profile', 'profile.id', '=', 'users.id')->join('userinfo', 'userinfo.id', '=', 'users.id')->where('stories.storyid', $id)->get();
	$readTitle = $data[0]->title;
	$categories_array = explode('->',$data[0]->category);
	$len = sizeof($categories_array);
	$categories =  array_splice($categories_array,1, $len - 2);
	Session::put("readTitle", $data[0]->title);
    return View::make('story.read', array('storyid' => $id, "readStory" => $data, 'categories' => $categories));
  	
})->before('visited');

Route::get('edit/{id?}', function($id = null){
	app()->setLocale(Session::has('locale') ? Session::get('locale') : 'en');
	if(DB::table('stories')->where('userid',Auth::user()['id'])->where('storyid', $id)->count()){
		DB::table('cover')->where('uid', Auth::user()['id'])->delete();
		DB::table('currentStory')->where('uid', Auth::user()['id'])->delete();
		DB::table('currentStory')->insert(array('uid' => Auth::user()['id'],'storyid' => $id));
		$data = DB::table('stories')->join('users', 'stories.userid', '=', 'users.id')->where('stories.storyid', $id)->get();
	//	return $data;
		$categories = explode('->',$data[0]->category);
	    return View::make('story.editStory', array('storyid' => $id, "readStory" => $data, 'categories' => $categories));
	}
	else{
		return Redirect::To('/');
	}
	
	
})->before('auth');


Route::post('edit/setup_profile_image', function(){
	$res = ProfileModel::coverEdit(Input::all());
	return $res;
});

Route::post('deleteStory/{id}', function($id = null){
	$res = Storie::deleteStory($id);
	return $res;
});


Route::get('narrate', function()
{
	app()->setLocale(Session::has('locale') ? Session::get('locale') : 'en');
	DB::table('currentStory')->where('uid', Auth::user()['id'])->count() ? DB::table('currentStory')->where('uid', Auth::user()['id'])->delete() : '';
	DB::table('cover')->where('uid', Auth::user()['id'])->count() ? DB::table('cover')->where('uid', Auth::user()['id'])->delete() : '';
    return View::make('story.narrate');
})->before('auth')->before('oauthlogin')->before('visited');

/* social authecation :: start to be moved later */

Route::get('login/fb', function()
{
	$code = Input::get( 'code' );
	$fb = OAuth::consumer( 'Facebook' );
	if ( !empty( $code ) ) {
		$token = $fb->requestAccessToken( $code );
		$result = json_decode( $fb->request( '/me' ), true );
		//return $result;
        //$message = 'Your unique facebook user id is: ' . $result['id'] . ' and your name is ' . $result['id'];
       // echo $result['first_name']. "<br/>".$result['last_name']. "<br/>";
		//Session::put('confirmOauthEmail', $result['email']);
		if(array_key_exists('email',$result)) {
			//alert('hi');
			
			//$id = DB::table('users')->where('emailid', $result['email'])->pluck('id');
			//return $id;
			$id = DB::table('users')->insertGetId(array('firstname' => $result['first_name'], 'lastname' => $result['last_name'], 'emailid' => $result['email'], 'visited' => 1));	
		}
		else{
			return Redirect::To('sign-up')->with(array('fname' => $result['first_name'], 'lname' => $result['last_name']));	

			//return $id;
			// Mail::send('confirmOauth', array('name' => $result['first_name'],'id' => $id, 'hashed1' => str_random(20), 'hashed2' => str_random(20)), function($message)
			// 			{
			// 				$message->to(Session::get('confirmOauthEmail'), '')->subject('Welcome!');
			// });
		}
		
		//$res = DB::table('oauthCheck')->insert(array('id' => $id, 'viaOauth' => 1));
		$user = Auth::loginUsingId($id);
			if ( ! $user)
			{
				throw new Exception('Error logging in');
			}
			else{
				Session::put('oauth_login',1);
				return Redirect::To('/');
			}
	}
    // if not ask for permission first
    else {
        // get fb authorization
        $url = $fb->getAuthorizationUri();

        // return to facebook login url
         return Redirect::to( (string)$url );
    }
})->before('guest');

// facbook sign-up authentication :: start
Route::get('social/fb', function(){
  // get data from input
	//login/fb/signup
    $code = Input::get( 'code' );
    Session::put('social_action',true);
    // get fb service
    $fb = OAuth::consumer( 'Facebook' );

    // check if code is valid

    // if code is provided get user data and sign in
    if ( !empty( $code ) ) {

        // This was a callback request from facebook, get the token
        $token = $fb->requestAccessToken( $code );

        // Send a request with it
        $result = json_decode( $fb->request( '/me' ), true );

        //$message = 'Your unique facebook user id is: ' . $result['id'] . ' and your name is ' . $result['name'];
        //echo $message. "<br/>";

        //Var_dump
        //display whole array()
        //return $result;	
//        $social = true;
        if(array_key_exists('email',$result)) { 
        
        return Redirect::To('sign-up/social')->with(array('fname' => $result['first_name'], 'lname' => $result['last_name'], 'email' => $result['email']));
        
        }
        else {
        return Redirect::To('sign-up/social')->with(array('fname' => $result['first_name'], 'lname' => $result['last_name']));
        	
        }
        //dd($result);

    }
    // if not ask for permission first
    else {
        // get fb authorization
        $url = $fb->getAuthorizationUri();

        // return to facebook login url
         return Redirect::to( (string)$url );
    }
    //return 'sach';
});

Route::post('oauthConfirm', function(){
	//return Input::all();
	$res = ProfileModel::oauthConfirm(Input::all());
	return $res;
});
// facbook sign-up authentication :: ends

// Gmail sign-up authentication start
Route::get('login/gm/login', function(){
	Session::put('social_action',true);
	return Redirect::To('login/gmail');
});
Route::get('login/gm/signup', function(){
	Session::put('social_action','up');
	return Redirect::To('login/gmail');
});

Route::get('login/fb', function(){
	Session::put('social_action','in');
	return Redirect::To('social/fb');
});
Route::get('signup/fb', function(){
	Session::put('social_action','up');
	return Redirect::To('social/fb');
});

Route::get('login/gmail', function(){
  	//return Session::get('login_gmail_id');
  	//if(Session::get('login_gmail_id') == 'up'){
  		// get data from input
	    $code = Input::get( 'code' );
	    Session::put('social_action',true);
	    // get google service
	    $googleService = OAuth::consumer( 'Google' );

	    // check if code is valid

	    // if code is provided get user data and sign in
	    if ( !empty( $code ) ) {

	        // This was a callback request from google, get the token
	        $token = $googleService->requestAccessToken( $code );

	        // Send a request with it
	        $result = json_decode( $googleService->request( 'https://www.googleapis.com/oauth2/v1/userinfo' ), true );
	        //return $result;

	        $message = 'Your unique Google user id is: ' . $result['id'] . ' and your name is ' . $result['name'];
	        echo $message. "<br/>";

	        //Var_dump
	        //display whole array().
	        return Redirect::To('sign-up/social')->with(array('fname' => $result['given_name'], 'lname' => $result['family_name'], 'email' => $result['email']));
	        dd($result);

	    }
	    // if not ask for permission first
	    else {
	        // get googleService authorization
	        $url = $googleService->getAuthorizationUri();

	        // return to google login url
	        return Redirect::to( (string)$url );
	    }	
  	//}
    /*else{
    	//return 'in';
		$code = Input::get( 'code' );

			// get google service
			$googleService = OAuth::consumer( 'Google' );

			// check if code is valid

			// if code is provided get user data and sign in
			if ( !empty( $code ) ) {

				// This was a callback request from google, get the token
				$token = $googleService->requestAccessToken( $code );

				// Send a request with it
				$result = json_decode( $googleService->request( 'https://www.googleapis.com/oauth2/v1/userinfo' ), true );
				//return $result;
				//return Redirect::To('sign-up')->with(array('data' => $result));
				return Redirect::To('sign-up')->with(array('fname' => $result['given_name'], 'lname' => $result['family_name'], 'email' => $result['email']));
				//return View::make('sign-up', array('data' => $result)) ;
		 //       $message = 'Your unique Google user id is: ' . $result['given_name'] . ' and your name is ' . $result['family_name'];
		   //     echo $message. "<br/>";
	   			$id = DB::table('users')->insertGetId(array('firstname' => $result['given_name'], 'lastname' => $result['family_name'], 'emailid' => $result['email']));
				$res = DB::table('oauthCheck')->insert(array('id' => $id, 'viaOauth' => 1));
				$user = Auth::loginUsingId($id);
					if ( ! $user)
					{
						throw new Exception('Error logging in');
					}
					else{
						return Redirect::To('/');
					}

				//Var_dump
				//display whole array().
				//dd($result);

			}
			// if not ask for permission first
			else {
				// get googleService authorization
				$url = $googleService->getAuthorizationUri();

				// return to google login url
				return Redirect::to( (string)$url );
			}
			//return View::make('blades.login');
		}*/	    	
})->before('guest');

// Gmail sign-up authentication fixed
/* social authecation :: start to be moved later */

// //-------------routes.php   -- create Stories folder in laravel
// Route::post('/save/', function(){
// 	// $title = 'sacc';
// 	if(File::isDirectory('Stories/'.Auth::user()['id'])){
// 	}
// 	else{
// 		$d = File::makeDirectory('Stories/'.Auth::user()['id'] , 0775, true );
// 	}
// 	$storyid = DB::table('stories')->max('storyid') + 1;
// 	$path = 'Stories/'.Auth::user()['id'].'/'.$storyid	.'.txt';
// 	File::put($path, '<p class = ');
// 	File::append($path, Input::all());
// 	//$res = DB::table('stories')->insertGetId(array('storyid' => $storyid , 'userid' => Auth::user()['id'], 'filepath' => $path, 'title' => $title, 'saved' => 1));
// 	//return Input::all();
// })->before('auth');

Route::post('setup_cover_image', function(){
	$res = ProfileModel::uploadCoverPic(Input::all());
	return $res;
})->before('auth');

Route::post('edit/setup_cover_image', function(){
	$res = ProfileModel::uploadCoverPic(Input::all());
	return $res;
})->before('auth');

Route::post('setup_profile_image', function(){
	$res = ProfileModel::uploadProfilePic(Input::all());
	return $res;
})->before('auth');

Route::post('/upload', function(){
	//return Request::header(). "\n". Request::file('photo');
	if(File::isDirectory('public/uploads/'.Auth::user()['id'])){
			}	
			else{
				$d = File::makeDirectory('public/uploads/'.Auth::user()['id'], 0777, true );
			}
	move_uploaded_file($_FILES["file"]["tmp_name"], "public/uploads/".Auth::user()['id']."/". $_FILES["file"]["name"]);
	$ret = \Cloudinary\Uploader::upload('public/uploads/'.Auth::user()['id']."/".$_FILES["file"]["name"], array("folder" => "uploads/".Auth::user()['id']));
	return  "https://res.cloudinary.com/triptroop/image/upload/w_700,c_limit,q_95/v1434511754"."/" .$ret['public_id'];
	
})->before('auth');

// Route::post('saveStory/{title?}/{categories?}', function($title = null, $categories = null) {
// 	//return 'save';
// 	if(File::isDirectory('Stories/'.Auth::user()['id'])){
// 	}	
// 	else{
// 		$d = File::makeDirectory('Stories/'.Auth::user()['id'] , 0775, true );
// 	}
		
// 			try{
// 				$cid =  DB::table('currentStory')->max('id'); 
// 				$storyid = DB::table('stories')->max('storyid') + 1;
// 				if($cid){
// 					$storyid  = $cid;	
// 				}
// 				else{
// 					DB::table('currentStory')->insert(array('id' => $storyid));
// 				}
				
// 				if(DB::table('stories')->where('storyid', $storyid)->count())
// 				{
// 					$res = DB::table('stories')->where('storyid', $storyid)->update(array( 'title' => $title, 'saved' => 1, 'category' => $categories));	
// 				}
// 				else{
// 					$path = 'Stories/'.Auth::user()['id'].'/'.$storyid	.'.txt';
// 					$res = DB::table('stories')->insertGetId(array('storyid' => $storyid , 'userid' => Auth::user()['id'], 'filepath' => $path, 'title' => $title, 'saved' => 1, 'category' => $categories));
// 				}
// 				return 1;
// 			}
// 			catch(Exception $e){
// 				return 3;
// 			}
			
// })->before('auth');
Route::post('edit/saveStory', function() {
		
	try{

	$title = Input::get('storyTitle');
	$description = Input::get('storyDescription');
	$date = Input::get('dateData');
	$expense = Input::get('expenseData');
	$currency = Input::get('currency');
	$categories = Input::get('checks');
	$saved = Input::get('save') ? Input::get('save') : 0;
	$published = Input::get('publish') ? Input::get('publish') :0 ;
	// if(File::isDirectory('Stories/'.Auth::user()['id'])){
	// }	
	// else{
	// 	$d = File::makeDirectory('Stories/'.Auth::user()['id'] , 0775, true );
	// }
			//try{
				$cid =  DB::table('currentStory')->where('uid', Auth::user()['id'])->pluck('storyid'); 
			//	$storyid = DB::table('stories')->max('storyid') + 1;
				//if($cid){
			//		$storyid  = $cid;	
				// }
				// else{
				// 	DB::table('currentStory')->insert(array('id' => $storyid));
				// }
				$oldImg = 'public'.DB::table('stories')->where('storyid', $cid)->pluck('cover');
				$oldImgCl = DB::table('stories')->where('storyid', $cid)->pluck('coverClPath');
				$tempCover = DB::table('cover')->where('uid', Auth::user()['id'])->pluck('image') ? DB::table('cover')->where('uid', Auth::user()['id'])->pluck('image') : $oldImg;
				$tempCoverCl = DB::table('cover')->where('uid', Auth::user()['id'])->pluck('clPath') ? DB::table('cover')->where('uid', Auth::user()['id'])->pluck('clPath') : $oldImgCl;
				if($tempCover){
							$res = DB::table('stories')->where('storyid', $cid)->update(array( 'title' => $title,'storyDescription' => $description,'filepath' => Input::get('editorData'),'published' => $published ,'saved' => $saved, 'category' => $categories, 'cover' => $tempCover, 'coverClPath' => $tempCoverCl,'currency' => $currency, 'expense' => $expense, 'tripDate' => $date, 'updatedAt' => date('y-d-m')));	
							if(strcmp('public'.$tempCover ,$oldImg) != 0){
								File::delete($oldImg);	
							}
						if($published){
							return array('published' => $cid);
							alert('published');
						}	else{
							return 'Story saved.';
						}	
				}
				else{
						return 'Select cover image for your story.';
				}
			}
			catch(Exception $e){
				return 'Exception';
			}

			
})->before('auth')->before('oauthlogin');

Route::post('saveStory', function() {
	try{

	$title = Input::get('storyTitle');
	$description = Input::get('storyDescription');
	$date = Input::get('dateData');
	$expense = Input::get('expenseData');
	$currency = Input::get('currency');
	$categories = Input::get('checks');
	$saved = Input::get('save') ? Input::get('save') : 0;
	$published = Input::get('publish') ? Input::get('publish') :0 ;
	
				$tempCover = DB::table('cover')->where('uid', Auth::user()['id'])->pluck('image') ;
				$tempCoverCl = DB::table('cover')->where('uid', Auth::user()['id'])->pluck('clPath') ;
				if($tempCover){
					if(DB::table('currentStory')->where('uid', Auth::user()['id'])->count())
						{
							$cid =  DB::table('currentStory')->where('uid', Auth::user()['id'])->pluck('storyid'); 
							$res = DB::table('stories')->where('storyid', $cid)->update(array( 'title' => $title,'storyDescription' => $description,'published' => $published ,'saved' => $saved,'filepath' => Input::get('editorData') ,'category' => $categories, 'cover' => $tempCover, 'coverClPath' => $tempCoverCl,'currency' => $currency, 'expense' => $expense, 'tripDate' => $date, 'createdAt' => date('y-d-m'), 'updatedAt' => date('y-d-m')));	
							$oldImg = 'public'.DB::table('stories')->where('storyid', $cid)->pluck('cover');
							$oldImgCl = 'public'.DB::table('stories')->where('storyid', $cid)->pluck('coverClPath');
							if(strcmp('public'.$tempCover ,$oldImg) != 0){
								File::delete($oldImg);	
							}
						}
						else{
							$res = DB::table('stories')->insertGetId(array('storyid' => '' ,'count' => 0 , 'userid' => Auth::user()['id'], 'filepath' => Input::get('editorData'), 'title' => $title,'storyDescription' => $description, 'saved' => $saved, 'published' => $published, 'category' => $categories, 'cover' => $tempCover, 'coverClPath' => $tempCoverCl,'currency' => $currency, 'expense' => $expense, 'tripDate' => $date, 'hits' => 0, 'createdAt' => date('y-d-m'), 'updatedAt' => date('y-d-m')));
							DB::table('currentStory')->insert(array('uid' => Auth::user()['id'],'storyid' => $res));
						}
						if($published){
							return array('published' => DB::table('currentStory')->where('uid', Auth::user()['id'])->pluck('storyid'));
						}	
						else{
							return 'Story saved.';
						}		
				}
				else{
						return 'Select cover image for your story.';
				}
				
				//return 'Story saved';
			}
			catch(Exception $e){
				return $e;
			}
			
})->before('auth')->before('oauthlogin');



Route::post('publishStory/{title?}/{categories?}', function($title = null, $categories = null) {
	//return 'save';
	$cid =  DB::table('currentStory')->max('id'); 
	$storyid = DB::table('stories')->max('storyid') + 1;
	if($cid){
		$storyid  = $cid;	
	}
	else{
		DB::table('currentStory')->insert(array('id' => $storyid));
	}
	if(File::isDirectory('Stories/'.Auth::user()['id'])){
	}	
	else{
		$d = File::makeDirectory('Stories/'.Auth::user()['id'] , 0775, true );
	}
		
		try{
				$storyid = DB::table('currentStory')->max('id');

			if(DB::table('stories')->where('storyid', $storyid)->count())
			{
				$res = DB::table('stories')->where('storyid', $storyid)->update(array('title' => $title,'published' => 1));	
			}
			else{
				$path = 'Stories/'.Auth::user()['id'].'/'.$storyid	.'.txt';
				$res = DB::table('stories')->insertGetId(array('storyid' => $storyid , 'userid' => Auth::user()['id'], 'filepath' => $path, 'title' => $title, 'published' => 1, 'category' => $categories));
			
			}
			DB::table('currentStory')->delete();
			return 1;
		}
		catch(Exception $e)
		{
			return 3;
		}
			//return "yup";

})->before('auth');

/* Search routes :: starts */
Route::get('search', function()
{
	app()->setLocale(Session::has('locale') ? Session::get('locale') : 'en');
    return View::make('search.search');
});

Route::post('searchData', function(){
	$vals = explode(" ", Input::get('searchText'));
	$theme = Input::get('themeValue');
	$country = Input::get('countryValue');
	$sort = Input::get('sortValue');
	if($theme == "all"){
		$theme = '%';
	}
	else{
		//$theme = DB::table('cat_name')->where('default_name', 'like', '%'.$theme.'%')->pluck('actual_name');
		$theme = '%'.$theme.'%';
	}
	if($country == "World Wide"){
		$country = '%';
	}
	else{
		$country = '%'.$country.'%';	
	}
	
	if($sort == "Most Recent"){
		$sort = '%';
	}
	else{

	}

	
	$cnt =  count(explode(" ", Input::get('searchText')));
	$res = DB::table('stories')->where('title', 'like', '%'.$vals[0].'%')->where('published', '1')->join('users','users.id', '=','stories.userid')->join('userInfo','userInfo.id', '=','stories.userid')->orderBy('stories.storyId','desc')->select('stories.storyid', 'users.id', 'stories.title', 'stories.coverClPath', 'users.firstname', 'users.lastname', 'stories.cover')->get();
	for($i = 1; $i < $cnt; $i++){
		if(DB::table('stories')->where('title', 'like', '%'.$vals[$i].'%')->where('published', '1')->join('users','users.id', '=','stories.userid')->join('userInfo','userInfo.id', '=','stories.userid')->orderBy('stories.storyId','desc')->get()){
			if (in_array(DB::table('stories')->where('title', 'like', '%'.$vals[$i].'%')->where('published', '1')->join('users','users.id', '=','stories.userid')->join('userInfo','userInfo.id', '=','stories.userid')->orderBy('stories.storyId','desc')->select('stories.storyid', 'users.id', 'stories.title', 'stories.coverClPath', 'users.firstname', 'users.lastname', 'stories.cover')->get()[0], $res)) {
		    
		}
		else{
				array_push($res,DB::table('stories')->where('title', 'like', '%'.$vals[$i].'%')->where('published', '1')->join('users','users.id', '=','stories.userid')->join('userInfo','userInfo.id', '=','stories.userid')->orderBy('stories.storyId','desc')->select('stories.storyid', 'users.id', 'stories.title', 'stories.coverClPath', 'users.firstname', 'users.lastname', 'stories.cover')->get()[0]);
			}	
		}
	}		
	
	return $res;
	//return View::make('search.search', array('stories' => $res));
});

Route::post('searchPeopleData', function(){
	$vals = explode(" ", Input::get('searchText'));
	Session::put('searchStr', $vals[0]);
	//return $vals;
	//$theme = Input::get('themeValue');
	$country = Input::get('countryValue');
	$sort = Input::get('sortValue');
	
	if($country == "World Wide"){
		$country = '%';
	}
	else{
		$country = '%'.$country.'%';	
	}
	
	if($sort == "Most Recent"){
		$sort = '%';
	}
	else{

	}

	$cnt =  count(explode(" ", Input::get('searchText')));
	$res = DB::table('users')->join('profile','users.id', '=','profile.id')->where('confirmed',1)->
	where(function($q){
        $q->where('users.firstname', 'like', '%'.Session::get('searchStr').'%');
        $q->orWhere('users.lastname', 'like', '%'.Session::get('searchStr').'%');
    })
    ->orderBy('users.id','desc')->select('users.id', 'users.firstname', 'users.lastname', 'users.emailid', 'profile.clPath')
	->get();

	
//	$res = DB::table('users')->where('users.confirmed', '=',1)->where('firstname', 'like', '%'.$vals[0].'%')->orWhere('lastname', 'like', '%'.$vals[0].'%')->join('profile','users.id', '=','profile.id')->orderBy('users.id','desc')->select('users.id', 'users.firstname', 'users.lastname', 'users.emailid', 'profile.clPath')->get();
	//$res;
	for($i = 1; $i < $cnt; $i++){
		Session::put('searchStr', $vals[$i]);
		if(DB::table('users')->join('profile','users.id', '=','profile.id')->where('confirmed',1)->where(function($q){
        $q->where('users.firstname', 'like', '%'.Session::get('searchStr').'%');
        $q->orWhere('users.lastname', 'like', '%'.Session::get('searchStr').'%');
    })->orderBy('users.id','desc')->select('users.id', 'users.firstname', 'users.lastname', 'users.emailid', 'profile.clPath')->get()){
			if (in_array(DB::table('users')->join('profile','users.id', '=','profile.id')->where('confirmed',1)->where(function($q){
        $q->where('users.firstname', 'like', '%'.Session::get('searchStr').'%');
        $q->orWhere('users.lastname', 'like', '%'.Session::get('searchStr').'%');
    })->orderBy('users.id','desc')->select('users.id', 'users.firstname', 'users.lastname', 'users.emailid', 'profile.clPath')->get()[0], $res)) {
		    
		}
		else{
			array_push($res,DB::table('users')->join('profile','users.id', '=','profile.id')->where('confirmed',1)->where(function($q){
        $q->where('users.firstname', 'like', '%'.Session::get('searchStr').'%');
        $q->orWhere('users.lastname', 'like', '%'.Session::get('searchStr').'%');
    })->orderBy('users.id','desc')->select('users.id', 'users.firstname', 'users.lastname', 'users.emailid', 'profile.clPath')->get()[0]);
			}	
		}
	}		
	
	return $res;
	//return View::make('search.search', array('stories' => $res));
});


//Route::controller('Categories', 'Category');

Route::get('/category/{category?}', function($category = null)
{
		$data = DB::table('stories')->join('users', 'stories.userid', '=', 'users.id')->where('stories.published', 1)->where('stories.category', 'like', '%'.'->'.$category.'->'.'%')->take(20)->get();
		return View::make('themes.categories', array('data' => $data)) ;
	
	
});
Route::get('/folks', function()
{
	app()->setLocale(Session::has('locale') ? Session::get('locale') : 'en');
		$data = DB::table('users')->join('profile', 'users.id', '=', 'profile.id')->where('confirmed', 1)->orderBy('profile.id', 'DESC')->paginate(40);
	//	return $data;
	return View::make('storytellers/storytellers', array('data' => $data));
	//return $data;
	
});
Route::get('/popular', function()
{
	app()->setLocale(Session::has('locale') ? Session::get('locale') : 'en');
		$data = DB::table('stories')->join('users', 'stories.userid', '=', 'users.id')->join('profile', 'stories.userid', '=', 'profile.id')->where('stories.count', '>', '10')->orderBy('stories.count', 'DESC')->take(20)->get();
		return View::make('popular.popular', array('data' => $data)) ;
		//return $data;
	
});


Route::get('/terms', function()
{
	app()->setLocale(Session::has('locale') ? Session::get('locale') : 'en');
		return View::make('terms.terms') ;

});


Route::get('/privacy', function()
{
	app()->setLocale(Session::has('locale') ? Session::get('locale') : 'en');
		return View::make('terms.privacy') ;
	
});

/* Notification:: starts */
Route::get('/user_notifications', function()
{
	return View::make('shared.user_notifications');
});
Route::post('/read_user_notifications', function()
{
	return NotificationModel::modifyRead();
});
Route::get('/notifications', function()
{
	return View::make('/notifications');
});
Route::get('/new_notification_count', function()
{
	return NotificationModel::anyNewNotification();
});
/* Notification:: ends */


// Route::get('/themes', function()
// {
// 		return View::make('themes/themes');
	
	
// });
Route::get('/themes/{category?}', function($category = null)
{
	app()->setLocale(Session::has('locale') ? Session::get('locale') : 'en');
		$check_category = DB::table('cat_name')->where('default_name', $category)->count();
		//return $check_category;
		$get_category_actual = DB::table('cat_name')->where('default_name', $category)->pluck('actual_name');
		//return $get_category_actual;
		if($check_category){
			$data = DB::table('stories')->join('users', 'stories.userid', '=', 'users.id')->join('profile', 'stories.userid', '=', 'profile.id')->where('stories.published', 1)->where('stories.category', 'like', '%'.'->'.$get_category_actual.'->'.'%')->take(20)->get();
		$category =  $category;
		return View::make('themes.categories', array('data' => $data, 'category' => $category)) ;
	
		}
		else{
			return View::make('themes/themes');	
		}
		
});

Route::get('/guide1', function()
{
	return View::make('guide.graphfb');
});
Route::get('/map', function()
{
	return View::make('guide.map');
});
Route::get('/guide', function()
{
	return View::make('guide.countries');
});
Route::get('/getCountries', function()
{
	return View::make('guide.countries-list');
});