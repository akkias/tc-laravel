<?php
class Storie extends Eloquent  {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
//	protected $table = 'users';
	 protected $table = 'stories';
	public $timestamps = false;
//	public $remember_token = false;
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	static function getStories(){
		return DB::table('stories')->join('users', 'stories.userid', '=', 'users.id')->join('profile', 'stories.userid', '=', 'profile.id')->where('stories.count', '>', '10')->orderBy('stories.count', 'DESC')->take(8)->get();
	}
	
	static function getAllStories(){
		return DB::table('stories')->join('users', 'stories.userid', '=', 'users.id')->join('profile', 'stories.userid', '=', 'profile.id')->where('stories.published', 1)->orderBy('updatedAt', 'DESC')->paginate(20);
	}

	static function deleteStory($id){
		try{
			DB::table('stories')->where('storyid', $id)->delete();
			return 1;
		}
		catch(Exception $ex){
			return $ex;
		}
		
	}

	static function hitCounter($id){
		try{
			//check if null of not
			$hits = DB::table('stories')->where('storyid', $id)->pluck('hits');
			if($hits == Null){
				DB::table('stories')->where('storyid', $id)->update(array('hits' => 1));
			}
			else{
				DB::table('stories')->where('storyid', $id)->increment('hits');
			}
			//;
			//return 1;
		}
		catch(Exception $ex){
			return $ex;
		}
		
	}



	static function getLikingUsers($query){
			$queryData =  DB::table('stories')->select('title','storyid','userid')->get();
			$returnLikeUsersData = [];
			return $returnLikeUsersData;
		}







}