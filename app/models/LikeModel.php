<?php
class LikeModel extends Eloquent  {

	protected $table = 'users';
	public $timestamps = false;
	//follow table
	static  function likeTo($id){
				// user had liked at least single story
				if(Auth::guest()){
					return null;
				}
			$inserted1 = 0;
			if(DB::table('liked')->where('id', Auth::user()['id'])->where('likeTo', 'like', '%'.'->'.$id.'->'.'%')->count())
			{
				return 2;
			}
			else{
				$check = DB::table('liked')->where('id', Auth::user()['id'])->count();
				DB::table('stories')->where('storyid', $id)->increment('count');
				if($check){
					$oldData = DB::table('liked')->where('id', Auth::user()['id'])->pluck('likeTo');
					if($oldData == Null ){
							$oldData = '->';
						}
					$inserted1 = DB::table('liked')->where('id', Auth::user()['id'])->update(array('likeTo' => $oldData.$id.'->'));
					$userId = DB::table('stories')->where('storyid', $id)->pluck('userid');
					$storyName = DB::table('stories')->where('storyid', $id)->pluck('title');
					$storyId = DB::table('stories')->where('storyid', $id)->pluck('storyid');
					$storyCover = DB::table('stories')->where('storyid', $id)->pluck('coverClPath');
					$storyLikes = DB::table('stories')->where('storyid', $id)->pluck('count');
					$narrator = DB::table('users')->where('id', $userId)->pluck('firstname');
					$emailId = DB::table('users')->where('id', $userId)->pluck('emailid');
					$likersDP = DB::table('profile')->where('id', Auth::user()['id'])->pluck('clPath');
					$currentUserFirstName = DB::table('users')->where('id', Auth::user()['id'])->pluck('firstname');
					$currentUserLastName = DB::table('users')->where('id', Auth::user()['id'])->pluck('lastname');
					$currentUserId = DB::table('users')->where('id', Auth::user()['id'])->pluck('id');
					if(!DB::table("Notification")->where('notifier' , Auth::user()['id'])->where('storyid', $id)->count()){
							DB::table("Notification")->insert(array('id' => $userId, 'notifier' => Auth::user()['id'], 'notify' => ' Liked your ' .$storyName. ' story.',  'storyid' => $id, 'notifiedAt' => date('y-d-m'), 'readMark' => 'false'));
					}
					Session::Put('likeMailId', $emailId);
					//Session::Put('sub', $currentUserFirstName);
					 Mail::send('emails.like', array('currentUserFirstName' => $currentUserFirstName, 'currentUserLastName' => $currentUserLastName, 'storyCover' => $storyCover, 'storyLikes' => $storyLikes, 'currentUserId' => $currentUserId, 'userId' => $userId, 'storyName' => $storyName, 'storyId' => $storyId, 'narrator' => $narrator, 'likersDP' => $likersDP), function($message)
					{
							$message->to(Session::get('likeMailId'))->subject('Someone liked one of your Stories!');
							//$message->to('akshaysonawaane@gmail.com', '')->subject('Welcome!');
					});
				}
				else{
					$inserted1 = DB::table('liked')->insert(array('id' => Auth::user()['id'] ,'likeTo' => '->'.$id.'->'));
					$inserted1 = 1;
					$userId = DB::table('stories')->where('storyid', $id)->pluck('userid');;
					$emailId = DB::table('users')->where('id', $userId)->pluck('emailid');
					$emailName = DB::table('users')->where('id', Auth::user()['id'])->pluck('firstname');
					Session::Put('likeMailId', $emailId);
				 //Mail::send('emails.like', array('name' => $emailName, 'id' => $id), function($message)
				//{
							//$message->to(Session::get('likeMailId'))->subject('Like mail!');
						     //$message->to('akshaysonawaane@gmail.com', '')->subject('Welcome!');
				//});
				}		
			}
			return  $inserted1;
		}

	static  function likeToRemove($id){
				// user had liked at least single story
				if(Auth::guest()){
					return null;
				}
				$data = DB::table('liked')->where('id', Auth::user()['id'])->pluck('likeTo');
				$str = '->'.$id.'->';
				$replaced = str_replace($str, "->", $data);
				$res = DB::table('liked')->where('id', Auth::user()['id'])->update(array('likeTo' => 	$replaced));
				if(DB::table('stories')->where('storyid', $id)->pluck('count') == 0){

				}
				else{
					DB::table('stories')->where('storyid', $id)->decrement('count');
				}
				
			return  $res;
		}
		
		static function getLikingUsers($storyId){
			$likeUsers =  DB::table('liked')->where('likeTo', 'like', '%->'.$storyId.'->%')->select('id')->get();
			$returnLikeUsersData = [];
			foreach($likeUsers as $userId) {
			  if($userId != ""){
			  	$likeUsersData = DB::table('users')->where('users.id', $userId->id)->join('profile','users.id', '=','profile.id')->select('users.id', 'users.firstname', 'users.lastname', 'profile.clPath')->get() ? DB::table('users')->where('users.id', $userId->id)->join('profile','users.id', '=','profile.id')->select('users.id', 'users.firstname', 'users.lastname', 'profile.clPath')->get()[0] : Null;
			  	if($likeUsersData != Null){
			  		array_push($returnLikeUsersData, $likeUsersData);
			  	}
			  	
			  }
			}
			return $returnLikeUsersData;
		}
	}	