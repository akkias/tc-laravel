<?php


class WishModel extends Eloquent  {

	protected $table = 'users';
	public $timestamps = false;
	//follow table
	static  function wishTo($id){
				// user had liked at least single story
				if(Auth::guest()){
					return null;
				}
			$inserted1 = 0;
		//	return DB::table('wishlist')->where('id', Auth::user()['id'])->where('wishTo', 'like', '%'.'->'.'12'.'->'.'%')->count();
			if(DB::table('wishlist')->where('id', Auth::user()['id'])->where('wishTo', 'like', '%'.'->'.$id.'->'.'%')->count())
			{
				return 2;
			}
			else{
				$check = DB::table('wishlist')->where('id', Auth::user()['id'])->count();
				if($check){
					$oldData = DB::table('wishlist')->where('id', Auth::user()['id'])->pluck('wishTo');
					$inserted1 = DB::table('wishlist')->where('id', Auth::user()['id'])->update(array('wishTo' => $oldData.$id.'->'));
				}
				else{
					$inserted1 = DB::table('wishlist')->insert(array('id' => Auth::user()['id'] ,'wishTo' => '->'.$id.'->'));
				}		
			}
			return  $inserted1;
		}

	static  function wishToRemove($id){
				// user had liked at least single story
				if(Auth::guest()){
					return null;
				}
				$data = DB::table('wishlist')->where('id', Auth::user()['id'])->pluck('wishTo');
				$str = '->'.$id.'->';
				$replaced = str_replace($str, "->", $data);
				$res = DB::table('wishlist')->where('id', Auth::user()['id'])->update(array('wishTo' => 	$replaced));
				
			return  $res;
		}
		
	}	
