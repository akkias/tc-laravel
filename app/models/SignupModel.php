<?php


class SignupModel extends Eloquent  {

	/**
	 * The database table used by the model.
	 *
	 */
//	protected $table = 'users';
	public $timestamps = false;
	static function signUpNewUser($input, $social){

		try{
			$fname = Input::get('firstname');
			$lname = Input::get('lastname');
			$eid = Input::get('emailid');
			$pswd = Input::get('setpwd'); 
			$confirmed = $social ? 1 : 0; 
			$checkEmail = User::where('emailid',  $eid)->count();	
			if($checkEmail){
					return 1;
			}
			else{
				$id = DB::table('users')->insertGetId(array('firstname' => $fname, 'lastname' => $lname, 'emailid' => $eid, 'password' => Hash::make($pswd), 'confirmed' => $confirmed));
				if($id){
					DB::table('userinfo')->insert(array('id' => $id));
					DB::table('profile')->insert(array('id' => $id, 'dpPath'=> 'https://res.cloudinary.com/triptroop/image/upload/assets/avatar.png', 'clPath'=> '/v1447128662/assets/avatar.png'));
					$mailPage = $confirmed ? 'emails.welcome' : 'confirmAccount';
					Mail::send($mailPage, array('name' => $fname,'id' => $id, 'hashed1' => str_random(20), 'hashed2' => str_random(20)), function($message)
						{
						//	$email = DB::table('users')->where('id', $id)->get()[0]->emailid;
							//return $email;
							$message->to(Input::get('emailid'), '')->subject('Welcome!');
					     //$message->to('akshaysonawaane@gmail.com', '')->subject('Welcome!');
					});
					return 2;
				}
				else{
					return 3;	
				}
					
			}
		}
		catch(Exception $e){
			return $e;
		}	
	}
	/**
	 * The attributes excluded from the model's JSON form.
	 */
//	protected $hidden = array('password', 'remember_token');

}