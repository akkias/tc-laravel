<?php


class LoginModel extends Eloquent  {

	/**
	 * The database table used by the model.
	 *
	 */
//	protected $table = 'users';
	public $timestamps = false;
	static function loginUser($eid, $pswd){
		try{
			$old_pswd = User::where('emailid', $eid)->pluck('password');
			$data = User::where('emailid',  $eid)->pluck('id');
			$confirmed = User::where('id',  $data)->pluck('confirmed');	
			if(Hash::check($pswd, $old_pswd)){
				if($confirmed){
					$user = Auth::loginUsingId($data);
					if ( ! $user)
					{
			    		throw new Exception('Error logging in');
					}
					else{
						if(DB::table('users')->where('id', $data)->pluck('visited')){
							return 1;	
						}
						return 5;
					}
				}	
				else{
					return 2;	
				}
				
			}
			else{
					return 3;
			}
		}
		catch(Exception $e){
			return $e;
		}	
	}
	/**
	 * The attributes excluded from the model's JSON form.
	 */
//	protected $hidden = array('password', 'remember_token');

}