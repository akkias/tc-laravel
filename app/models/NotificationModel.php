<?php


class NotificationModel extends Eloquent  {

	protected $table = 'users';
	public $timestamps = false;
	//follow table
	static function getNotification(){
		//$notifierId = 
		try{
				return DB::table('notification')->where('id', Auth::user()['id'])->where('readMark', "false")->get();
		}
		catch(Exception $ex){
			//exception
			return "";
		}
	}
	static function getAllNotification(){
		//$notifierId = 
		try{
				return DB::table('notification')->where('id', Auth::user()['id'])->get();
		}
		catch(Exception $ex){
			//exception
			return "";
		}
	}

	static function getNotifierPic($id){
		try{
			return DB::table('profile')->where('id', $id)->pluck('clPath');
		}
		catch(exception $ex){
			return "https://res.cloudinary.com/triptroop/image/upload/w_90/v1431406595/assets/avatar.png";
		}
	}

	static function getNotifierName($id){
		try{
			$fname = DB::table('users')->where('id', $id)->pluck('firstname');
			$lname = DB::table('users')->where('id', $id)->pluck('lastname');		
			return $fname." ".$lname;
		}
		catch(exception $ex){
			return "";	
		}
		
	}

	static function modifyRead(){
		try{
			DB::table('notification')->where('id', Auth::user()['id'])->where('readMark', "false")->update(array('readMark' => "true" ));
			return 1;			
		}
		catch(exception $ex){
			return 2;	
		}
		
	}

	static function anyNewNotification(){
		try{
			return DB::table('notification')->where('id', Auth::user()['id'])->where('readMark', 'false')->count();
		}
		catch(exception $ex){
			return 2;	
		}
		
	}	


	static  function follow($id2, $id1){
	if(DB::table('follow')->where('id', $id1)->where('followers', 'like', '%'.'->'.$id2.'->'.'%')->count())
	{
		try{
					$res1 = DB::table('users')->where('id', $id1)->decrement('followers');
				$res2 = DB::table('users')->where('id', $id2)->decrement('following');	
				//remove followers data
				$old_data = DB::table('follow')->where('id', $id1)->pluck('followers');
				$replaced = str_replace('->'.$id2.'->',"->",$old_data);
				DB::table('follow')->where('id', $id1)->update(array('followers'=> $replaced));
				//remove following data
				$old_following = DB::table('follow')->where('id', $id2)->pluck('following');
				$replaced = str_replace('->'.$id1.'->',"->",$old_following);
				DB::table('follow')->where('id', $id2)->update(array('following'=> $replaced));
				//;	
				$obj = array('ret' =>2, 'val' =>DB::table('users')->where('id', $id1)->pluck('followers'));
				return $obj;
		}
		catch(Exception $ex){
			return $ex;
		}

	}
	else{
			try{
				$res1 = DB::table('users')->where('id', $id1)->increment('followers');
				$res2 = DB::table('users')->where('id', $id2)->increment('following');
				$followingPresent = DB::table('follow')->where('id', $id1)->count();
				if($followingPresent){
						$oldData = DB::table('follow')->where('id', $id1)->pluck('followers');
						//$oldData = $oldData
						if($oldData == Null ){
							$oldData = '->';
						}
						$inserted1 = DB::table('follow')->where('id', $id1)->update(array('followers' => $oldData.$id2.'->'));
				}
				else{
						$inserted1 = DB::table('follow')->insert(array('id' => $id1 ,'followers' => '->'.$id2.'->'));
				}
				
				$followerPresent = DB::table('follow')->where('id', $id2)->count();
				if($followerPresent){
						if(DB::table('follow')->where('id', $id2)->where('following', 'like', '%'.'->'.$id1.'->'.'%')->count()){

						}
						$oldData = DB::table('follow')->where('id', $id2)->pluck('following');
						if($oldData == Null ){
							$oldData = '->';
						}
						$inserted2 = DB::table('follow')->where('id', $id2)->update(array('following' => $oldData.$id1.'->'));
						
					}	
				else{
						$inserted2 = 	DB::table('follow')->insert(array('id' => $id2 ,'following' => '->'.$id1.'->'));
					}
					
		//		$data = DB::table('follow')->where('id', $id1)->increment('followers');
				$obj = array('ret' =>1, 'val' =>DB::table('users')->where('id', $id1)->pluck('followers'));
				
				$emailId = DB::table('users')->where('id', $id1)->pluck('emailid');
				
				$firstName = DB::table('users')->where('id', $id2)->pluck('firstname');
				$lastName = DB::table('users')->where('id', $id2)->pluck('lastname');
				$followerDesc = DB::table('userInfo')->where('id', $id2)->pluck('description');

				$followerDp = DB::table('profile')->where('id', $id2)->pluck('clPath');


				Session::Put('followMailId', $emailId);
				 
				 Mail::send('emails.follow', array('firstName' => $firstName, 'id' => $id2, 'lastName' => $lastName, 'followerDp' => $followerDp, 'followerDesc' => $followerDesc), function($message)
				{
						$message->to(Session::get('followMailId'))->subject('You have got new follower on TripTroop!');
						     //$message->to('akshaysonawaane@gmail.com', '')->subject('Welcome!');
				});
				return $obj;
			}
			catch(Exception $ex){
				return $ex;	
			}
		
		}
	}	
	
	static function checkIfFollows($from, $to){
		//return $to.$from;
		return DB::table('follow')->where('id', $to)->where('followers', 'like', '%'.'->'.$from.'->'.'%')->count();
	}


	static function getFollowers($id){
		//return $to.$from;
		$followers =  DB::table('follow')->where('followers', 'like', '%'.'->'.$id.'->'.'%')->pluck('followers');
		$followers_array = explode("->", $followers);
		$returnFollowersData = [];
		foreach($followers_array as $followerId) {
		  if($followerId != ""){
		  	$followerData = DB::table('users')->where('users.id', $followerId)->join('profile','users.id', '=','profile.id')->select('users.id', 'users.firstname', 'users.lastname', 'profile.clPath')->get()[0];
		  	array_push($returnFollowersData, $followerData);
		  }
		}
		return $returnFollowersData;
	}

	static function getFollowing($id){
		//return $to.$from;
		$following =  DB::table('follow')->where('following', 'like', '%'.'->'.$id.'->'.'%')->pluck('following');
		$following_array = explode("->", $following);
		$returnFollowingData = [];
		foreach($following_array as $followingId) {
		  if($followingId != ""){
		  	$followingData = DB::table('users')->where('users.id', $followingId)->join('profile','users.id', '=','profile.id')->select('users.id', 'users.firstname', 'users.lastname', 'profile.clPath')->get()[0];
		  	array_push($returnFollowingData, $followingData);
		  }
		}
		return $returnFollowingData;
	}



	static function NotificationRead(){
		DB::table('notification')->where('id', Auth::user()['id'])->update(array('read' => 1));
	}

	/**
	 * The attributes excluded from the model's JSON form.
	 */
//	protected $hidden = array('password', 'remember_token');

}