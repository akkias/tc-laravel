<?php


class ProfileModel extends Eloquent  {

	protected $table = 'users';
	public $timestamps = false;
	//follow table
		static function createAuthFolder(){
			if(File::isDirectory('public/storyCover/'.Auth::user()['id'])){
			}	
			else{
				$d = File::makeDirectory('public/storyCover/'.Auth::user()['id'], 0777, true );
			}
		}
		static function uploadCoverPic($img){

		if(is_array($_FILES)) {
			ProfileModel::createAuthFolder();
			$var = DB::table('stories')->where('userid', Auth::user()['id'])->count()+1;
			if(File::isDirectory('public/storyCover/'.Auth::user()['id']."/".$var)){
			}	
			else{
				$d = File::makeDirectory('public/storyCover/'.Auth::user()['id']."/".$var, 0777, true );
			}
			$targetPath = "public/storyCover/".Auth::user()['id']."/".$var."/".$_FILES['profilePic']['name'];
			//return (string)File::exists($targetPath);
			// if((string)File::exists($targetPath) == '1'){
			// 	return 'File with given name already exists';
			// }
			// else{
			if(is_uploaded_file($_FILES['profilePic']['tmp_name'])) {
				$sourcePath = $_FILES['profilePic']['tmp_name'];
				$targetPath = "public/storyCover/".Auth::user()['id']."/".$var.'/'.$_FILES['profilePic']['name'];
//				$targetPath = "public/storyCover/".$_FILES['profilePic']['name'];
						if(move_uploaded_file($sourcePath,$targetPath)) {
							$ret = \Cloudinary\Uploader::upload('public/storyCover/'.Auth::user()['id']."/".$var.'/'.$_FILES['profilePic']['name'], array("folder" => "storyCover/".Auth::user()['id']."/".$var));
								//$present = DB::table('profile')->where('id', Auth::user()['id'])->count();
							$present = DB::table('cover')->where('uid', Auth::user()['id'])->count();
						 	if($present){
						 		$oldImg = 'public'.DB::table('cover')->where('uid', Auth::user()['id'])->pluck('image');
						 		$oldCLImg = DB::table('cover')->where('uid', Auth::user()['id'])->pluck('clPath');
						 		$res = DB::table('cover')->where('uid', Auth::user()['id'])->update(array('image'=> "/storyCover/".Auth::user()['id']."/".$var."/".$_FILES['profilePic']['name'], 'clPath' => $ret['public_id']));
						 		File::delete($oldImg);	
						 		\Cloudinary\Uploader::destroy($oldCLImg);
						 	} else{
						 		$res = DB::table('cover')->insert(array('uid' => Auth::user()['id'],'image' => "/storyCover/".Auth::user()['id']."/".$var."/".$_FILES['profilePic']['name'], 'clPath' => $ret['public_id']));
						 	}	
						 	if($res){
						 		return '/storyCover/'.Auth::user()['id']."/".$var."/".$_FILES['profilePic']['name'];	
						 	}
							return 'failed';
						}
						else{
							return "failed";
						}
				}
			//	}	
		}
		else{
			return 'No file selected.';
		}

	}

	static function saveGettingCategories($cats){
		try{
//			return $cats;
			if(DB::table('userInfo')->where('id', Auth::user()['id'])->count()){
				$res = DB::table('userInfo')->where('id', Auth::user()['id'])->update(array('categories' => $cats));	
				return 2;
			}
			else{
				$res = DB::table('userInfo')->insert(array('categories' => $cats, 'id' => Auth::user()['id']));
				return 1;
			}
			
			return 3;	
		}
		catch(Exception $e){
			return $e;
		}
		
	}

	static function saveGettingName($name){
		try{
//			return $cats;
				$res = DB::table('users')->where('id', Auth::user()['id'])->update(array('firstname' => $name));	
					return 1;
				
					
		}
		catch(Exception $e){
			return $e;
		}
		
	}


	static function coverEdit($img){
		if(is_array($_FILES)) {
			$targetPath = "public/storyCover/".$_FILES['profilePicEdit']['name'];
			//return (string)File::exists($targetPath);
			if((string)File::exists($targetPath) == '1'){
				return 'File with given name already exists';
			}
			else{
				if(is_uploaded_file($_FILES['profilePicEdit']['tmp_name'])) {
					$sourcePath = $_FILES['profilePicEdit']['tmp_name'];
					$targetPath = "public/storyCover/".$_FILES['profilePicEdit']['name'];
							if(move_uploaded_file($sourcePath,$targetPath)) {
								$present = DB::table('cover')->count();
							 	if($present){
							 		$oldImg = 'public'.DB::table('cover')->pluck('image');
							 		$res = DB::table('cover')->update(array('image'=> "/storyCover/".$_FILES['profilePicEdit']['name']));
							 		File::delete($oldImg);	
							 	} else{
							 		$res = DB::table('cover')->insert(array('image' => "/storyCover/".$_FILES['profilePicEdit']['name']));
							 	}	
							 	if($res){
							 		return '/storyCover/'.$_FILES['profilePicEdit']['name'];	
							 	}
								return 'failed';
							}
							else{
								return "failed";
							}
					}
				}	
		}
		else{
			return 'No file selected.';
		}

	}
	static function uploadProfilePic($img){
		//alert('uploadProfilepic');
		if(is_array($_FILES)) {
			$var = Auth::user()['id'];
			if(File::isDirectory('public/profilePics/'.$var)){
			}	
			else{
				$d = File::makeDirectory('public/profilePics/'.$var, 0775, true );
			}
			$targetPath = "public/profilePics/".$var.'/'.$_FILES['profilePic']['name'];
			//return (string)File::exists($targetPath);

			// if((string)File::exists($targetPath) == '1'){
			// 	return 'File with given name already exists';
			// }
			// else{
				if(is_uploaded_file($_FILES['profilePic']['tmp_name'])) {
					$sourcePath = $_FILES['profilePic']['tmp_name'];
					$targetPath = "public/profilePics/".$var.'/'.$_FILES['profilePic']['name'];
					
							if(move_uploaded_file($sourcePath,$targetPath) ) {
								$ret = \Cloudinary\Uploader::upload('public/profilePics/'.$var.'/'.$_FILES['profilePic']['name'], array("folder" => "profilePics/".$var));
								$present = DB::table('profile')->where('id', Auth::user()['id'])->count();
							 	if($present){
							 		$oldImg = 'public'.DB::table('profile')->where('id', Auth::user()['id'])->pluck('dpPath');
							 		$oldCLImg = DB::table('profile')->where('id', Auth::user()['id'])->pluck('clPath');
							 		$res = DB::table('profile')->where('id', Auth::user()['id'])->update(array('dpPath'=> "/profilePics/".$var.'/'.$_FILES['profilePic']['name'] , 'clPath' => $ret['public_id']));
							 		File::delete($oldImg);	
							 		\Cloudinary\Uploader::destroy($oldCLImg);
							 	} else{
							 		$res = DB::table('profile')->insert(array('id' => Auth::user()['id'], 'dpPath' => "/profilePics/".$var.'/'.$_FILES['profilePic']['name'], 'clPath' => $ret['public_id']));
							 	}	
							 	if($res){
										// if(){
			
										// }
										// else{
										// 	return "false";
										// }

							 		return '/profilePics/'.$var.'/'.$_FILES['profilePic']['name'];	
							 	}
								return 'failed';
							}
							else{
								return "failed";
							}
					}
	//			}	
		}
		else{
			return 'No file selected.';
		}

	}

	static function updatePassword($old, $new, $confirm){
		//return $confirm;
		$old_pswd = DB::table('users')->where('id', Auth::user()['id'])->pluck('password');

		if(Hash::check($old, $old_pswd)){
			if(strcmp($new ,$confirm) != 0){ // if new and confirmed not match
				return 3;
			}	
			//return 1;
			if(DB::table('users')->where('id', Auth::user()['id'])->update(array('password' => Hash::make($new)))){
				return 1;
			}
			else{
				return 4;
			}
		}
		else{		// if entered old massword doesnt match
			return 2;
		}
		
	}

	static function updateBase($fname, $lname, $eid,$pin,$telNo,$location,$desc,$cats, $gender){
		try{
			$res1 = DB::table('users')->where('id', Auth::user()['id'])->update(array('firstname' => $fname, 'lastname' => $lname, 'emailid' => $eid ));
			$res2 = DB::table('userInfo')->where('id', Auth::user()['id'])->update(array('mbl' => $telNo, 'location' => $location, 'description' => $desc , 'pin' => $pin, 'categories' => $cats, 'gender' => $gender));
			//if($res1 && $res2){
				return 1;
				
		}
		catch(Exception $e){
			return $e;
		}
	}
	static  function publishedStories($id){
				// user had liked at least single story
		return DB::table('stories')->where('userid', $id)->get() ? DB::table('stories')->join('users', 'stories.userid', '=', 'users.id')->join('profile', 'stories.userid', '=', 'profile.id')->where('userid', $id)->where('published', 1)->get() : [];	
		}
	static  function draftStories($id){
				// user had liked at least single story
		return DB::table('stories')->where('userid', $id)->get() ? DB::table('stories')->join('users', 'stories.userid', '=', 'users.id')->join('profile', 'stories.userid', '=', 'profile.id')->where('userid', $id)->where('published', 0)->get() : [];	
		}	

		static function getFollowers($id){
			$followersData = explode("->", DB::table('follow')->where('id', $id)->pluck('followers'));
			$followersArray = array();
			foreach($followersData as $follower){
				if(is_numeric($follower)){
					array_push($followersArray, DB::table('users')->where('id', $follower)->lists('lastname','firstname')) ;
				}
			}
			return $followersArray;
		}

		static function getFollowing($id){
			$followingData = explode("->", DB::table('follow')->where('id', $id)->pluck('following'));
			//return $followingData;
			$followingArray = array();
			foreach($followingData as $followTo){
				// return $followTo;
				if(is_numeric($followTo)){
					//return "true";
					array_push($followingArray, DB::table('users')->where('id', $followTo)->lists('lastname','firstname')) ;
				}
			}
			//return "sachie";
			return $followingArray;
		}
	
	static  function wishlistStories($id){
				// user had liked at least single story
				$data = DB::table('wishlist')->where('id', $id)->pluck('wishTo');
				DB::table('tempwish')->delete();
				$arr = explode("->", $data);
				$len = count($arr);
				$i = 0;
				for($i = 0; $i < $len ; $i++){
					if($i == 0 || $i == ($len - 1)){
						continue;
					}
					DB::table('tempWish')->insert(array('id' => $arr[$i]));
					
				}
				//return $data;
				//DB::table('tempWish')->insert(explode("->", $data));
		return DB::table('stories')->join('tempwish', 'tempwish.id', '=', 'stories.storyid')->join('users', 'stories.userid', '=' , 'users.id')->get() ? DB::table('stories')->join('tempwish', 'tempwish.id', '=', 'stories.storyid')->join('users', 'stories.userid', '=' , 'users.id')->join('profile', 'stories.userid', '=', 'profile.id')->get() : [];	
		//return DB::table('tempwish')->get();
		}

		static  function recoverPassword($eid){
				// user had liked at least single story
			try{
					if(DB::table('users')->where('emailid', $eid)->count()){				// if users presents for this email
					//str_random(20)
					$id = DB::table('users')->where('emailid', $eid)->pluck('id');
					$name = DB::table('users')->where('emailid', $eid)->pluck('firstname');
					$eid = DB::table('users')->where('emailid', $eid)->pluck('emailid');
					Session::put('recoverEid', $eid);
					
					Mail::send('emails.recover', array('eid' => $id, 'name' => $name, 'str1' => str_random(20), 'str2' => str_random(20)), function($message)
							{
								$message->to(Session::get('recoverEid'), '')->subject('Recover password!');
						});	
					return 1;
				}	
				else{
					return "This email address is not registered with us. Please register.";	
				}
			}
			catch(Exception $ex){
				return $ex;	
			}
			

		}

		static  function confirmRecoverPassword($eid, $pswd, $cpswd){
				// user had liked at least single story
			if(DB::table('users')->where('emailid', $eid)->count()){				// if users presents for this email
				//str_random(20)
				$name = DB::table('users')->where('emailid', $eid)->pluck('firstname');
				Session::put('newPassword', $eid);
				if($pswd == $cpswd){
					DB::table('users')->where('emailid', $eid)->update(array('password' =>  Hash::make($pswd)));
					Mail::send('emails.newPassword', array('name' => $name), function($message)
						{
							$message->to(Session::get('newPassword'), '')->subject('New Password!');
					});
				}
				else{	//pswd and confirm pswd not same
					return 2;
				}					
				return 1;
			}	
			else{	// "No user registered for given email id."
				return 3;
			}

		}

		static function oauthConfirm($data){
			//return $data['name'];
			Session::put('oauthConfirmEmail',$data['email']);
			try{
				if(DB::table('users')->where('emailid', $data['email'])->count()){
					$id = DB::table('users')->where('emailid', $data['email'])->update(array('password' => Hash::make($data['pswd']), 'confirmed' => 1));
					 Mail::send('emails.welcome', array('name' => $data['name']), function($message)
							{
								$message->to( Session::get('oauthConfirmEmail'))->subject('Welcome!');
						     //$message->to('akshaysonawaane@gmail.com', '')->subject('Welcome!');
						});
					return 1;
				}
				else{
					return 2;
				}
				
				
			}
			catch(Exception $ex){
				return $ex;
			}
		
		}	
	
	}	
