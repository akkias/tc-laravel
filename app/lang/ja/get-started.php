<?php

return array(
	
	/* Start Page */

	'we-are-glad-you-are-here' => '私たちは、あなたがここにいる喜んでいます',
	'get-start-message' => '私たちは、美しく、地球のすべてのコーナーから言われ、私たちの生活や経験、の共有レコードは地球上の私たちのほとんどの時間を作るために、旅行に、そして私たちの周りの人や場所の世話をするために私たちの多くを刺激すると考えています。すべてのあなたの旅行、あなたの物語を教えてください、私たちはあなたが設定に役立ちます。物事がちょうどこの辺りで始めるされており、私たちは創業コミュニティ...幸せなストーリーテリングの一部としてあなたを数えることを誇りに思っています！',

	'lets-go' => '行きましょう',


	/* Slider Banner  */

	'slide-one' => 'あなたは物語を書くように、テキストを選択し、別の書式を選択することができます。',
	'slide-two' => 'アップロード新しい段落の左のアイコンを任意の新しい段落クリックを開始しました。',
	'slide-three' => 'あなたが別のレイアウトとサイズからお選びにアップロードしたすべての段落をクリックします。',
	'slide-four' => 'あなたの物語をより検出できるようにするには、テキスト内のそれぞれの場所を選択し、場所の言及を追加',

	'what-interested' => 'あなたは何に興味があります?',
	'what-interested-message' => '下記のオプションを選択する場合にクリックしますと、私たちはあなたのためにいくつかの良いものを提案します',
	'continue' => '続けます',

	'customize-your-profile' => 'あなたのプロフィールをカスタマイズします',
	'customize-your-profile-message' => 'あなたの個性とスタイルを表示するために写真を追加します.',

	'find-people-know' => 'あなたが知っている人を見つけます',
	'find-people-know-message' => 'だから、彼らが共有しているかを見ることができますあなたが知っていることがあり人を見つけます。あなたの許可なしに私たちはあなたの連絡先を電子メールで送信されません、心配しないでください.',
	'get-start-message1' => '私たちは私たちの生活と、地球のすべてのコーナーから美しく言った経験の共有レコードは旅行を最大限に私達の時間のケアを地球上の人々 と私たちの周りの場所のために私たちの多くを刺激すると信じています。',
    'get-start-message2' => 'すべてのあなたの旅行、あなたのストーリーについて教えてくださいと設定を得るお手伝いをします。',
    'get-start-message3' => 'ここの周り物事は始まったばかりと我々 は建国のコミュニティーの一部としてカウントする誇りに思って幸せな物語 ！',
    'flowers' => '花',
    'previous' => '以前',
    'next' => '次',
    'step-1-of-4' => 'ステップ 1 の 3',
    'step-2-of-4' => 'ステップ 2 の 3',
    'step-3-of-4' => 'ステップ 3 の 3',
	'step-4-of-4' => 'ステップ 4 の 4'

);