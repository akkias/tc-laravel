<?php

return array(
	'basic-info' => '基本情報',
    'basic-info-message' => 'あなたの基本的なアカウントと言語設定を変更します。',

    'primary-lang' => '一次言語',
    'email-info' => 'メールは公開されません'
);