<?php

return array(
	'change-password' => 'あなたのパスワードを変更します',
    'current-password' => '現在のパスワード',
    'new-password'=> '新しいパスワード',
    'confirm-password' => 'パスワードを認証します',
    'update-password' => '更新パスワード',

    'forgot-your-password' => 'パスワードをお忘れですか？',
    'forgot-password-info' => '以下の E メールを入力して、パスワードを入力し、ログインしてくださいアカウント TravelCircles にリセットを送信します。',

    'reset-your-password' => 'あなたのパスワードをリセットしますか ?',
    'reset-password-info' => 'あなたの新しいパスワードを入力してアカウント TravelCircles 以下を入力してログインします。'
    
);