<?php

return array(
	
	'name-your-story' => 'あなたの物語に名前を付け',
	'select-duration' => '旅の選択期間',
	'total-expenditure' => '総支出額',
	'insert-ammount' => '挿入量',

	'classify-trip' => 'あなたの旅行を分類します',

	'select-cover' => 'カバーを選択します。',
	'add-hashtags' => 'ハッシュタグを追加します。',
	'category-hashtags' => 'カテゴリ & ハッシュタグ',
	'invite-friends' => '友人を招待します。',

	'categories' => 'カテゴリ',
	'what-stories-discover?' => '発見する、何のストーリー',
	'explore-categories' => 'カテゴリを参照します。'

);