<?php

return array(
	'already-account' => 'すでにアカウントを持っています?',
    'login-message' => '新しいアカウントを作成するには、次のいずれかのオプションを選択し、私たちのコミュニティに参加.',

    'create-an-account' => 'アカウントを作成します',
    'accept-terms' => '承諾します',
    'terms-services' => '用語·サービスの利用規約'
);