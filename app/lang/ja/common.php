<?php

return array(

	'sign-up' => 'サインアップ',
    'sign-in' => 'サインイン',

    'narrate-story' => 'ナレーションの物語',
    'home' => 'ホーム',
    'editors-picks' => '写真編集者',
    'storytellers' => '落語家',
    'themes' => 'テーマ',
    
    // Categories
    'fav-travel-activities' => '好きな旅行活動',
    
    'advanture' => '大冒険',
    'arts-lieasure' => 'アート＆レジャー',
    'backpacking' => 'バックパッキング',
    'beach' => 'ビーチ',
    'budget-traval' => '予算旅行',
    'coasts-islands' => '海岸や島',
    'diving-snorkelling' => 'ダイビングやシュノーケル',
    'ecotourism' => '豪州エコツーリズム協会',
    'family-travel' => '家族旅行',
    'festival-events' => '祭とイベント',
    'film-television' => '映画とテレビ',
    'food-drink' => 'お食事やお飲み物',
    'gear-tech' => 'ギアおよび技術',
    'honymoon-romance' => 'honymoonとロマンス',
    'luxury-travel' => '贅沢な旅行',
    'music' => '音楽',
    'off-beaten-track' => 'は打たれたトラックをオフに',
    'planes-trains' => '計画と列車',
    'road-trips' => '長距離ドライブ',
    'round-world-travel' => 'ラウンドの世界旅行',
    'travel-photoghraphy' => '旅行 写真',
    'travel-shopping' => '旅行ショッピング',
    'walking-trekking' => 'ウォーキングとトレッキング',
    'wildlife-nature' => 'ワイルドライフと自然',

    'recent-stories' => '最近の記事',
    'popular-stories' => '人気の記事',
    'popular-story' => '人気の物語',

    'all-stories' => 'すべての物語',

    'email' => 'メール',
    'password' => 'パスワード',
    'forgot-password' => 'パスワードを忘れました',

    'first-name' => 'ファーストネーム',
    'last-name' => '苗字',
    'set-password' => 'パスワードを設定し',
    'gender' => 'ジェンダー',
    'male' => '男性',
    'female' => '女性',
    'phone-number' => '電話番号',
    'location' => '場所',
    'description' => '説明',

    'my-profile' => 'マイプロフィール',
    'drafts' => '下書き',
    'Dreamlist' => 'ブックマーク',
    'settings' => '[設定]',
    'sign-out' => 'サインアウト',

    /* Profile Page */
    'likes' => 'のが好きだ',
    'follow' => 'を実行します。',
    'following' => '次の',
    'followers' => 'フォロワー',
    'social' => '社会の',
    'publish-trip' => '旅を公開',

    /* Settings */
    'account' => 'アカウント',


    'stories' => 'ストーリー',

    'save' => 'ドラフトとして保存',
    'save1' => '保存',
    'publish' => '発行',
    'update' => '更新プログラム',

    'all-themes' => 'すべてのテーマ', /* new added */
    'about-me' => '私について',  /* new added */
    'my-drafts' => '私のドラフト', /* new added */
    'bio' => 'バイオ', /* new added */
    'follow' => '従う' /* new added */


);