<?php

return array(

	'sign-up' => 'サインアップ',
    'sign-in' => 'サインイン',

    'narrate-story' => 'ナレーションの物語',
    'home' => 'ホーム',
    'editors-picks' => '写真編集者',
    'storytellers' => '落語家',
    'themes' => 'テーマ',
    
    // Categories
    
    'advanture' => '大冒険',
    'arts-lieasure' => '芸術とlieasure',
    'beach' => 'ビーチ',
    'budget-traval' => '予算旅行',
    'coasts-islands' => '海岸や島',
    'diving-snorkelling' => 'ダイビングやシュノーケル',
    'ecotourism' => '豪州エコツーリズム協会',
    'family-travel' => '家族旅行',
    'festival-events' => '祭とイベント',
    'film-television' => '映画とテレビ',
    'food-drink' => 'お食事やお飲み物',
    'gear-tech' => 'ギアおよび技術',
    'honymoon-romance' => 'honymoonとロマンス',
    'luxury-travel' => '贅沢な旅行',
    'music' => '音楽',
    'off-beaten-track' => 'は打たれたトラックをオフに',
    'planes-trains' => '計画と列車',
    'road trips' => '長距離ドライブ',
    'round-world-travel' => 'ラウンドの世界旅行',
    'travel-photoghraphy' => '旅行 写真',
    'travel-shopping' => '旅行ショッピング',
    'walking-trekking' => 'ウォーキングとトレッキング',
    'wildlife-nature' => 'ワイルドライフと自然',

    'recent-stories' => '最近の記事',
    'popular-stories' => '人気の記事',

    'all-stories' => 'All Stories'
    
);