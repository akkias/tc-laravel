<?php

return array(
	'message' => 'それはちょうど旅行の話ではない、経験したものを使用しており、世界の他の部分には何を秘めたブリッジのインターフェイス。 潜在的な物語 ...... 私たちの世界を認識するために、を表示していますが明らかに待機しています。',
    
    'tagline' => 'その約行先約旅行',
    'tagline-home-stories' => 'の旅行物語ハブ、あなたの旅行の話',
    'tagline-share-stories' => 'あなたの物語を共有し、他の人には、感動',
    'get-started' => '始める',
    'or' => 'or',
    'exporer-stories' => 'または 事例を紹介',
    'all-stories' => 'すべての物語'
);