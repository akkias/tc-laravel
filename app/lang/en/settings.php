<?php

return array(
	'basic-info' => 'Basic Information',
    'basic-info-message' => 'Change your basic account and language settings.',

    'primary-lang' => "Primary Language",
    'email-info' => "Email will not be publicly displayed"
);