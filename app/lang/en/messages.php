<?php

return array(
	'home' => 'Home',
	'editors-pick' => 'Editor&amp;s Picks',
	'storytellers' => 'Storytellers',
	'themes' => 'Themes',
    'home-travel-stories' => "Home to everyone's travel stories",
    'message' => 'It’s not just a travel story, it’s an interface to bridge what you have experienced and what is undiscovered to the rest of the world. Latent stories are waiting to be uncovered, we are going to reveal them for the world to recognize.'
);