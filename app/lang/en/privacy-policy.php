<?php

return array(

	'priv-policy-title' => 'Privacy Policy for TripTroop',
	
	'privacy-content1' => 'If you require any more information or have any questions about our privacy policy, please feel free to contact us by email at',
	'privacy-content2' => 'At <a href="https://www.triptroop.me">https://www.triptroop.me</a> we consider the privacy of our visitors to be extremely important. This privacy policy document describes in detail the types of personal information is collected and recorded by <a href="https://www.triptroop.me">https://www.triptroop.me</a> and how we use it.',
	
	'privacy-log-title' => 'Log Files',
	'privacy-log-content' => "Like many other Web sites, <a href='https://www.triptroop.me'>https://www.triptroop.me</a> makes use of log files. These files merely logs visitors to the site - usually a standard procedure for hosting companies and a part of hosting services's analytics. The information inside the log files includes internet protocol (IP) addresses, browser type, Internet Service Provider (ISP), date/time stamp, referring/exit pages, and possibly the number of clicks. This information is used to analyze trends, administer the site, track user's movement around the site, and gather demographic information. IP addresses, and other such information are not linked to any information that is personally identifiable.",

	'privacy-cookies-title' => 'Cookies and Web Beacons',
	'privacy-cookies-content' => "uses cookies to store information about visitors' preferences, to record user-specific information on which pages the site visitor accesses or visits, and to personalize or customize our web page content based upon visitors' browser type or other information that the visitor sends via their browser.",

	'privacy-child-info' => "Children's Information",
	'privacy-child-info-content' => "We believe it is important to provide added protection for children online. We encourage parents and guardians to spend time online with their children to observe, participate in and/or monitor and guide their online activity. <a href='https://www.triptroop.me'>https://www.triptroop.me</a> does not knowingly collect any personally identifiable information from children under the age of 13. If a parent or guardian believes that <a href='https://www.triptroop.me'>https://www.triptroop.me</a> has in its database the personally-identifiable information of a child under the age of 13, please contact us immediately (using the contact in the first paragraph) and we will use our best efforts to promptly remove such information from our records.",

	'privacy-online' => "Online Privacy Policy",
	'privacy-online-content' => "This privacy policy applies only to our online activities and is valid for visitors to our website and regarding information shared and/or collected there. This policy does not apply to any information collected offline or via channels other than this website.",

	'privacy-consent' => "Consent",
	'privacy-consent-content' => 'By using our website, you hereby consent to our privacy policy and agree to its terms.',

	'privacy-update' => 'Update',
	'privacy-update-content' => 'This Privacy Policy was last updated on: Sunday, June 7th, 2015.'

);
