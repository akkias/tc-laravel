<?php

return array(
	
	/* Start Page */

	'we-are-glad-you-are-here' => 'We are glad you are here',
	'get-start-message' => 'We believe that a shared record of our lives and experiences, beautifully told from all corners of the planet, will inspire more of us to travel, to make the most of our time on Earth, and to care for the people and places around us. Tell us about all your trips, your stories and we will help you get set up. Things are just getting started around here and we are proud to count you as part of our founding community... happy storytelling!',

	'lets-go' => 'Let’s go',


	/* Slider Banner  */

	'slide-one' => 'As you write a story, you can select the text and choose different formatting.',
	'slide-two' => 'At the start any new paragraph clicks on the icon to the left to the upload a new paragraph.',
	'slide-three' => 'Click on any paragraph that you have uploaded to choose from different layout and sizes.',
	'slide-four' => 'To make your story more discoverable, select each location in your text and add a place mention',

	'what-interested' => 'What are you interested in?',
	'what-interested-message' => "Click to choose options below and we'll suggest some good stuff for you.",
	'continue' => 'Continue',

	'customize-your-profile' => 'Customize your profile',
	'customize-your-profile-message' => 'Add a photo to show your unique personality and style.',

	'find-people-know' => 'Find people you know',
	'find-people-know-message' => "Find people you may know so you can see what they are sharing. Don'+'t worry, we won't email your contacts without your permission.",
	'get-start-message1' => 'We believe that a shared record of our lives and experiences, beautifully told from all corners of the planet, will inspire more of us to travel, to make the most of our time on Earth, and to care for the people and places around us.',
    'get-start-message2' => 'Tell us about all your trips, your stories and we will help you get set up.',
    'get-start-message3' => 'Things are just getting started around here and we are proud to count you as part of our founding community... happy storytelling!',
    'flowers' => 'Flowers',
    'previous' => 'Previous',
    'next' => 'Next',
    'step-1-of-4' => 'Step 1 of 3',
    'step-2-of-4' => 'Step 2 of 3',
    'step-3-of-4' => 'Step 3 of 3',
	'step-4-of-4' => 'Step 4 of 4'

);