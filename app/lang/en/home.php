<?php

return array(
	'message' => 'It’s not just a travel story, it’s an interface to bridge what you have experienced and what is undiscovered to the rest of the world. Latent stories are waiting to be uncovered......we are going to reveal them for the world to recognize.',
    
    'tagline' => 'Its not about the destination its about journey',
    'tagline-home-stories' => "The hub of travel stories, your travel stories",
    'tagline-share-stories' => 'Share your stories and be an inspiration to others',
    'get-started' => 'get started',
    'or' => 'or',
    'home-message' => '',
    'exporer-stories' => 'explore stories',
    'all-stories' => 'All Stories'
);