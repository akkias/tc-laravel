<?php

return array(
	'already-account' => 'Already have an account?',
    'login-message' => 'Choose one of the options below to create your new account and join our community.',

    'create-an-account' => 'creating an account',
    'accept-terms' => "I accept",
    'terms-services' => 'Terms and Conditions'
);