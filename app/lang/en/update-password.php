<?php

return array(
	'change-password' => 'Change your password',
    'current-password' => 'Current Password',
    'new-password'=> 'New Password',
    'confirm-password' => 'Confirm Password',
    'update-password' => 'Update Password',

    'forgot-your-password' => 'Forgot Your Password?',
    'forgot-password-info' => 'Enter your email below and we will send you to Reset your password and login to your TripTroop account.',

    'reset-your-password' => 'Reset Your Password?',
    'reset-password-info' => 'Enter your new password below to login your TripTroop account.'
    
);