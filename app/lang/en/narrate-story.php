<?php

return array(
	
	'name-your-story' => 'Name your story',
	'select-duration' => 'Select duration of the trip',
	'total-expenditure' => 'Total expenditure',
	'insert-ammount' => 'Insert ammount',

	'classify-trip' => 'Classify your trip',

	'select-cover' => 'Select Cover',
	'add-hashtags' => 'Add hashtags',
	'category-hashtags' => 'CATEGORIES & HASHTAGS',
	'invite-friends' => 'Invite friends from',

	'categories' => 'Categories',
	'what-stories-discover?' => 'What stories do you want to discover?',
	'explore-categories' => 'Explore Categories'

);