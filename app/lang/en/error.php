<?php

return array(
	'you-got-lost' => 'You got lost',
    'ouch' => 'Ouch!',

    'you-have-arrived' => 'You have arrived.',
    'look-like-something-wrong' => 'Look like something went wrong',
);