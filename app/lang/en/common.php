<?php

return array(

	'sign-up' => 'Sign Up',
    'sign-in' => 'Sign In',

    'narrate-story' => 'Narrate A Story',
    'home' => 'Home',
    'editors-picks' => 'Editors Pics',
    'storytellers' => 'Storytellers',
    'themes' => 'Themes',
    
    // Categories
    'fav-travel-activities' => 'Favorite Travel Activities',
    
    'advanture' => 'Adventure',
    'arts-lieasure' => 'Arts and Leiasure',
    'backpacking' => 'Backpacking',
    'beach' => 'Beach',
    'budget-traval' => 'Budget Traval',
    'coasts-islands' => 'Coasts and Islands',
    'diving-snorkelling' => 'Diving and snorkelling',
    'ecotourism' => 'Ecotourism',
    'family-travel' => 'Family Travel',
    'festival-events' => 'Festival and Events',
    'film-television' => 'Film and television',
    'food-drink' => 'Food and Drink',
    'gear-tech' => 'Gear and Tech',
    'honymoon-romance' => 'Honeymoon and Romance',
    'luxury-travel' => 'Luxury Travel',
    'music' => 'Music',
    'off-beaten-track' => 'Off the beaten track',
    'planes-trains' => 'Planes and Trains',
    'road-trips' => 'Road Trips',
    'round-world-travel' => 'Round the world Travel',
    'travel-photoghraphy' => 'Travel Photoghraphy',
    'travel-shopping' => 'Travel Shopping',
    'walking-trekking' => 'Walking and Trekking',
    'wildlife-nature' => 'Wild life and Nature',

    'recent-stories' => 'Recent Stories',
    'popular-stories' => 'Popular Stories',
    'popular-story' => 'popular story',
    'all-stories' => 'All Stories',

    'email' => 'Email',
    'password' => 'password',
    'forgot-password' => 'Forgotten your password',
    
    'first-name' => 'First name',
    'last-name' => 'Last name',
    'set-password' => 'Set password',
    'gender' => 'Gender',
    'male' => 'Male',
    'female' => 'Female',
    'phone-number' => 'Phone Number',
    'location' => 'Location',
    'description' => 'Description',

   
    'my-profile' => 'My Profile',
    'drafts' => 'Drafts',
    'dreamlist' => 'Bookmarks',
    'settings' => 'Settings',
    'sign-out' => 'Sign Out',
    


    /* Profile Page */
    'likes' => 'Likes',
    'follow' => 'Follow',
    'following' => 'Following',
    'followers' => 'Followers',
    'social' => 'Social',
    
    'publish-trip' => 'Publish Trip',

    /* Settings */
    'account' => 'Account',

    'stories' => 'Stories',

    'save' => 'Save as draft',
    'save1' => 'Save',
    'publish' => 'Publish story',
    'update' => 'Update',

    'all-themes' => 'All Themes', /* new added */
    'about-me' => 'About Me',  /* new added */
    'my-drafts' => 'My Drafts', /* new added */
    'bio' => 'Bio', /* new added */
    'follow' => 'Follow' /* new added */

);