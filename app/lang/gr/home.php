<?php

return array(
	'message' => 'Es ist nicht einfach nur ein Reisen Geschichte, es ist eine Schnittstelle zu überbrücken, was sie erlebt haben und was ist unentdeckt in den Rest der Welt. Latente Geschichten warten auf ungedeckte......wir zeigen Ihnen die Welt zu erkennen.',
    
    'tagline' => 'Es ist nicht über das Ziel Ihrer Reise über',
    'tagline-home-stories' => 'Die Nabe der Reiseberichte, ihre Reiseberichte',
    'tagline-share-stories' => 'Erzählen Sie Ihre Geschichten und eine Inspiration für andere',
    'get-started' => 'loslegen',
    'or' => 'or',
    'exporer-stories' => 'explore stories',
    'all-stories' => 'Alle Geschichten'  
);