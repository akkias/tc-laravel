<?php

return array(
	'change-password' => 'Ändern Sie Ihr Kennwort',
    'current-password' => 'Aktuelles Passwort',
    'new-password'=> 'Neues Kennwort',
    'confirm-password' => 'Passwort bestätigen',
    'update-password' => 'Kennwort aktualisieren',

    'forgot-your-password' => 'Haben Sie Ihr Passwort vergessen?',
    'forgot-password-info' => 'Geben Sie unten Ihre e-mail Adresse ein und wir senden Ihnen Ihr Passwort zurückzusetzen und Login zu Ihrem Konto TripTroop.',

    'reset-your-password' => 'Ihr Kennwort zurücksetzen?',
    'reset-password-info' => 'Geben Sie Ihr neues Passwort für Login Ihr Konto TripTroop.'
    
);