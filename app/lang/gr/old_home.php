<?php

return array(
	'home-travel-stories' => 'Startseite für Reise stroies',
    'message' => 'Wir glauben, dass eine gut durchdachte Handlung kann eine spürbare, positive Auswirkungen in der ganzen Welt, und alle von uns, in jeder Ecke des globalen, haben einzigartige Geschichten zu teilen.'
    
    'tagline' => 'Es ist nicht über das Ziel Ihrer Reise über',
    'tagline-home-stories' => 'Startseite für Reise stroies',
    'tagline-share-stories' => 'Erzählen Sie Ihre Geschichten und Inspiration für andere',
    'get-started' => 'loslegen',
    'or' => 'or',
    'exporer-stories' => 'explore stories',
    'all-stories' => 'Alle Geschichten'    
);