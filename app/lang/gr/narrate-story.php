<?php

return array(
	
	'name-your-story' => 'Name ihre Geschichte',
	'select-duration' => 'Wählen Sie Dauer der Reise',
	'total-expenditure' => 'Ausgaben insgesamt',
	'insert-ammount' => 'Menge einfügen',

	'classify-trip' => 'Klassifizieren Sie Ihre Reise',

	'select-cover' => 'Wählen Sie Abdeckung',
	'add-hashtags' => 'Hinzufügen hashtags',
	'category-hashtags' => 'Kategorien & hashtags',
	'invite-friends' => 'Einladen von Freunden',

	'categories' => 'Kategorien',
	'what-stories-discover?' => 'Welche Geschichten möchten Sie entdecken?',
	'explore-categories' => 'Kategorien erkunden.'

);