<?php

return array(

	'priv-policy-title' => 'Datenschutz (Privacy Policy) für TripTroop',
	
	'privacy-content1' => 'Wenn Sie weitere Informationen benötigen oder Fragen zu unseren Datenschutzrichtlinien haben, kontaktieren Sie uns bitte per E-Mail an',
	'privacy-content2' => "Bei <a href='https://www.triptroop.me'>https://www.triptroop.me</a> betrachten wir die Privatsphäre unserer Besucher sehr wichtig. Diese privacy policy Dokument beschreibt im Detail die Art der persönlichen Informationen gesammelt und aufgezeichnet von <a href='https://www.triptroop.me'>https://www.triptroop.me</a> und wie wir sie verwenden.",
	
	'privacy-log-title' => 'Log Dateien',
	'privacy-log-content' => "Wie viele andere Websites, <a href='https://www.triptroop.me'>https://www.triptroop.me</a> macht die Verwendung von LOG-Dateien. Diese Dateien lediglich Protokolle Besucher auf der Seite - in der Regel ein Standardverfahren für das Hosting von Unternehmen und ein Teil der Hosting Services Die analytics. Die Informationen in den Protokolldateien enthält Internet Protocol (IP)-Adressen, Browser-Typ, Internet Service Provider (ISP), Datums-/Zeitstempel, Bezug/Exit Pages und möglicherweise die Anzahl der Klicks. Diese Information wird verwendet, um Trends zu analysieren, die Verwaltung der Site, Anschluss des Benutzers Bewegung rund um den Ort, und demografische Daten zu erfassen. IP-Adressen und andere solche Informationen werden nicht mit Informationen, die sich persönlich identifizierbar sind.",

	'privacy-cookies-title' => 'Cookies und Web Beacons',
	'privacy-cookies-content' => "Verwendet Cookies, um Informationen über Präferenzen der Besucher, um Benutzer-spezifische Informationen darüber, welche Seiten der Site Besucher oder besucht, und zu personalisieren oder passen unsere Webseite Inhalte basierend auf Besucher' Browser Typ oder andere Informationen, die der Besucher sendet über Ihren Browser.",

	'privacy-child-info' => "Kinder Informationen",
	'privacy-child-info-content' => "Wir glauben, dass es wichtig ist zu bieten zusätzlichen Schutz für Kinder im Internet. Wir bitten Eltern und Erziehungsberechtigte Zeit online mit ihren Kindern verbringen zu beobachten, sich daran beteiligen und/oder Überwachen und führen Ihre Online-Aktivitäten. <a href='https://www.triptroop.me'>https://www.triptroop.me</a> nicht wissentlich sammeln keine persönlich identifizierbaren Informationen von Kindern unter 13 Jahren. Wenn ein Elternteil oder ein Vormund ist der Auffassung, dass <a href='https://www.triptroop.me'>https://www.triptroop.me</a> hat in seiner Datenbank der persönlich identifizierbaren Informationen von Kindern unter 13 Jahren, nehmen Sie bitte umgehend Kontakt mit uns (über den Kontakt im ersten Absatz) und wir werden unser Möglichstes, umgehend entfernen diese Informationen aus unseren Aufzeichnungen.",

	'privacy-online' => "Richtlinien für den Datenschutz",
	'privacy-online-content' => "Diese Datenschutzerklärung bezieht sich nur auf unsere Online Aktivitäten und ist gültig für die Besucher unserer Website und über freigegebene Informationen und/oder dort gesammelt. Diese Richtlinie findet keine Anwendung auf Daten, die offline erfasst werden oder über andere Kanäle als dieser Website.",

	'privacy-consent' => "Zustimmung",
	'privacy-consent-content' => 'Indem Sie unsere Website nutzen, stimmen Sie hiermit zu unserer privacy policy und um sich an die Bedingungen zu binden.',

	'privacy-update' => 'Aktualisierung',
	'privacy-update-content' => 'Diese Datenschutzerklärung wurde zuletzt aktualisiert am: Sonntag, Juni 7th, 2015.'

);
