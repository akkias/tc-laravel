<?php

return array(

	/* Common.php*/

	'all-themes' => 'All Themes', /* new added in common.php */
	'about-me' => 'About Me'  /* new added */
	'my-drafts' => 'My Drafts', /* new added */
	'bio' => 'Bio', /* new added */
	'follow' => 'Follow', /* new added */


	/* Error.php */

	'you-got-lost' => 'You got lost',
    'ouch' => 'Ouch!',
    'you-have-arrived' => 'You have arrived.'
    'look-like-something-wrong' => 'Look like something went wrong',


    /* getting Started */

    'get-start-message1' => 'We believe that a shared record of our lives and experiences, beautifully told from all corners of the planet, will inspire more of us to travel, to make the most of our time on Earth, and to care for the people and places around us.'
    'get-start-message2' => 'Tell us about all your trips, your stories and we will help you get set up.'
    'get-start-message3' => 'Things are just getting started around here and we are proud to count you as part of our founding community... happy storytelling!'
    'flowers' => 'Flowers'
    'previous' => 'Previous'
    'next' => 'Next'
    'step-1-of-4' => 'Step 1 of 4'
    'step-2-of-4' => 'Step 2 of 4'
    'step-3-of-4' => 'Step 3 of 4'
	'step-4-of-4' => 'Step 4 of 4'


	/* Popular.php */

	'no-popular-story-message' => 'There are no popular stories available at this time',
	'first-one-narrate' => 'Be the first one to narrate a'

	/* Profile.php */

	'bio-info' => 'A social psychologist and marketer, Jennifer Doe is the General Atlantic Professor of Marketing and Ormond Family Faculty at Stanford Universityâ€™s Graduate School of Business. Her research spans time, money and happiness. She focuses on questions such as: What actually makes people happy, as opposed to what they think makes them happy? tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor.sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.',
	

	/* Narrate Story.php */
	'select-cover' => 'Select Cover'
	'add-hashtags' => 'Add hashtags'
	'category-hashtags' => 'CATEGORIES & HASHTAGS'
	'invite-friends' => 'Invite friends from',
	'categories' = 'Categories',
	'what-stories-discover?' => 'What stories do you want to discover?',
	'explore-categories' => 'Explore Categories'


	/* Search Page */

	'sorted-by' => 'SORTED BY',
	'most-recent' => 'Most Recent',
	'country' => 'COUNTRY',
	'people' => 'PEOPLE',

);