<?php

return array(
	'sign-up' => 'anheuern',
    'sign-in' => 'anmelden',

    'narrate-story' => 'ine Geschichte erzählen',
    'home' => 'Nach Hause',
    'editors-picks' => 'Redaktion Pics',
    'storytellers' => 'Märchenerzähler',
    'themes' => 'Leitmotiv',

    'advanture' => 'Abenteuér',
    'arts-lieasure' => 'Kunst- und Lieasure',
    'beach' => 'Strand',
    'budget-traval' => 'Günstige Reisen',
    'coasts-islands' => 'Küsten und Inseln',
    'diving-snorkelling' => 'Tauchen und Schnorcheln',
    'ecotourism' => 'Ökotourismus',
    'family-travel' => 'Familie Reisen',
    'festival-events' => 'Festivals und Veranstaltungen',
    'film-television' => 'Film und Fernsehen',
    'food-drink' => 'Essen und Getränk',
    'gear-tech' => 'Gang und Tech',
    'honymoon-romance' => 'Hochzeitsreise und Romantik',
    'luxury-travel' => 'Luxusreisen',
    'music' => 'Musik',
    'off-beaten-track' => 'Abseits vom Touristenrummel',
    'planes-trains' => 'Flugzeuge und Züge',
    'road trips' => 'eine Autoreise',
    'round-world-travel' => 'Rund um die Welt reisen',
    'travel-photoghraphy' => 'Reise photoghraphy',
    'travel-shopping' => 'Reisen Shopping',
    'walking-trekking' => 'Wandern und Trekking',
    'wildlife-nature' => 'Wilde Tiere und Natur',

    'recent-stories' => 'Den letzten Geschichten',
    'popular-stories' => 'Beliebte Geschichten',

    'all-stories' => 'Alle Geschichten'
    
);