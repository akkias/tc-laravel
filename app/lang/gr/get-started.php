<?php

return array(
	
	/* Start Page */

	'we-are-glad-you-are-here' => 'Wir sind froh, dass Sie sich hier',
	'get-start-message' => 'Wir glauben, dass ein freigegebenen Datensatz der unser Leben und unsere Erfahrungen, wunderschön erzählt aus allen Ecken des Planeten, begeistern mehr von uns zu reisen, um das Beste aus unserer Zeit auf der Erde und für die Versorgung der Menschen und Orte um uns herum. Informieren Sie uns über all Ihre Reisen, ihre Geschichten und wir werden Ihnen helfen, einrichten. Die Dinge sind nur erste Schritte hier in der Gegend, und wir sind stolz zu zählen sie als Teil unserer Gründung Gemeinschaft... happy Storytelling!',

	'lets-go' => 'Lass uns gehen',


	/* Slider Banner  */

	'slide-one' => 'Wie Sie Geschichte schreiben, können Sie den Text und wählen Sie unterschiedliche Formatierungen.',
	'slide-two' => 'Zu Beginn jeder neuen Absatz klickt auf das Symbol auf der linken Seite zum Hochladen eines neuen Absatzes.',
	'slide-three' => 'Klicken Sie auf eine beliebige Ziffer, die Sie hochgeladen haben, wählen Sie aus verschiedenen Layouts und Größen.',
	'slide-four' => 'Um ihre Geschichte mehr erkennbar, wählen Sie jede Stelle in ihrem Text und fügen Sie einen Ort nennen',

	'what-interested' => 'Was interessiert Sie?',
	'what-interested-message' => 'Klicken Sie zum Auswählen von Optionen unten aus, und wir schlagen einige gute Sachen für Sie.',
	'continue' => 'Weiter',

	'customize-your-profile' => 'Personalisieren Sie Ihr Profil',
	'customize-your-profile-message' => 'nzufügen eines Fotos zu zeigen, ihre einzigartige Persönlichkeit und Stil.',

	'find-people-know' => 'Finden Sie Menschen, die Sie kennen',
	'find-people-know-message' => 'Personen suchen Sie vielleicht wissen, damit Sie sehen können, was Sie freigeben. Keine Sorge, wir werden nicht email ihre Kontakte ohne Ihre Erlaubnis.',
	'get-start-message1' => 'Wir glauben, dass ein freigegebenen Datensatz der unser Leben und unsere Erfahrungen, wunderschön erzählt aus allen Ecken des Planeten, begeistern mehr von uns zu reisen, um das Beste aus unserer Zeit auf der Erde und für die Versorgung der Menschen und Orte um uns herum.',
    'get-start-message2' => 'Informieren Sie uns über all Ihre Reisen, ihre Geschichten und wir werden Ihnen helfen, einrichten.',
    'get-start-message3' => 'Die Dinge sind nur erste Schritte hier in der Gegend, und wir sind stolz zu zählen sie als Teil unserer Gründung Gemeinschaft... happy Storytelling!',
    'flowers' => 'Blumen',
    'previous' => 'Vorherige',
    'next' => 'Weiter',
    'step-1-of-4' => 'Schritt 1 von 3',
    'step-2-of-4' => 'Schritt 2 von 3',
    'step-3-of-4' => 'Schritt 3 von 3',
	'step-4-of-4' => 'Schritt 4 von 4'

);