<?php

return array(
	'already-account' => 'Sie haben bereits ein Konto?',
    'login-message' => 'Wählen Sie eine der folgenden Optionen zum Erstellen des neuen Kontos und der community beitreten.',

    'create-an-account' => 'Erstellen eines Kontos',
    'accept-terms' => 'Ich akzeptiere',
    'terms-services' => 'Begriffe Begriffe-Services'
);