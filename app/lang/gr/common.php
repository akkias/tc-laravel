<?php

return array(

	'sign-up' => 'anheuern',
    'sign-in' => 'anmelden',

    'narrate-story' => 'ine Geschichte erzählen',
    'home' => 'Nach Hause',
    'editors-picks' => 'Redaktion Pics',
    'storytellers' => 'Märchenerzähler',
    'themes' => 'Themen',

    // Categories
    'fav-travel-activities' => 'Favorit Reisen Aktivitäten',

    'advanture' => 'Abenteuér',
    'arts-lieasure' => 'Kunst- und Freizeit',
    'backpacking' => 'Rucksackreisen',
    'beach' => 'Strand',
    'budget-traval' => 'Günstige Reisen',
    'coasts-islands' => 'Küsten und Inseln',
    'diving-snorkelling' => 'Tauchen und Schnorcheln',
    'ecotourism' => 'Ökotourismus',
    'family-travel' => 'Familie Reisen',
    'festival-events' => 'Festivals und Veranstaltungen',
    'film-television' => 'Film und Fernsehen',
    'food-drink' => 'Essen und Getränk',
    'gear-tech' => 'Gang und Tech',
    'honymoon-romance' => 'Hochzeitsreise und Romantik',
    'luxury-travel' => 'Luxusreisen',
    'music' => 'Musik',
    'off-beaten-track' => 'Abseits vom Touristenrummel',
    'planes-trains' => 'Flugzeuge und Züge',
    'road-trips' => 'eine Autoreise',
    'round-world-travel' => 'Rund um die Welt reisen',
    'travel-photoghraphy' => 'Reise photoghraphy',
    'travel-shopping' => 'Reisen Shopping',
    'walking-trekking' => 'Wandern und Trekking',
    'wildlife-nature' => 'Wilde Tiere und Natur',

    'recent-stories' => 'Den letzten Geschichten',
    'popular-stories' => 'Beliebte Geschichten',
    'popular-story' => 'beliebte Geschichte',

    'all-stories' => 'Alle Geschichten',

    'email' => 'E-Mail',
    'password' => 'Kennwort',
    'forgot-password' => 'Passwort vergessen',

    'first-name' => 'Vorname',
    'last-name' => 'Nachname',
    'set-password' => 'Kennwort festlegen',
    'gender' => 'Geschlecht',
    'male' => 'Männlich',
    'female' => 'Weiblich',
    'phone-number' => 'Telefonnummer',
    'location' => 'Lage',
    'description' => 'Beschreibung',

    'my-profile' => 'Mein Profil',
    'drafts' => 'Entwürfe',
    'Dreamlist' => 'Lesezeichen',
    'settings' => 'Einstellungen',
    'sign-out' => 'Abmelden',

    /* Profile Page */
    'likes' => 'Mag',
    'follow' => 'Folgen',
    'following' => 'Folgende',
    'followers' => 'Anhänger',
    'social' => 'Soziale',
    'publish-trip' => 'Reise veröffentlichen',

    /* Settings */
    'account' => 'Konto',


    'stories' => 'Geschichten',

    'save' => 'Als Entwurf speichern',
    'save1' => 'Speichern',
    'publish' => 'Veröffentlichen',
    'update' => 'Aktualisierung',

    'all-themes' => 'Alle Themen', /* new added */
    'about-me' => 'Über mich',  /* new added */
    'my-drafts' => 'Meine Entwürfe', /* new added */
    'bio' => 'Bio', /* new added */
    'follow' => 'Folgen' /* new added */



);