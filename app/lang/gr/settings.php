<?php

return array(
	'basic-info' => 'Grundlegende Informationen',
    'basic-info-message' => 'Ändern Sie Ihre Basic Account- und Spracheinstellungen.',

    'primary-lang' => 'Primäre Sprache',
    'email-info' => 'E-Mail wird nicht öffentlich angezeigt.'
);