<?php

return array(
	'message' => 'Não se trata apenas de uma história sobre a viagem, é uma interface para a ponte que você tem vivido e que é desconhecido para o resto do mundo. Histórias latentes estão à espera de ser descobertos ...... vamos revelá-los para o mundo a reconhecer.',
    
    'tagline' => 'Não é sobre o destino de viagem',
    'tagline-home-stories' => 'O cubo de histórias de viagem, suas histórias de viagem',
    'tagline-share-stories' => 'Partilhe as suas histórias e ser uma fonte de inspiração para outros',
    'get-started' => 'Começar',
    'or' => 'ou',
    'exporer-stories' => 'explorar histórias',
    'all-stories' => 'Todas as histórias'
);