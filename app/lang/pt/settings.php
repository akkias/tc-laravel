<?php

return array(
	'basic-info' => 'Informações Básicas',
    'basic-info-message' => 'Alterar a conta básica e as definições de idioma.',

    'primary-lang' => 'Idioma principal',
    'email-info' => 'E-mail não será exibida publicamente'
);