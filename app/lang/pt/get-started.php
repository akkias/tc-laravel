<?php

return array(
	
	/* Start Page */

	'we-are-glad-you-are-here' => 'Estamos muito felizes por você está aqui',
	'get-start-message' => 'Acreditamos que um registro compartilhado de nossas vidas e experiências, maravilhosamente contada de todos os cantos do planeta, irá inspirar mais de nós para viajar, para fazer a maior parte do nosso tempo na terra, e para cuidar de pessoas e lugares em torno de nós. Conte-nos sobre todas as suas viagens, as suas histórias e que irá ajudá-lo a obter. As coisas estão apenas começando por aqui e estamos orgulhosos de contar-vos como parte de nossa fundação comunidade ... feliz narrativa!',

	'lets-go' => 'Vamos',


	/* Slider Banner  */

	'slide-one' => 'Como você escreve uma história, você pode selecionar o texto e escolher diferentes formatações.',
	'slide-two' => 'No início qualquer novo nº cliques no ícone à esquerda para o upload de um novo parágrafo.',
	'slide-three' => 'Clique em qualquer ponto que você tem carregado a escolha do layout diferente e tamanhos.',
	'slide-four' => 'Para tornar a sua história mais detectável, selecione cada local no seu texto e adicione um lugar referência',

	'what-interested' => 'O que você está interessado?',
	'what-interested-message' => 'Clique para escolher as opções abaixo e vamos sugerir algumas coisas boas para você.',
	'continue' => 'Continuar',

	'customize-your-profile' => 'Personalize o seu perfil',
	'customize-your-profile-message' => 'Adicionar uma foto para mostrar a sua personalidade única e estilo.',

	'find-people-know' => 'Encontrar pessoas que você conhece',
	'find-people-know-message' => 'Encontrar pessoas que você pode conhecer, para que você possa ver o que é que eles estão compartilhando. Não se preocupe, não vai enviar e-mails aos seus contatos sem a sua permissão.',
	'get-start-message1' => 'Acreditamos que um registro compartilhado de nossas vidas e experiências, maravilhosamente contada de todos os cantos do planeta, irá inspirar mais de nós para viajar, para fazer a maior parte do nosso tempo na terra, e para cuidar de pessoas e lugares em torno de nós.',
    'get-start-message2' => 'Conte-nos sobre todas as suas viagens, as suas histórias e que irá ajudá-lo a obter.',
    'get-start-message3' => 'As coisas estão apenas começando por aqui e estamos orgulhosos de contar-vos como parte de nossa fundação comunidade ... feliz narrativa!',
    'flowers' => 'Flores',
    'previous' => 'Anterior',
    'next' => 'Próxima',
    'step-1-of-4' => 'Passo 1 de 3',
    'step-2-of-4' => 'Passo 2 de 3',
    'step-3-of-4' => 'Passo 3 de 3',
	'step-4-of-4' => 'Passo 4 de 4'

);