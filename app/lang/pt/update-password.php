<?php

return array(
	'change-password' => 'Alterar sua senha',
    'current-password' => 'Senha Atual',
    'new-password'=> 'Nova Senha',
    'confirm-password' => 'Confirmar Senha',
    'update-password' => 'Atualização de Senha', 

    'forgot-your-password' => 'Esqueceu-se da sua palavra-passe?',
    'forgot-password-info' => 'Digite seu e-mail abaixo e nós lhe enviaremos para redefinir sua senha e login para o seu TripTroop conta.',

    'reset-your-password' => 'Redefina sua senha?',
    'reset-password-info' => 'Digite sua nova senha abaixo para efetuar login seu TripTroop conta.'
    
);