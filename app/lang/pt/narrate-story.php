<?php

return array(
	
	'name-your-story' => 'Nome sua história',
	'select-duration' => 'Selecione a duração da viagem',
	'total-expenditure' => 'As despesas totais',
	'insert-ammount' => 'Insira relaçao',

	'classify-trip' => 'Classificar a sua viagem',

	'select-cover' => 'Selecione Cobrir',
	'add-hashtags' => 'AAdicionar hashtags',
	'category-hashtags' => 'Categorias & hashtags',
	'invite-friends' => 'Convido os amigos a partir de',

	'categories' => 'Categorias',
	'what-stories-discover?' => 'Que histórias você deseja descobrir?',
	'explore-categories' => 'Explore as Categorias'

);