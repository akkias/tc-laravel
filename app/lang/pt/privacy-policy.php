<?php

return array(

	'priv-policy-title' => 'Política de Privacidade TripTroop',
	
	'privacy-content1' => 'Se você precisar de mais informações ou tem alguma dúvida sobre nossa política de privacidade, por favor, sinta-se livre para entrar em contato conosco através de e-mail para',
	'privacy-content2' => "A <a href='https://www.triptroop.me'>https://www.triptroop.me</a> consideramos a privacidade dos nossos visitantes é muito importante. Esta política de privacidade documento descreve em detalhes os tipos de informações pessoais são coletados e registrados pelo <a href='https://www.triptroop.me'>https://www.triptroop.me</a> e como podemos usá-lo.",
	
	'privacy-log-title' => 'Arquivos de Log',
	'privacy-log-content' => "Como muitos outros sites, <a href='https://www.triptroop.me'>https://www.triptroop.me</a> faz uso de arquivos de log. Estes arquivos apenas registra os visitantes do site - geralmente um procedimento padrão para empresas de hospedagem e uma parte de serviços de hospedagem do analytics. As informações dentro dos arquivos de log inclui endereços de protocolo de internet (IP), tipo de navegador, provedor de serviços de Internet (ISP), carimbo de data/hora, páginas de referência/saída, e possivelmente o número de cliques. Esta informação é utilizada para analisar tendências, administrar o site, via movimento do usuário por todo o site e reunir informações demográficas. Endereços IP, e outras informações não estão ligados a qualquer informação que seja pessoalmente identificável.",

	'privacy-cookies-title' => 'Os Cookies e Web Beacons',
	'privacy-cookies-content' => "Usa cookies para armazenar informações sobre preferências dos visitantes, registar informações específicas de usuário sobre as páginas que o site que o visitante acessa ou visitas, personalizar ou customizar nosso conteúdo da página da web com base no tipo de navegador dos visitantes ou outro tipo de informação que o visitante envia através do seu navegador.",

	'privacy-child-info' => "As informações das Crianças",
	'privacy-child-info-content' => "Acreditamos que é importante para proporcionar maior proteção para as crianças on-line. Nós encorajamos os pais e responsáveis a passar tempo online com os seus filhos a observar, participar e/ou acompanhar e orientar a sua actividade online. <a href='https://www.triptroop.me'>https://www.triptroop.me</a> não recolhemos intencionalmente quaisquer informações pessoalmente identificáveis de crianças sob a idade de 13 anos. Se um dos pais ou guardião acredita que <a href='https://www.triptroop.me'>https://www.triptroop.me</a> tem em seu banco de dados, a informação pessoalmente identificável de uma criança com menos de 13 anos, por favor entre em contato conosco imediatamente (através do contato no primeiro parágrafo) e vamos utilizar os nossos melhores esforços para remover rapidamente as informações de nossos registros.",

	'privacy-online' => "Política de Privacidade On-line",
	'privacy-online-content' => "Esta política de privacidade aplica-se apenas às nossas atividades on-line e é válida para os visitantes de nosso site e com relação as informações compartilhadas e/ou recolhidos no local. Esta política não se aplica a quaisquer informações coletadas offline ou através de canais que não este site.",

	'privacy-consent' => "Consentimento",
	'privacy-consent-content' => 'Ao utilizar nosso site, você, por meio deste, consente em nossa política de privacidade e concorda com seus termos.',

	'privacy-update' => 'atualizar',
	'privacy-update-content' => 'Esta Política de Privacidade foi actualizada pela última vez em: domingo, Junho 7th, 2015.'

);
