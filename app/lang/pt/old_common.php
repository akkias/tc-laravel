<?php

return array(

	'sign-up' => 'Inscreva-se',
    'sign-in' => 'Iniciar sessão',

    'narrate-story' => 'Narrar uma História',
    'home' => 'casa',
    'editors-picks' => 'Os Editores Pics',
    'storytellers' => 'As presidiárias',
    'themes' => 'Temas',
    
    // Categories
    'fav-travel-activities' => 'Favorite Travel Activities',
    
    'advanture' => 'Advanture',
    'arts-lieasure' => 'Artes e Lieasure',
    'beach' => 'Praia',
    'budget-traval' => 'Traval Orçamento',
    'coasts-islands' => 'Costas e Ilhas',
    'diving-snorkelling' => 'Mergulho e snorkelling',
    'ecotourism' => 'O ecoturismo',
    'family-travel' => 'Viagens em família',
    'festival-events' => 'Festas e Eventos',
    'film-television' => 'Cinema e Televisão',
    'food-drink' => 'Comida e Bebida',
    'gear-tech' => 'Artes e tecnologia',
    'honymoon-romance' => 'E Romance Honymoon',
    'luxury-travel' => 'Viagens de Luxo',
    'music' => 'Música',
    'off-beaten-track' => 'O trilho batido',
    'planes-trains' => 'Os aviões e comboios',
    'road trips' => 'longa viagem',
    'round-world-travel' => 'Viagem à Volta do Mundo',
    'travel-photoghraphy' => 'Viajar Photoghraphy',
    'travel-shopping' => 'Shopping Viagens',
    'walking-trekking' => 'A caminhada e Trekking',
    'wildlife-nature' => 'Vida selvagem e natureza',

    'recent-stories' => 'Histórias recentes',
    'popular-stories' => 'Histórias Populares',

    'all-stories' => 'Todas as histórias'
    
);