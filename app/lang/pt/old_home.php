<?php

return array(
	'home-travel-stories' => 'Casa para todos viajar stroies',
    'message' => 'Queremos acreditar que as atenciosas narrativa pode tornar tangível, impacto positivo no mundo, e que todos nós, em cada canto do mundo, têm histórias únicas para partilhar.'
    
    'tagline' => 'Não é sobre o destino de viagem',
    'tagline-home-stories' => 'Casa para todos viajar stroies',
    'tagline-share-stories' => 'Share your stories and be inspiration to others',
    'get-started' => 'Começar',
    'or' => 'ou',
    'exporer-stories' => 'explorar histórias',
    'all-stories' => 'Todas as histórias'
);