<?php

return array(
	'dont-account' => 'Ainda não tem uma conta',
    'login-message' => 'Escolha uma das opções abaixo para criar sua nova conta e junte-se à nossa comunidade.'
);