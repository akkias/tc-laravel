<?php

return array(
	'already-account' => 'Já tem uma conta?',
    'login-message' => 'Escolha uma das opções abaixo para criar sua nova conta e junte-se à nossa comunidade.',

    'create-an-account' => 'Criar uma conta',
    'accept-terms' => 'Aceito',
    'terms-services' => 'Termos dos termos de serviços'
);