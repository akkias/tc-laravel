<?php

return array(

	'sign-up' => 'Inscreva-se',
    'sign-in' => 'Iniciar sessão',

    'narrate-story' => 'Narrar uma História',
    'home' => 'casa',
    'editors-picks' => 'Os Editores Pics',
    'storytellers' => 'As presidiárias',
    'themes' => 'Temas',
    
    // Categories
    'fav-travel-activities' => 'Actividades viagens favoritos',
    
    'advanture' => 'Aventura',
    'arts-lieasure' => 'Artes e Lazer',
    'backpacking' => 'Mochila',
    'beach' => 'Praia',
    'budget-traval' => 'Traval Orçamento',
    'coasts-islands' => 'Costas e Ilhas',
    'diving-snorkelling' => 'Mergulho e snorkelling',
    'ecotourism' => 'O ecoturismo',
    'family-travel' => 'Viagens em família',
    'festival-events' => 'Festas e Eventos',
    'film-television' => 'Cinema e Televisão',
    'food-drink' => 'Comida e Bebida',
    'gear-tech' => 'Artes e tecnologia',
    'honymoon-romance' => 'E Romance Honymoon',
    'luxury-travel' => 'Viagens de Luxo',
    'music' => 'Música',
    'off-beaten-track' => 'O trilho batido',
    'planes-trains' => 'Os aviões e comboios',
    'road-trips' => 'longa viagem',
    'round-world-travel' => 'Viagem à Volta do Mundo',
    'travel-photoghraphy' => 'Viajar Photoghraphy',
    'travel-shopping' => 'Shopping Viagens',
    'walking-trekking' => 'A caminhada e Trekking',
    'wildlife-nature' => 'Vida selvagem e natureza',

    'recent-stories' => 'Histórias recentes',
    'popular-stories' => 'Histórias Populares',
    'popular-story' => 'história popular',

    'all-stories' => 'Todas as histórias',

    'email' => 'E-Mail',
    'password' => 'Senha',
    'forgot-password' => 'Esqueceu a sua senha',

    'first-name' => 'Primeiro nome',
    'last-name' => 'Último nome',
    'set-password' => 'Definir senha',
    'gender' => 'Sexo',
    'male' => 'Sexo Masculino',
    'female' => 'Sexo Feminino',
    'phone-number' => 'Número de Telefone',
    'location' => 'Localização',
    'description' => 'Descrição',

    'my-profile' => 'Meu Perfil',
    'drafts' => 'Rascunhos',
    'Dreamlist' => 'Marcadores',
    'settings' => 'definições',
    'sign-out' => 'Inscreva-se',

    /* Profile Page */
    'likes' => 'Gosta',
    'follow' => 'Siga',
    'following' => 'Seguinte',
    'followers' => 'Seguidores',
    'social' => 'Social',
    'publish-trip' => 'Publicar Viagem',

    /* Settings */
    'account' => 'Conta',


    'stories' => 'Histórias',

    'save' => 'Salvar como rascunho',
    'save1' => 'Salvar',
    'publish' => 'Publicar',
    'update' => 'atualizar',

    'all-themes' => 'Todos os temas', /* new added */
    'about-me' => 'Acerca de mim',  /* new added */
    'my-drafts' => 'Os meus projectos', /* new added */
    'bio' => 'Bio', /* new added */
    'follow' => 'Siga' /* new added */


);