<?php

return array(
	
	'name-your-story' => 'Nom votre histoire',
	'select-duration' => 'Sélectionnez Durée du voyage',
	'total-expenditure' => 'Total des dépenses',
	'insert-ammount' => 'Insérez ammount',

	'classify-trip' => 'Classer votre voyage',

	'select-cover' => 'Sélectionner Cover',
	'add-hashtags' => 'Ajouter hashtags',
	'category-hashtags' => 'Catégories & hashtags',
	'invite-friends' => 'Inviter des amis à partir de',

	'categories' => 'Catégories',
	'what-stories-discover?' => 'Quelles histoires souhaitez-vous découvrir ?',
	'explore-categories' => 'Explorez les catégories'

);