<?php

return array(
	'home-travel-stories' => 'Maison à tout le monde voyage stroies',
    'message' => 'Nous sommes d'+'avis que réfléchie se raconter peut rendre tangible et impact positif dans le monde, et que nous devons tous, dans tous les coins du monde, ont des histoires originales à partager.'
    
    'tagline' => 'Ses pas sur la destination de son voyage',
    'tagline-home-stories' => 'Maison à tout le monde voyage stroies',
    'tagline-share-stories' => 'Partager vos histoires et être inspiration à d'+'autres',
    'get-started' => 'commencer',
    'or' => 'ou',
    'exporer-stories' => 'Explorez des histoires',
    'all-stories' => 'Toutes les histoires'
);