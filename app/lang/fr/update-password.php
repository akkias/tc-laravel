<?php

return array(
	'change-password' => 'Modifier votre mot de passe',
    'current-password' => 'Mot de passe actuel',
    'new-password'=> 'Nouveau mot de passe',
    'confirm-password' => 'Confirmer le mot de passe',
    'update-password' => 'Mise à jour du mot de passe',

    'forgot-your-password' => 'Vous avez oublié votre mot de passe?',
    'forgot-password-info' => 'Entrez votre email ci-dessous et nous vous enverrons pour réinitialiser votre mot de passe et vous connecter à votre compte TripTroop.',

    'reset-your-password' => 'Réinitialiser votre mot de passe?',
    'reset-password-info' => 'Entrez votre nouveau mot de passe ci-dessous pour vous connecter votre compte TripTroop.'
    
);