<?php

return array(
	'message' => "Il n'est pas seulement un récit de voyage, c'est une interface à combler ce que vous avez connu et ce qui n'est pas découvertes au reste du monde. Histoires latentes sont en attente d'être découvert......nous allons révéler eux que le monde reconnaisse.",
    
    'tagline' => 'Ses pas sur la destination de son voyage',
    'tagline-home-stories' => 'Le moyeu de récits de voyage, vos histoires de voyage',
    'tagline-share-stories' => "Partagez vos histoires et être une source d'inspiration pour d'autres",
    'get-started' => 'commencer',
    'or' => 'ou',
    'exporer-stories' => 'Explorez des histoires',
    'all-stories' => 'Toutes les histoires'
);