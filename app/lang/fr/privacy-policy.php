<?php

return array(

	'priv-policy-title' => 'Politique de confidentialité pour TripTroop',
	
	'privacy-content1' => "Si vous avez besoin de plus amples renseignements ou avez des questions sur notre politique de confidentialité, n'hésitez pas à nous contacter par courriel à",
	'privacy-content2' => "À <a href='https://www.triptroop.me'>https://www.triptroop.me</a> nous considérons la protection de la vie privée de nos visiteurs d'être extrêmement important. Ce document de politique de confidentialité décrit en détail les types de renseignements personnels sont recueillies et consignées par <a href='https://www.triptroop.me'>https://www.triptroop.me</a> et comment nous les utilisons.",
	
	'privacy-log-title' => 'Fichiers journaux',
	'privacy-log-content' => "Comme de nombreux autres sites Web, <a href='https://www.triptroop.me'>https://www.triptroop.me</a> utilise des fichiers journaux. Ces fichiers journaux simplement les visiteurs du site - habituellement une procédure standard pour des sociétés d'hébergement et une partie des services d'hébergement's Analytics. Les informations dans les fichiers journaux comprend les adresses de protocole Internet (IP), le type de navigateur, le fournisseur de services Internet (ISP), date/time stamp, les pages de renvoi/sortie, et peut-être le nombre de clics. Cette information est utilisée pour analyser les tendances, administrer le site, suivre la navigation de l'utilisateur de circulation autour du site, et de recueillir des informations démographiques. Les adresses IP et autres informations de ce type ne sont pas liées à des informations qui sont personnellement identifiables.",

	'privacy-cookies-title' => 'Les cookies et les balises Web',
	'privacy-cookies-content' => "Utilise des cookies pour stocker les informations sur les préférences des visiteurs, à enregistrer des informations spécifiques à l'utilisateur sur les pages du site visiteur accède à ou de visites, et de personnaliser ou de personnaliser notre contenu de la page web basée sur les visiteurs' type de navigateur ou d'autres informations que le visiteur envoie via leur navigateur.",

	'privacy-child-info' => "Renseignements sur les enfants",
	'privacy-child-info-content' => "Nous pensons qu'il est important de fournir une protection supplémentaire pour les enfants en ligne. Nous encourageons les parents et les tuteurs à passer du temps en ligne avec leurs enfants à observer, participer à et/ou de suivre et d'orienter leur activité en ligne. <a href='https://www.triptroop.me'>https://www.triptroop.me</a> ne recueille sciemment aucune information personnellement identifiable auprès d'enfants de moins de 13 ans. Si un parent ou un tuteur croit que <a href='https://www.triptroop.me'>https://www.triptroop.me</a> a dans sa base de données les informations personnelles identifiables d'un enfant de moins de 13 ans, veuillez nous contacter immédiatement (à l'aide du contact dans le premier paragraphe) et nous ferons de notre mieux pour retirer rapidement ces renseignements de nos dossiers.",

	'privacy-online' => "Politique de confidentialité en ligne",
	'privacy-online-content' => "Cette politique de confidentialité s'applique uniquement à nos activités en ligne et est valable pour les visiteurs de notre site web et au sujet de l'information partagée et/ou qui y sont recueillis. Cette politique ne s'applique pas aux renseignements recueillis hors ligne ou via d'autres canaux que le présent site web.",

	'privacy-consent' => "Consentement",
	'privacy-consent-content' => 'En utilisant notre site web, vous consentez à notre politique de confidentialité et acceptez ses conditions.',

	'privacy-update' => 'Mise à jour',
	'privacy-update-content' => 'Cette politique de confidentialité a été mise à jour le : Dimanche, Juin 7th, 2015.'

);
