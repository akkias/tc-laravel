<?php

return array(
	'dont-account' => "Vous n'avez pas encore de compte",
    'login-message' => "Choisissez l'une des options ci-dessous pour créer votre nouveau compte et rejoignez notre communauté."
);