<?php

return array(
	'already-account' => 'Vous avez déjà un compte ?',
    'login-message' => 'Choose one of the options below to create your new account and join our community.',

    'create-an-account' => "Créer un compte",
    'accept-terms' => "J'accepte",
    'terms-services' => 'Termes de termes-services'
);