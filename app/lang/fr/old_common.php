<?php

return array(
	'sign-up' => 'signer',
    'sign-in' => 'singer dans',

    'narrate-story' => 'racontent une histoire',
    'home' => 'maison',
    'editors-picks' => 'Les éditeurs de photos',
    'storytellers' => 'conteur',
    'themes' => 'Leitmotiv',

    'advanture' => 'Abenteuér',
    'arts-lieasure' => 'Arts et Lieasure',
    'beach' => 'Plage ',
    'budget-traval' => 'Budget Voyager',
    'coasts-islands' => 'Côtes et les Îles',
    'diving-snorkelling' => 'Plongée Sous-marine',
    'ecotourism' => 'écotourisme',
    'family-travel' => 'Voyages en Famille',
    'festival-events' => 'Festival et Événements',
    'film-television' => 'Du film et de la télévision',
    'food-drink' => 'Nourriture et boisson',
    'gear-tech' => 'Pignon et tech',
    'honymoon-romance' => 'Lune de miel et de Romance',
    'luxury-travel' => 'Voyages de luxe',
    'music' => 'musique',
    'off-beaten-track' => 'Hors des sentiers battus',
    'planes-trains' => 'Des trains et des avions',
    'road trips' => 'Voyages par la route',
    'round-world-travel' => 'Tour du monde Voyage',
    'travel-photoghraphy' => 'Voyage Photoghraphy',
    'travel-shopping' => 'Voyages Shopping',
    'walking-trekking' => 'Promenade et de randonnée',
    'wildlife-nature' => 'La vie sauvage et de la Nature',

    'recent-stories' => 'récentes histoires',
    'popular-stories' => 'Contes populaires',

    'all-stories' => 'Alle Geschichten'
    
);