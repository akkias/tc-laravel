<?php

return array(
	
	/* Start Page */

	'we-are-glad-you-are-here' => 'Nous sommes heureux de vous voir ici',
	'get-start-message' => "Nous pensons qu'un dossier partagé de nos vies et de leurs expériences, magnifiquement raconté de tous les coins de la planète, ils inciteront plus de nous de voyager, de faire le plus clair de notre temps sur la terre, et aux soins pour les personnes et les lieux autour de nous. Parlez-nous de vos voyages, vos histoires et nous allons vous aider à configurer. Les choses en sont à leurs premiers pas ici et nous sommes fiers de vous compter comme partie de notre communauté fondateurs... happy conte!",

	'lets-go' => 'Allons-y',


	/* Slider Banner  */

	'slide-one' => 'Comme vous écrire une histoire, vous pouvez sélectionner le texte et choisissez une mise en page différente.',
	'slide-two' => "Au début de tout nouveau paragraphe clique sur l'icône à gauche de l'upload d'un nouveau paragrapheh.",
	'slide-three' => "Cliquez sur n'importe quel paragraphe que vous avez téléchargé pour choisir de différentes tailles et de disposition.",
	'slide-four' => 'Pour rendre votre histoire plus détectable, sélectionnez chaque emplacement dans votre texte et ajouter un lieu mentionner',

	'what-interested' => 'Quels sujets vous intéressent?',
	'what-interested-message' => 'Cliquez pour choisir les options ci-dessous et nous allons suggérer quelques bonnes choses pour vous.',
	'continue' => 'Continuer',

	'customize-your-profile' => 'Personnalisez votre profil',
	'customize-your-profile-message' => 'Ajouter une photo pour montrer votre personnalité unique et style.',

	'find-people-know' => 'Trouver des personnes que vous connaissez',
	'find-people-know-message' => "Trouver des personnes que vous connaissez peut-être de sorte que vous pouvez voir ce qu'ils partagent. Ne vous inquiétez pas, nous n'allons pas vos contacts e-mail sans votre permission.",
	'get-start-message1' => "Nous pensons qu'un dossier partagé de nos vies et de leurs expériences, magnifiquement raconté de tous les coins de la planète, ils inciteront plus de nous de voyager, de faire le plus clair de notre temps sur la terre, et aux soins pour les personnes et les lieux autour de nous.",
    'get-start-message2' => 'Parlez-nous de vos voyages, vos histoires et nous allons vous aider à configurer.',
    'get-start-message3' => 'Les choses en sont à leurs premiers pas ici et nous sommes fiers de vous compter comme partie de notre communauté fondateurs... happy conte!',
    'flowers' => 'Fleurs',
    'previous' => 'précédente',
    'next' => 'Prochaine',
    'step-1-of-4' => 'Étape 1 de 3',
    'step-2-of-4' => 'Étape 2 de 3',
    'step-3-of-4' => 'Étape 3 de 3',
	'step-4-of-4' => 'Étape 4 de 4'

);