<?php

return array(
	'basic-info' => 'Informations de base',
    'basic-info-message' => 'Modifier votre compte de base et les paramètres de langue.',

    'primary-lang' => 'Langue principale',
    'email-info' => 'E-mail ne sera pas affiché publiquement'
);