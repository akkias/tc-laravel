<?php

return array(

	'sign-up' => 'signer',
    'sign-in' => 'singer dans',

    'narrate-story' => 'racontent une histoire',
    'home' => 'maison',
    'editors-picks' => 'Les éditeurs de photos',
    'storytellers' => 'conteur',
    'themes' => 'Thèmes',

    // Categories
    'fav-travel-activities' => 'Activités de voyage préférés',
    
    'advanture' => 'Aventure',
    'arts-lieasure' => 'Arts et loisirs',
    'backpacking' => 'Randonnée',
    'beach' => 'Plage ',
    'budget-traval' => 'Budget Voyager',
    'coasts-islands' => 'Côtes et les Îles',
    'diving-snorkelling' => 'Plongée Sous-marine',
    'ecotourism' => 'écotourisme',
    'family-travel' => 'Voyages en Famille',
    'festival-events' => 'Festival et Événements',
    'film-television' => 'Du film et de la télévision',
    'food-drink' => 'Nourriture et boisson',
    'gear-tech' => 'Pignon et tech',
    'honymoon-romance' => 'Lune de miel et de Romance',
    'luxury-travel' => 'Voyages de luxe',
    'music' => 'musique',
    'off-beaten-track' => 'Hors des sentiers battus',
    'planes-trains' => 'Des trains et des avions',
    'road-trips' => 'Voyages par la route',
    'round-world-travel' => 'Tour du monde Voyage',
    'travel-photoghraphy' => 'Voyage Photoghraphy',
    'travel-shopping' => 'Voyages Shopping',
    'walking-trekking' => 'Promenade et de randonnée',
    'wildlife-nature' => 'La vie sauvage et de la Nature',

    'recent-stories' => 'récentes histoires',
    'popular-stories' => 'Contes populaires',
    'popular-story' => 'histoire populaire',

    'all-stories' => 'Alle Geschichten',

    'email' => 'Courriel',
    'password' => 'Mot de passe',
    'forgot-password' => 'Oublié votre mot de passe',

    'first-name' => 'Prénom',
    'last-name' => 'Dernier nom',
    'set-password' => 'Définir le mot de passe',
    'gender' => 'Sexe',
    'male' => 'Hommes',
    'female' => 'Femme',
    'phone-number' => 'Numéro de téléphone',
    'location' => 'Emplacement',
    'description' => 'décrire',

    'my-profile' => 'Mon profil',
    'drafts' => 'Brouillons',
    'Dreamlist' => 'Signets',
    'settings' => 'Paramètres',
    'sign-out' => 'Se déconnecter',

    /* Profile Page */
    'likes' => 'Aime',
    'follow' => 'Suivre',
    'following' => 'Suite',
    'followers' => 'Suiveurs',
    'social' => 'Sociaux',
    'publish-trip' => 'Publier Voyage',

    /* Settings */
    'account' => 'Compte',


    'stories' => 'Histoires',

    'save' => 'Enregistrer comme brouillon',
    'save1' => 'Enregistrer',
    'publish' => 'Publier',
    'update' => 'Mise à jour',

    'all-themes' => 'Tous les thèmes', /* new added */
    'about-me' => 'À propos de moi',  /* new added */
    'my-drafts' => 'Mes projets', /* new added */
    'bio' => 'Bio', /* new added */
    'follow' => 'Suivre' /* new added */


);