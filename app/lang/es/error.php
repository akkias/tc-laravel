<?php

return array(
	'you-got-lost' => 'Que se perdió',
    'ouch' => '¡ay!',

    'you-have-arrived' => 'Que ha llegado.',
    'look-like-something-wrong' => 'Esperamos que algo ha ido mal',
);