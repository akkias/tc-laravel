<?php

return array(
	
	/* Start Page */

	'we-are-glad-you-are-here' => 'Nos complace que estén aquí',
	'get-start-message' => 'Creemos que un registro compartido de nuestras vidas y experiencias, bellamente contada desde todos los rincones del planeta, le ofrecerán más de nosotros para viajar, para hacer la mayor parte de nuestro tiempo en la Tierra, y para el cuidado de las personas y los lugares alrededor de nosotros. Nos hablan de todos sus viajes, sus historias y le ayudaremos a obtener. Las cosas se acaba de empezar por aquí y estamos orgullosos de contar, como parte de nuestra fundación comunidad narración... feliz!',

	'lets-go' => '¡Vamos',


	/* Slider Banner  */

	'slide-one' => 'A la hora de escribir una historia, puede seleccionar el texto y elegir diferentes formatos.',
	'slide-two' => 'En el inicio un nuevo párrafo hace clic en el icono de la izquierda para la carga de un nuevo párrafo.',
	'slide-three' => 'Haga clic en cualquiera de los párrafos que haya cargado de elegir entre diferentes tamaños y diseño.',
	'slide-four' => 'Para que la historia sea más detectable, seleccione cada ubicación en el texto y agregar un lugar mencionar',

	'what-interested' => 'Qué le interesa?',
	'what-interested-message' => 'Haga clic para seleccionar opciones que aparecen a continuación y le sugeriremos algunas cosas buenas para usted.',
	'continue' => 'Continuar',

	'customize-your-profile' => 'Personalizar su perfil',
	'customize-your-profile-message' => 'Añadir una foto para mostrar su personalidad y estilo.',

	'find-people-know' => 'Encontrar a personas que conoces',
	'find-people-know-message' => 'Encontrar personas con las que usted podría saber que se puede ver qué es lo que están compartiendo. No te preocupes, no enviaremos correo electrónico tus contactos sin su permiso.',
	'get-start-message1' => 'Creemos que un registro compartido de nuestras vidas y experiencias, bellamente contada desde todos los rincones del planeta, le ofrecerán más de nosotros para viajar, para hacer la mayor parte de nuestro tiempo en la Tierra, y para el cuidado de las personas y los lugares alrededor de nosotros.',
    'get-start-message2' => 'Nos hablan de todos sus viajes, sus historias y le ayudaremos a obtener.',
    'get-start-message3' => 'Las cosas se acaba de empezar por aquí y estamos orgullosos de contar, como parte de nuestra fundación comunidad narración... feliz!',
    'flowers' => 'Flores',
    'previous' => 'Anterior',
    'next' => 'Siguiente',
    'step-1-of-4' => 'Paso 1 de 3',
    'step-2-of-4' => 'Paso 2 de 3',
    'step-3-of-4' => 'Paso 3 de 3',
	'step-4-of-4' => 'Paso 4 de 4'

);