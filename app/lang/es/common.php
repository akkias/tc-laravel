<?php

return array(

	'sign-up' => 'apuntarse',
    'sign-in' => 'registro',

    'narrate-story' => 'Narrar una historia',
    'home' => 'casa',
    'editors-picks' => 'Editores fotos',
    'storytellers' => 'cuentista',
    'themes' => 'tema',
    
    // Categories
    'fav-travel-activities' => 'Las actividades relacionadas con los viajes favoritos',
    
    'advanture' => 'Aventuras',
    'arts-lieasure' => 'Arte y Ocio',
    'backpacking' => 'Mochilero',
    'beach' => 'playa',
    'budget-traval' => 'presupuesto viajes',
    'coasts-islands' => 'Costas e Islas',
    'diving-snorkelling' => 'Buceo y snorkel',
    'ecotourism' => 'ecoturismo',
    'family-travel' => 'Viaje en familia',
    'festival-events' => 'Festival y Eventos',
    'film-television' => 'Cine y televisión',
    'food-drink' => 'Comida y bebida',
    'gear-tech' => 'Equipo y tecnología',
    'honymoon-romance' => 'Honymoon y Romance',
    'luxury-travel' => 'Viajes de lujo',
    'music' => 'música',
    'off-beaten-track' => 'De los caminos trillados',
    'planes-trains' => 'Aviones y trenes',
    'road-trips' => 'Viajes por carretera',
    'round-world-travel' => 'La vuelta al mundo de Viaje',
    'travel-photoghraphy' => 'Viajes Photoghraphy',
    'travel-shopping' => 'viajes de compras',
    'walking-trekking' => 'El andar y Trekking',
    'wildlife-nature' => 'Vida Silvestre y la naturaleza',

    'recent-stories' => 'Historias recientes',
    'popular-stories' => 'Cuentos populares',
    'popular-story' => 'historia popular',

    'all-stories' => 'Todas las historias',

    'email' => 'Correo electrónico',
    'password' => 'Contraseña',
    'forgot-password' => 'Ha olvidado su contraseña',

    'first-name' => 'Primer Nombre',
    'last-name' => 'Apellido',
    'set-password' => 'Establecer contraseña',
    'gender' => 'El género',
    'male' => 'Macho',
    'female' => 'Mujeres',
    'phone-number' => 'Número de teléfono',
    'location' => 'Ubicación',
    'description' => 'Descripción',

    'my-profile' => 'Mi proyecto',
    'drafts' => 'Proyectos',
    'Dreamlist' => 'Marcadores',
    'settings' => 'Ajustes',
    'sign-out' => 'Cerrar la sesión',

    /* Profile Page */
    'likes' => 'Le gusta',
    'follow' => 'Seguimiento',
    'following' => 'Siguiente',
    'followers' => 'Seguidores',
    'social' => 'Social',
    'publish-trip' => 'Publicar Viaje',

    /* Settings */
    'account' => 'Cuenta',
    
    'stories' => 'Historias',

    'save' => 'Guardar como borrador',
    'save1' => 'Guardar',
    'publish' => 'Publicar',
    'update' => 'Actualizar',

    'all-themes' => 'Todos los temas', /* new added */
    'about-me' => 'Acerca de mí',  /* new added */
    'my-drafts' => 'Mis Borradores', /* new added */
    'bio' => 'Bio', /* new added */
    'follow' => 'Seguimiento' /* new added */


);