<?php

return array(
	'home' => 'Casa',
	'editors-pick' => 'Selecciones del editor',
	'storytellers' => 'narradores',
	'themes' => 'temas',
    'home-travel-stories' => 'Hogar de historias de viajes de todo el mundo',
    'message' => 'Creemos que pensativo narración puede tener un impacto tangible y positivo en el mundo, y que todos nosotros, en cada rincón del planeta, tienen historias únicas para compartir.'
);