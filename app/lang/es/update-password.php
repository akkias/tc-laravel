<?php

return array(
	'change-password' => 'Cambiar la contraseña',
    'current-password' => 'Contraseña Actual',
    'new-password'=> 'Nueva Contraseña',
    'confirm-password' => 'Confirmar contraseña',
    'update-password' => 'Actualizar contraseña',

    'forgot-your-password' => 'Ha olvidado su contraseña?',
    'forgot-password-info' => 'Introduzca su dirección de correo electrónico y le enviaremos para restablecer su contraseña y acceda a su cuenta TripTroop.',

    'reset-your-password' => 'Restablecer la contraseña?',
    'reset-password-info' => 'Introduzca la nueva contraseña para conectar su TripTroop cuenta.'
    
);