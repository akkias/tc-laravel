<?php

return array(
	
	'name-your-story' => 'Nombre tu historia',
	'select-duration' => 'Seleccione la duración del viaje',
	'total-expenditure' => 'Los gastos totales',
	'insert-ammount' => 'Inserte humoristas',

	'classify-trip' => 'Clasificar su viaje',

	'select-cover' => 'Seleccione Cubierta',
	'add-hashtags' => 'Agregar hashtags',
	'category-hashtags' => 'Categorías y hashtags',
	'invite-friends' => 'Invitar a los amigos de',

	'categories' => 'Categorías',
	'what-stories-discover?' => 'Qué historias ¿desea descubrir?',
	'explore-categories' => 'explorar Categorías'

);