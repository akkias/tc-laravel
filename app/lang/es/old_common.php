<?php

return array(
	'sign-up' => 'apuntarse',
    'sign-in' => 'registro',

    'narrate-story' => 'Narrar una historia',
    'home' => 'casa',
    'editors-picks' => 'Editores fotos',
    'storytellers' => 'cuentista',
    'themes' => 'tema',

    'advanture' => 'Advanture',
    'arts-lieasure' => 'Artes y Lieasure',
    'beach' => 'Playa',
    'budget-traval' => 'Presupuesto Viajes ',
    'coasts-islands' => 'Costas e Islas',
    'diving-snorkelling' => 'Buceo y Snorkel',
    'ecotourism' => 'Ecoturismo',
    'family-travel' => 'Viaje en Familia',
    'festival-events' => 'Festival y Eventos',
    'film-television' => 'Cine y Televisión',
    'food-drink' => 'Comida y Bebida',
    'gear-tech' => 'Equipo y Tecnología',
    'honymoon-romance' => 'Honymoon y Romance',
    'luxury-travel' => 'Viajes de Tujo',
    'music' => 'Música',
    'off-beaten-track' => 'De los Caminos Trillados',
    'planes-trains' => 'Aviones y Trenes',
    'road trips' => 'Viajes por Carretera',
    'round-world-travel' => 'La vuelta al mundo de Viaje',
    'travel-photoghraphy' => 'Viajes Photoghraphy',
    'travel-shopping' => 'Viajes de Compras',
    'walking-trekking' => 'El andar y Trekking',
    'wildlife-nature' => 'Vida Silvestre y la Naturaleza',

    'recent-stories' => 'Historias Recientes',
    'popular-stories' => 'Popular Stories',

    'all-stories' => 'todas las historias'
    
);