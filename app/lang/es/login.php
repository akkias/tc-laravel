<?php

return array(
	'dont-account' => 'Todavía no tienes una cuenta',
    'login-message' => 'Elija una de las siguientes opciones para crear una nueva cuenta y únase a nuestra comunidad.'
);