<?php

return array(
	'already-account' => 'Ya tiene una cuenta?',
    'login-message' => 'Elija una de las siguientes opciones para crear una nueva cuenta y únase a nuestra comunidad.',

    'create-an-account' => "Crear una cuenta",
    'accept-terms' => "ACEPTO",
    'terms-services' => 'Términos de los términos de servicios'
);