<?php

return array(
	'basic-info' => 'Información básica',
    'basic-info-message' => 'Cambiar la cuenta básica y los ajustes de idioma.',

    'primary-lang' => 'Idioma principal',
    'email-info' => 'Correo electrónico no se mostrará públicamente'
);