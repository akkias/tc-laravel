<?php

return array(
	'sign-up' => 'apuntarse',
    'sign-in' => 'registro',
    'tagline' => 'No es sobre el destino su viaje',
    'tagline-home-stories' => 'Casa para todo aquel viaje stroies',
    'tagline-share-stories' => 'Comparta sus historias y ser inspiración para otros',
    'get-started' => 'empezar',
    'or' => 'o',
    'exporer-stories' => 'explorar historias',
    'message' => 'Creemos que esa narración reflexiva puede hacer un impacto positivo y tangible en el mundo, y que todos nosotros, en cada rincón de la global, tenemos historias únicas para compartir.',
    'all-stories' => 'Todas las Historias'
);	



