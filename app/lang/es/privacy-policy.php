<?php

return array(

	'priv-policy-title' => 'Política de Privacidad para TripTroop',
	
	'privacy-content1' => 'Si necesita más información o tiene alguna pregunta sobre nuestra política de privacidad, por favor no dude en ponerse en contacto con nosotros por correo electrónico a',
	'privacy-content2' => 'En <a href="https://www.triptroop.me">https://www.triptroop.me</a> consideramos que la privacidad de nuestros visitantes es muy importante. Esta política de privacidad documento se describen en detalle los tipos de información personal es recopilada y registrada por <a href="https://www.triptroop.me">https://www.triptroop.me</a> y el uso que hacemos de ella.',
	
	'privacy-log-title' => 'Archivos de registro',
	'privacy-log-content' => "Al igual que muchos otros sitios Web, <a href='https://www.triptroop.me'>https://www.triptroop.me</a> hace uso de archivos de registro. Estos archivos sólo los registros de los visitantes del sitio, por lo general un procedimiento estándar para empresas de alojamiento y una parte de análisis de servicios de hosting. La información dentro de los archivos de registro incluye las direcciones IP (protocolo de internet), el tipo de navegador, proveedor de servicios de Internet (ISP), fecha y hora, páginas de salida y entrada, y posiblemente el número de clics. Esta información se utiliza para analizar tendencias, administrar el sitio, rastrear el movimiento del usuario por el sitio, y para recopilar información demográfica. Direcciones IP y otra información de este tipo no están vinculados a ninguna información que es personalmente identificable.",

	'privacy-cookies-title' => 'Cookies y Web beacons',
	'privacy-cookies-content' => "Utiliza cookies para almacenar información sPolítica de Privacidad En Líneaobre las preferencias de los visitantes, registrar información específica del usuario en las páginas que los visitantes de su sitio web accede o visitas, personalizar o adaptar nuestro contenido de la página web basado en el tipo de navegador los visitantes o cualquier otra información que el visitante envía a través de su navegador.",

	'privacy-child-info' => "Información de los Niños",
	'privacy-child-info-content' => "Creemos que es importante proporcionar una mayor protección a los niños en línea. Animamos a los padres y tutores a pasar tiempo en línea con sus hijos a observar, participar y/o supervisar y orientar su actividad en línea. <a href='https://www.triptroop.me'>https://www.triptroop.me</a> no recolecta información personal identificable de niños menores de 13 años. Si un padre, madre o tutor <a href='https://www.triptroop.me'>https://www.triptroop.me</a> cree que tiene en su base de datos la información personalmente identificable de un niño de 13 años de edad, por favor, póngase en contacto con nosotros inmediatamente (mediante el contacto en el primer párrafo) y utilizaremos nuestro mejor esfuerzo para extraer rápidamente dicha información de nuestros registros.",

	'privacy-online' => "Política de Privacidad En Línea",
	'privacy-online-content' => "Esta política de privacidad se aplica sólo a las actividades en línea y nuestro es válida para los visitantes de nuestro sitio web y por la información compartida y/o. Esta política no se aplica a toda la información recopilada fuera de línea o por vías distintas a la de este sitio web.",

	'privacy-consent' => "Consentimiento",
	'privacy-consent-content' => 'Mediante nuestro sitio web, usted da su consentimiento a nuestra política de privacidad y acuerdo con sus términos.',

	'privacy-update' => 'Actualizar',
	'privacy-update-content' => 'Esta Política de Privacidad fue actualizada por última vez el: Domingo, 7 junio, 2015.'

);
