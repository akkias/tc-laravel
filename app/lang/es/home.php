<?php

return array(
    'message' => 'No se trata tan sólo de un viaje historia, es una interfaz para salvar lo que has vivido y lo que es desconocido para el resto del mundo. Latente historias están a la espera de ser descubierto... vamos a revelar de manera que el mundo reconoce.',
    
    'tagline' => 'No es sobre el destino su viaje',
    'tagline-home-stories' => 'El cubo de relatos de viajes, relatos de viajes',
    'tagline-share-stories' => 'Comparta sus historias y ser una inspiración para otros',
    'get-started' => 'empezar',
    'or' => 'o',
    'exporer-stories' => 'explorar historias',
    'all-stories' => 'Todas las Historias'
);