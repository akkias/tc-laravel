<?php

return array(
	'home' => 'Home',
	'editors-pick' => 'Editor&amp;s Picks',
	'storytellers' => 'Storytellers',
	'themes' => 'Themes',
    'home-travel-stories' => "Home to everyone's travel stories",
    'message' => 'We believe that thoughtful storytelling can make a tangible, positive impact in the world, and that all of us, in every corner of the globe, have unique stories to share.'
);