<?php

class LogoutController extends BaseController {

    public function getLogout(){
		$userId = Auth::user()['id'];
        Auth::logout();
		//if()
		$check = DB::table('oauthCheck')->where('id', $userId)->count();
		if($check){
			DB::table('users')->where('id', $userId)->delete();
		}
		DB::table('oauthCheck')->delete();
        return Redirect::back()->with('msg', 'You have successfully logged out.'); 
    }

}
