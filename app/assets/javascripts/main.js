console.log('\'Allo \'Allo!');



/* Post carousel */
function initPostCarousel(){
	/*jshint expr:true */
	'use strict';
	var myCarousel = $('#postCarousel');
	myCarousel.append('<ol class="carousel-indicators"></ol>');
	var indicators = $('.carousel-indicators');
	myCarousel.find('.carousel-inner').children('.item').each(function(index) {
		(index === 0) ?
		indicators.append('<li data-target="#postCarousel" data-slide-to="'+index+'" class="active"></li>') :
		indicators.append('<li data-target="#postCarousel" data-slide-to="'+index+'"></li>');
	});
};

function initTitleFontSize(){
	'use strict';
	var $title = $('.title-wrapper h1');
	var $numWords = $title.text().length;
	if (($numWords >= 1) && ($numWords < 7)) {
		$title.css('font-size', '160px');
	}
	else if (($numWords >= 7) && ($numWords < 10)) {
		$title.css('font-size', '124px');
	}
	else if (($numWords >= 10) && ($numWords < 11)) {
		$title.css('font-size', '100px');
	}
	else if (($numWords >= 11) && ($numWords < 12)) {
		$title.css('font-size', '90px');
	}
	else if (($numWords >= 12) && ($numWords < 15)) {
		$title.css('font-size', '75px');
	}
	else if (($numWords >= 15) && ($numWords < 35)) {
		$title.css('font-size', '67px');
	}
	else if (($numWords >= 35) && ($numWords < 39)) {
		$title.css('font-size', '54px');
	}
	else if (($numWords >= 39) && ($numWords < 62)) {
		$title.css('font-size', '48px');
	}
	else {
		$title.css('font-size', '160px');
	}
};

function initPreventEnter() {
	$(window).keydown(function(event){
		if(event.keyCode == 13) {
			event.preventDefault();
			return false;
		}
	});
};


function initPostSearchQuery() {
	///setup before functions
	var typingTimer;                //timer identifier
	var doneTypingInterval = 3000;  //time in ms, 5 second for example
	//on keyup, start the countdown
	$('#magicInput').keyup(function(){
		clearTimeout(typingTimer);
		if($('#magicInput').val) {
			typingTimer = setTimeout(function(){
	            //do stuff here e.g ajax call etc....
	            var v = $("#magicInput").val();
	            alert(v);
	        }, doneTypingInterval);
		}
	});
};

function initInfiniteScroll() {
	var ias = $.ias({
		container:  '.scroller',
		negativeMargin: 350,
		item:       '.story-card',
		pagination: '.pager',
		next:       '.pager li + li a'
	});
	ias.extension(new IASSpinnerExtension());
	ias.extension(new IASTriggerExtension({text: 'Load more stories',offset: 3}));
	ias.extension(new IASNoneLeftExtension({text: "You reached the end"}));
}



function init(){
	'use strict';
	initPostCarousel();
	initTitleFontSize();
	initPostSearchQuery();
	initInfiniteScroll()

	$('.title-wrapper h1').on('keydown', function(){
		initTitleFontSize();
		initPreventEnter();
	});
	$('#startName').on('keydown', function(){
		initPreventEnter();
	});

	$('.dropdown').on('show.bs.dropdown', function() {
		$(this).find('.dropdown-menu').first().stop(true, true).fadeIn('fast');
	});
	$('.dropdown').on('hide.bs.dropdown', function() {
		$(this).find('.dropdown-menu').first().stop(true, true).fadeOut('fast');
	});

	$('#take_tour_link').click(function(){
		introJs().start();
	});
	
	$(".open-menu").click(function() {
		$(".sidemenu").openMenu();
	});
	$('[data-toggle="tooltip"]').tooltip();

	$(".chosen-select").chosen();

	
	$(window).load(function(){
		$('#myModal').modal('show');
	});
   $('.languageSelect').on('click', function() {
  	 $.ajax({
      url: "/changeLocale/"+$(this).attr('langVal'),
      type: "GET",
      data:  new FormData(this),
      contentType: false,
      cache: true,
      processData:false,
      success: function(data){
    	location.reload();    
      },
      error: function(){}           
    });
  	
	});	

};

$(document).ready(function(){
	'use strict';
	init();

});
