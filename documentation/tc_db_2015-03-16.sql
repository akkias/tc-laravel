# ************************************************************
# Sequel Pro SQL dump
# Version 4135
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.6.23)
# Database: tc_db
# Generation Time: 2015-03-15 19:02:40 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table category_map
# ------------------------------------------------------------

DROP TABLE IF EXISTS `category_map`;

CREATE TABLE `category_map` (
  `ckey` varchar(10) DEFAULT NULL,
  `cvalue` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `category_map` WRITE;
/*!40000 ALTER TABLE `category_map` DISABLE KEYS */;

INSERT INTO `category_map` (`ckey`, `cvalue`)
VALUES
	('cat1','Adventure'),
	('cat2','Arts and Liesure'),
	('cat3','Beaches'),
	('cat4','Budget Travel'),
	('cat5','Coasts and Islands'),
	('cat6','Diving and Snorkelling'),
	('cat7','Ecotourism'),
	('cat8','Family Travel'),
	('cat9','Festival and Events'),
	('cat10','Film and Television'),
	('cat11','Food and Drink'),
	('cat12','Gear and Tech'),
	('cat23','Honeymoon and Romance'),
	('cat14','Luxury Travel'),
	('cat15','Music'),
	('cat16','Off the Beaten Track'),
	('cat17','Planes and Trains'),
	('cat18','Road Trips'),
	('cat19','Round the World Travel'),
	('cat20','Travel Photograhpy'),
	('cat21','Travel Shopping'),
	('cat22','Walking and Trekking'),
	('cat23','Wlid life and Nature');

/*!40000 ALTER TABLE `category_map` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cover
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cover`;

CREATE TABLE `cover` (
  `image` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table currentstory
# ------------------------------------------------------------

DROP TABLE IF EXISTS `currentstory`;

CREATE TABLE `currentstory` (
  `id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table editgallery
# ------------------------------------------------------------

DROP TABLE IF EXISTS `editgallery`;

CREATE TABLE `editgallery` (
  `name` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `editgallery` WRITE;
/*!40000 ALTER TABLE `editgallery` DISABLE KEYS */;

INSERT INTO `editgallery` (`name`)
VALUES
	('/uploads/Koala.jpg'),
	('/uploads/Lighthouse.jpg'),
	('/uploads/Penguins.jpg');

/*!40000 ALTER TABLE `editgallery` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table follow
# ------------------------------------------------------------

DROP TABLE IF EXISTS `follow`;

CREATE TABLE `follow` (
  `id` int(11) DEFAULT NULL,
  `following` longtext,
  `followers` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `follow` WRITE;
/*!40000 ALTER TABLE `follow` DISABLE KEYS */;

INSERT INTO `follow` (`id`, `following`, `followers`)
VALUES
	(20,NULL,'->25->'),
	(25,'->20->',NULL);

/*!40000 ALTER TABLE `follow` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table gallery
# ------------------------------------------------------------

DROP TABLE IF EXISTS `gallery`;

CREATE TABLE `gallery` (
  `storyid` int(11) DEFAULT NULL,
  `images` varchar(2048) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `gallery` WRITE;
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;

INSERT INTO `gallery` (`storyid`, `images`)
VALUES
	(5,'->Koala.jpg->Lighthouse.jpg->Penguins.jpg->'),
	(6,'->Hydrangeas.jpg->Jellyfish.jpg->Koala.jpg->Lighthouse.jpg->'),
	(23,'->Hydrangeas.jpg->Jellyfish.jpg->Koala.jpg->Lighthouse.jpg->');

/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table liked
# ------------------------------------------------------------

DROP TABLE IF EXISTS `liked`;

CREATE TABLE `liked` (
  `id` int(11) DEFAULT NULL,
  `likeTo` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `liked` WRITE;
/*!40000 ALTER TABLE `liked` DISABLE KEYS */;

INSERT INTO `liked` (`id`, `likeTo`)
VALUES
	(19,'->6->'),
	(20,'->5->'),
	(25,'->25->17->18->9->12->11->5->22->27->');

/*!40000 ALTER TABLE `liked` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table oauthcheck
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauthcheck`;

CREATE TABLE `oauthcheck` (
  `id` int(11) DEFAULT NULL,
  `viaOauth` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table profile
# ------------------------------------------------------------

DROP TABLE IF EXISTS `profile`;

CREATE TABLE `profile` (
  `id` int(11) DEFAULT NULL,
  `dpPath` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `profile` WRITE;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;

INSERT INTO `profile` (`id`, `dpPath`)
VALUES
	(25,'/profilePics/25/Koala.jpg'),
	(20,'/profilePics/20/Penguins.jpg');

/*!40000 ALTER TABLE `profile` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table stories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `stories`;

CREATE TABLE `stories` (
  `storyid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `filepath` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `saved` int(11) NOT NULL,
  `category` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `published` int(11) NOT NULL,
  `cover` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`storyid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `stories` WRITE;
/*!40000 ALTER TABLE `stories` DISABLE KEYS */;

INSERT INTO `stories` (`storyid`, `userid`, `title`, `filepath`, `saved`, `category`, `published`, `cover`, `count`)
VALUES
	(23,'25','hydra','Stories/25/45.txt',1,'->cat3->cat4->',1,'/storyCover/1.jpg',0),
	(24,'25','peng','Stories/25/46.txt',0,'->cat4->',1,'/storyCover/2.jpg',0),
	(25,'25','sachie','Stories/25/46.txt',0,'->cat2->cat3->',1,'/storyCover/3.jpg',0),
	(26,'25','makad','Stories/25/46.txt',1,'->cat4->',1,'/storyCover/4.jpg',0),
	(27,'25','makadasdasdasdsadsada','Stories/25/46.txt',0,'->cat4->',1,'/storyCover/5.jpg',1),
	(28,'25','asdasdasdad','Stories/25/46.txt',1,'->cat4->',1,'/storyCover/6.jpg',5),
	(29,'25','asdasdasdad','Stories/25/46.txt',0,'->cat2->cat3->',1,'/storyCover/7.jpg',0),
	(30,'25','asdasdasdad','Stories/25/46.txt',0,'->cat2->cat3->',1,'/storyCover/8.jpg',0),
	(31,'25','asdasdasdad','Stories/25/46.txt',0,'->cat2->cat3->',1,'/storyCover/9.jpg',0),
	(32,'25','asdasdasdad','Stories/25/46.txt',0,'->cat2->cat3->',1,'/storyCover/10.jpg',0),
	(33,'25','asdasdasdad','Stories/25/46.txt',0,'->cat2->cat3->',1,'/storyCover/Lighthouse.jpg',0),
	(34,'25','asdasdasdad','Stories/25/46.txt',0,'->cat2->cat3->',1,'/storyCover/12.jpg',0),
	(35,'25','asdasdasdad','Stories/25/46.txt',0,'->cat2->cat3->',1,'/storyCover/login_bg.jpg',0);

/*!40000 ALTER TABLE `stories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tempgallery
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tempgallery`;

CREATE TABLE `tempgallery` (
  `images` varchar(2048) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tempgallery` WRITE;
/*!40000 ALTER TABLE `tempgallery` DISABLE KEYS */;

INSERT INTO `tempgallery` (`images`)
VALUES
	('->Hydrangeas.jpg->Jellyfish.jpg->Koala.jpg->Lighthouse.jpg->');

/*!40000 ALTER TABLE `tempgallery` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tempwish
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tempwish`;

CREATE TABLE `tempwish` (
  `id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tempwish` WRITE;
/*!40000 ALTER TABLE `tempwish` DISABLE KEYS */;

INSERT INTO `tempwish` (`id`)
VALUES
	(25),
	(10),
	(11),
	(17),
	(5),
	(22);

/*!40000 ALTER TABLE `tempwish` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table userinfo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userinfo`;

CREATE TABLE `userinfo` (
  `id` int(11) NOT NULL,
  `gender` varchar(64) NOT NULL,
  `mbl` varchar(10) NOT NULL,
  `location` varchar(64) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `categories` varchar(512) NOT NULL,
  `pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `userinfo` WRITE;
/*!40000 ALTER TABLE `userinfo` DISABLE KEYS */;

INSERT INTO `userinfo` (`id`, `gender`, `mbl`, `location`, `description`, `categories`, `pin`)
VALUES
	(25,'Male','9970016888','pune','pune','->cat1->cat2->cat3->cat4->cat5->cat8->','+91'),
	(0,'','','','','->cat3->cat4->',''),
	(0,'','','','','0',''),
	(0,'','','','','0','');

/*!40000 ALTER TABLE `userinfo` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `emailid` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `verified` int(11) NOT NULL,
  `following` int(11) NOT NULL,
  `followers` int(11) NOT NULL,
  `confirmed` int(11) NOT NULL,
  `visited` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `firstname`, `lastname`, `emailid`, `password`, `verified`, `following`, `followers`, `confirmed`, `visited`)
VALUES
	(20,'harish','boke','harish.boke@gmail.com','$2y$10$XaxTrrrShBpUjIjtUryISuUP1BbsjgT1pzeO/AC.zclipeq5ORuXK',0,0,1,0,1),
	(25,'sachin','jadhav','jadhavsachin174@gmail.com','$2y$10$avj6rkk.lJ2W99JorUSINe900Ou.4ulnbMubLQl..GwJkLl5Mry7G',0,1,0,1,1),
	(29,'Sachin','Jadhav','','',0,0,0,0,0),
	(30,'Sachin','Jadhav','','',0,0,0,0,0);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wishlist
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wishlist`;

CREATE TABLE `wishlist` (
  `id` int(11) DEFAULT NULL,
  `wishTo` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `wishlist` WRITE;
/*!40000 ALTER TABLE `wishlist` DISABLE KEYS */;

INSERT INTO `wishlist` (`id`, `wishTo`)
VALUES
	(19,'->2->5->6->'),
	(20,'->5->'),
	(25,'->25->10->11->17->5->22->27->');

/*!40000 ALTER TABLE `wishlist` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
