-- MySQL dump 10.13  Distrib 5.6.21, for Win32 (x86)
--
-- Host: localhost    Database: tc_db
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cat_name`
--

DROP TABLE IF EXISTS `cat_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cat_name` (
  `default_name` varchar(128) DEFAULT NULL,
  `actual_name` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cat_name`
--

LOCK TABLES `cat_name` WRITE;
/*!40000 ALTER TABLE `cat_name` DISABLE KEYS */;
INSERT INTO `cat_name` VALUES ('adventure','cat1'),('arts-and-liesure','cat2'),('backpacking','cat3'),('beaches','cat4'),('budget-travel','cat5'),('coasts-and-islands','cat6'),('diving-and-snorkelling','cat7'),('ecotourism','cat8'),('family-travel','cat9'),('festival-and-events','cat10'),('film-and-television','cat11'),('food-and-drink','cat12'),('gear-and-tech','cat13'),('honeymoon-and-romance','cat14'),('luxury-travel','cat15'),('music','cat16'),('off-the-beaten-track','cat17'),('planes-and-trains','cat18'),('road-trips','cat19'),('round-the-world-travel','cat20'),('travel-photograhpy','cat21'),('travel-shopping','cat22'),('walking-and-trekking','cat23'),('wildlife-and-nature','cat24');
/*!40000 ALTER TABLE `cat_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_map`
--

DROP TABLE IF EXISTS `category_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_map` (
  `ckey` varchar(10) DEFAULT NULL,
  `cvalue` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_map`
--

LOCK TABLES `category_map` WRITE;
/*!40000 ALTER TABLE `category_map` DISABLE KEYS */;
INSERT INTO `category_map` VALUES ('cat1','Adventure'),('cat2','Arts and Liesure'),('cat3','Backpacking'),('cat4','Beaches'),('cat5','Budget Travel'),('cat6','Coasts and Islands'),('cat7','Diving and Snorkelling'),('cat8','Ecotourism'),('cat9','Family Travel'),('cat10','Festival and Events'),('cat11','Film and Television'),('cat12','Food and Drink'),('cat23','Gear and Tech'),('cat14','Honeymoon and Romance'),('cat15','Luxury Travel'),('cat16','Music'),('cat17','Off the Beaten Track'),('cat18','Planes and Trains'),('cat19','Road Trips'),('cat20','Round the World Travel'),('cat21','Travel Photograhpy'),('cat22','Travel Shopping'),('cat23','Walking and Trekking'),('cat24','Wild life and Nature');
/*!40000 ALTER TABLE `category_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cover`
--

DROP TABLE IF EXISTS `cover`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cover` (
  `image` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cover`
--

LOCK TABLES `cover` WRITE;
/*!40000 ALTER TABLE `cover` DISABLE KEYS */;
/*!40000 ALTER TABLE `cover` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currentstory`
--

DROP TABLE IF EXISTS `currentstory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currentstory` (
  `id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currentstory`
--

LOCK TABLES `currentstory` WRITE;
/*!40000 ALTER TABLE `currentstory` DISABLE KEYS */;
/*!40000 ALTER TABLE `currentstory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `editgallery`
--

DROP TABLE IF EXISTS `editgallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `editgallery` (
  `name` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `editgallery`
--

LOCK TABLES `editgallery` WRITE;
/*!40000 ALTER TABLE `editgallery` DISABLE KEYS */;
INSERT INTO `editgallery` VALUES ('/uploads/Koala.jpg'),('/uploads/Lighthouse.jpg'),('/uploads/Penguins.jpg');
/*!40000 ALTER TABLE `editgallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `follow`
--

DROP TABLE IF EXISTS `follow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `follow` (
  `id` int(11) DEFAULT NULL,
  `following` longtext,
  `followers` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `follow`
--

LOCK TABLES `follow` WRITE;
/*!40000 ALTER TABLE `follow` DISABLE KEYS */;
INSERT INTO `follow` VALUES (20,'25->','->25->'),(25,'->undefined->20->','20->'),(0,NULL,'->25->'),(31,NULL,'->');
/*!40000 ALTER TABLE `follow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gallery`
--

DROP TABLE IF EXISTS `gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gallery` (
  `storyid` int(11) DEFAULT NULL,
  `images` varchar(2048) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gallery`
--

LOCK TABLES `gallery` WRITE;
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;
INSERT INTO `gallery` VALUES (5,'->Koala.jpg->Lighthouse.jpg->Penguins.jpg->'),(6,'->Hydrangeas.jpg->Jellyfish.jpg->Koala.jpg->Lighthouse.jpg->'),(23,'->Hydrangeas.jpg->Jellyfish.jpg->Koala.jpg->Lighthouse.jpg->');
/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `liked`
--

DROP TABLE IF EXISTS `liked`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `liked` (
  `id` int(11) DEFAULT NULL,
  `likeTo` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `liked`
--

LOCK TABLES `liked` WRITE;
/*!40000 ALTER TABLE `liked` DISABLE KEYS */;
INSERT INTO `liked` VALUES (19,'->6->'),(20,'->5->'),(25,'->25->17->18->9->12->11->5->22->27->32->35->');
/*!40000 ALTER TABLE `liked` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauthcheck`
--

DROP TABLE IF EXISTS `oauthcheck`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauthcheck` (
  `id` int(11) DEFAULT NULL,
  `viaOauth` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauthcheck`
--

LOCK TABLES `oauthcheck` WRITE;
/*!40000 ALTER TABLE `oauthcheck` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauthcheck` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile` (
  `id` int(11) DEFAULT NULL,
  `dpPath` varchar(256) DEFAULT NULL,
  `clPath` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile`
--

LOCK TABLES `profile` WRITE;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` VALUES (25,'/assets/images/user.jpg','profilePics/25/rcwbh0lfkuftmwwf0xa9'),(20,'/assets/images/user.jpg','profilePics/25/rcwbh0lfkuftmwwf0xa9'),(31,'/assets/images/user.jpg','profilePics/25/rcwbh0lfkuftmwwf0xa9'),(32,'/profilePics/32/1658491_615053045217161_268545801_o.jpg','profilePics/32/iigxotzpxso0fa40ahdq');
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stories`
--

DROP TABLE IF EXISTS `stories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stories` (
  `storyid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `filepath` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `saved` int(11) NOT NULL,
  `category` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `published` int(11) NOT NULL,
  `cover` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(11) NOT NULL,
  `currency` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `expense` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `tripDate` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`storyid`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stories`
--

LOCK TABLES `stories` WRITE;
/*!40000 ALTER TABLE `stories` DISABLE KEYS */;
INSERT INTO `stories` VALUES (23,'25','hydra','Stories/25/45.txt',1,'->cat3->cat4->',1,'/storyCover/1.jpg',0,'','',''),(24,'25','peng','Stories/25/46.txt',0,'->cat4->',1,'/storyCover/2.jpg',0,'','',''),(25,'25','sachie','Stories/25/46.txt',0,'->cat2->cat3->',1,'/storyCover/3.jpg',0,'','',''),(26,'25','makad','Stories/25/46.txt',1,'->cat4->',1,'/storyCover/4.jpg',0,'','',''),(27,'25','makadasdasdasdsadsada','Stories/25/46.txt',0,'->cat4->',1,'/storyCover/5.jpg',1,'','',''),(28,'25','asdasdasdad','Stories/25/46.txt',1,'->cat4->',1,'/storyCover/6.jpg',5,'','',''),(29,'25','asdasdasdad','Stories/25/46.txt',0,'->cat2->cat3->',1,'/storyCover/7.jpg',0,'','',''),(30,'25','asdasdasdad','Stories/25/46.txt',0,'->cat2->cat3->',1,'/storyCover/8.jpg',0,'','',''),(31,'25','asdasdasdad','Stories/25/46.txt',0,'->cat2->cat3->',1,'/storyCover/9.jpg',0,'','',''),(32,'25','asdasdasdad','Stories/25/46.txt',0,'->cat2->cat3->',1,'/storyCover/10.jpg',1,'','',''),(33,'25','asdasdasdad','Stories/25/46.txt',0,'->cat2->cat3->',1,'/storyCover/Lighthouse.jpg',0,'','',''),(34,'25','asdasdasdad','Stories/25/46.txt',0,'->cat2->cat3->',1,'/storyCover/12.jpg',0,'','',''),(35,'25','asdasdasdad','Stories/25/46.txt',0,'->cat2->cat3->',1,'/storyCover/login_bg.jpg',1,'','',''),(36,'25','Road back home','Stories/25/36.txt',1,'->cat1->cat2->cat3->cat4->cat5->',1,'/storyCover/36/Deepside-Deejays-feat-Viky-Red---The-Road-Back-Home-single-nou.jpg',0,'','',''),(37,'25','lai bhari!!!','Stories/25/37.txt',0,'->cat11->',1,'/storyCover/37/photo-1427606694672-8f86d75947ce.jpeg',0,'','',''),(38,'25','Name your story','Stories/25/38.txt',1,'0',0,'public',0,'','',''),(39,'25','Wall','Stories/25/39.txt',0,'->cat1->',1,'/storyCover/39/wall.jpg',0,'','',''),(40,'25','Name your story','Stories/25/40.txt',1,'0',0,'public',0,'','',''),(41,'25','Name your story','Stories/25/41.txt',1,'->cat3->cat4->cat5->',1,'/storyCover/41/baby.jpg',0,'USD','90000','04/09/2015 - 04/21/2015');
/*!40000 ALTER TABLE `stories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tempgallery`
--

DROP TABLE IF EXISTS `tempgallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tempgallery` (
  `images` varchar(2048) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tempgallery`
--

LOCK TABLES `tempgallery` WRITE;
/*!40000 ALTER TABLE `tempgallery` DISABLE KEYS */;
INSERT INTO `tempgallery` VALUES ('->Hydrangeas.jpg->Jellyfish.jpg->Koala.jpg->Lighthouse.jpg->');
/*!40000 ALTER TABLE `tempgallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tempwish`
--

DROP TABLE IF EXISTS `tempwish`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tempwish` (
  `id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tempwish`
--

LOCK TABLES `tempwish` WRITE;
/*!40000 ALTER TABLE `tempwish` DISABLE KEYS */;
INSERT INTO `tempwish` VALUES (25),(10),(11),(17),(5),(22),(27),(32),(35),(39);
/*!40000 ALTER TABLE `tempwish` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userinfo`
--

DROP TABLE IF EXISTS `userinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo` (
  `id` int(11) NOT NULL,
  `gender` varchar(64) NOT NULL,
  `mbl` varchar(10) NOT NULL,
  `location` varchar(64) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `categories` varchar(512) NOT NULL,
  `pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userinfo`
--

LOCK TABLES `userinfo` WRITE;
/*!40000 ALTER TABLE `userinfo` DISABLE KEYS */;
INSERT INTO `userinfo` VALUES (25,'Male','9970016888','pune','pune','->cat1->cat2->cat3->cat4->cat6->cat7->','+91'),(0,'','','','','->cat3->cat4->',''),(0,'','','','','0',''),(0,'','','','','0',''),(31,'','','','','->cat2->cat3->cat4->',''),(31,'','','','','',''),(20,'','','','','->cat1->cat2->',''),(32,'','','','','->cat2->cat7->cat8->cat13->','');
/*!40000 ALTER TABLE `userinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `emailid` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `verified` int(11) NOT NULL,
  `following` int(11) NOT NULL,
  `followers` int(11) NOT NULL,
  `confirmed` int(11) NOT NULL,
  `visited` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (20,'harish','boke','harish.boke@gmail.com','$2y$10$XaxTrrrShBpUjIjtUryISuUP1BbsjgT1pzeO/AC.zclipeq5ORuXK',0,1,1,1,1),(25,'sachin','jadhav','jadhavsachin174@gmail.com','$2y$10$avj6rkk.lJ2W99JorUSINe900Ou.4ulnbMubLQl..GwJkLl5Mry7G',0,1,1,1,1),(29,'Sachin','Jadhav','','',0,0,0,0,0),(30,'Sachin','Jadhav','','',0,0,0,0,0),(31,'sach','jadhav','sachiejadhav026@gmail.com','$2y$10$JMKFTWArxKFrtkIxHs163Op5atqqYH4BACwYObGrp3FZrr7AbSsFa',0,0,0,1,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wishlist`
--

DROP TABLE IF EXISTS `wishlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wishlist` (
  `id` int(11) DEFAULT NULL,
  `wishTo` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wishlist`
--

LOCK TABLES `wishlist` WRITE;
/*!40000 ALTER TABLE `wishlist` DISABLE KEYS */;
INSERT INTO `wishlist` VALUES (19,'->2->5->6->'),(20,'->5->'),(25,'->25->10->11->17->5->22->27->32->35->39->');
/*!40000 ALTER TABLE `wishlist` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-04-10  1:28:16
