-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 19, 2015 at 10:43 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tc_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `category_map`
--

CREATE TABLE IF NOT EXISTS `category_map` (
  `ckey` varchar(10) DEFAULT NULL,
  `cvalue` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_map`
--

INSERT INTO `category_map` (`ckey`, `cvalue`) VALUES
('cat1', 'Adventure'),
('cat2', 'Arts and Liesure'),
('cat3', 'Backpacking'),
('cat4', 'Beaches'),
('cat5', 'Budget Travel'),
('cat6', 'Coasts and Islands'),
('cat7', 'Diving and Snorkelling'),
('cat8', 'Ecotourism'),
('cat9', 'Family Travel'),
('cat10', 'Festival and Events'),
('cat11', 'Film and Television'),
('cat12', 'Food and Drink'),
('cat23', 'Gear and Tech'),
('cat14', 'Honeymoon and Romance'),
('cat15', 'Luxury Travel'),
('cat16', 'Music'),
('cat17', 'Off the Beaten Track'),
('cat18', 'Planes and Trains'),
('cat19', 'Road Trips'),
('cat20', 'Round the World Travel'),
('cat21', 'Travel Photograhpy'),
('cat22', 'Travel Shopping'),
('cat23', 'Walking and Trekking'),
('cat24', 'Wild life and Nature');

-- --------------------------------------------------------

--
-- Table structure for table `cat_name`
--

CREATE TABLE IF NOT EXISTS `cat_name` (
  `default_name` varchar(128) DEFAULT NULL,
  `actual_name` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cat_name`
--

INSERT INTO `cat_name` (`default_name`, `actual_name`) VALUES
('adventure', 'cat1'),
('arts-and-liesure', 'cat2'),
('backpacking', 'cat3'),
('beaches', 'cat4'),
('budget-travel', 'cat5'),
('coasts-and-islands', 'cat6'),
('diving-and-snorkelling', 'cat7'),
('ecotourism', 'cat8'),
('family-travel', 'cat9'),
('festival-and-events', 'cat10'),
('film-and-television', 'cat11'),
('food-and-drink', 'cat12'),
('gear-and-tech', 'cat13'),
('honeymoon-and-romance', 'cat14'),
('luxury-travel', 'cat15'),
('music', 'cat16'),
('off-the-beaten-track', 'cat17'),
('planes-and-trains', 'cat18'),
('road-trips', 'cat19'),
('round-the-world-travel', 'cat20'),
('travel-photograhpy', 'cat21'),
('travel-shopping', 'cat22'),
('walking-and-trekking', 'cat23'),
('wildlife-and-nature', 'cat24');

-- --------------------------------------------------------

--
-- Table structure for table `cover`
--

CREATE TABLE IF NOT EXISTS `cover` (
  `image` varchar(512) DEFAULT NULL,
  `clPath` varchar(128) DEFAULT NULL,
  `uid` int(10) DEFAULT NULL,
  `storyid` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `currentstory`
--

CREATE TABLE IF NOT EXISTS `currentstory` (
  `uid` int(10) DEFAULT NULL,
  `storyid` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `editgallery`
--

CREATE TABLE IF NOT EXISTS `editgallery` (
  `name` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `follow`
--

CREATE TABLE IF NOT EXISTS `follow` (
  `id` int(11) DEFAULT NULL,
  `following` longtext,
  `followers` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `follow`
--

INSERT INTO `follow` (`id`, `following`, `followers`) VALUES
(3, NULL, '->2->'),
(2, '->3->', NULL),
(8, '->10->', '->10->'),
(10, '->8->', '->8->');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
  `storyid` int(11) DEFAULT NULL,
  `images` varchar(2048) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `liked`
--

CREATE TABLE IF NOT EXISTS `liked` (
  `id` int(11) DEFAULT NULL,
  `likeTo` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `liked`
--

INSERT INTO `liked` (`id`, `likeTo`) VALUES
(2, '->5->4->8->7->'),
(8, '->4->32->33->'),
(10, '->32->');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
  `id` int(5) DEFAULT NULL,
  `notifier` int(5) DEFAULT NULL,
  `notify` varchar(1024) DEFAULT NULL,
  `storyid` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `notifier`, `notify`, `storyid`) VALUES
(3, 8, 'Harish B Liked your story.', 4),
(8, 8, 'Harish B Liked your story.', 32),
(10, 8, 'Harish B Liked your story.', 33),
(8, 10, 'Triptroop Bot Liked your story.', 32),
(10, 8, 'Harish B Liked your story.', 33);

-- --------------------------------------------------------

--
-- Table structure for table `oauthcheck`
--

CREATE TABLE IF NOT EXISTS `oauthcheck` (
  `id` int(11) DEFAULT NULL,
  `viaOauth` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `id` int(11) DEFAULT NULL,
  `dpPath` varchar(256) DEFAULT NULL,
  `clPath` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `dpPath`, `clPath`) VALUES
(2, '/profilePics/2/10511208_10202306096400399_7888562466553956249_n.jpg', 'profilePics/2/10511208_10202306096400399_7888562466553956249_n'),
(3, '/profilePics/2/logo_final.png', 'profilePics/2/logo_final'),
(4, 'https://res.cloudinary.com/triptroop/image/upload/assets/avatar.png', '/assets/avatar.png'),
(5, 'https://res.cloudinary.com/triptroop/image/upload/assets/avatar.png', '/assets/avatar.png'),
(6, 'https://res.cloudinary.com/triptroop/image/upload/assets/avatar.png', '/assets/avatar.png'),
(7, 'https://res.cloudinary.com/triptroop/image/upload/assets/avatar.png', '/assets/avatar.png'),
(8, 'https://res.cloudinary.com/triptroop/image/upload/assets/avatar.png', '/assets/avatar.png'),
(9, 'https://res.cloudinary.com/triptroop/image/upload/assets/avatar.png', '/v1436554760/assets/avatar.png'),
(10, 'https://res.cloudinary.com/triptroop/image/upload/assets/avatar.png', '/v1436554760/assets/avatar.png');

-- --------------------------------------------------------

--
-- Table structure for table `stories`
--

CREATE TABLE IF NOT EXISTS `stories` (
`storyid` int(10) unsigned NOT NULL,
  `userid` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `filepath` longtext COLLATE utf8_unicode_ci,
  `saved` int(11) NOT NULL,
  `category` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `published` int(11) NOT NULL,
  `cover` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(11) NOT NULL,
  `currency` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `expense` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `tripDate` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `coverClPath` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hits` int(15) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `stories`
--

INSERT INTO `stories` (`storyid`, `userid`, `title`, `filepath`, `saved`, `category`, `published`, `cover`, `count`, `currency`, `expense`, `tripDate`, `coverClPath`, `createdAt`, `hits`) VALUES
(4, '3', 'Akshay', '  <p class="graf graf--p graf--first" name="wx2an3766r"><span class="defaultValue defaultValue--root">Tell your story…</span><br></p><p class="graf graf--p graf--last" name="2lwfnoecdi"> <br></p>', 1, '->cat9->', 1, '/storyCover/2/1/activity-feed-empty-state.png', 2, 'USD', '', '', 'storyCover/2/1/activity-feed-empty-state', '10-06-15', 3),
(5, '3', 'Another Story', '  <p class="graf graf--p graf--first" name="wx2an3766r"><span class="defaultValue defaultValue--root">Tell your story…</span><br></p><p class="graf graf--p graf--last" name="2lwfnoecdi"> <br></p>', 1, '->cat9->', 1, '/storyCover/2/1/activity-feed-empty-state.png', 1, 'USD', '', '', 'storyCover/2/1/activity-feed-empty-state', '10-06-15', NULL),
(7, '2', 'Name yourasdsada story', '  <p class="graf graf--p graf--first" name="r1vhxv42t9">as dsad saldkjas dkasj dkasdjkasa<br></p><p class="graf graf--p graf--last" name="1bxjnvobt9"> <br></p>     ', 0, '->cat23->', 1, 'publicpublicpublicpublicpublicpublicpublic/storyCover/2/1/avatar.jpg', 1, 'USD', '1111', '06/11/2015 - 06/15/2015', 'storyCover/2/1/avatar', '15-06-15', 4),
(8, '2', 'Name your story', '  <figure contenteditable="false" class="graf graf--figure is-defaultValue graf--first" name="pr7qjb57b9" tabindex="0"> <div style="max-width: 1100px; max-height: 687.5px;" class="aspectRatioPlaceholder is-locked"> <div style="padding-bottom: 62.5%;" class="aspect-ratio-fill"></div> <img src="/uploads/Screen Shot 2015-06-15 at 3.33.38 pm.png" class="graf-image"> </div> <figcaption contenteditable="true" data-default-value="Type caption for image (optional)" class="imageCaption"> <span class="defaultValue">Type caption for image (optional)</span> <br> </figcaption> </figure><figure contenteditable="false" class="graf graf--figure is-defaultValue" name="lxbk68byb9" tabindex="0"> <div style="max-width: 1100px; max-height: 687.5px;" class="aspectRatioPlaceholder is-locked"> <div style="padding-bottom: 62.5%;" class="aspect-ratio-fill"></div> <img src="/uploads/Screen Shot 2015-06-15 at 3.33.18 pm.png" class="graf-image"> </div> <figcaption contenteditable="true" data-default-value="Type caption for image (optional)" class="imageCaption"> <span class="defaultValue">Type caption for image (optional)</span> <br> </figcaption> </figure><figure contenteditable="false" class="graf graf--figure is-defaultValue" name="k119yhkt9" tabindex="0"> <div style="max-width: 1100px; max-height: 1804.11458333333px;" class="aspectRatioPlaceholder is-locked"> <div style="padding-bottom: 164.010416666667%;" class="aspect-ratio-fill"></div> <img src="/uploads/Happy-Internal-Page.jpg" class="graf-image"> </div> <figcaption contenteditable="true" data-default-value="Type caption for image (optional)" class="imageCaption"> <span class="defaultValue">Type caption for image (optional)</span> <br> </figcaption> </figure><figure contenteditable="false" class="graf graf--figure is-defaultValue" name="8v9d2kpgb9" tabindex="0"> <div style="max-width: 1100px; max-height: 548.688915375447px;" class="aspectRatioPlaceholder is-locked"> <div style="padding-bottom: 49.880810488677%;" class="aspect-ratio-fill"></div> <img src="/uploads/Screen Shot 2015-06-15 at 11.10.06 am.png" class="graf-image"> </div> <figcaption contenteditable="true" data-default-value="Type caption for image (optional)" class="imageCaption"> <span class="defaultValue">Type caption for image (optional)</span> <br> </figcaption> </figure><figure contenteditable="false" class="graf graf--figure is-defaultValue" name="wp7hk3mcxr" tabindex="0"> <div style="max-width: 1100px; max-height: 5526.30288166769px;" class="aspectRatioPlaceholder is-locked"> <div style="padding-bottom: 502.391171060699%;" class="aspect-ratio-fill"></div> <img src="/uploads/college-blog-full.png" class="graf-image"> </div> <figcaption contenteditable="true" data-default-value="Type caption for image (optional)" class="imageCaption"> <span class="defaultValue">Type caption for image (optional)</span> <br> </figcaption> </figure><p class="graf graf--p" name="xst08me7b9"><br></p><p class="graf graf--p graf--last" name="pbmkju4n29"> <br></p> ', 1, '', 1, 'public/storyCover/2/2/Happy-Internal-Page.jpg', 1, 'USD', '', '', 'storyCover/2/2/Happy-Internal-Page', '16-06-15', 4),
(9, '2', 'This is the new story we are talking about', '  <p class="graf graf--p graf--first" name="hgcj9vn29">Beginning of the story<br></p><p class="graf graf--p graf--last is-selected" name="jqys1exw29"> <br></p><p class="graf graf--p" name="1b5fbkpgb9">Beginning of the story<br></p><p class="graf graf--p" name="vhpwe1att9"><br class="Apple-interchange-newline"></p>', 0, '->cat1->cat3->cat4->', 1, '/storyCover/2/3/11059977_909220229148592_5171459884580793943_n.jpg', 0, 'EUR', '32', '', 'storyCover/2/3/yijezca1gobhcapx98h2', '14-07-15', 4),
(10, '2', 'NAME YOUR STORYNAME YOUR STORYNAME YOUR STORYNAME YOUR STORYNAME YOUR STORYNAME YOUR STORYNAME YOUR STORYNAME YOUR STORYNAME YOUR STORYNAME YOUR STORYNAME YOUR STORYNAME YOUR STORYNAME YOUR STORYNAME YOUR STORYNAME YOUR STORYNAME YOUR STORYNAME YOUR STORYN', '  <p class="graf graf--p graf--first" name="n7get57b9"><span class="defaultValue defaultValue--root">Tell your story…</span><br></p><p class="graf graf--p graf--last" name="vmw75jyvi"> <br></p>', 1, '', 0, '/storyCover/2/4/10308285_819176038170616_6715547394565001033_n.jpg', 0, 'USD', '', '', 'storyCover/2/4/gg9dkvl6fw5ktlgjawfd', '14-07-15', 0),
(11, '2', 'Name your story', '  <p class="graf graf--p graf--first" name="jnqnbnvcxr"><span class="defaultValue defaultValue--root">Tell your story…</span><br></p><p class="graf graf--p graf--last" name="iitvivbo6r"> <br></p>', 1, '', 0, '/storyCover/2/5/10308285_819176038170616_6715547394565001033_n.jpg', 0, 'USD', '', '', 'storyCover/2/5/pg5rbkyzj0nttc09c3jw', '14-07-15', 0),
(12, '8', 'Default', '  <p class="graf graf--p graf--first" name="ts1pfx0f6r"><span class="defaultValue defaultValue--root">Tell your story…</span><br></p><p class="graf graf--p graf--last" name="3278a7nwmi"> <br></p> ', 1, '', 0, 'public/storyCover/8/1/modern-POP-bedroom-ceiling-designs-with-spread-lighting.jpg', 0, 'USD', '', '', 'storyCover/8/1/yqgucdimi416vtynxv1o', '01-08-15', 1),
(13, '8', 'HARISH BOKE', '  <p class="graf graf--p graf--first" name="f7dlpu8fr"><span class="defaultValue defaultValue--root">Tell your story…</span><br></p><p class="graf graf--p graf--last" name="pex3ri19k9"> <br></p>  ', 1, '', 0, '/storyCover/8/4/modern-bedroom-ceiling-4ta3pidu.jpg', 0, 'USD', '', '', 'storyCover/8/4/jpy5lrrditbosygtpstv', '02-08-15', 6),
(17, '8', 'Test Tes``t', '  <p class="graf graf--p graf--first" name="yogdp9zfr"><span class="defaultValue defaultValue--root">Tell your story…</span><br></p><p class="graf graf--p graf--last" name="y0e7bymn29"> <br></p>   ', 1, '', 0, 'publicpublicpublicpublicpublicpublicpublic/storyCover/8/6/modern-POP-bedroom-ceiling-designs-with-spread-lighting.jpg', 0, 'USD', '', '', 'storyCover/8/6/y8hox3gftkvtvni8wrpy', '02-08-15', 1),
(23, '8', 's', '  <p class="graf graf--p graf--first" name="424jzwipb9"><span class="defaultValue defaultValue--root">Tell your story…</span><br></p><p class="graf graf--p graf--last" name="do7oa5g66r"> <br></p>     ', 1, '', 0, 'publicpublicpublicpublicpublicpublic/storyCover/8/4/modern-POP-bedroom-ceiling-designs-with-spread-lighting.jpg', 0, 'INR', '9999999999999999', '', 'storyCover/8/4/yanhkeoyodv53znzajbj', '02-08-15', 1),
(24, '8', 'JSKDLFDJSLFJLSJDFJSJKDJFJSKLJ JSKDFLDSJLFJLKDSJFKL SFDSJKJS', '  <p class="graf graf--p graf--first" name="skjrgj5rk9"><span class="defaultValue defaultValue--root">Tell your story…</span><br></p><p class="graf graf--p graf--last" name="4761rlik9"> <br></p>     ', 1, '', 0, 'publicpublicpublicpublicpublicpublicpublicpublic/storyCover/8/5/modern-POP-bedroom-ceiling-designs-with-spread-lighting.jpg', 0, 'USD', '', '', 'storyCover/8/5/ylrhhgqx0cqsllvycf7w', '02-08-15', 2),
(25, '8', 'FKJSLLJDSFKJSLLJDS', '  <p class="graf graf--p graf--first" name="cgnwmi"><span class="defaultValue defaultValue--root">Tell your story…</span><br></p><p class="graf graf--p graf--last" name="ibaux47vi"> <br></p>', 1, '', 0, '/storyCover/8/6/modern-POP-bedroom-ceiling-designs-with-spread-lighting.jpg', 0, 'USD', '', '', 'storyCover/8/6/mxlde8hvlc4hwenvnh6x', '03-08-15', 0),
(26, '8', 'fsdfsdfsd', '  <p class="graf graf--p graf--first" name="pjyf4j9k9"><span class="defaultValue defaultValue--root">Tell your story…</span><br></p><p class="graf graf--p graf--last" name="w7329ms4i"> <br></p>', 1, '', 0, '/storyCover/8/7/modern-POP-bedroom-ceiling-designs-with-spread-lighting.jpg', 0, 'USD', '', '', 'storyCover/8/7/p7hqqdmhjqmu2td2ng9n', '03-08-15', 0),
(27, '8', 'sdfsdf', '  <p class="graf graf--p graf--first" name="lodaacq5mi"><span class="defaultValue defaultValue--root">Tell your story…</span><br></p><p class="graf graf--p graf--last" name="a6zfmq85mi"> <br></p>', 1, '', 0, '/storyCover/8/8/04_01.jpg', 0, 'USD', '', '', 'storyCover/8/8/zlhivgehothrgtabxq4h', '03-08-15', 0),
(28, '8', 'sdf', '  <p class="graf graf--p graf--first" name="a2095m6lxr"><span class="defaultValue defaultValue--root">Tell your story…</span><br></p><p class="graf graf--p graf--last" name="29bno8ncdi"> <br></p>', 1, '', 0, '/storyCover/8/9/modern-POP-bedroom-ceiling-designs-with-spread-lighting.jpg', 0, 'USD', '', '', 'storyCover/8/9/o2pebsjx4jpagajuf12b', '03-08-15', 0),
(29, '8', 'asdasdasdasda', '  <p class="graf graf--p graf--first" name="5lnsdfs9k9"><span class="defaultValue defaultValue--root">Tell your story…</span><br></p><p class="graf graf--p graf--last" name="n1giudi"> <br></p>', 1, '', 0, '/storyCover/8/10/11059977_909220229148592_5171459884580793943_n.jpg', 0, 'USD', '', '', 'storyCover/8/10/mbvmt5lvutc3nqvqzwjs', '15-07-08', 0),
(30, '8', 'sdfsdfsdfsd', '  <p class="graf graf--p graf--first" name="06k4sd1jor"><span class="defaultValue defaultValue--root">Tell your story…</span><br></p><p class="graf graf--p graf--last" name="2vpesfko6r"> <br></p>  ', 1, '', 0, 'publicpublic/storyCover/8/11/11059977_909220229148592_5171459884580793943_n.jpg', 0, 'USD', '', '', 'storyCover/8/11/bisuia09h6nuofnpmsfk', '15-07-08', 4),
(31, '8', 'Test draft stories saves categories', '  <p class="graf graf--p graf--first" name="g536zs38fr"><span class="defaultValue defaultValue--root">Tell your story…</span><br></p><p class="graf graf--p graf--last" name="qev53tyb9"> <br></p>  ', 1, '', 0, 'publicpublic/storyCover/8/12/11059977_909220229148592_5171459884580793943_n.jpg', 0, 'USD', '', '', 'storyCover/8/12/jcg9srcsjokps26tnhzs', '15-07-08', 3),
(32, '8', 'test harish story', '  <p class="graf graf--p graf--first" name="kaf62sm7vi"><span class="defaultValue defaultValue--root">Tell your story…</span><br></p><p class="graf graf--p graf--last" name="ncb0ara4i"> <br></p>', 0, '0', 1, '/storyCover/8/13/2012-08-22_13-48-16_897.jpg', 2, 'USD', '', '', 'storyCover/8/13/mlcpdkhqmyinyt4rr0r4', '15-12-08', 2),
(33, '10', 'Story of TTBot ', '  <p class="graf graf--p graf--first" name="6d2jn0cnmi"><br></p><p class="graf graf--p graf--last is-selected" name="7k4lrz4cxr"> This is start of the Story</p>', 1, '->cat2->cat14->cat20->', 1, '/storyCover/10/1/1 (4).jpg', 1, 'USD', '100000', '', 'storyCover/10/1/xbru7mylptdnuqdtvzj6', '15-13-08', 5);

-- --------------------------------------------------------

--
-- Table structure for table `tempgallery`
--

CREATE TABLE IF NOT EXISTS `tempgallery` (
  `images` varchar(2048) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tempwish`
--

CREATE TABLE IF NOT EXISTS `tempwish` (
  `id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tempwish`
--

INSERT INTO `tempwish` (`id`) VALUES
(5),
(7),
(4);

-- --------------------------------------------------------

--
-- Table structure for table `userinfo`
--

CREATE TABLE IF NOT EXISTS `userinfo` (
  `id` int(11) NOT NULL,
  `gender` varchar(64) NOT NULL,
  `mbl` varchar(10) NOT NULL,
  `location` varchar(64) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `categories` varchar(512) NOT NULL,
  `pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userinfo`
--

INSERT INTO `userinfo` (`id`, `gender`, `mbl`, `location`, `description`, `categories`, `pin`) VALUES
(1, '', '', '', '', '', ''),
(2, '', '', '', '', '->cat13->cat14->cat15->', ''),
(3, '', '', '', '', '', ''),
(4, '', '', '', '', '', ''),
(5, '', '', '', '', '', ''),
(6, '', '', '', '', '', ''),
(7, '', '', '', '', '', ''),
(8, '', '', '', '', '', ''),
(9, '', '', '', '', '', ''),
(10, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(10) unsigned NOT NULL,
  `firstname` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `emailid` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `verified` int(11) NOT NULL,
  `following` int(11) NOT NULL,
  `followers` int(11) NOT NULL,
  `confirmed` int(11) NOT NULL,
  `visited` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `emailid`, `password`, `verified`, `following`, `followers`, `confirmed`, `visited`) VALUES
(2, 'Akshay', 'Sonawane', 'akshaysonawaane@gmail.com', 'password@123', 1, 1, 0, 1, 1),
(3, 'a', 'a', 'akshaysonawaane7@gmail.com', '$2y$10$rvHpUKiwY7KsBRBYB7txnO7zQNWsQ/N1pqgPKCijJtksUpo/73E0W', 0, 0, 1, 1, 1),
(4, 'sachin', 'jadhav', 'jadhavsachin174@gmail.com', '$2y$10$ri/azXqXPHRFiDUZmkdP0uh9xmwXEAWZgde4PVTMgJ3UxnI7iQcgG', 0, 0, 0, 0, 0),
(5, 'one', 'onw', 'one@gmail.com', '$2y$10$rYebFGAo.TUGIW6Oml5MmOaypDI.w798yUAVHocJ.m.RsZUeNOTJm', 0, 0, 0, 0, 0),
(8, 'Harish', 'B', 'harish.boke@gmail.com', '$2y$10$BHgTRMEM.N2mE8NCeaNH/ugqYXNRoPh7GLJq20lqzvIEDqtXyFi3.', 1, 1, 1, 1, 1),
(9, 'Robot', 'Test', 'robot@gmail.com', '$2y$10$2mDY29IqCmgKR1r1Fmuh5OqvogxOFgiN9bT325m.mi6b4fNP5Iy2i', 0, 0, 0, 1, 1),
(10, 'Triptroop', 'Bot', 'ttbot@gmail.com', '$2y$10$3/pLIQe5DWifJQyMEiguPOwCvu0kovMqkql7.Hf8FgtrXIsacPIsW', 0, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE IF NOT EXISTS `wishlist` (
  `id` int(11) DEFAULT NULL,
  `wishTo` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wishlist`
--

INSERT INTO `wishlist` (`id`, `wishTo`) VALUES
(2, '->5->7->4->8->');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `stories`
--
ALTER TABLE `stories`
 ADD PRIMARY KEY (`storyid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `stories`
--
ALTER TABLE `stories`
MODIFY `storyid` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
