# ************************************************************
# Sequel Pro SQL dump
# Version 4135
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.5.38)
# Database: tc_db
# Generation Time: 2015-04-09 15:10:14 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table cat_name
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cat_name`;

CREATE TABLE `cat_name` (
  `default_name` varchar(128) DEFAULT NULL,
  `actual_name` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `cat_name` WRITE;
/*!40000 ALTER TABLE `cat_name` DISABLE KEYS */;

INSERT INTO `cat_name` (`default_name`, `actual_name`)
VALUES
	('adventure','cat1'),
	('arts-and-liesure','cat2'),
	('backpacking','cat3'),
	('beaches','cat4'),
	('budget-travel','cat5'),
	('coasts-and-islands','cat6'),
	('diving-and-snorkelling','cat7'),
	('ecotourism','cat8'),
	('family-travel','cat9'),
	('festival-and-events','cat10'),
	('film-and-television','cat11'),
	('food-and-drink','cat12'),
	('gear-and-tech','cat13'),
	('honeymoon-and-romance','cat14'),
	('luxury-travel','cat15'),
	('music','cat16'),
	('off-the-beaten-track','cat17'),
	('planes-and-trains','cat18'),
	('road-trips','cat19'),
	('round-the-world-travel','cat20'),
	('travel-photograhpy','cat21'),
	('travel-shopping','cat22'),
	('walking-and-trekking','cat23'),
	('wildlife-and-nature','cat24');

/*!40000 ALTER TABLE `cat_name` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table category_map
# ------------------------------------------------------------

DROP TABLE IF EXISTS `category_map`;

CREATE TABLE `category_map` (
  `ckey` varchar(10) DEFAULT NULL,
  `cvalue` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `category_map` WRITE;
/*!40000 ALTER TABLE `category_map` DISABLE KEYS */;

INSERT INTO `category_map` (`ckey`, `cvalue`)
VALUES
	('cat1','Adventure'),
	('cat2','Arts and Liesure'),
	('cat3','Backpacking'),
	('cat4','Beaches'),
	('cat5','Budget Travel'),
	('cat6','Coasts and Islands'),
	('cat7','Diving and Snorkelling'),
	('cat8','Ecotourism'),
	('cat9','Family Travel'),
	('cat10','Festival and Events'),
	('cat11','Film and Television'),
	('cat12','Food and Drink'),
	('cat23','Gear and Tech'),
	('cat14','Honeymoon and Romance'),
	('cat15','Luxury Travel'),
	('cat16','Music'),
	('cat17','Off the Beaten Track'),
	('cat18','Planes and Trains'),
	('cat19','Road Trips'),
	('cat20','Round the World Travel'),
	('cat21','Travel Photograhpy'),
	('cat22','Travel Shopping'),
	('cat23','Walking and Trekking'),
	('cat24','Wild life and Nature');

/*!40000 ALTER TABLE `category_map` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cover
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cover`;

CREATE TABLE `cover` (
  `image` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table currentstory
# ------------------------------------------------------------

DROP TABLE IF EXISTS `currentstory`;

CREATE TABLE `currentstory` (
  `id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table editgallery
# ------------------------------------------------------------

DROP TABLE IF EXISTS `editgallery`;

CREATE TABLE `editgallery` (
  `name` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `editgallery` WRITE;
/*!40000 ALTER TABLE `editgallery` DISABLE KEYS */;

INSERT INTO `editgallery` (`name`)
VALUES
	('/uploads/Koala.jpg'),
	('/uploads/Lighthouse.jpg'),
	('/uploads/Penguins.jpg');

/*!40000 ALTER TABLE `editgallery` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table follow
# ------------------------------------------------------------

DROP TABLE IF EXISTS `follow`;

CREATE TABLE `follow` (
  `id` int(11) DEFAULT NULL,
  `following` longtext,
  `followers` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `follow` WRITE;
/*!40000 ALTER TABLE `follow` DISABLE KEYS */;

INSERT INTO `follow` (`id`, `following`, `followers`)
VALUES
	(20,'25->','->25->'),
	(25,'->undefined->20->','20->'),
	(0,NULL,'->25->'),
	(31,NULL,'->');

/*!40000 ALTER TABLE `follow` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table gallery
# ------------------------------------------------------------

DROP TABLE IF EXISTS `gallery`;

CREATE TABLE `gallery` (
  `storyid` int(11) DEFAULT NULL,
  `images` varchar(2048) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `gallery` WRITE;
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;

INSERT INTO `gallery` (`storyid`, `images`)
VALUES
	(5,'->Koala.jpg->Lighthouse.jpg->Penguins.jpg->'),
	(6,'->Hydrangeas.jpg->Jellyfish.jpg->Koala.jpg->Lighthouse.jpg->'),
	(23,'->Hydrangeas.jpg->Jellyfish.jpg->Koala.jpg->Lighthouse.jpg->');

/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table liked
# ------------------------------------------------------------

DROP TABLE IF EXISTS `liked`;

CREATE TABLE `liked` (
  `id` int(11) DEFAULT NULL,
  `likeTo` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `liked` WRITE;
/*!40000 ALTER TABLE `liked` DISABLE KEYS */;

INSERT INTO `liked` (`id`, `likeTo`)
VALUES
	(19,'->6->'),
	(20,'->5->'),
	(25,'->25->17->18->9->12->11->5->22->27->32->35->');

/*!40000 ALTER TABLE `liked` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table oauthcheck
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauthcheck`;

CREATE TABLE `oauthcheck` (
  `id` int(11) DEFAULT NULL,
  `viaOauth` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table profile
# ------------------------------------------------------------

DROP TABLE IF EXISTS `profile`;

CREATE TABLE `profile` (
  `id` int(11) DEFAULT NULL,
  `dpPath` varchar(256) DEFAULT NULL,
  `clPath` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `profile` WRITE;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;

INSERT INTO `profile` (`id`, `dpPath`, `clPath`)
VALUES
	(25,'/assets/images/user.jpg','profilePics/25/rcwbh0lfkuftmwwf0xa9'),
	(20,'/assets/images/user.jpg','profilePics/25/rcwbh0lfkuftmwwf0xa9'),
	(31,'/assets/images/user.jpg','profilePics/25/rcwbh0lfkuftmwwf0xa9'),
	(32,'/profilePics/32/1658491_615053045217161_268545801_o.jpg','profilePics/32/iigxotzpxso0fa40ahdq');

/*!40000 ALTER TABLE `profile` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table stories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `stories`;

CREATE TABLE `stories` (
  `storyid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `filepath` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `saved` int(11) NOT NULL,
  `category` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `published` int(11) NOT NULL,
  `cover` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`storyid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `stories` WRITE;
/*!40000 ALTER TABLE `stories` DISABLE KEYS */;

INSERT INTO `stories` (`storyid`, `userid`, `title`, `filepath`, `saved`, `category`, `published`, `cover`, `count`)
VALUES
	(23,'25','hydra','Stories/25/45.txt',1,'->cat3->cat4->',1,'/storyCover/1.jpg',0),
	(24,'25','peng','Stories/25/46.txt',0,'->cat4->',1,'/storyCover/2.jpg',0),
	(25,'25','sachie','Stories/25/46.txt',0,'->cat2->cat3->',1,'/storyCover/3.jpg',0),
	(26,'25','makad','Stories/25/46.txt',1,'->cat4->',1,'/storyCover/4.jpg',0),
	(27,'25','makadasdasdasdsadsada','Stories/25/46.txt',0,'->cat4->',1,'/storyCover/5.jpg',1),
	(28,'25','asdasdasdad','Stories/25/46.txt',1,'->cat4->',1,'/storyCover/6.jpg',5),
	(29,'25','asdasdasdad','Stories/25/46.txt',0,'->cat2->cat3->',1,'/storyCover/7.jpg',0),
	(30,'25','asdasdasdad','Stories/25/46.txt',0,'->cat2->cat3->',1,'/storyCover/8.jpg',0),
	(31,'25','asdasdasdad','Stories/25/46.txt',0,'->cat2->cat3->',1,'/storyCover/9.jpg',0),
	(32,'25','asdasdasdad','Stories/25/46.txt',0,'->cat2->cat3->',1,'/storyCover/10.jpg',1),
	(33,'25','asdasdasdad','Stories/25/46.txt',0,'->cat2->cat3->',1,'/storyCover/Lighthouse.jpg',0),
	(34,'25','asdasdasdad','Stories/25/46.txt',0,'->cat2->cat3->',1,'/storyCover/12.jpg',0),
	(35,'25','asdasdasdad','Stories/25/46.txt',0,'->cat2->cat3->',1,'/storyCover/login_bg.jpg',1),
	(36,'25','Road back home','Stories/25/36.txt',1,'->cat1->cat2->cat3->cat4->cat5->',1,'/storyCover/36/Deepside-Deejays-feat-Viky-Red---The-Road-Back-Home-single-nou.jpg',0),
	(37,'25','lai bhari!!!','Stories/25/37.txt',0,'->cat11->',1,'/storyCover/37/photo-1427606694672-8f86d75947ce.jpeg',0),
	(38,'25','Name your story','Stories/25/38.txt',1,'0',0,'public',0),
	(39,'25','Wall','Stories/25/39.txt',0,'->cat1->',1,'/storyCover/39/wall.jpg',0),
	(40,'25','Name your story','Stories/25/40.txt',1,'0',0,'public',0);

/*!40000 ALTER TABLE `stories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tempgallery
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tempgallery`;

CREATE TABLE `tempgallery` (
  `images` varchar(2048) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tempgallery` WRITE;
/*!40000 ALTER TABLE `tempgallery` DISABLE KEYS */;

INSERT INTO `tempgallery` (`images`)
VALUES
	('->Hydrangeas.jpg->Jellyfish.jpg->Koala.jpg->Lighthouse.jpg->');

/*!40000 ALTER TABLE `tempgallery` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tempwish
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tempwish`;

CREATE TABLE `tempwish` (
  `id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tempwish` WRITE;
/*!40000 ALTER TABLE `tempwish` DISABLE KEYS */;

INSERT INTO `tempwish` (`id`)
VALUES
	(25),
	(10),
	(11),
	(17),
	(5),
	(22),
	(27),
	(32),
	(35),
	(39);

/*!40000 ALTER TABLE `tempwish` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table userinfo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userinfo`;

CREATE TABLE `userinfo` (
  `id` int(11) NOT NULL,
  `gender` varchar(64) NOT NULL,
  `mbl` varchar(10) NOT NULL,
  `location` varchar(64) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `categories` varchar(512) NOT NULL,
  `pin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `userinfo` WRITE;
/*!40000 ALTER TABLE `userinfo` DISABLE KEYS */;

INSERT INTO `userinfo` (`id`, `gender`, `mbl`, `location`, `description`, `categories`, `pin`)
VALUES
	(25,'Male','9970016888','pune','pune','->cat1->cat2->cat3->cat4->cat6->cat7->','+91'),
	(0,'','','','','->cat3->cat4->',''),
	(0,'','','','','0',''),
	(0,'','','','','0',''),
	(31,'','','','','->cat2->cat3->cat4->',''),
	(31,'','','','','',''),
	(20,'','','','','->cat1->cat2->',''),
	(32,'','','','','->cat2->cat7->cat8->cat13->','');

/*!40000 ALTER TABLE `userinfo` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `emailid` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `verified` int(11) NOT NULL,
  `following` int(11) NOT NULL,
  `followers` int(11) NOT NULL,
  `confirmed` int(11) NOT NULL,
  `visited` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `firstname`, `lastname`, `emailid`, `password`, `verified`, `following`, `followers`, `confirmed`, `visited`)
VALUES
	(20,'harish','boke','harish.boke@gmail.com','$2y$10$XaxTrrrShBpUjIjtUryISuUP1BbsjgT1pzeO/AC.zclipeq5ORuXK',0,1,1,1,1),
	(25,'sachin','jadhav','jadhavsachin174@gmail.com','$2y$10$avj6rkk.lJ2W99JorUSINe900Ou.4ulnbMubLQl..GwJkLl5Mry7G',0,1,1,1,1),
	(29,'Sachin','Jadhav','','',0,0,0,0,0),
	(30,'Sachin','Jadhav','','',0,0,0,0,0),
	(31,'sach','jadhav','sachiejadhav026@gmail.com','$2y$10$JMKFTWArxKFrtkIxHs163Op5atqqYH4BACwYObGrp3FZrr7AbSsFa',0,0,0,1,0);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wishlist
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wishlist`;

CREATE TABLE `wishlist` (
  `id` int(11) DEFAULT NULL,
  `wishTo` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `wishlist` WRITE;
/*!40000 ALTER TABLE `wishlist` DISABLE KEYS */;

INSERT INTO `wishlist` (`id`, `wishTo`)
VALUES
	(19,'->2->5->6->'),
	(20,'->5->'),
	(25,'->25->10->11->17->5->22->27->32->35->39->');

/*!40000 ALTER TABLE `wishlist` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
