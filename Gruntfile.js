//Gruntfile

module.exports = function(grunt) {

//Initializing the configuration object
grunt.initConfig({

    //pkg: grunt.file.readJSON('package.json'),
// Paths variables
paths: {
        // Development where put SASS files, etc
        assets: {
          css: './public/assets/stylesheets/',
          js: './public/assets/javascripts/'
        },
        // Production where Grunt output the files
        css: './public/css/',
        js: './public/js/'

      },

// Task configuration
concat: {
      js: {
        src: [
        './public/bower_components/jquery/dist/jquery.min.js',
        './public/assets/javascripts/bootstrap.min.js',
        './public/bower_components/chosen/chosen.jquery.min.js',
        './public/assets/javascripts/notifications.js',
        './public/assets/javascripts/main.js'
        ],
              dest: './public/assets/javascripts/application.js'
          }
        },
      uglify: {
        options: {
           // Grunt can replace variables names, but may not be a good idea for you,
          // I leave this option as false
          mangle: false
        },
        js: {
        // Grunt will search for "**/*.js" when the "minify" task
        // runs and build the appropriate src-dest file mappings then, so you
        // don't need to update the Gruntfile when files are added or removed.
        files: {
          './public/assets/javascripts/application.js': './public/assets/javascripts/application.js'
        }
      }
    },    
    sass: {
      development: {
        options: {                       // Target options
          debugInfo: false,
          style: 'compressed'
        },
        files: {
          "./public/assets/stylesheets/application.css":"./app/assets/stylesheets/application.scss",
          "./public/assets/stylesheets/main.css":"./app/assets/stylesheets/main.scss",
          "./public/assets/stylesheets/card.css":"./app/assets/stylesheets/card.scss",
          "./public/assets/stylesheets/app.css":"./app/assets/stylesheets/app.scss",
          "./public/assets/stylesheets/profile_new.css":"./app/assets/stylesheets/profile_new.scss",
          "./public/assets/stylesheets/profile.css":"./app/assets/stylesheets/profile.scss",
          "./public/assets/stylesheets/search.css":"./app/assets/stylesheets/search.scss",
          "./public/assets/stylesheets/login-signup.css":"./app/assets/stylesheets/login-signup.scss",
          "./public/assets/stylesheets/dante-overridden.css":"./app/assets/stylesheets/dante-overridden.scss",
          "./public/assets/stylesheets/landing.css":"./app/assets/stylesheets/landing.scss",
          "./public/assets/stylesheets/theme.css":"./app/assets/stylesheets/theme.scss",
          "./public/assets/stylesheets/places.css":"./app/assets/stylesheets/places.scss",
          //"./public/assets/stylesheets/storytellers.css":"./app/assets/stylesheets/storytellers.scss",
          "./public/assets/stylesheets/post.css":"./app/assets/stylesheets/post.scss",
          "./public/assets/stylesheets/dante.css":"./public/bower_components/dante/app/assets/stylesheets/dante.scss",
          "./public/assets/stylesheets/mqueries.css":"./app/assets/stylesheets/mqueries.scss"
        }
      }
    },
    cssmin: {  
      sitecss: {  
        options: {  
          banner: '/* My minified css file */'  ,
          outputStyle: 'compressed'
        },  
        files: {  
          './public/assets/stylesheets/app.min.css': [  
          "./public/assets/stylesheets/application.css",
          "./public/assets/stylesheets/fonts.css",
          "./public/assets/stylesheets/main.css",
          "./public/assets/stylesheets/app.css",
          "./public/assets/stylesheets/card.css",
          "./public/assets/stylesheets/profile_new.css",
          './public/assets/stylesheets/daterangepicker-bs3.css',
          "./public/assets/stylesheets/post.css",
          "./public/assets/stylesheets/profile.css",
          "./public/assets/stylesheets/search.css",
          "./public/assets/stylesheets/login-signup.css",
          "./public/assets/stylesheets/landing.css",
          "./public/assets/stylesheets/theme.css",
          "./public/assets/stylesheets/places.css",
          "./public/assets/stylesheets/travelcircles_icons.css",
          "./public/assets/stylesheets/mqueries.css",
          "./public/assets/stylesheets/animate.css"]
        }  
      }  
    }, 
    //"./public/assets/stylesheets/storytellers.css",
    wiredep: {
      app: {
        ignorePath: /(^(\/|\.+(?!\/[^\.]))+\.+\.)\/[a-z]*/,
        src: ['./app/views/layouts/*.php'],
            //exclude: ['./bower_components/bootstrap-sass-official/vendor/assets/javascripts/bootstrap/*.js'],
          }
        },    
        watch: {
          js: {
            files: [
            './public/bower_components/jquery/dist/jquery.min.js',
            './public/bower_components/bootstrap-sass-official/vendor/assets/javascripts/bootstrap/*js',
            './app/assets/javascripts/*.js',
            './public/assets/javascripts/*.js'
            ],
            tasks: ['concat:js', 'uglify:js']
          },
          sass: {
            files: ['./app/assets/stylesheets/*.{scss,sass}'],
            tasks: ['sass']
          }
        }   
      });

// Plugin loading
grunt.loadNpmTasks('grunt-wiredep');
grunt.loadNpmTasks('grunt-contrib-compass');
grunt.loadNpmTasks('grunt-contrib-sass');
grunt.loadNpmTasks('grunt-contrib-concat');
grunt.loadNpmTasks('grunt-contrib-uglify');
grunt.loadNpmTasks('grunt-contrib-cssmin');
grunt.loadNpmTasks('grunt-contrib-watch');

  // Task definition
  grunt.registerTask('default', ['watch','concat','uglify','cssmin']);
}